<?php

namespace App\Http\Criterias;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\CarMake;

class CarSmartCriteria extends BaseCriteria{	
	const RESULTS_PER_PAGE = 50;
	const DEFAULT_SORT_FIELD = 'id';
	const DEFAULT_SORT_DIRECTION = 'desc';
	public function __construct( $request, $perPage = '' ){
		$this->request = $request;
		$this->perPage = ($perPage) ? $perPage : self::RESULTS_PER_PAGE;
	}

	public function execute($connection = ''){
		$searchFields = $this->getSearchFields();		
		$entity = new Car();

		if ($connection) {
			$entity = $entity->setConnection($connection);
		}

		if( !\Auth::user() ){		
			$entity = $entity->where( 'show_on_site', 1 )->where( 'sold', 0 );
		}

		if( !empty( $searchFields['search-keyword'] ) ){			
			// Make
			$entity = $entity->where(function ($query) use ($searchFields) {
				if ($make = CarMake::where( 'name', 'like', '%' . $searchFields['search-keyword'] . '%' )->first()) {
					$models = $make->models;
					foreach ($models as $model) {
						$query->orWhere( 'model_id', $model->id );					
					}
				}			

				// Model
				if ($model = CarModel::where( 'name', $searchFields['search-keyword'] )->first()) {
					$query->orWhere( 'model_id', $model->id );
				}			

				// VIN			
				$query->orWhereHas('properties', function($subQuery) use ($searchFields){
					$subQuery->where( 'prop_code', 'vin_number' )->where( 'value', 'like', '%' . $searchFields['search-keyword'] . '%' );
				});

				// LOT NUMBER
				$query->orWhereHas('properties', function($subQuery) use ($searchFields){
					$subQuery->where( 'prop_code', 'lot_number' )->where( 'value', 'like', '%' . $searchFields['search-keyword'] . '%' );
				});

				$query->orWhere( 'year', $searchFields['search-keyword'] );						
			});			
		}		

		if( isset( $searchFields['sold'] ) ){
			$searchFields['sold'] = ($searchFields['sold'] == 'on') ? 1 : 0;
			$entity = $entity->where( 'sold', $searchFields['sold'] );
		}
				
		$sortFields = $this->getSortFields();
		if( !empty( $sortFields ) ){
			foreach( $sortFields as $field => $direction ){
				$entity = $entity->orderBy( $field, $direction );
			}
		}
		else{
			$entity = $entity->orderBy( self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION );
		}
		
		return $entity->paginate( $this->perPage );
	}
}