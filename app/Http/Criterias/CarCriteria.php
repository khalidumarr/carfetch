<?php

namespace App\Http\Criterias;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\CarMake;

class CarCriteria extends BaseCriteria{
	const RESULTS_PER_PAGE = 50;
	const DEFAULT_SORT_FIELD = 'id';
	const DEFAULT_SORT_DIRECTION = 'desc';
	public function __construct( $request ){
		$this->request = $request;
	}

	public function execute(){
		$searchFields = $this->getSearchFields();		
		$entity = new Car();

		if( !empty( $searchFields ) ){
			if( isset( $searchFields['model'] ) ){
				$entity = $entity->where( 'model_id', $searchFields['model'] );
			}

			if( isset( $searchFields['model_code'] ) ){
				$entity = $entity->where( 'model_id', CarModel::where( 'code', $searchFields['model_code'] )->get()->id );
			}

			if( isset( $searchFields['make_code'] ) ){
				$entity = $entity->whereHas( 'model', function($query) use ($searchFields){
					$query->where( 'car_make_id', CarMake::where( 'code', $searchFields['make_code'] )->first()->id );
				});
			}

			if( isset( $searchFields['year'] ) ){
				$entity = $entity->where( 'year', $searchFields['year'] );
			}

			if( isset( $searchFields['min_year'] ) ){
				$entity = $entity->where( 'year', '>=', $searchFields['min_year'] );
			}

			if( isset( $searchFields['max_year'] ) ){
				$entity = $entity->where( 'year', '<=', $searchFields['max_year'] );
			}

			if( isset( $searchFields['arrival_date_from'] ) ){
				$entity = $entity->whereHas( 'container', function($query) use ($searchFields){
					$query->where( 'arrival_date', '>=', date('Y-m-d H:i:s', strtotime($searchFields['arrival_date_from'])) );
				});				
			}

			if( isset( $searchFields['arrival_date_to'] ) ){
				$entity = $entity->whereHas( 'container', function($query) use ($searchFields){
					$query->where( 'arrival_date', '<=', date('Y-m-d H:i:s', strtotime($searchFields['arrival_date_to'])) );
				});
			}

			if( isset( $searchFields['vin_number'] ) ){
				$entity = $entity->whereHas('properties', function($query) use ($searchFields){
					$query->where( 'prop_code', 'vin_number' )->where( 'value', $searchFields['vin_number'] );
				});
			}

			if( isset( $searchFields['lot_number'] ) ){
				$entity = $entity->whereHas('properties', function($query) use ($searchFields){
					$query->where( 'prop_code', 'lot_number' )->where( 'value', $searchFields['lot_number'] );
				});
			}

			if( isset( $searchFields['sold'] ) ){
				$entity = $entity->where( 'sold', $searchFields['sold'] );
			}

			if( isset( $searchFields['latest'] ) ){
				$entity = $entity->where( 'latest', $searchFields['latest'] );
			}

			if( isset( $searchFields['user_id'] ) ){
				$entity = $entity->where( 'user_id', $searchFields['user_id'] );
			}
		}		

		if( !\Auth::user()){			
			if (!isset( $searchFields['user_id'] )) {
				$entity = $entity->where( 'show_on_site', 1 )->where( 'sold', 0 );
			}
		}
		elseif( \Auth::user()->getApplicableRole()->code == 'sale-purchase' ){
			$entity = $entity->where( 'agent_id', \Auth::id() );
		}
				
		$sortFields = $this->getSortFields();
		if( !empty( $sortFields ) ){
			foreach( $sortFields as $field => $direction ){
				$entity = $entity->orderBy( $field, $direction );
			}
		}
		else{
			$entity = $entity->orderBy( self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION );
		}
		
		return $entity->paginate( self::RESULTS_PER_PAGE );
	}
}