<?php

namespace App\Http\Criterias;
use Illuminate\Http\Request;

class BaseCriteria{
	protected $request;
	protected $entity;
	const DEFAULT_SORT_DIRECTION = 'asc';

	protected function getSearchFields(){		
		$fields = $this->request->all();
		unset( $fields['sort'] );
		return $fields;
	}

	protected function getSortFields(){
		$fields = $this->request->all();
		if( isset( $fields['sort'] ) ){
			$field = explode( ':', $fields['sort'] );
			return [
				$field[0] => $field[1]
			];
		}

		return null;
	}

	public function getSortDirection(){
		if( isset( $_REQUEST['sort'] ) ){
			return ( strpos( $_REQUEST['sort'], 'asc' ) !== false ) ? 'asc' : 'desc';
		}
		
		return self::DEFAULT_SORT_DIRECTION;
	}

	public function getInverseSortDirection(){
		if( isset( $_REQUEST['sort'] ) ){
			return ( strpos( $_REQUEST['sort'], 'asc' ) !== false ) ? 'desc' : 'asc';
		}
		
		return self::DEFAULT_SORT_DIRECTION;
	}

	public function getUrlQuery( $excludes ){
		$request = $_REQUEST;
		foreach( $excludes as $exlude ){
			unset( $request[$exlude] );
		}

		return http_build_query( $request );
	}
}