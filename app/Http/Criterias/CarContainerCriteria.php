<?php

namespace App\Http\Criterias;
use App\Models\CarContainer;


class CarContainerCriteria extends BaseCriteria{
	const RESULTS_PER_PAGE = 50;
	const DEFAULT_SORT_FIELD = 'id';
	const DEFAULT_SORT_DIRECTION = 'desc';
	public function __construct( $request ){
		$this->request = $request;
	}

	public function execute(){
		$searchFields = $this->getSearchFields();		
		$entity = new CarContainer();

		if( !empty( $searchFields ) ){
			if( isset( $searchFields['arrival_date_from'] ) ){
				$entity = $entity->where( 'arrival_date', '>=', date( 'Y-m-d H:i:s' ,strtotime( $searchFields['arrival_date_from'] ) ) );
			}

			if( isset( $searchFields['arrival_date_to'] ) ){
				$entity = $entity->where( 'arrival_date', '<=', date( 'Y-m-d H:i:s' ,strtotime( $searchFields['arrival_date_to'] ) ) );
			}			
		}		
				
		$sortFields = $this->getSortFields();
		if( !empty( $sortFields ) ){
			foreach( $sortFields as $field => $direction ){
				$entity = $entity->orderBy( $field, $direction );
			}
		}
		else{
			$entity = $entity->orderBy( self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION );
		}
		
		return $entity->paginate( self::RESULTS_PER_PAGE );
	}
}