<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;
use App\Models\Currency;
use App\Models\User;
use App\Models\Sale;
use App\Http\Criterias\CarCriteria;

class DashboardController extends Controller
{	
	public function cars(Request $request){
		if (!$request->get('login')) {
			return back()->with('login-errors', "Invalid Login Information");
		}

		$user = User::where('login', $request->get('login'))->where('active', 1)->first();

		if (!$user) {
			return back()->with('login-errors', "No user found with provided login");
		}

		$request->request->add(['user_id' => $user->id]);
		$criteria   = new CarCriteria( $request );

		$data       = [          
            'cars'          	=> $criteria->execute(),            
            'default_currency'  => Currency::getDefault(),
            'seo_index'			=> 'noindex',
            'seo_follow'		=> 'nofollow',
        ];
        
        return view('dashboard/cars', $data );
	}

	public function invoice(Request $request, $slug){		
		if (!$request->get('login')) {
			return back()->with('login-errors', "Invalid Login Information");
		}

		$user = User::where('login', $request->get('login'))->where('active', 1)->first();

		if (!$user) {
			return back()->with('login-errors', "No user found with provided login");
		}

		$car = Car::where( 'code', $slug )->first();

		$carUser = $car->user;
		if ($carUser && $car->user->id != $user->id) {
			return back()->with('login-errors', "You are not authorized to generate this invoice");	
		}	

		if (!$car->sale) {
			return back()->with('login-errors', "Car is not sold yet!");	
		}

		$data = [
            'sale' => $car->sale
        ];

        return view( 'dashboard/invoice', $data );
	}

	public function payments(Request $request){
		if (!$request->get('login')) {
			return back()->with('login-errors', "Invalid Login Information");
		}

		$user = User::where('login', $request->get('login'))->where('active', 1)->first();

		if (!$user) {
			return back()->with('login-errors', "No user found with provided login");
		}

		return view('dashboard/payments');
	}
}