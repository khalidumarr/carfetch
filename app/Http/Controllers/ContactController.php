<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mails\Enquiry;
use App\Mails\Contact;

class ContactController extends Controller{
	public function index(){
		return view('contact');
	}

	public function contact( Request $request ){
		$data = [
			'name' 			=> $request->get('name'),
			'phone' 		=> $request->get('phone'),
			'email' 		=> $request->get('email'),
			'offerPrice' 	=> $request->get('offer_price'),
			'msg' 			=> $request->get('message'),
			'url'			=> $request->get('url'),
		];		

		\Mail::to(config('mail.system_mail'))->send(new Enquiry($data));

		return back()->with('message', 'Enquiry Successfully sent!');
	}

	public function enquire( Request $request ){
		$data = [
			'name' 			=> $request->get('name'),
			'phone' 		=> $request->get('phone'),
			'email' 		=> $request->get('email'),
			'offerPrice' 	=> $request->get('offer_price'),
			'msg' 			=> $request->get('message'),
			'url'			=> $request->get('url'),
		];		

		\Mail::to(config('mail.system_mail'))->send(new Enquiry($data));

		return back()->with('message', 'Enquiry Successfully sent!');
	}
}