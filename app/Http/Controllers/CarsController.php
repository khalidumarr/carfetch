<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Car;
use App\Models\Currency;
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\User;
use App\Http\Criterias\CarCriteria;
use App\Http\Criterias\CarSmartCriteria;

class CarsController extends Controller
{
	const MIN_PRICE = 10000;
	const MAX_PRICE = 10000;

	public function index( Request $request ){
		// $key = $this->getCacheKey($request);

		// if (\Cache::has($key)) {
		// 	$data = \Cache::get($key);
		// 	return view('cars', unserialize(base64_decode($data)));
		// }

		if ($request->get('search-keyword')) {
			$criteria   = new CarSmartCriteria( $request );
		} else {
			$criteria   = new CarCriteria( $request );			
		}

		$years 		= [ 2005,2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017 ];
		arsort( $years );
        
        $data       = [          
            'cars'          	=> $criteria->execute(),
            'default_currency'	=> Currency::getDefault()
        ];

        $data['makes'] = CarMake::orderBy("name")->get();

        if( !empty( $request->get('make_code') ) ){
        	$data['models'] = $this->getApplicableModels( $request->get('make_code') );        	
        }

        if( !empty( $request->get('model') ) ){
        	$data['selected_model'] = CarModel::find( $request->get('model') );
        }

        $data['years'] = $this->getApplicableYears();

        // \Cache::put($key, base64_encode(serialize($data)), 30);
		return view('cars', $data );
	}

	public function getApplicableModels( $makeCode ){
		$models = CarModel::where( 'car_make_id', CarMake::where( 'code', $makeCode )->first()->id )->get();
		$modelsAr = [];
		foreach( $models as $k => $v ){
			$modelsAr[$v->name] = $v;
		}
		ksort($modelsAr);
		return $modelsAr;
	}

	public function details( Request $request, $slug ){
		$car = Car::where( 'code', $slug )->first();

		$data = [
			'car' => $car,
			'similar_cars' => Car::where('id', '!=', $car->id)->where( 'show_on_site', 1 )->where( 'sold', 0 )->where( 'sale_price', '>', ( $car->sale_price - self::MIN_PRICE ) )->where( 'sale_price', '<', ( $car->sale_price + self::MAX_PRICE ) )->limit(5)->get(),
			'default_currency'	=> Currency::getDefault()
		];

		return view('carsdetail', $data );
	}

	private function getApplicableMakes(){
		$makes = [];
		$cars = Car::all();
		foreach( $cars as $car ){
			$makes[$car->model->make->code] = $car->model->make;
		}

		ksort( $makes );
		return $makes;
	}

	private function getApplicableYears(){
		$years = [];
		$cars = Car::all();
		foreach( $cars as $car ){
			$years[$car->year] = $car->year;
		}

		asort( $years );
		return $years;
	}

	private function getCacheKey($request){
		return base64_encode(implode("_", $request->query()));
	}

	public function userCars(Request $request){
		if (!$request->get('login')) {
			return back()->with('login-errors', "Invalid Login Information");
		}

		$user = User::where('login', $request->get('login'))->first();

		if (!$user) {
			return back()->with('login-errors', "No user found with provided login");
		}

		$request->request->add(['user_id' => $user->id]);
		$criteria   = new CarCriteria( $request );

		$data       = [          
            'cars'          	=> $criteria->execute(),            
            'default_currency'  => Currency::getDefault(),
            'seo_index'			=> 'noindex',
            'seo_follow'		=> 'nofollow',
        ];

        return view('user_cars', $data );
	}
}