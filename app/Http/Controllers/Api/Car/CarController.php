<?php

namespace App\Http\Controllers\Api\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarRequest;
use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarProp;
use App\Models\CarPropsValue;
use App\Models\CarCostsValue;
use App\Models\CarRegion;
use App\Models\Dictionary;
use App\Models\Currency;
use App\Models\CarCost;
use App\Models\Upload;
use App\Models\User;
use App\Models\Receivable;
use App\Models\AccountsHead;
use App\Models\Sale;
use App\Http\Criterias\CarCriteria;

class CarController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function import(Request $request)
    {
        $criteria   = new CarCriteria( $request );
        $cars = $criteria->execute();
        
        var_dump($cars);die;
    }
}
