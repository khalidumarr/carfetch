<?php

namespace App\Http\Controllers;
use App\Models\Car;
use App\Models\Banner;

class IndexController extends Controller{
	public function index(){
		$data = [
			'banners'		=> Banner::getBanners(),
			'featured'	 	=> Car::where( 'sold', 0 )->where('show_on_site', 1)->where('featured', 1)->limit(3)->get(),
			'trending'		=> Car::where( 'sold', 0 )->where('show_on_site', 1)->where('trending', 1)->limit(3)->get(),
			'recent'		=> Car::where( 'sold', 0 )->where('show_on_site', 1)->where('featured', 0)->where('trending', 0)->where('created_at', '>', date('Y-m-d H:i:s', strtotime('-7 days')))->limit(3)->get(),
		];

		return view('welcome', $data);
	}
}