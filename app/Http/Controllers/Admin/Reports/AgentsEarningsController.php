<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Car;

class AgentsEarningsController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function myEarnings()
    {
        // get all cars where agent is current and sold is yes
        if( \Auth::user()->getApplicableRole()->code != 'sale-purchase' ){
            return back()->with('error', 'you are not allowed to view this page');
        }
        
        $cars = Car::where( 'agent_id', \Auth::id() )->where( 'sold', 1 )->get();
       

        $data = [
            'cars' => $cars
        ];

        return view( 'admin/reports/my-earnings', $data );
    }
}
