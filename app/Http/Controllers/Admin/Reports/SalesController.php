<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Sale;

class SalesController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'morris'        => Sale::getDailyTotalByDate( date('Y') . '-' . date('m') . '-01 00:00:00', date( 'y-m-d H:i:s' ) ),
            'current_month' => Sale::getTotalByDate( date('Y') . '-' . date('m') . '-01 00:00:00', date( 'y-m-d H:i:s' ) ) . ' AED',
            'current_week'  => Sale::getTotalByDate( date( 'Y-m-d H:i:s', strtotime( 'LAST SATURDAY' ) ), date( 'y-m-d H:i:s' ) ) . ' AED',
            'today'         => Sale::getTotalByDate( date( 'Y-m-d' ) . ' 00:00:00', date( 'Y-m-d H:i:s' ) ) . ' AED'
        ];

        return json_encode( $data );
    }

    /**
     * Total Sales Count.
     *
     * @return \Illuminate\Http\Response
     */
    public function totalSales(){
        if( \Gate::allows( 'total-sales-count' ) ){
            return count( Sale::all() );
        }
        else{            
            return count( sale::where( 'sold_by', Auth::id() )->get() );
        }
    }
}
