<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Expense;
use App\Models\CarContainer;

class ContainersController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $containers = CarContainer::orderBy('arrival_date', 'desc')->get();
        foreach( $containers as $key => $container ){
            $containers[$key]->month = date( 'M', strtotime( $container->arrival_date ) );
            $containers[$key]->day = date( 'd', strtotime( $container->arrival_date ) );
            $containers[$key]->shipper_name = $container->shipper->name;
        }

        return json_encode( $containers->toArray() );
    }
}
