<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarContainerRequest;
use App\Http\Controllers\Controller;
use App\Models\CarContainer;
use App\Models\User;
use App\Models\Currency;
use App\Models\Car;
use App\Http\Criterias\CarContainerCriteria;

class CarContainerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $criteria   = new CarContainerCriteria( $request );
        $data       = [
            'containers'     => $criteria->execute()
        ];

        return view( 'admin/cars/containers/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'shippers'  => User::getRoleUsers( 'shipper' ),
            'container' => new CarContainer(),
            'currencies'=> Currency::where('code', '!=', 'AED')->get(),
            'cars'      => Car::getNonContainedCars()
        ];

        return view( 'admin/cars/containers/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarContainerRequest $request)
    {        
        \DB::beginTransaction();
        $container = new CarContainer();
        $container->number          = $request->number;
        $container->shipper_id      = $request->shipper_id;
        $container->arrival_date    = date( 'Y-m-d H:i:s' ,strtotime( $request->arrival_date ) );
        $container->amount          = $request->amount;
        $container->currency_id     = $request->currency_id;                

        if( $container->save() ){
            foreach( $request->cars as $carId ){
                $car = Car::find( $carId );
                $car->container_id = $container->id;
                if( !$car->save() ){
                    return redirect('admin/car-containers')->with( 'errors', 'Unable to add container in car' );
                    \DB::rollBack();
                }
            }

            if( $container->amount > 0 ){
                // create payable entry
            }

            \DB::commit();
            return redirect('admin/car-containers/')->with( 'message', 'Car Container Added Successfully' );
        }
        else{
            \DB::rollBack();
            return redirect('admin/car-containers')->with( 'errors', 'Problem Adding Car Container' );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'shippers'           => User::getRoleUsers( 'shipper' ),
            'container'          => CarContainer::find( $id ),
            'currencies'         => Currency::where('code', '!=', 'AED')->get(),
            'cars'               => Car::where('container_id', null)->get(),
            'cars_ids'           => CarContainer::getCarsIds( $id )
        ];

        return view( 'admin/cars/containers/view', $data );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'shippers'           => User::getRoleUsers( 'shipper' ),
            'container'          => CarContainer::find( $id ),
            'currencies'         => Currency::where('code', '!=', 'AED')->get(),
            'cars'               => Car::where('container_id', null)->get(),
            'cars_ids'           => CarContainer::getCarsIds( $id )
        ];

        return view( 'admin/cars/containers/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarContainerRequest $request, $id)
    {
        \DB::beginTransaction();
        $container = CarContainer::find($id);
        $container->number          = $request->number;
        $container->shipper_id      = $request->shipper_id;
        $container->arrival_date    = date( 'Y-m-d H:i:s' ,strtotime( $request->arrival_date ) );
        $container->amount          = $request->amount;
        $container->currency_id     = $request->currency_id;                

        if( $container->save() ){
            CarContainer::resetCars( $id );

            foreach( $request->cars as $carId ){
                $car = Car::find( $carId );
                $car->container_id = $container->id;
                if( !$car->save() ){
                    return redirect('admin/car-containers')->with( 'errors', 'Unable to update container in car' );
                    \DB::rollBack();
                }
            }

            if( $container->amount > 0 ){
                // create payable entry
            }

            \DB::commit();
            return redirect('admin/car-containers/')->with( 'message', 'Car Container updated Successfully' );
        }
        else{
            \DB::rollBack();
            return redirect('admin/car-containers')->with( 'errors', 'Problem updating Car Container' );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $container = CarContainer::find($id);
        if( $container ){
            CarContainer::resetCars( $id );
            if( $container->delete() ){
                return redirect('admin/car-containers')->with( 'message', 'Car Container Deleted Successfully' );                
            }

            return redirect('admin/car-containers')->with( 'errors', 'Problem Deleting Car Container' );
        }

        return redirect('admin/car-containers')->with( 'errors', 'No Car Container Found with provided ID' );
    }
}
