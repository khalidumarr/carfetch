<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarPropRequest;
use App\Http\Controllers\Controller;
use App\Models\CarProp;
use App\Models\CarPropsType;
use App\Models\Upload;

class CarPropertyController  extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort           = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction      = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $properties     = CarProp::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data = [
            'properties' => $properties     
        ];

        return view( 'admin/cars/properties/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $types = CarPropsType::all();
        return view( 'admin/cars/properties/add', [ 'property' => new CarProp(), 'types' => $types ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarPropRequest $request)
    {
        $property = new CarProp();
        $property->name             = $request->name;
        $property->code             = $request->code;
        $property->car_prop_type    = $request->car_prop_type;
        $property->show_on_site     = ( $request->show_on_site == 'on' ) ? 1 : 0;
        $property->is_required      = ( $request->is_required == 'on' ) ? 1 : 0;

        if( $property->save() ){
            return redirect('admin/car-properties/')->with( 'message', 'Car property Added Successfully' );
        }
        else{
            return redirect('admin/car-properties')->with( 'errors', 'Problem Adding Car property' );
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = CarProp::find( $id );
        $types = CarPropsType::all();
        return view( 'admin/cars/properties/edit', [ 'property' => $property, 'types' => $types ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarPropRequest $request, $id)
    {
        $property = CarProp::find($id);
        if( $property ){
            $property->name             = $request->name;
            $property->code             = $request->code;
            $property->car_prop_type    = $request->car_prop_type;
            $property->show_on_site     = ( $request->show_on_site == 'on' ) ? 1 : 0;
            $property->is_required      = ( $request->is_required == 'on' ) ? 1 : 0;

            if( $property->save() ){
                return redirect('admin/car-properties')->with( 'message', 'Car Property Updated Successfully!' );
            }
            else{
                return redirect('admin/car-properties')->with( 'errors', 'Problem Updating Car Property' );
            }
        }

        return redirect('admin/car-properties')->with( 'errors', 'No Car property Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = CarProp::find($id);
        if( $property ){
            if( $property->delete() ){
                return redirect('admin/car-properties')->with( 'message', 'Car Property Deleted Successfully' );                
            }

            return redirect('admin/car-properties')->with( 'errors', 'Problem Deleting Car Property' );
        }

        return redirect('admin/car-properties')->with( 'errors', 'No Car Property Found with provided ID' );
    }
}
