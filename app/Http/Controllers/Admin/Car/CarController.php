<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarRequest;
use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarProp;
use App\Models\CarPropsValue;
use App\Models\CarCostsValue;
use App\Models\CarRegion;
use App\Models\Dictionary;
use App\Models\Currency;
use App\Models\CarCost;
use App\Models\Upload;
use App\Models\User;
use App\Models\Receivable;
use App\Models\AccountsHead;
use App\Models\Sale;
use App\Http\Criterias\CarCriteria;

class CarController extends Controller
{
    private $sellRequiredFields = [
        'amount'            => 'Amount',
        'received_amount'   => 'Received Amount',
        'currency_id'       => 'Currency'
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {        
        $criteria   = new CarCriteria( $request );
        $data       = [
            'models'        => CarModel::all(),
            'cars'          => $criteria->execute()
        ];

        return view( 'admin/cars/cars/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'car'           => new Car(),
            'car_regions'   => CarRegion::all(),
            'sale_types'    => Dictionary::getByType( 'sale_types' ),
            'stock_types'   => Dictionary::getByType( 'stock_types' ),
            'customers'     => User::getRoleUsers( 'customer' ),
            'currencies'    => Currency::all(),
            'car_models'    => CarModel::all(),
            'car_properties'=> CarProp::all(),
            'car_costs'     => CarCost::all(),
            'agents'        => User::getRoleUsers( 'sale-purchase' )
        ];

        return view( 'admin/cars/cars/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        \DB::beginTransaction();
        $car = new Car();
        $car->title            = $request->title;
        $car->code             = Car::getUniqueCode( $request->title );
        $car->model_id         = $request->model_id;
        $car->year             = $request->year;
        $car->sale_price       = $request->sale_price;
        $car->currency_id      = $request->currency_id;
        $car->user_id          = $request->user_id;
        $car->source_id        = $request->source_id;
        $car->destination_id   = $request->destination_id;
        $car->sale_type_id     = $request->sale_type_id;
        $car->stock_type_id    = $request->stock_type_id;
        $car->show_on_site     = ( $request->show_on_site == 'on' ) ? 1 : 0;
        $car->sold             = ( $request->sold == 'on' ) ? 1 : 0;
        $car->featured         = ( $request->featured == 'on' ) ? 1 : 0;
        $car->trending         = ( $request->trending == 'on' ) ? 1 : 0;
        $car->latest           = ( $request->latest == 'on' ) ? 1 : 0;
        $car->description      = $request->description;
        $car->updated_by       = \Auth::user()->id;

        if( $car->stockType->code == 'agent-stock' ){
            if( !$request->agent_id ){
                return back()->with( 'error', 'Please Select Agent!' );
            }

            $car->agent_id = $request->agent_id;
        }
        else{
            $car->agent_id = 0;
        }
        
        if( !$car->save() ){
            \DB::rollBack();
            return redirect('admin/cars')->with( 'errors', 'Problem Adding Car' );
        }

        $props = CarProp::all();        
        foreach( $props as $prop ){
            if( isset( $request->{$prop->code} ) ){
                $propVal = new CarPropsValue();
                $propVal->prop_id           = $prop->id;
                $propVal->car_id            = $car->id;
                $propVal->prop_type_code    = $prop->type->code;
                $propVal->prop_name         = $prop->name;
                $propVal->prop_code         = $prop->code;
                $propVal->value             = $request->{$prop->code};

                if( !$propVal->save() ){
                    \DB::rollBack();
                    return redirect('admin/cars')->with( 'errors', 'Problem Adding ' . $prop->name . ' Property.' );
                }
            }
        }

        $costs = CarCost::all();
        foreach( $costs as $cost ){
            if( isset( $request->{$cost->code} ) ){
                $costVal = new CarCostsValue();
                $costVal->car_id            = $car->id;
                $costVal->cost_id           = $cost->id;
                $costVal->currency_id       = $cost->currency_id;
                $costVal->cost_name         = $cost->name;
                $costVal->cost_code         = $cost->code;
                $costVal->value             = (int)$request->{$cost->code};
                

                if( !$costVal->save() ){
                    \DB::rollBack();
                    return redirect('admin/cars')->with( 'errors', 'Problem Adding ' . $cost->name . ' Cost.' );
                }
            }
        }

        \DB::commit();

        if( $request->file('image') ){
            foreach( $request->file('image') as $image ){
                if( !$image->isValid() ){
                    return redirect('admin/cars/')->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type = Car::class; 
                $file->uploadable_id = $car->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return redirect('admin/cars/')->with( 'message', 'Car Updated But unable to save the image!' );
                }              
            }
        }

        \Cache::flush();
        
        return redirect('admin/cars/')->with( 'message', 'Car Added Successfully' );
               
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $car = Car::find( $id );
        if( !$car ){
            return redirect( '/admin/cars/' )->with( 'errors', 'No car found with provided ID' );
        }

        $data = [
            'car'               => $car,
            'currencies'        => Currency::all(),
            'customers'         => User::getRoleUsers( 'customer' ),
            'payment_methods'   => Dictionary::getByType( 'payment_methods' ),
            'default_currency'  => Currency::getDefault(),
            'payable_heads'     => AccountsHead::getByType( 'garage' ),
            'receivable_heads'  => AccountsHead::getByType( 'receivable-expenses' )
        ];

        return view('admin/cars/cars/view', $data );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $data = [
            'car'           => Car::find( $id ),
            'car_regions'   => CarRegion::all(),
            'sale_types'    => Dictionary::getByType( 'sale_types' ),
            'stock_types'   => Dictionary::getByType( 'stock_types' ),
            'customers'     => User::getRoleUsers( 'customer' ),
            'currencies'    => Currency::all(),
            'car_models'    => CarModel::all(),
            'car_properties'=> CarProp::all(),
            'car_costs'     => CarCostsValue::injectValues( $id, CarCost::all() ),
            'agents'        => User::getRoleUsers( 'sale-purchase' )            
        ];

        return view( 'admin/cars/cars/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarRequest $request, $id)
    {        
        $car = Car::find($id);
        if( !$car ){
            return redirect('admin/cars')->with( 'errors', 'No Car Found with provided ID' );
        }

        \DB::beginTransaction();
        $car->title            = $request->title;
        $car->model_id         = $request->model_id;
        $car->year             = $request->year;
        $car->sale_price       = $request->sale_price;
        $car->currency_id      = $request->currency_id;
        $car->user_id          = $request->user_id;
        $car->source_id        = $request->source_id;
        $car->destination_id   = $request->destination_id;
        $car->sale_type_id     = $request->sale_type_id;
        $car->stock_type_id    = $request->stock_type_id;
        $car->show_on_site     = ( $request->show_on_site == 'on' ) ? 1 : 0;
        $car->sold             = ( $request->sold == 'on' ) ? 1 : 0;
        $car->featured         = ( $request->featured == 'on' ) ? 1 : 0;
        $car->trending         = ( $request->trending == 'on' ) ? 1 : 0;
        $car->latest           = ( $request->latest == 'on' ) ? 1 : 0;
        $car->description      = $request->description;
        $car->updated_by       = \Auth::user()->id;

        if( $car->stockType->code == 'agent-stock' ){
            if( !$request->agent_id ){
                return back()->with( 'error', 'Please Select Agent!' );
            }

            $car->agent_id       = $request->agent_id;
        }
        else{
            $car->agent_id = 0;
        }

        if ($car->sold && $car->sale && $request->user_id != $car->sale->customer->id) {
            return back()->with( 'error', 'Sold car\'s user cannot be changed to another user' );
        }
        
        if( !$car->save() ){
            \DB::rollBack();
            return redirect('admin/cars')->with( 'errors', 'Problem Adding Car' );
        }

        $props = CarProp::all();
        foreach( $props as $prop ){
            if( isset( $request->{$prop->code} ) ){

                // create or getting existing prop value
                $propVal = $car->properties()->where('prop_code',  $prop->code )->first();

                if( !$propVal ){
                    $propVal = new CarPropsValue();
                }
                                
                $propVal->prop_id           = $prop->id;
                $propVal->car_id            = $car->id;
                $propVal->prop_type_code    = $prop->type->code;
                $propVal->prop_name         = $prop->name;
                $propVal->prop_code         = $prop->code;
                $propVal->value             = $request->{$prop->code};

                if( !$propVal->save() ){
                    \DB::rollBack();
                    return back()->with( 'errors', 'Problem Adding ' . $prop->name . ' Property.' );
                }
            }
        }

        $costs = CarCost::all();
        foreach( $costs as $cost ){
            if( isset( $request->{$cost->code} ) ){

                // create or getting existing cost value
                $costVal = $car->costs()->where('cost_code',  $cost->code )->first();
                if( !$costVal ){
                    $costVal = new CarCostsValue();
                }

                $costVal->car_id            = $car->id;
                $costVal->cost_id           = $cost->id;
                $costVal->currency_id       = $cost->currency_id;
                $costVal->cost_name         = $cost->name;
                $costVal->cost_code         = $cost->code;
                $costVal->value             = $request->{$cost->code};

                if( !$costVal->save() ){
                    \DB::rollBack();
                    return back()->with( 'errors', 'Problem Adding ' . $cost->name . ' Cost.' );
                }
            }
        }

        \DB::commit();
        \Cache::flush();

        if( $request->file('image') ){

            foreach( $request->file('image') as $image ){                 
                if( !$image->isValid() ){
                    return redirect('admin/cars/')->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type = Car::class; 
                $file->uploadable_id = $car->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    \Cache::flush();
                    return back()->with( 'message', 'Car Updated But unable to save the image!' );
                }              
            }
        }

        return back()->with( 'message', 'Car Added Successfully' );        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        if( \Gate::denies('delete-car') ){
            return back()->with('errors', 'You are not allowed to delete car');
        }

        $car = Car::find($id);
        if( $car ){
            if( $car->delete() ){
                $carProps = CarPropsValue::where( 'car_id', $car->id );
                if( $carProps ){
                    if( $carProps->delete() ){
                        $carCosts = CarCostsValue::where( 'car_id', $car->id );
                        if( $carCosts ){
                            if( $carCosts->delete() ){
                                $carPhotos = Upload::where( 'uploadable_id', $car->id )->where( 'uploadable_type', Car::class );                                
                                if( $carPhotos->count() ){
                                    if( $carPhotos->delete() ){
                                        \DB::commit();
                                        return redirect('admin/cars')->with( 'message', 'Car Deleted Successfully' ); 
                                    }

                                    \DB::rollBack();
                                    return redirect('admin/cars')->with( 'errors', 'Unable to Delete Car Pictures' );                            
                                }

                                \DB::commit();
                                \Cache::flush();
                                return redirect('admin/cars')->with( 'message', 'Car Deleted Successfully' );
                            }

                            \DB::rollBack();
                            return redirect('admin/cars')->with( 'errors', 'Unable to Delete Car Costs' );                        
                        }

                        \DB::commit();
                        return redirect('admin/cars')->with( 'message', 'Car Deleted Successfully' );                               
                    }

                    \DB::rollBack();
                    return redirect('admin/cars')->with( 'errors', 'Unable to Delete Car Porperties' ); 
                }

                \DB::commit();
                return redirect('admin/cars')->with( 'message', 'Car Deleted Successfully' );
            }

            \DB::rollBack();
            return redirect('admin/cars')->with( 'errors', 'Problem Deleting Car' );
        }        

        return redirect('admin/cars')->with( 'errors', 'No Car Found with provided ID' );
    }

    public function uploadPictures( Request $request, $id ){
        $car = Car::find($id);
        if( $request->file('image') ){

            foreach( $request->file('image') as $image ){                 
                if( !$image->isValid() ){
                    return back()->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type = Car::class; 
                $file->uploadable_id = $car->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return back()->with( 'errors', 'Unable to save the image!' );
                }              
            }

            \Cache::flush();
            return back()->with( 'message', 'Image Added Successfully' ); 
        }

        return back()->with( 'errors', 'Please select the image' );
    }

    public function updateSort( Request $request ){
        if( Upload::reArrange( $request->get('image_sort') ) ){
            return 'success';
        }

        return 'failure';
    }
}
