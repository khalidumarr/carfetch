<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarExpenseRequest;
use App\Http\Controllers\Controller;
use App\Models\CarExpense;
use App\Models\Currency;
use App\Models\Upload;
use App\Models\Receivable;
use App\Models\Payable;
use App\Models\Car;
use App\Models\AccountsHead;

class CarExpenseController extends Controller
{
    private $requiredFields = [
        'title' => 'Title',
        'value' => 'Value',
        'currency_id' => 'Currency'
    ];

    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort       = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction  = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $expenses   = CarExpense::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data       = [
            'expenses'     => $expenses     
        ];

        return view( 'admin/cars/expenses/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currencies = Currency::all();
        return view( 'admin/cars/expenses/add', [ 'expense' => new CarExpense(), 'currencies' => $currencies ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        foreach( $this->requiredFields as $field => $title ){
            if( !$request->get($field) ){
                return redirect( $request->get('back_url') )->with( 'errors', $title . ' is Required' );
            }
        }

        \DB::beginTransaction();
        $expense = new CarExpense();
        $expense->title            = $request->title;
        $expense->car_id           = $request->car_id;
        $expense->currency_id      = $request->currency_id;
        $expense->value            = $request->value;
        $expense->approved         = 0;
        $expense->description       = $request->description;
        $expense->responsibility_id = $request->responsibility_id;

        if( $expense->save() ){
            if( $request->file('invoice_image') ){
                $image = $request->file('invoice_image');
                
                if( !$image->isValid() ){
                    \DB::rollback();
                    return redirect( $request->get('back_url') )->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type = CarExpense::class; 
                $file->uploadable_id = $expense->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    \DB::rollback();
                    return redirect( $request->get('back_url') )->with( 'message', 'Invalid Image Format' );
                }                
            }

            // approving by default, later we can remove this bunch of code to get it approved from admin
            if( \Auth::id() != $expense->responsibility->user_id ){
                \DB::rollback();
                return redirect($request->get('back_url'))->with( 'errors', 'Sorry, You can only add your expense' );
            }

            $expense->approved = 1;
            if( !$expense->save() ){
                \DB::rollback();
                return redirect($request->get('back_url'))->with( 'errors', 'Problem approving car expense' );            
            }

            $receivable                 = new Receivable();
            $receivable->created_by     = \Auth::id();
            $receivable->created_for    = $expense->responsibility->user_id;
            $receivable->debit          = 0;
            $receivable->head_id        = AccountsHead::where('code', 'car-expense-added')->first()->id;
            $receivable->value          = $expense->value;
            $receivable->currency_id    = $expense->currency_id;

            if( !$receivable->save() ){
                \DB::rollback();
                return redirect($request->get('back_url'))->with( 'errors', 'Problem Creating Entry in Receivables' );
            }
            // approval code ends here

            \DB::commit();

            return redirect($request->get('back_url'))->with( 'message', 'Car expense Added Successfully' );
        }
        else{
            return redirect($request->get('back_url'))->with( 'errors', 'Problem Adding Car expense' );
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense       = CarExpense::find( $id );
        $currencies = Currency::all();
        return view( 'admin/cars/expenses/edit', [ 'expense' => $expense, 'currencies' => $currencies ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarExpenseRequest $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = CarExpense::find($id);
        if( $expense ){
            if( $expense->delete() ){
                return redirect('admin/car-expense')->with( 'message', 'Car expense Deleted Successfully' );                
            }

            return redirect('admin/car-expense')->with( 'errors', 'Problem Deleting Car expense' );
        }

        return redirect('admin/car-expense')->with( 'errors', 'No Car expense Found with provided ID' );
    }

    /**
     * Called from other endpoint
     *     
     * @return \Illuminate\Http\Response
     */
    public function delete( Request $request, $id ){
        \DB::beginTransaction();
        $expense = CarExpense::find($id);
        if( $expense ){
            if( $expense->approved == 1 ){
                \DB::rollback();
                return redirect($request->get('back_url'))->with( 'errors', 'Approved Expenses can not be deletes' );
            }

            if( $expense->delete() ){
                if( $expense->payable_id ){
                    $payable = Payable::find( $expense->payable_id );
                    if( !$payable->delete() ){
                        \DB::rollback();
                        return back()->with( 'Unable to delete attached payable to expense' );
                    }
                }

                \DB::commit();
                return redirect($request->get('back_url'))->with( 'message', 'Car Expense Deleted Successfully' );                
            }

            \DB::rollback();
            return redirect($request->get('back_url'))->with( 'errors', 'Problem Deleting Car Expense' );
        }

        \DB::rollback();
        return redirect($request->get('back_url'))->with( 'errors', 'No Car Expense Found with provided ID' );
    }

    /**
     * Approve from other endpoint
     *     
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function approve( Request $request, $id ){        
        if( \Auth::user()->getApplicableRole()->code != 'admin' ){
            return redirect($request->get('back_url'))->with( 'errors', 'Sorry, You dont have permission to approve Expense' );
        }

        $expense = CarExpense::find($id);
        if( !$expense ){
            return redirect($request->get('back_url'))->with( 'errors', 'No Car Expense Found with provided ID' );
        }

        if( $expense->receivable_id || $expense->payable_id ){
            return redirect($request->get('back_url'))->with( 'errors', 'Pyable or Receivables expenses doesnt need to be approved' );
        }

        \DB::beginTransaction();
        $expense->approved = 1;
        if( !$expense->save() ){
            return redirect($request->get('back_url'))->with( 'errors', 'Problem approving car expense' );            
        }

        $receivable                 = new Receivable();
        $receivable->created_by     = \Auth::id();
        $receivable->created_for    = $expense->responsibility->user_id;
        $receivable->debit          = 0;
        $receivable->head_id        = AccountsHead::where('code', 'car-expense-added')->first()->id;
        $receivable->value          = $expense->value;
        $receivable->currency_id    = $expense->currency_id;

        if( !$receivable->save() ){
            \DB::rollback();
            return redirect($request->get('back_url'))->with( 'errors', 'Problem Creating Entry in Receivables' );
        }

        \DB::commit();
        return redirect($request->get('back_url'))->with( 'message', 'Car Expense Approved Successfully' );
    }

    public function addPayableExpense( Request $request ){
        if( \Gate::denies( 'add-payable-expense' ) ){
            return back()->with( 'error', 'You are not authorized to add Payable expense' );
        }

        \DB::beginTransaction();
        $expense = new CarExpense();
        $expense->title            = $request->title;
        $expense->car_id           = $request->car_id;
        $expense->currency_id      = $request->currency_id;
        $expense->value            = $request->value;
        $expense->approved         = 1;
        $expense->description       = $request->description;
        $expense->responsibility_id = 0;
        if( !$expense->save() ){
            \DB::rollback();
            return back()->with( 'error', 'Unable to add Car Expense' );
        }

        // check if required account head is there or not
        $accountHead = AccountsHead::find( $request->head_id );
        if( $accountHead->type->type != 'account_head_types' ){
            \DB::rollback();
            return back()->with( 'error', 'Required Account head (garage) is not existed!' );
        }

        $payable = new Payable();
        $payable->head_id        = $request->head_id;
        $payable->value          = $request->value;
        $payable->debit          = 0;
        $payable->currency_id    = $request->currency_id;
        $payable->created_by     = \Auth::user()->id;

        if( !$payable->save() ){
            \DB::rollback();
            return back()->with( 'error', 'Unable to save the payable' );
        }

        $expense->payable_id = $payable->id;
        if( !$expense->save() ){
            \DB::rollback();
            return back()->with( 'error', 'Unable to add Payable of expense' );
        }

        \DB::commit();
        return back()->with( 'message', 'Payable Expense Added Successfully' );
    }

    public function addReceivableExpense( Request $request ){
        if( \Gate::denies( 'add-receivable-expense' ) ){
            return back()->with( 'error', 'You are not authorized to add Payable expense' );
        }

        \DB::beginTransaction();
        $expense = new CarExpense();
        $expense->title            = $request->title;
        $expense->car_id           = $request->car_id;
        $expense->currency_id      = $request->currency_id;
        $expense->value            = $request->value;
        $expense->approved         = 1;
        $expense->description       = $request->description;
        $expense->responsibility_id = 0;
        if( !$expense->save() ){
            \DB::rollback();
            return back()->with( 'error', 'Unable to add Car Expense' );
        }

        $car = Car::find( $request->car_id );
        if( !$car ){
            \DB::rollback();
            return back()->with( 'error', 'Unable to find any car with provided ID' );   
        }

        if( !$car->sold ){
            \DB::rollback();
            return back()->with( 'error', 'Car must be sold before receivable expense can be added' );      
        }

        // check if required account head is there or not
        $accountHead = AccountsHead::find( $request->head_id );
        if( !$accountHead || $accountHead->type->code != 'receivable-expenses' ){
            \DB::rollback();
            return back()->with( 'error', 'Required Account head (Receivable Expense) is not existed!' );
        }

        $receivable = new Receivable();
        $receivable->head_id = $request->head_id;
        $receivable->created_for = $car->sale->sold_to;
        $receivable->value = $request->value;
        $receivable->debit = 1;
        $receivable->currency_id = $request->currency_id;
        $receivable->created_by = \Auth::user()->id;

        if( !$receivable->save() ){
            \DB::rollback();
            return back()->with( 'error', 'Unable to save the Receivable' );
        }

        $expense->receivable_id = $receivable->id;
        if( !$expense->save() ){
            \DB::rollback();
            return back()->with( 'error', 'Unable to add Receivable of expense' );
        }

        \DB::commit();
        return back()->with( 'message', 'Receivable Expense Added Successfully' );
    }
}
