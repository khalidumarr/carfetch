<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Currency;
use App\Models\Receivable;
use App\Models\Payable;
use App\Models\AccountsHead;

class ChequesController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $data = [ 
            'sales_users'       => User::all(),
            'heads'             => AccountsHead::all(),
            'currencies'        => Currency::all(),
            'payables'          => Payable::all(),
            'total_payable'     => Payable::getTotal()
        ];

        return view('admin/accounts/payables/list', $data);
    }

    public function withdraw( Request $request ){
        // add debit = o entry in receivables
    }
}
