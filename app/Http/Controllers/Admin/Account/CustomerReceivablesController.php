<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Currency;
use App\Models\Receivable;
use App\Models\AccountsHead;
use App\Models\Sale;

class CustomerReceivablesController extends \App\Http\Controllers\Controller
{
    private $requiredFields = [
        'head_id'       => 'Accounts Head',
        'amount'        => 'Amount',
        'currency_id'   => 'Currency'
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        if( \Gate::allows( 'own-customers-receivables' ) ){              
            $customers = Sale::getUserCustomers( \Auth::id() );            
        }
        else{
            $customers = User::getRoleUsers( 'customer' );
        }
        
        $data = [
            'customers' => []
        ];
        
        foreach( $customers as $customer ){
            $credit = Receivable::getUserCredit( $customer->id );
            $debit  = Receivable::getUserDebit( $customer->id );
            $remaining = $credit - $debit;

            if( $remaining <= 0 ){
                continue;
            }

            $sales = Sale::where( 'sold_to', $customer->id )->get();
            $data['customers'][] = [
                'user'              => $customer,
                'total_purchases'   => count( $sales ),
                'total_payable'     => $credit,
                'total_paid'        => $debit
            ];


            // $sales = Sale::where( 'sold_to', $customer->id )->get();

            // $total_payable = 0;
            // $total_paid = 0;
            // foreach( $sales as $sale ){
            //     $total_payable  += $sale->price;
            //     $total_paid     += $sale->amount_paid;
            // }

            // if( $total_payable <= 0 || ( $total_payable - $total_paid ) == 0 ){
            //     continue;
            // }

            // $data['customers'][] = [
            //     'user'              => $customer,
            //     'total_purchases'   => count( $sales ),
            //     'total_payable'     => $total_payable,
            //     'total_paid'        => $total_paid                
            // ];
        }

        return view('admin/accounts/receivables/customer-receivables', $data);
    }

    public function receive( Request $request ){
        foreach( $this->requiredFields as $field => $title ){
            if( !$request->get($field) ){
                return redirect( $request->get('back_url') )->with( 'errors', $title . ' is Required' );
            }
        }

        if( !$request->recieved_from_id ){
            return redirect( $request->get('back_url') )->with( 'error', 'Received From must be selected' );
        }

        $receivable = new Receivable();
        $receivable->head_id = $request->head_id;
        $receivable->created_for = $request->recieved_from_id;
        $receivable->value = $request->amount;
        $receivable->debit = 0;
        $receivable->currency_id = $request->currency_id;
        $receivable->created_by = Auth::user()->id;

        if( $receivable->save() ){
            return redirect( $request->get('back_url') )->with( 'message', 'Successfully Received!' );
        }

        return redirect( $request->get('back_url') )->with( 'error', 'Problem Receiving!' );
    }

    public function release( Request $request ){
        foreach( $this->requiredFields as $field => $title ){
            if( !$request->get($field) ){
                return redirect( $request->get('back_url') )->with( 'errors', $title . ' is Required' );
            }
        }

        if( !$request->released_to_id ){
            return redirect( $request->get('back_url') )->with( 'errors', 'Released To must be selected' );
        }

        $receivable = new Receivable();
        $receivable->head_id = $request->head_id;
        $receivable->created_for = $request->released_to_id;
        $receivable->value = $request->amount;
        $receivable->debit = 1;
        $receivable->currency_id = $request->currency_id;
        $receivable->created_by = Auth::user()->id;

        if( $receivable->save() ){
            return redirect( $request->get('back_url') )->with( 'message', 'Successfully Released!' );
        }

        return redirect( $request->get('back_url') )->with( 'error', 'Problem Releasing!' );
    }
}
