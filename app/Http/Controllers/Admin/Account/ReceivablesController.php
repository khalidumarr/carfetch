<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Currency;
use App\Models\Receivable;
use App\Models\AccountsHead;

class ReceivablesController extends \App\Http\Controllers\Controller
{
    private $requiredFields = [
        'head_id'       => 'Accounts Head',
        'amount'        => 'Amount',
        'currency_id'   => 'Currency'
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $data = [ 
            'sales_users'       => User::all(),
            'heads'             => AccountsHead::all(),
            'currencies'        => Currency::all(),
            'receivables'       => Receivable::all(),
            'total_receivable'  => Receivable::getTotal()
        ];

        return view('admin/accounts/receivables/list', $data);
    }

    public function receive( Request $request ){
        foreach( $this->requiredFields as $field => $title ){
            if( !$request->get($field) ){
                return redirect( $request->get('back_url') )->with( 'errors', $title . ' is Required' );
            }
        }

        if( !$request->received_from_id ){
            return redirect( $request->get('back_url') )->with( 'error', 'Received From must be selected' );
        }

        $receivable = new Receivable();
        $receivable->head_id = $request->head_id;
        $receivable->created_for = $request->received_from_id;
        $receivable->value = $request->amount;
        $receivable->debit = 0;
        $receivable->currency_id = $request->currency_id;
        $receivable->created_by = Auth::user()->id;

        if( $receivable->save() ){
            return redirect( $request->get('back_url') )->with( 'message', 'Successfully Received!' );
        }

        return redirect( $request->get('back_url') )->with( 'error', 'Problem Receiving!' );
    }

    public function release( Request $request ){
        foreach( $this->requiredFields as $field => $title ){
            if( !$request->get($field) ){
                return redirect( $request->get('back_url') )->with( 'errors', $title . ' is Required' );
            }
        }

        if( !$request->released_to_id ){
            return redirect( $request->get('back_url') )->with( 'errors', 'Released To must be selected' );
        }

        $receivable = new Receivable();
        $receivable->head_id = $request->head_id;
        $receivable->created_for = $request->released_to_id;
        $receivable->value = $request->amount;
        $receivable->debit = 1;
        $receivable->currency_id = $request->currency_id;
        $receivable->created_by = Auth::user()->id;

        if( $receivable->save() ){
            return redirect( $request->get('back_url') )->with( 'message', 'Successfully Released!' );
        }

        return redirect( $request->get('back_url') )->with( 'error', 'Problem Releasing!' );
    }
}
