<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Requests\ExpenseRequest;
use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\AccountsHead;
use App\Models\Currency;

class ExpensesController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn      = 'id';
    public $sortDirection   = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort               = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction          = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $expenses           = Expense::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data       = [
            'expenses'     => $expenses    
        ];

        return view( 'admin/accounts/expenses/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'heads'         => AccountsHead::all(),
            'expense'       => new Expense(),
            'currencies'    => Currency::all()
        ];

        return view( 'admin/accounts/expenses/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request)
    {
        $expense = new Expense();
        $expense->year           = date('Y');
        $expense->month          = date('m');
        $expense->head_id     = $request->head_id;
        $expense->value          = $request->value;
        $expense->currency_id    = $request->currency_id;
        $expense->added_by       = \Auth::user()->id;

        if( $expense->save() ){
            return redirect('admin/accounts-expenses')->with( 'message', 'Expense Added Successfully' );
        }
        else{
            return redirect('admin/accounts-expenses')->with( 'errors', 'Problem Adding Expense' );
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'heads'         => AccountsHead::all(),
            'expense'       => Expense::find( $id ),
            'currencies'    => Currency::all()
        ];

        return view( 'admin/accounts/expenses/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseRequest $request, $id)
    {
        $expense = Expense::find($id);
        if( $expense ){
            $expense->year           = date('Y');
            $expense->month          = date('m');
            $expense->head_id        = $request->head_id;
            $expense->value          = $request->value;
            $expense->currency_id    = $request->currency_id;
            $expense->added_by       = \Auth::user()->id;
                
            if( $expense->save() ){
                return redirect('admin/accounts-expenses')->with( 'message', 'Expense Updated Successfully!' );
            }
            else{
                return redirect('admin/accounts-expenses')->with( 'errors', 'Problem Updating Expense' );
            }
        }

        return redirect('admin/accounts-expenses')->with( 'errors', 'No Expense Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::find($id);
        if( $expense ){
            if( $expense->delete() ){
                return redirect('admin/accounts-expenses')->with( 'message', 'Expense Deleted Successfully' );                
            }

            return redirect('admin/accounts-expenses')->with( 'errors', 'Problem Deleting Expense' );
        }

        return redirect('admin/accounts-expenses')->with( 'errors', 'No Expense Found with provided ID' );
    }
}
