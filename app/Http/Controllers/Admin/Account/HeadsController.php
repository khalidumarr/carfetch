<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Requests\AccountsHeadRequest;
use App\Http\Controllers\Controller;
use App\Models\AccountsHead;
use App\Models\Currency;
use App\Models\Dictionary;

class HeadsController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn      = 'id';
    public $sortDirection   = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort       = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction  = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $heads    = AccountsHead::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data       = [
            'heads'     => $heads    
        ];

        return view( 'admin/accounts/heads/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'head'          => new AccountsHead(),
            'currencies'    => Currency::all(),
            'types'         => Dictionary::getByType( 'account_head_types' )
        ];

        return view( 'admin/accounts/heads/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountsHeadRequest $request)
    {
        $head = new AccountsHead();
        $head->title            = $request->title;
        $head->code             = $request->code;
        $head->description      = $request->description;
        $head->type_id          = $request->type_id;
        $head->default_value    = $request->default_value;
        $head->currency_id      = $request->currency_id;

        if( $head->save() ){
            return redirect('admin/accounts-heads')->with( 'message', 'Account Head Added Successfully' );
        }
        else{
            return redirect('admin/accounts-heads')->with( 'errors', 'Problem Adding Account Head' );
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'head'          => AccountsHead::find( $id ),
            'currencies'    => Currency::all(),
            'types'         => Dictionary::getByType( 'account_head_types' )
        ];

        return view( 'admin/accounts/heads/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountsHeadRequest $request, $id)
    {
        $head = AccountsHead::find($id);
        if( $head ){
            $head->title            = $request->title;
            $head->code             = $request->code;
            $head->description      = $request->description;
            $head->type_id          = $request->type_id;
            $head->default_value    = $request->default_value;
            $head->currency_id      = $request->currency_id;
            
            if( $head->save() ){
                return back()->with( 'message', 'Account Head Updated Successfully!' );
            }
            else{
                return back()->with( 'errors', 'Problem Updating Account Head' );
            }
        }

        return redirect('admin/accounts-heads')->with( 'errors', 'No Account Head Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $head = AccountsHead::find($id);
        if( $head ){
            if( $head->delete() ){
                return redirect('admin/accounts-heads')->with( 'message', 'Account Head Deleted Successfully' );                
            }

            return redirect('admin/accounts-heads')->with( 'errors', 'Problem Deleting Account Head' );
        }

        return redirect('admin/accounts-heads')->with( 'errors', 'No Account Head Found with provided ID' );
    }
}
