<?php

namespace App\Http\Controllers\Admin\Sale;

use Illuminate\Http\Request;
use App\Http\Requests\SaleRequest;
use App\Http\Requests\PayRemainingRequest;
use App\Http\Requests\SaleApproveAmountRequest;
use App\Http\Controllers\Controller;
use App\Events\CarSold;
use App\Models\Sale;
use App\Models\Receivable;
use App\Models\User;
use App\Models\AccountsHead;
use App\Models\CarRegion;
use App\Models\Car;
use App\Models\SalesPayment;
use App\Models\Dictionary;

class SalesController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort       = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction  = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $sales      = Sale::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data       = [
            'sales'     => $sales     
        ];

        return view( 'admin/sales/list', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaleRequest $request)
    {
        \DB::beginTransaction();
        $car = Car::find($request->car_id);
        if( !$car ){
            return back()->with( 'error', 'No Car Found with provided ID' );
        }

        // creating Sale entry
        $soldTo = User::find( $request->sold_to );
        if( !$soldTo ){
            \DB::rollBack();
            return back()->with( 'error', 'No Sold To user found' );
        }

        if ($car->user_id && $soldTo->id != $car->user_id) {
            \DB::rollBack();
            return back()->with( 'error', 'Car cannot be sold because its already assign to [' . $car->user->name . ' - ' . $car->user->login . ']');   
        }

        $car->sold = 1;
        if (!$car->user_id) {
            $car->user_id = $soldTo->id;
        }

        if( !$car->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to sell the car' );
        }        

        // handling cheque payments
        $paymentMethod = Dictionary::find( $request->payment_method );
        if( !$paymentMethod ){
            \DB::rollBack();
            return back()->with( 'error', 'No Payment method exists with provided ID' );
        }

        if( $paymentMethod->code != 'cash' && !$request->document_number ){
            \DB::rollBack();
            return back()->with( 'error', 'Document number must be provided in case non cash payment' );
        }        

        // creating sales table entry
        $sale                           = new Sale();
        $sale->price                    = $request->amount;
        $sale->currency_id              = $request->currency_id;
        $sale->car_id                   = $request->car_id;
        $sale->sold_to                  = $soldTo->id;
        $sale->sold_by                  = \Auth::id();
        $sale->number                   = Sale::getUniqueNumber();
        $sale->manual_bill_number       = $request->manual_bill_number;
        $sale->cx_name                  = $soldTo->name;
        $sale->cx_phone                 = $soldTo->phone;
        $sale->cx_city                  = CarRegion::getDefaultRegion()->id;
        $sale->amount_paid              = 0;
        $sale->payment_method           = $paymentMethod->id;
        if( !$sale->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to save Sale' );
        }

        // sale payments entry
        $payment = new SalesPayment();
        $payment->sale_id           = $sale->id;
        $payment->payment_method    = $sale->payment_method;
        $payment->amount            = $request->received_amount;
        $payment->document_number   = $request->document_number;
        $payment->approved          = 0;
        $payment->currency_id       = $sale->currency_id;
        $payment->user_id           = $sale->sold_to;
        $payment->updated_by        = \Auth::id();
        
        if( !$payment->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to store the payment Information' );
        }

        // creating receivable entry
        $receivable                     = new Receivable();
        $receivable->created_by         = \Auth::id();
        $receivable->created_for        = $soldTo->id;
        $receivable->debit              = 1;
        $receivable->head_id            = AccountsHead::where('code', 'car-sold')->first()->id;
        $receivable->value              = $request->amount;
        $receivable->currency_id        = $request->currency_id;
        if( !$receivable->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to add receivable of the car' );
        }
        
        // fire event of CarSold
        event( new CarSold( $car ) );
        \DB::commit();

        if (\Gate::allows('auto-approval')) {
            $this->approvePayment($payment->id,$sale->id);
        }
        
        return back()->with( 'message', 'Car Sold Successfully' );
    }

    public function approvePaid( SaleApproveAmountRequest $request, $id ){        
        if( \Gate::denies('approve-received-cash') ){
            return back()->with( 'error', 'You are not authorized to approve the collected cash' );
        }

        // approving payment
        $this->approvePayment($request->payment_id,$id);
        
        return back()->with( 'message', 'Amount successfully approved' );
    }

    private function approvePayment($paymentId, $saleId)
    {
        \DB::beginTransaction();
        $payment = SalesPayment::find( $paymentId);
        if( !$payment ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to find any payment with provided ID' );
        }

        $payment->approved = 1;
        if( !$payment->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to mark the payment as approved' );
        }


        $sale   = Sale::find( $saleId );
        if( !$sale ){
            \DB::rollBack();
            return back()->with( 'error', 'No payment found with provided ID' );
        }

        $sale->amount_paid += $payment->amount;
        if( !$sale->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to set paid amount for selected sale' );
        }        

        // creating receivable entry for paid amount
        $receivable                 = new Receivable();
        $receivable->created_by     = \Auth::id();
        $receivable->created_for    = $sale->customer->id;
        $receivable->debit          = 0;
        $receivable->head_id        = AccountsHead::where('code', 'car-sold')->first()->id;
        $receivable->value          = $payment->amount;
        $receivable->currency_id    = $payment->currency_id;

        if( !$receivable->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to add receivable of the car' );
        } 

        \DB::commit();
        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    public function invoice($id){
        $data = [
            'sale' => Sale::find( $id )
        ];

        return view( 'admin/sales/invoice', $data );
    }

    public function payRemaining(PayRemainingRequest $request, $id){
        \DB::beginTransaction();
        $sale = Sale::find( $id );
        if( !$sale ){
            \DB::rollBack();
            return back()->with( 'message', 'No Sale Found with provided ID' );
        }        


        if( ( $sale->getAllPaymentsTotal() + $request->received_amount ) > $sale->price ){
            \DB::rollBack();
            return back()->with( 'error', 'You cannot pay more than car price' );
        }

        // handling cheque payments
        $paymentMethod = Dictionary::find( $request->payment_method );
        if( !$paymentMethod ){
            \DB::rollBack();
            return back()->with( 'error', 'No Payment method exists with provided ID' );
        }

        if( $paymentMethod->code != 'cash' && !$request->document_number ){
            \DB::rollBack();
            return back()->with( 'error', 'Document number must be provided in case non cash payment' );
        }

        // sale payments entry
        $payment = new SalesPayment();
        $payment->sale_id           = $sale->id;
        $payment->payment_method    = $paymentMethod->id;
        $payment->amount            = $request->received_amount;
        $payment->document_number   = $request->document_number;
        $payment->approved          = 0;
        $payment->currency_id       = $sale->currency_id;
        $payment->user_id           = $sale->sold_to;
        $payment->updated_by        = \Auth::id();

        if( !$payment->save() ){
            return back()->with( 'message', 'Unable to store the payment!' );
        }

        \DB::commit();
        return back()->with( 'message', 'Payment Successfully Updated!' );
    }

    public function changeCustomer( Request $request ){
        $sale = Sale::find( $request->sale_id );
        if( !$sale ){
            return back()->with( 'errors', 'No sale made for provided car' );
        }

        $payments = SalesPayment::where( 'user_id', $sale->sold_to )->where( 'sale_id', $sale->id )->get();
        if( count( $payments ) > 0 ){
            return back()->with( 'error', 'Customer cannot be changed because some Payment Transactions are attached to him.' );  
        }


        $sale->sold_to = $request->sold_to;
        if( !$sale->save() ){
            return back()->with( 'error', 'Problem changing car customer' );            
        }                

        return back()->with( 'message', 'Car Customer successfully changed' );
    }
}
