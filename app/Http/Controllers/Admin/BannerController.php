<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Library\Crop\CropAvatar;
use App\Models\Banner;

class BannerController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'banners' => Banner::getBanners()
        ];

        return view('admin/settings/banners/index', $data);
    }

    public function store(Request $request){
        $crop = new CropAvatar(
          isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
          isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
          isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
        );

        $response = array(
          'state'  => 200,
          'message' => $crop -> getMsg(),
          'result' => $crop -> getResult()
        );

        echo json_encode($response);
    }

    public function destroy(Request $request){
        if( unlink( storage_path('app/banners/') . $request->get('image') ) ){
            echo "success";
        }
        else{
            echo "failed";
        }
    }
}
