<?php

namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Models\User;
use App\Models\Role;
use App\Models\Upload;
use App\Models\AccountsHead;
use App\Models\Currency;
use App\Http\Criterias\UserCriteria;

class UserController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $criteria   = new UserCriteria( $request );
        $data       = [
            'roles'        => Role::all(),
            'users'          => $criteria->execute()
        ];

        return view( 'admin/users/users/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user   = new User();
        $roles  = Role::all();
        return view( 'admin/users/users/add', [ 'user' => $user, 'roles' => $roles ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if ($request->login) {
            $user = User::where('login', $request->login)->first();
            if ($user) {
                return back()->with( 'errors', 'User with ' . $request->login . ' Already Exists!' );
            }
        }

        \DB::beginTransaction();
        $user = new User();
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->phone    = $request->phone;
        $user->login    = $request->login;
        $user->address  = $request->address;
        $user->active   = ( $request->active == 'on' ) ? 1 : 0;
        $user->password = bcrypt( $request->password );        

        if( $user->save() ){
            if( !$user->roles()->sync( $request->get('role') ) ){
                \DB::rollBack();
                return redirect('admin/user/')->with( 'message', 'User Added Successfully! But Roles Cannot be added' );
            }            

            event( new UserCreated( $user ) );            
            \DB::commit();

            return redirect('admin/user/')->with( 'message', 'User Added Successfully!' );
        }
        else{
            \DB::rollBack();
            return redirect('admin/user/')->with( 'errors', 'Problem Adding User' );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user   = User::find( $id );        
        $roles  = Role::all();

        $userRoles = array();
        foreach( $user->roles as $role ){
            $userRoles[] = $role->id;
        }

        $data = [
            'user'  => $user, 
            'roles' => $roles,
            'user_roles' => $userRoles
        ];

        return view( 'admin/users/users/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {        
        \DB::beginTransaction();
        $user = User::find($id);

        if ($request->login) {
            $findUser = User::where('login', $request->login)->first();
            if ($findUser && $findUser->id != $user->id) {
                return back()->with( 'errors', 'User with ' . $request->login . ' Already Exists!' );
            }
        }

        if( $user ){
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->login = $request->login;
            $user->address  = $request->address;
            $user->active = ( $request->active == 'on' ) ? 1 : 0;
            
            $user->roles()->sync( $request->get('role') );

            if( $request->password ){
                $user->password = bcrypt( $request->password );
            }

            if( !$user->save() ){
                \DB::rollBack();
                return redirect('admin/user/')->with( 'message', 'Problem Updating User!' );
            }

            if( $request->hasFile( 'image' ) ){
                if( !$request->file('image')->isValid() ){
                    return redirect('admin/user/')->with( 'errors', 'Invalid Image Selected for Dispaly Picture' );
                }

                $path = $request->image->store('uploads');

                // delete if available
                $prevImage = Upload::where( 'uploadable_type', User::class )
                            ->where( 'uploadable_id', $user->id );
                if( $prevImage ){
                    $prevImage->delete();
                }

                $file = new Upload();
                $file->uploadable_type = User::class; 
                $file->uploadable_id = $user->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return redirect('admin/user/')->with( 'message', 'User Updated But unable to save the image!' );
                }
            }

            if( $request->hasFile( 'identity_picture' ) ){
                if( !$request->file('identity_picture')->isValid() ){
                    return redirect('admin/user/')->with( 'errors', 'Invalid Image Selected for Emiates ID' );
                }

                $path = $request->identity_picture->store('uploads');

                // delete if available
                $prevImage = Upload::where( 'uploadable_type', 'App\Models\UserIdentity' )
                            ->where( 'uploadable_id', $user->id );
                if( $prevImage ){
                    $prevImage->delete();
                }

                $file = new Upload();
                $file->uploadable_type  = 'App\Models\UserIdentity'; 
                $file->uploadable_id    = $user->id; 
                $file->name             = str_replace( 'uploads/', '', $path ); 
                $file->url              = '/' . $path; 
                if( $file->save() ){
                    $user->identity_picture = $file->id;
                    if( !$user->save() ){
                        return redirect('admin/user/')->with( 'message', 'User Updated But unable to save the image!' );                        
                    } 
                }
            }

            event( new UserUpdated( $user ) );            
            \DB::commit();

            return back()->with( 'message', 'User Updated Successfully!' );
        }

        \DB::rollBack();
        return redirect('admin/user/')->with( 'errors', 'No User Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if( \Gate::denies('delete-user') ){
            return back()->with('errors', 'You are not allowed to delete user');
        }

        $user = User::find($id);
        if( $user ){
            if( $user->delete() ){
                return redirect('admin/user/')->with( 'message', 'user Deleted Successfully' );                
            }

            return redirect('admin/user/')->with( 'errors', 'Problem Deleting user' );
        }

        return redirect('admin/user/')->with( 'errors', 'No Role Found with provided ID' );
    }
}
