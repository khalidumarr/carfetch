<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->getApplicableRole()->code;
        if( $role == 'admin' ){
            $role .= '-temp';
        }
        
        return view('admin/dashboards/' . $role );
    }

    public function dashboard(){
        $role = Auth::user()->getApplicableRole()->code;
        return view('admin/dashboards/' . $role );   
    }
}
