<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Upload;

class UploadsController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upload = Upload::find($id);        
        if( $upload ){
            if( $upload->delete() ){
                echo json_encode([
                    'status' => true,
                    'message' => 'File successfully deleted'
                ]);die;                
            }
        }

        echo json_encode([
            'status' => false,
            'message' => 'Unable to find the file to delete'
        ]);die;
    }
}
