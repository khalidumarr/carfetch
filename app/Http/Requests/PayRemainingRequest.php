<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayRemainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch( $this->method() ){
            case 'GET':break;

            case 'DELETE': {
                return [
                    'id' => 'required'
                ];
            }


            case 'POST': {
                return [
                    'received_amount'   => 'required|integer',
                    'payment_method'    => 'required|integer',
                    'currency_id'       => 'required|integer'
                ];
            }


            case 'PUT':
            case 'PATCH': {
                return [
                    'received_amount'   => 'required|integer',
                    'payment_method'    => 'required|integer',
                    'currency_id'       => 'required|integer'
                ];
            }

            default:break;
        }
    }
}
