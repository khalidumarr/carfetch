<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarCostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch( $this->method() ){
            case 'GET':break;

            case 'DELETE': {
                return [
                    'id' => 'required'
                ];
            }


            case 'POST': {
                return [
                    'name' 			=> 'required|min:3',
                    'code' 			=> 'required|min:3',
                    'currency_id' 	=> 'integer'
                ];
            }


            case 'PUT':
            case 'PATCH': {
                return [
                    'name' 			=> 'required|min:3',
                    'code' 			=> 'required|min:3',
                    'currency_id' 	=> 'integer'
                ];
            }

            default:break;
        }
    }
}
