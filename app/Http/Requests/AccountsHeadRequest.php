<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountsHeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch( $this->method() ){
            case 'GET':break;

            case 'DELETE': {
                return [
                    'id' => 'required'
                ];
            }


            case 'POST': {
                return [
                    'title' 		=> 'required',
                    'code'          => 'required',
                    'type_id' 		=> 'required',
                    'description'	=> 'required',
                    'currency_id' 	=> 'required|integer'
                ];
            }


            case 'PUT':
            case 'PATCH': {
                return [
                    'title' 		=> 'required',
                    'code'          => 'required',
                    'type_id' 	    => 'required',
                    'description'	=> 'required',
                    'currency_id' 	=> 'required|integer'
                ];
            }

            default:break;
        }
    }
}
