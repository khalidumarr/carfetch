<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarContainer extends Model
{
    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function shipper(){
        return $this->belongsTo( 'App\Models\User', 'shipper_id' );
    }

    public function cars(){
        return $this->hasMany( 'App\Models\Car', 'container_id');        
    }

    public static function getCarsIds( $id ){
    	$cars 	 = self::find( $id )->cars;
    	$carsIds = [];
    	foreach( $cars as $car ){
    		$carsIds[] = $car->id;
    	}

    	return $carsIds;
    }

    public static function resetCars( $id ){
    	$cars 	 = self::find( $id )->cars;
    	foreach( $cars as $car ){
    		$car->container_id = 0;
    		$car->save();
    	}
    }
}
