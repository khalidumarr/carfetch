<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarMake extends Model
{
    public function logos(){
        return $this->morphMany('App\Models\Upload', 'uploadable');
    }

    public function models(){
    	return $this->hasMany( 'App\Models\CarModel', 'car_make_id');
    }
}
