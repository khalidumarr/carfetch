<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $table = 'dictionary';

    public static function getByType( $type, $code = null ){
    	if( $code !== null ){
    		return self::where('type', $type)->where('code', $code)->get();    		
    	}

    	return self::where('type', $type)->get();
    }
}
