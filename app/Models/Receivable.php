<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receivable extends Model
{
    public function createdBy(){
        return $this->belongsTo( 'App\Models\User', 'created_by' );
    }

    public function createdFor(){
        return $this->belongsTo( 'App\Models\User', 'created_for' );
    }

    public function currency(){
        return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function head(){
        return $this->belongsTo( 'App\Models\AccountsHead', 'head_id' );
    }

    /**
     * Calculate User Credit
     * 
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public static function getUserCredit( $userId ){
    	return self::where( 'created_for', $userId )->where('debit', 1)->sum('value');
    }

    /**
     * Calculate User Debit
     * 
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public static function getUserDebit( $userId ){
    	return self::where( 'created_for', $userId )->where('debit', 0)->sum('value');
    }

    /**
     * get user available balance 
     * 
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public static function getUserAvailableBalance( $userId ){
    	return self::getUserCredit( $userId ) - self::getUserDebit( $userId );
    }

    public static function getTotal(){
        $out = self::where( 'debit', 1 )->sum('value');
        $in  = self::where( 'debit', 0 )->sum('value');

        return $out - $in;
    }
}
