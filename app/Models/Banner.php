<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public static function getBanners(){
    	$dir = config('filesystems.disks.local.root') . '/uploads/banners';
    	return array_filter(scandir($dir), function($file) { 
            return ( $file == '.' || $file == '..' || strpos( $file, 'original' ) !== false ) ? false : true; 
        });        
    }
}
