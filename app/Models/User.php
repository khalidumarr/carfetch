<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role;
use App\Models\UserRole;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function hashPassword( $password ){
        return sha1( $password );
    }

    public function roles(){
        return $this->belongsToMany( 'App\Models\Role', 'user_roles', 'user_id', 'role_id' );
    }

    public function images(){
        return $this->morphMany('App\Models\Upload', 'uploadable');
    }

    public function purchases(){
        return $this->hasMany( 'App\Models\Sale', 'sold_to');        
    }

    public function getApplicableRole(){
        $roles = $this->roles;
        $applicableRole = $roles[0];
        foreach( $roles as $role ){
            if( $role->priority < $applicableRole->priority ){
                $applicableRole = $role;
            }
        }

        return $applicableRole;
    }

    public static function getRoleUsers( $roleCode, $query = false ){
        $role = Role::where( 'code', $roleCode )->first();
        if( !$role ){
            throw new \Exception( 'No role found with provided code' );
        }

        $userRoles = UserRole::where( 'role_id', $role->id )->get();
        $userIds = [];
        foreach( $userRoles as $userRole ){
            $userIds[] = $userRole->user_id;
        }

        if( $query === false ){
            return self::whereIn( 'id', $userIds )->get();        
        }
        else{
            return self::whereIn( 'id', $userIds );
        }
    }

    public function getTotalPayable(){
        return \App\Models\Receivable::getUserAvailableBalance( $this->id );
    }

    public function identityPicture(){
        return $this->belongsTo( 'App\Models\Upload', 'identity_picture' );
    }

    public function accountHead(){
        $head = AccountsHead::where( 'code', 'agent-' . $this->id . '-earnings' )->first();

        if( !$head ){
            return new AccountsHead();
        }

        return $head;
    }
}
