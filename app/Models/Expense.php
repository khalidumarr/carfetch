<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
	public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function head(){
    	return $this->belongsTo( 'App\Models\AccountsHead', 'head_id' );
    }

    public static function getTotalByDate( $start, $end ){
    	$expenses = self::where( 'created_at', '>', $start )
    				->where( 'created_at', '<', $end )->get();

        $total = 0;
        foreach( $expenses as $expense ){
            $total += ( $expense->currency->conversion ) * ( $expense->value );
        }

        return $total;
    }

    public static function getDailyTotalByDate( $start, $end ){
        $expenses = self::where( 'created_at', '>', $start )
                    ->where( 'created_at', '<', $end )->get();

        
        $dailyExpenses = [];
        foreach( $expenses as $expense ){
            $date = date( 'Y-m-d', strtotime( $expense->created_at ) );
            if( !isset( $dailyExpenses[$date] ) ){
                $dailyExpenses[$date] = 0;
            }

            $dailyExpenses[$date] += ( $expense->currency->conversion ) * ( $expense->value );
        }

        // reformating array
        $dailyExpensesFormated = [];
        foreach( $dailyExpenses as $date => $total ){
            $dailyExpensesFormated[] = [
                'd'         => $date,
                'expenses'  => $total
            ];
        }

        return $dailyExpensesFormated;
    }

    public static function getMonthlyTotal(){
        $expenses = self::all();

        $monthlyExpenses = [];
        $max = 0;
        foreach( $expenses as $expense ){
            $month = date( 'M', strtotime( $expense->created_at ) );
            if( !isset( $monthlyExpenses[$month] ) ){
                $monthlyExpenses[$month] = 0;
            }

            $value = ( $expense->currency->conversion ) * ( $expense->value );
            $monthlyExpenses[$month] += $value;
            if( $max < $monthlyExpenses[$month] ){
                $max = $monthlyExpenses[$month];
            }
        }

        // reformating array
        foreach( $monthlyExpenses as $month => $total ){
            $dailyExpensesFormated[] = [
                'month'         => $month,
                'expenses'      => $total,
                'percentage'    => ceil( ( $total / $max ) * 100 )
            ];
        }

        return $dailyExpensesFormated;
    }
}
