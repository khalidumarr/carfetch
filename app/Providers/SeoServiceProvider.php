<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class SeoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('seo_index', "index");
        View::share('seo_follow', "follow");
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
