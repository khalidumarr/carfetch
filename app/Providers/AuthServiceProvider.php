<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\AccountsHead;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('delete-car', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('edit-car', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('manage-responsibility', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('delete-user', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('add-car', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('edit-user', function ($user, $userObj) {
            return in_array( $user->getApplicableRole()->code, [ 'admin', 'sales', 'sale-purchase' ] );
        });

        Gate::define('sell-car', function ($user, $car) {
            return $user->getApplicableRole()->code == 'sale-purchase';
        });

        Gate::define('edit-container', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('delete-container', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('change-user-roles', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('approve-received-cash', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('auto-approval', function ($user) {
            return $user->getApplicableRole()->code == 'sales';
        });

        Gate::define('own-customers-receivables', function ($user) {
            return !in_array( $user->getApplicableRole()->code, array( 'admin', 'sales' ) );
        }); 

        Gate::define('view-car-costs', function ($user) {
            return !in_array( $user->getApplicableRole()->code,  array( 'sale-purchase', 'sales', 'repairing' ) );
        });

        Gate::define('view-car-total-costs', function ($user) {
            return !in_array( $user->getApplicableRole()->code,  array( 'sales', 'repairing' ) );
        });

        Gate::define('total-sales-count', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('total-inventory-count', function ($user) {
            return in_array( $user->getApplicableRole()->code,  array( 'sales', 'admin' ) );
        });

        Gate::define('delete-car-picture', function ($user) {
            return in_array( $user->getApplicableRole()->code,  array( 'sales', 'admin' ) );
        });

        Gate::define('add-car-picture', function ($user) {
            return in_array( $user->getApplicableRole()->code,  array( 'sales', 'admin' ) );
        });

        Gate::define('change-car-customer', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('view-sale-payments', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('add-payable-expense', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('add-receivable-expense', function ($user) {
            return $user->getApplicableRole()->code == 'admin';
        });

        Gate::define('pay-remaining-amount', function ($user) {
            return in_array( $user->getApplicableRole()->code, [ 'admin', 'sales', 'sale-purchase' ] );
        });

        Gate::define('view-net-earnings', function ($user) {
            return in_array( $user->getApplicableRole()->code, [ 'admin' ] );
        });

        Gate::define('create-agent-payable', function ($user, $newUser) {
            if( $newUser->getApplicableRole()->code != 'sale-purchase' ){
                return false;
            }
            
            if( !AccountsHead::where( 'code', 'agent-' . $newUser->id . '-earnings' )->first() ){
                return true;
            }

            return false;
        });
    }
}
