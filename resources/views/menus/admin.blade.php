<ul class="nav side-menu">
  <li><a href="/admin"><i class="fa fa-home"></i> Dashboard </a></li>
  <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/admin/user">List</a></li>
      <li><a href="/admin/role">Roles</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-automobile"></i> Cars <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/admin/cars">List</a></li>
      <li><a href="/admin/car-costs">Costs List</a></li>
      <li><a href="/admin/car-properties">Properties</a></li>
      <li><a href="/admin/car-makes">Makes</a></li>
      <li><a href="/admin/car-models">Models</a></li>               
      <li><a href="/admin/car-containers">Containers</a></li>               
    </ul>
  </li>
  <li><a><i class="fa fa-book"></i> Accounts <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/admin/accounts-heads">Account Heads</a></li>
      <li><a href="/admin/accounts-payables">Payables</a></li>
      <li><a href="/admin/accounts-expenses">Expenses</a></li>
      <li><a href="/admin/accounts-receivables">Receivables</a></li>
      <li><a href="/admin/customer-receivables">Customer Receivables</a></li>
      <li><a href="/admin/agents-earnings">Agent's Earnings</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-recycle"></i> Repair Responsibility <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/admin/car-responsibility">Responsible User Cars</a></li>                   
    </ul>
  </li>
  <li><a><i class="fa fa-bar-chart"></i> Reports <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li class=""><a href="/admin/get-dashboard">Dashboard</a></li>
      <li class=""><a href="/admin/report/inventory">Inventory</a></li>
      <li class=""><a href="/admin/report/sales">Sales</a></li>
      <li><a href="/admin/report/expenses">Expenses</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-wrench"></i> Settings <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/admin/banners">Banners</a></li>      
    </ul>
  </li>                  
</ul>