<ul class="nav side-menu">
  <li><a href="/admin"><i class="fa fa-home"></i> Dashboard </a></li>
  <li><a><i class="fa fa-automobile"></i> Cars <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/admin/cars">List</a></li>              
    </ul>
  </li>
  <li><a><i class="fa fa-recycle"></i> Repair Responsibility <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/admin/car-responsibility">My Cars</a></li>                   
    </ul>
  </li>                
</ul>