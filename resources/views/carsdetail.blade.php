@extends('layouts.frontend')

@section('title_and_meta')  
    <title>Al Mutafawiq | {{ $car->title }} For sale in UAE</title>
    <meta name="title" content="Al Mutafawiq | {{ $car->title }} For sale in UAE">
    <meta name="description" content="For sale {{ $car->title }} in Dubai UAE. Car is in Perfect condition. Buy this car now at amazing and cheapest price in UAE."/>   
@endsection


@section('content')

<div class="mens">    
  <div class="main">
     <div class="wrap">
     	<ul class="breadcrumb breadcrumb__t"><a class="home" href="#">Home</a> / <a href="#">Cars</a> / {{ $car->title }} / ALN{{ (($car->getTotalCost() * 2) / 1000) }}</ul>
		<div class="cont span_3_of_3">
			@if( Session::has('message') )
			    <div class="alert alert-success alert-dismissible fade in" role="alert">
			        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			        </button>
			        {{ Session::get('message') }}
			    </div>
			@endif
		  	<div class="grid images_3_of_2">
		  		<div class="fotorama image_container" data-nav="thumbs">
			  		@if( $car->getMainImage() !== false )						
						@foreach( $car->images as $image )
							<img src="{{ $image->url }}" class="img-responsive">
						@endforeach						
					@else
						<img src="/images/notavailable.jpg">
					@endif
				</div>
				@if( count( $car->images ) > 0 )
					<div class="row">
						<a href="javascript:void(0);" target="_blank" class="btn btn-default download_pictures_zip">Download ZIP</a>
					</div>	
				@endif
				<div class="clearfix"></div>
            </div>
			<div class="desc1 span_3_of_2">
				@can( 'edit-car' )
					<a target="_blank" class="btn btn-default" href="/admin/cars/{{ $car->id }}/edit">Edit</a>
				@endif
				<h3 class="m_3">{{ $car->title }}</h3>
			 	<p class="m_5">{{ $car->sale_price }} {{ $default_currency->code }}</p>
				<div class="btn_form">
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="z-index:1;">Enquire Now</button>
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Modal Header</h4>
							</div>
							<div class="modal-body">
								{!! Form::open(['url' => '/contact/enquire/']) !!}
									<div class="to">
										<input type="hidden" name="url" value="/cars/{{$car->code}}">
				                     	<input data-validation="required" type="text" name="name" class="text" placeholder="Name">
									 	<input data-validation="required" type="text" name="phone" class="text" placeholder="Phone" style="margin-left: 10px">
									 	<input data-validation="email" type="text" name="email" class="text" placeholder="Email">									 	
										<input data-validation="required" type="text" name="offer_price" class="text" placeholder="Offer Price" style="margin-left: 10px">
									</div>
									<div class="text">
					                   <textarea data-validation="required" placeholder="Message:" name="message"></textarea>
					                </div>
					                				               
							</div>
							<div class="modal-footer">
								<input type="submit" class="btn btn-primary"value="Submit"></button>
								<input type="submit" class="btn btn-default" data-dismiss="modal" value="Close"></button>
							</div>
							{!! Form::close() !!}
						</div>

						</div>
					</div>
			 	</div>			 	
			 	<p class="m_text2">
					<ul>
						<li>Make : {{ $car->model->make->name }}</li>
						<li>Model : {{ $car->model->name }}</li>
						<li>LOT # : {{ $car->propByCode( 'lot_number' )->value }}</li>						
						
						@if( $car->propByCode( 'color' )->value )
							<li>Color : {{ $car->propByCode( 'color' )->value }}</li>
						@endif

						@if (isset($car->container->arrival_date))
							@if (strtotime($car->container->arrival_date) > time())
								<li>Expected Arrival Date : {{ date('d-m-Y', strtotime($car->container->arrival_date)) }}</li>
							@else
								<li>Expected Arrival Date : Arrived</li>
							@endif
						@endif								
					</ul>
				</p>
			 	<p class="m_text2">{{ $car->description }} </p>
			</div>
			<div class="clear"></div>	
	    <div class="clients">	    
    </div>
    </div>
	<div class="clear"></div>
	@if( $similar_cars )
		<h3 class="m_3">Similar Cars</h3>
		<ul id="flexiselDemo3">
			@foreach( $similar_cars as $car )
				<li><img src="@if( $car->getMainImage() ){{ $car->getMainImage()->url }}@else {{ "/images/wargaeyar.jpg" }} @endif" /><a href="/cars/{{ $car->code }}">{{ ucwords( $car->model->name ) }}</a><p>{{ $car->sale_price }} {{ $car->currency->code }}</p></li>
			@endforeach
		</ul>
	@endif
	<script type="text/javascript">
		$(window).load(function() {
			$.validate();
		
			$("#flexiselDemo3").flexisel({
				visibleItems: 5,
				animationSpeed: 1000,
				autoPlay: false,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
		    	responsiveBreakpoints: { 
		    		portrait: { 
		    			changePoint:480,
		    			visibleItems: 1
		    		}, 
		    		landscape: { 
		    			changePoint:640,
		    			visibleItems: 2
		    		},
		    		tablet: { 
		    			changePoint:768,
		    			visibleItems: 3
		    		}
		    	}
		    });
		    
		});
	</script>
	<script type="text/javascript" src="/js/jquery.flexisel.js"></script>
		   </div>

		</div>
@endsection