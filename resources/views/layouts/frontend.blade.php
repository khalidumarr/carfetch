<!DOCTYPE HTML>
<html>
<head>	
	@yield('title_and_meta', View::make('default_meta'))
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script src="/admin_assets/vendors/jquery/dist/jquery.min.js"></script>
	<meta name="robots" content="{{ $seo_index }}, {{ $seo_follow }}">
	
	<!-- start menu -->
	<link href="/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="/js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
	
	<!--start slider -->
    <link rel="stylesheet" href="/css/fwslider.css" media="all">
    
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/fwslider.js"></script>
	<!--end slider -->

	<!-- fotorama.css & fotorama.js. -->
	<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->

	<!-- dropdown -->
	<script src="/js/jquery.easydropdown.js"></script>

	<!-- bootstrap -->
    <link href="/admin_assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="/admin_assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- validation -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="/admin_assets/js/moment/moment.min.js"></script>
    <script src="/admin_assets/js/datepicker/daterangepicker.js"></script>

    <!-- JS ZIP -->	
    <script src="/js/jszip.min.js"></script>

    <!-- File Saver -->
    <script src="/js/fileSaver.js"></script>

    <!-- Fake Loader -->
    <link href="/css/fakeLoader.css" rel="stylesheet">
    <script src="/js/fakeLoader.min.js"></script>

	<!-- custom JS -->
	<script src="/js/script.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164141562-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-164141562-1');
	</script>
</head>
<div id="fakeLoader"></div>
<body>
    <div class="header-top">
	</div>
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="/"><img src="/images/logo.png" alt=""/></a>
				</div>
				<div class="menu">
		            <ul class="megamenu skyblue">
						<li class="active grid"><a href="/">Home</a></li>
						<li><a class="color4" href="/cars">Cars</a></li>				
						<li><a class="color5" href="/about">About</a></li>
						<li><a class="color6" href="/contact">Contact</a></li>
						<li><a class="color6 special-button" href="/cars?latest=1">Latest</a></li>
					</ul>
				</div>				
			</div>				
			<div class="header-form">
				<span class="search">
					<form action="/cars" method="GET">
						<input type="text" placeholder="Search cars" class="text pull-right lot_no_input" name="search-keyword" value="<?=(isset($_REQUEST['search-keyword'])) ? $_REQUEST['search-keyword'] : ''?>">
						<input type="submit">
					</form>					
				</span>
				<span class="customer-login">
					<form action="/dashboard" method="GET">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="text" placeholder="Account Login" class="text pull-right lot_no_input" name="login" value="<?=(isset($_REQUEST['login'])) ? $_REQUEST['login'] : ''?>">
						<input type="submit" >
					</form>					
				</span>
				@if( Session::has('login-errors') )	
					<span class="login-errors">			        
			        	{{ Session::get('login-errors') }}
			       	</span>
			    @endif
			</div>
     		<div class="clear"></div>
     	</div>
	</div>

	@yield('content')

	<div class="footer">
		<div class="footer-top">
			<div class="wrap">
				<div class="section group example">
					<div class="col_1_of_2 span_1_of_2">
						<ul class="f-list">
							<li><img src="/images/2.png"><span class="f-text">Free Shipping All over UAE</span><div class="clear"></div></li>
						</ul>
					</div>
					<div class="col_1_of_2 span_1_of_2">
						<ul class="f-list">
							<li><img src="/images/3.png"><span class="f-text">Call us! 056 985 4255</span><div class="clear"></div></li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="wrap">
				<div class="copy">
					<p>Built with <img src="/images/coffee.png"> in Dubai</p>
				</div>
				<div class="f-list2">
					<ul>
						<li class="active"><a href="/about">About Us</a></li> |
						<li><a href="/terms">Terms & Conditions</a></li> |
						<li><a href="/contact">Contact Us</a></li> 
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</body>
</html>