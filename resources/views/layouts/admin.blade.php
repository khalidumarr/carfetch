<!DOCTYPE html> <html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    
    <!-- jQuery -->
    <script src="/admin_assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <link href="/admin_assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/admin_assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/admin_assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/admin_assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="/admin_assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/admin_assets/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="/admin_assets/build/css/custom.min.css" rel="stylesheet">

    <link href="/admin_assets/css/custom.css" rel="stylesheet">

    <!-- chosen -->
    <link href="/admin_assets/css/chosen.css" rel="stylesheet">

    <!-- Switchery -->
    <link href="/admin_assets/vendors/switchery/dist/switchery.min.css" rel="stylesheet">

    <!-- Data table style -->
    <link href="/admin_assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/admin_assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/admin_assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/admin_assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/admin_assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />

    <!-- cropper -->
    <link rel="stylesheet" href="/admin_assets/css/cropper/cropper.min.css">
    <link rel="stylesheet" href="/admin_assets/css/cropper/main.css">
  </head>
<div id="fakeLoader"></div>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed ">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-automobile"></i> <span> {{ config('app.name', 'Laravel') }}</span></a>
            </div>

            <div class="clearfix"></div>
            
            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                @if( count( Auth::user()->images ) > 0 )
                  <a href="{{ Auth::user()->images[0]->url }}" data-fancybox><img src="{{ Auth::user()->images[0]->url }}" alt="..." class="img-circle profile_img"></a>
                @else
                  <img src="/images/img.jpg" alt="..." class="img-circle profile_img">
                @endif
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Admin Panel</h3>
                 @include('menus.' . Auth::user()->getApplicableRole()->code )                
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>              
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">                
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu no-print">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    @if( count( Auth::user()->images ) > 0 )
                      <img src="{{ Auth::user()->images[0]->url }}" alt="..." >
                    @else
                      <img src="/images/img.jpg" alt="..." >
                    @endif
                    {{ Auth::user()->name }}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/profile/"> Profile</a></li>
                    <li>
                      <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">                        
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <i class="fa fa-sign-out pull-right"></i> Log Out
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

          @yield('content')

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer class="no-print">
          <div class="pull-right">
            Built under <span style="font-size: 20px;">&#9728;</span> in Dubai
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="/admin_assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/admin_assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/admin_assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/admin_assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/admin_assets/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/admin_assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/admin_assets/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/admin_assets/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="/admin_assets/vendors/Flot/jquery.flot.js"></script>
    <script src="/admin_assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/admin_assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/admin_assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/admin_assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/admin_assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/admin_assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/admin_assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/admin_assets/vendors/DateJS/build/date.js"></script>

    <!-- JQVMap -->
    <script src="/admin_assets/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/admin_assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/admin_assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/admin_assets/js/moment/moment.min.js"></script>
    <script src="/admin_assets/js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/admin_assets/build/js/custom.min.js"></script>

    <!-- Custom Admin panel Script -->
    <script src="/admin_assets/js/custom.js"></script>

    <!-- Switchery -->
    <script src="/admin_assets/vendors/switchery/dist/switchery.min.js"></script>

    <!-- Data tables -->
    <script src="/admin_assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/admin_assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/admin_assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/admin_assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    
    <!-- chosen -->
    <script src="/admin_assets/js/chosen.js"></script>

    <!-- Morris JS -->
    <script src="/admin_assets/js/morris/morris-0.4.3.min.js"></script>
    
    <!-- Raphael JS -->
    <script src="/admin_assets/js/morris/raphael-min.js"></script>
    
    <!-- Javascript ZIP -->
    <script src="/js/jszip.min.js"></script>
    
    <!-- File Saver -->
    <script src="/js/fileSaver.js"></script>

    <!-- custom JS -->
    <script src="/js/script.js"></script> 
  
    <!-- fancy box -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
    
    <!-- cropit -->
    <script src="/admin_assets/js/jquery.cropit.js"></script>

    <!-- cropper -->
    <script src="/admin_assets/js/cropper/cropper.min.js"></script>
    <script src="/admin_assets/js/cropper/main.js"></script>

    <!-- Jquery UI -->
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </body>
</html>
