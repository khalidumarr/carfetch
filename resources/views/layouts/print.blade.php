<!DOCTYPE HTML>
<html>
<head>	
	@yield('title_and_meta', View::make('default_meta'))
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script src="/admin_assets/vendors/jquery/dist/jquery.min.js"></script>
	<meta name="robots" content="{{ $seo_index }}, {{ $seo_follow }}">
	
	<!-- start menu -->
	<link href="/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="/js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
	
	<!--start slider -->
    <link rel="stylesheet" href="/css/fwslider.css" media="all">
    
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/fwslider.js"></script>
	<!--end slider -->

	<!-- fotorama.css & fotorama.js. -->
	<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->

	<!-- dropdown -->
	<script src="/js/jquery.easydropdown.js"></script>

	<!-- bootstrap -->
    <link href="/admin_assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="/admin_assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- validation -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="/admin_assets/js/moment/moment.min.js"></script>
    <script src="/admin_assets/js/datepicker/daterangepicker.js"></script>

    <!-- JS ZIP -->	
    <script src="/js/jszip.min.js"></script>

    <!-- File Saver -->
    <script src="/js/fileSaver.js"></script>

    <!-- Fake Loader -->
    <link href="/css/fakeLoader.css" rel="stylesheet">
    <script src="/js/fakeLoader.min.js"></script>

	<!-- custom JS -->
	<script src="/js/script.js"></script>
</head>
<div id="fakeLoader"></div>
<body>  
	<div class="container" style="width: 70%">
		@yield('content')	
	</div>  	
	<div class="print" style="display: none; width: 100%">
		@yield('content')
	</div>
</body>
</html>