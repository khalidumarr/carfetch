@extends('layouts/admin')
@section('content')
<h3> Edit Monthly Expense </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($expense, ['route' => ['accounts-expenses.update', $expense], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <div class="form-group">
            {{ Form::label('head_id', 'Account Head*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="head_id" class="form-control select2_expense">
                    <option disabled="disabled">Select Account Head</option>
                    @foreach( $heads as $key => $head )
                        <option value="{{ $head->id }}"> {{ $head->title }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('value', 'Value*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('value', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="currency_id" class="form-control" disabled="disabled">
                    <option disabled="disabled">Select Cost Currency</option>
                    @foreach( $currencies as $currency )
                        <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Update', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>
@stop