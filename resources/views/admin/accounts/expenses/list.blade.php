@extends('layouts/admin')
@section('content')
<h3> Monthly Expenses List </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row">
    <div class="col-sm-9">
        <form action="/admin/accounts-expenses/">
        </form>
    </div>
    <div class="col-sm-3">
        <div class="section_add text-right">
            <a href="/admin/accounts-expenses/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New Current Month Expense</a>
        </div>
    </div>
</div>      
<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th>
          <input type="checkbox" id="check-all" class="flat">
        </th>
        <th class="column-title">Year </th>
        <th class="column-title">Month </th>
        <th class="column-title">Expense </th>
        <th class="column-title">Value</th>
        <th class="column-title">Created at</th>
        <th class="column-title">Updated at</th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @if( count( $expenses ) == 0 )
            <tr class="even pointer">
                <td colspan="8">No Monthly Expeses Found</td>                
            </tr>
        @else
            @foreach( $expenses as $expense )
                <tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                    <td>{{ $expense->year }}</td>
                    <td>{{ $expense->month }}</td>
                    <td>{{ $expense->head->title }}</td>
                    <td>{{ $expense->value }} {{ $expense->currency->code }}</td>
                    <td>{{ $expense->created_at }}</td>
                    <td>{{ $expense->updated_at }}</td>
                    <td>
                        <a class="btn btn-default source" href="/admin/accounts-expenses/{{ $expense->id }}/edit/">Edit</a>
                        {{ Form::open( [ 'url' => '/admin/accounts-expenses/' . $expense->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                            {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                        {{ Form::close() }}
                    </td>
                </tr>    
            @endforeach
        @endif   
    </tbody>
  </table>
   {{ $expenses->links() }}
</div>
@stop