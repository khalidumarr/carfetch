@extends('layouts/admin')
@section('content')
<h3> Add New head </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($head, ['route' => ['accounts-heads.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <div class="form-group">
            {{ Form::label('title', 'Title*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('code', 'Code*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('code', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('type_id', 'Type*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}            
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="type_id" class="form-control">
                    <option value="">Select Type</option>
                    @foreach( $types as $type )
                        <option value="{{ $type->id }}"> {{ $type->title }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('default_value', 'Value', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('default_value', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="currency_id" class="form-control" disabled="disabled">
                    <option disabled="disabled">Select Cost Currency</option>
                    @foreach( $currencies as $currency )
                        <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::textarea('description', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
    
        <div class="ln_solid"></div> 
        
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on( 'input', 'input[name=name], input[name=title]', function(){
            $('input[name=code]').val( $(this).val().split(' ').join('-').toLowerCase() );
        })
    });
</script>
@stop