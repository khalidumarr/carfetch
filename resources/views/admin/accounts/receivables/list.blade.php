@extends('layouts/admin')
@section('content')

<div class="">
  <div class="page-title">
    <div class="row tile_count">
      <div class="col-md-3 col-sm-5 col-xs-7 tile_stats_count">
        <span class="count_top"><i class="fa fa-dollar"></i> Total Receivable</span>
        <div class="count">{{ $total_receivable }} AED</div>
      </div>
    </div>
    <div class="row">
      @if( Session::has('message') )
          <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              {{ Session::get('message') }}
          </div>
      @endif
      @if( Session::has('errors') )
          <div class="alert alert-danger alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              {{ Session::get('errors') }}
          </div>
      @endif
      <div class="col-md-1">
        <!-- Small modal -->
        <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-sm">Recieve</button>
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Receive Amount</h4>
              </div>
              <div class="modal-body">                
                {!! Form::open(['url' => '/admin/accounts-receivables/receive', 'method' => 'POST']) !!}
                  <input type="hidden" name="back_url" value="{{ $_SERVER['REQUEST_URI'] }}">
                  <!-- General Start -->
                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('received_from', 'Recieved From', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="received_from_id" class="form-control select2_received_from">
                              <option value="">Select Recieved From</option>
                              @foreach( $sales_users as $received_from )
                                  <option value="{{ $received_from->id }}">[{{ $received_from->getApplicableRole()->code }}] {{ $received_from->name }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('head_id', 'Accounts Head*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="head_id" class="form-control select2_expense">
                              <option disabled="disabled">Select Head</option>
                              @foreach( $heads as $head )
                                  <option value="{{ $head->id }}"> {{ $head->title }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::text('amount', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="currency_id" class="form-control" disabled="disabled">
                              @foreach( $currencies as $currency )
                                  <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Add">
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /modals -->
      </div>
      <div class="col-md-1">
        <!-- Small modal -->
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm2">Release</button>
        <div class="modal fade bs-example-modal-sm2" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Release Amount</h4>
              </div>
              <div class="modal-body">
              {!! Form::open(['url' => '/admin/accounts-receivables/release', 'method' => 'POST']) !!}
              <input type="hidden" name="back_url" value="{{ $_SERVER['REQUEST_URI'] }}">
                  <!-- General Start -->
                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('released_to_id', 'Released To', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="released_to_id" class="form-control select2_released_to">
                              <option value="">Select Released To</option>
                              @foreach( $sales_users as $release_to )
                                  <option value="{{ $release_to->id }}">[{{ $release_to->getApplicableRole()->code }}] {{ $release_to->name }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('head_id', 'Accounts Head*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="head_id" class="form-control select2_expense">
                              <option disabled="disabled">Select Head</option>
                              @foreach( $heads as $head )
                                  <option value="{{ $head->id }}"> {{ $head->title }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::text('amount', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="currency_id" class="form-control" disabled="disabled">
                              @foreach( $currencies as $currency )
                                  <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Add">
              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /modals -->
      </div>
    </div>
  </div>

  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Accounts Receivables</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Created For</th>
              <th>Created By</th>
              <th>Account Head</th>
              <th>Debit</th>
              <th>Value</th>
              <th>Date Create</th>
            </tr>
          </thead>


          <tbody>
            @foreach( $receivables as $receivable )
              <tr>
                <td>{{ ( isset( $receivable->createdFor ) ) ? $receivable->createdFor->name : '' }}</td>
                <td>{{ ( isset( $receivable->createdBy ) ) ? $receivable->createdBy->name : '' }}</td>
                <td>{{ $receivable->head->title }}</td>
                <td>{{ ( $receivable->debit == 1 ) ? 'Yes' : 'No' }}</td>
                <td>{{ $receivable->value }} {{ $receivable->currency->code }}</td>
                <td>{{ $receivable->created_at }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    ///////////////////////////
    // expense receive modal //
    ///////////////////////////

    $(".select2_currency").chosen();
    /////////////////////////////
    // expense receive modal \ //
    /////////////////////////////
  });
</script>
@stop