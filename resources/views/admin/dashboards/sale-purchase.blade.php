@extends('layouts/admin')
@section('content')
  <!-- top tiles -->
<div class="row ">	
<!-- 	<div class="tile_count">
		<div class="col-md-4 col-sm-8 col-xs-6 tile_stats_count">
		  <span class="count_top"><i class="fa fa-user"></i> My Received Earnings</span>
		  <div class="count green received_earnings">0 AED</div>
		</div>		
	</div>
	<div class="tile_count">
		<div class="col-md-4 col-sm-8 col-xs-6 tile_stats_count">
		  <span class="count_top"><i class="fa fa-user"></i> My Pending Earnings</span>
		  <div class="count green pending_earnings">0 AED</div>
		</div>		
	</div> -->
	<div class="tile_count">
		<div class="col-md-4 col-sm-8 col-xs-6 tile_stats_count">
			<a href="/admin/customer-receivables">
				<span class="count_top"><i class="fa fa-user"></i> My Customer Receivables </span>
				<div class="count green customer_receivables">0 AED</div>
			</a>
		</div>		
	</div>		
</div>
<div class="row">	
	<div class="tile_count">
		<div class="col-md-4 col-sm-8 col-xs-6 tile_stats_count">
		  <span class="count_top"><i class="fa fa-user"></i> My Total Sales</span>
		  <div class="count green my_total_sales">0</div>
		</div>		
	</div>
	<div class="tile_count">
		<a href="/admin/cars">
			<div class="col-md-4 col-sm-8 col-xs-6 tile_stats_count">
			  <span class="count_top"><i class="fa fa-user"></i> My Units Available for sale</span>
			  <div class="count green my_units_available">0</div>
			</div>		
		</a>
	</div>
</div>
  <!-- /top tiles -->

<script type="text/javascript">
	$(document).ready(function(){
		// $.ajax({
		// 	url : '/admin/reports/my-received-earnings',
		// 	success : function(data){
		// 		$('.received_earnings').text( data );
		// 	}
		// });

		// $.ajax({
		// 	url : '/admin/reports/my-pending-earnings',
		// 	success : function(data){
		// 		$('.pending_earnings').text( data );
		// 	}
		// });

		$.ajax({
			url : '/admin/reports/get-cx-receivables',
			success : function(data){
				$('.customer_receivables').text( data );
			}
		});

		$.ajax({
			url : '/admin/reports/my-total-sales',
			success : function(data){
				$('.my_total_sales').text( data );
			}
		});

		$.ajax({
			url : '/admin/reports/my-units-available',
			success : function(data){
				$('.my_units_available').text( data );
			}
		});
	});
</script>
@stop