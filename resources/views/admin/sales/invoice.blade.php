@extends('layouts/admin')
@section('content')
  <div class="">
    <div class="page-title no-print">
      <div class="title_left">
        <h3>Invoice <small># {{ $sale->number }}</small></h3>
      </div>  
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_content">

            <section class="content invoice">
              <!-- title row -->
              <div class="row">
                <div class="col-xs-12 invoice-header">
                  <h1>
                                  <img src="/images/logo.png" width="200">
                                  <small class="pull-right">Date: {{ $sale->created_at }}</small>
                              </h1>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                                  <strong>Al Mutafawif Used Cars</strong>
                                  <br>Shed 7, Industrial area 4,Sharjah UAE
                                  <br>Phone: 0554457898,0501631761,0569854255
                                  <br>Email: almutafawiqusedcars@gmail.com
                                  <br>Website: www.amusedcars.net
                              </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                                  <strong>{{ $sale->customer->name }}</strong>
                                  <br>{{ $sale->customer->address }}
                                  <br>Phone: {{ $sale->customer->phone }}
                              </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice #{{ $sale->number }}</b>
                  <br>                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-xs-12 table">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th style="width: 40%">Name</th>
                        <th>Vin #</th>
                        <th>Color</th>                        
                        <th>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{ $sale->car->title }}</td>
                        <td>{{ $sale->car->propByCode( 'vin_number' )->value }}</td>
                        <td><?=($sale->car->color) ? $sale->car->color : '-'?></td>                                                
                        <td>{{ number_format( $sale->price ) }} AED</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
                <h3>CONDITIONS OF THE DEAL</h3>
                  <p class="text-muted well well-sm no-shadow" >                    
                    <br>1. Balance will be paid with in 3 days
                    <br>2. In case of failure to pay balance, the car will be resold.
                    <br>3. Once vehicle ownership transferred, will not be returned or exchanged.
                    <br>4. The customer is requested to check the vehicle thoroughly, as the 
                    <br>   company will neither take any responsibility after the deal nor will 
                    <br>   attend any claims
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-xs-6">
                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                        <tr>
                          <th style="width:50%">Subtotal:</th>
                          <td>{{ number_format( $sale->price ) }} AED</td>
                        </tr>
                        <tr>
                          <th>Paid:</th>
                          <td>{{ number_format( $sale->amount_paid ) }} AED</td>
                        </tr>
                        <tr>
                          <th>Balance:</th>
                          <td>{{ number_format( ( $sale->price - $sale->amount_paid ) ) }} AED</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-xs-12">
                  <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>                  
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>

<style type="text/css">
  @media print{    
      .no-print{
          display: none !important;
      }
  }
</style>
@stop