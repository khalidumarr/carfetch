@extends('layouts/admin')
@section('content')
<h3> Car Models List </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    	</button>
        {{ Session::get('message') }}
    </div>
@endif
@if( Session::has('errors') )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('errors') }}
    </div>
@endif
<div class="row">
	<div class="col-sm-9">
		<form action="/admin/car-models/">
		</form>
	</div>
	<div class="col-sm-3">
		<div class="section_add text-right">
			<a href="/admin/car-models/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New Car Model</a>
		</div>
	</div>
</div>		
<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th>
          <input type="checkbox" id="check-all" class="flat">
        </th>
        <th class="column-title">Name </th>
        <th class="column-title">Make </th>
        <th class="column-title">Updated at </th>
        <th class="column-title">Created at </th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @if( count( $models ) == 0 )
            <tr class="even pointer">
                <td colspan="5">No Car models Found</td>                
            </tr>
        @else
            @foreach( $models as $model )
            	<tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                    <td>{{ $model->name }}</td>
                    <td>{{ $model->make->name }}</td>
                    <td>{{ $model->created_at }}</td>
                    <td>{{ $model->updated_at }}</td>
                    <td>
                    	<a class="btn btn-default source" href="/admin/car-models/{{ $model->id }}/edit/">Edit</a>
                        {{ Form::open( [ 'url' => '/admin/car-models/' . $model->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                            {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                        {{ Form::close() }}
                    </td>
                </tr>    
            @endforeach
        @endif   
    </tbody>
  </table>
   {{ $models->links() }}
</div>
@stop