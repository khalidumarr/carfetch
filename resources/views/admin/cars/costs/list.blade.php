@extends('layouts/admin')
@section('content')
<h3> Car Costs List </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    	</button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row">
	<div class="col-sm-9">
		<form action="/admin/car-costs/">
		</form>
	</div>
	<div class="col-sm-3">
		<div class="section_add text-right">
			<a href="/admin/car-costs/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New Car Cost</a>
		</div>
	</div>
</div>		
<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th>
          <input type="checkbox" id="check-all" class="flat">
        </th>
        <th class="column-title">Name </th>
        <th class="column-title">Code </th>
        <th class="column-title">Currency </th>
        <th class="column-title">Updated at </th>
        <th class="column-title">Created at </th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @if( count( $costs ) == 0 )
            <tr class="even pointer">
                <td colspan="5">No Car Costs Found</td>                
            </tr>
        @else
            @foreach( $costs as $property )
            	<tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                    <td>{{ $property->name }}</td>
                    <td>{{ $property->code }}</td>
                    <td>{{ $property->currency->name }}</td>
                    <td>{{ $property->created_at }}</td>
                    <td>{{ $property->updated_at }}</td>
                    <td>
                    	<a class="btn btn-default source" href="/admin/car-costs/{{ $property->id }}/edit/">Edit</a>
                        {{ Form::open( [ 'url' => '/admin/car-costs/' . $property->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                            {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                        {{ Form::close() }}
                    </td>
                </tr>    
            @endforeach
        @endif   
    </tbody>
  </table>
   {{ $costs->links() }}
</div>
@stop