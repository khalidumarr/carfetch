@extends('layouts/admin')
@section('content')
<h3> Add New Car Property </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($property, ['route' => ['car-properties.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <div class="form-group">
            {{ Form::label('type', 'Type*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="car_prop_type" class="form-control">
                    <option disabled="disabled">Select Car type</option>
                    @foreach( $types as $type )
                        <option value="{{ $type->id }}"> {{ $type->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('name', 'Name*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('name', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('code', 'Code*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('code', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('show_on_site', 'Show On Site ?', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('show_on_site', null, null, [ 'class' => 'js-switch', 'id' => 'show_on_site' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('is_required', 'Is required ?', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('is_required', null, null, [ 'class' => 'js-switch', 'id' => 'is_required' ]) }}
            </div>
        </div>
    
        <div class="ln_solid"></div>
        
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on( 'input', 'input[name=name], input[name=title]', function(){
            $('input[name=code]').val( $(this).val().split(' ').join('-').toLowerCase() );
        });
    });
</script>
@stop