@extends('layouts/admin')
@section('content')
<!-- Data table style -->
<link href="/admin_assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="/admin_assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="/admin_assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="/admin_assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/admin_assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<h3> Car Container </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($container, ['route' => ['car-containers.update', $container], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
            
        <div class="form-group">
            <label for="number" class="control-label col-md-3 col-sm-3 col-xs-12">Shipper*</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="shipper_id" class="form-control select2_shipper" disabled>
                  <option value="">Shipper</option>
                  @foreach( $shippers as $shipper )
                      <option value="{{ $shipper->id }}" @if( $container->shipper_id == $shipper->id ) selected @endif> {{ $shipper->name }} </option>
                  @endforeach
              </select> 
            </div>
        </div>
        
        <div class="form-group">
            {{ Form::label('number', 'Number*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('number', null, [ 'class' => 'form-control col-md-7 col-xs-12', 'disabled' => 'true' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('arrival_date', 'Arrival Date*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('arrival_date', null, [ 'class' => 'form-control col-md-7 col-xs-12', 'disabled' => 'true' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('amount', null, [ 'class' => 'form-control col-md-7 col-xs-12', 'disabled' => 'true' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="currency_id" class="form-control" disabled="disabled">
                    <option disabled="disabled">Select Currency</option>
                    @foreach( $currencies as $currency )
                        <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
    
        <div class="ln_solid"></div>
        <h3>Loaded Cars</h3>
        <div class="x_content">
          <table id="datatable-checkbox2" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Model</th>
                <th>Vin #</th>
                <th>Lot #</th>
                <th>year</th>
                <th>Source</th>                
                <th>Stock Type</th>
                <th>Created at</th>
              </tr>
            </thead>
            <tbody>
              @foreach( $container->cars as $car )
                <tr>                  
                  <td>{{ $car->model->name }}</td>
                  <td>{{ $car->propByCode('vin_number')->value }}</td>
                  <td>{{ $car->propByCode('lot_number')->value }}</td>
                  <td>{{ $car->year }}</td>
                  <td>{{ $car->source->name }}</td>
                  <td>{{ $car->stockType->title }}</td>
                  <td>{{ $car->created_at }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        
        <div class="ln_solid"></div>
        <h3>Not Loaded Cars</h3>
        <div class="x_content">
          <table id="datatable-checkbox" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Model</th>
                <th>Vin #</th>
                <th>Lot #</th>
                <th>year</th>
                <th>Source</th>                
                <th>Stock Type</th>
                <th>Created at</th>
              </tr>
            </thead>


            <tbody>
              @foreach( $cars as $car )
                <tr>
                  <td>{{ $car->model->name }}</td>
                  <td>{{ $car->propByCode('vin_number')->value }}</td>
                  <td>{{ $car->propByCode('lot_number')->value }}</td>
                  <td>{{ $car->year }}</td>
                  <td>{{ $car->source->name }}</td>
                  <td>{{ $car->stockType->title }}</td>
                  <td>{{ $car->created_at }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
     $(document).ready(function(){  

        $('#arrival_date').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

         var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

         TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        var $datatable = $('#datatable-checkbox');
        var $datatable2 = $('#datatable-checkbox2');

        $datatable.dataTable({
          'order': [[ 2, 'desc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });

        $datatable2.dataTable({
          'order': [[ 2, 'desc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });


        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        $datatable2.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
    });
</script>
@stop