@extends('layouts/admin')
@section('content')
<h3> Car Containers List </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    	</button>
        {{ Session::get('message') }}
    </div>
@endif
<form action="/admin/car-containers">
    <div class="row">   
        <div class="col-md-3">
            <input id="arrival_date_from" name="arrival_date_from" class="form-control search_field" placeholder="Arrival date from" value="{{ ( isset( $_REQUEST['arrival_date_from'] ) ) ? $_REQUEST['arrival_date_from'] : '' }}">
        </div>
        <div class="col-md-3">
            <input id="arrival_date_to" name="arrival_date_to" class="form-control search_field" placeholder="Arrival date from" value="{{ ( isset( $_REQUEST['arrival_date_to'] ) ) ? $_REQUEST['arrival_date_to'] : '' }}">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <input type="submit" value="Search" class="btn btn-default search_submit">
        </div>
    </div>
</form>
<div class="row">
	<div class="col-sm-9">
		<form action="/admin/car-containers/">
		</form>
	</div>
	<div class="col-sm-3">
		<div class="section_add text-right">
			<a href="/admin/car-containers/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New Car Container</a>
		</div>
	</div>
</div>		
<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th>
          <input type="checkbox" id="check-all" class="flat">
        </th>
        <th class="column-title">Shipper </th>
        <th class="column-title">Number </th>
        <th class="column-title">Arrival Date </th>
        <th class="column-title">Updated at </th>
        <th class="column-title">Created at </th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @if( count( $containers ) == 0 )
            <tr class="even pointer">
                <td colspan="6">No Car Containers Found</td>                
            </tr>
        @else
            @foreach( $containers as $container )
            	<tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                    <td>{{ $container->shipper->name }}</td>
                    <td>{{ $container->number }}</td>
                    <td>{{ $container->arrival_date }}</td>
                    <td>{{ $container->created_at }}</td>
                    <td>{{ $container->updated_at }}</td>
                    <td>
                        <a class="btn btn-default source" href="/admin/car-containers/{{ $container->id }}/">View</a>
                        @can('edit-container')
                    	   <a class="btn btn-default source" href="/admin/car-containers/{{ $container->id }}/edit/">Edit</a>
                        @endcan

                        @can('delete-container')
                            {{ Form::open( [ 'url' => '/admin/car-containers/' . $container->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                                {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                            {{ Form::close() }}
                        @endcan
                    </td>
                </tr>    
            @endforeach
        @endif   
    </tbody>
  </table>
   {{ $containers->links() }}
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#arrival_date_from').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#arrival_date_to').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
@stop