@extends('layouts/admin')
@section('content')
<div class="row">
	<div class="col-md-12">
		@if( Session::has('message') )
		    <div class="alert alert-success alert-dismissible fade in" role="alert">
		        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		        {{ Session::get('message') }}
		    </div>
		@endif
		@if( Session::has('error') )
		    <div class="alert alert-danger alert-dismissible fade in" role="alert">
		        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		        {{ Session::get('error') }}
		    </div>
		@endif
		@if( Session::has('errors') )
		    <div class="alert alert-danger alert-dismissible fade in" role="alert">
		        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		        <ul>
			        @foreach( $errors->all() as $error )
			        	<li>{{ $error }}</li>
			        @endforeach
		        </ul>
		    </div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<h3>General Details</h3>
		@if( !$car->sold )
			<button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-sm">Sell Car</button>
	        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
	          <div class="modal-dialog modal-md">
	            <div class="modal-content">

	              <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
	                </button>
	                <h4 class="modal-title" id="myModalLabel2">Add Invoice Details</h4>
	              </div>
	              <div class="modal-body">                
	                {!! Form::open(['url' => '/admin/sales/', 'method' => 'POST']) !!}
	                  <input type="hidden" name="car_id" value="{{ $car->id }}">
	                  <!-- General Start -->
	                  <div class="form-group">
	                    <div class="row">
	                      {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
	                      <div class="col-md-6 col-sm-6 col-xs-12">
	                          {{ Form::text('amount', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
	                      </div>
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <div class="row">
	                      {{ Form::label('received_amount', 'Received Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
	                      <div class="col-md-6 col-sm-6 col-xs-12">
	                          {{ Form::text('received_amount', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
	                      </div>
	                    </div>
	                  </div>
					  
					  <div class="form-group">
	                    <div class="row">
	                      {{ Form::label('manual_bill_number', 'Manual Bill Number', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
	                      <div class="col-md-6 col-sm-6 col-xs-12">
	                          {{ Form::text('manual_bill_number', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
	                      </div>
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <div class="row">
	                      {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
	                      <div class="col-md-9 col-sm-9 col-xs-12">
	                          <select name="currency_id" class="form-control">
	                              @foreach( $currencies as $currency )
	                                  <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
	                              @endforeach
	                          </select> 
	                      </div>
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <div class="row">
	                      {{ Form::label('payment_method', 'Payment Method*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
	                      <div class="col-md-9 col-sm-9 col-xs-12">
	                          <select name="payment_method" class="form-control">
	                              @foreach( $payment_methods as $payment_method )
	                                  <option value="{{ $payment_method->id }}" data-code="{{$payment_method->code  }}"> {{ $payment_method->title }} </option>
	                              @endforeach
	                          </select> 
	                      </div>
	                    </div>
	                  </div>

	                  <div class="form-group document_number_input" style="display:none;">
	                    <div class="row">
	                      {{ Form::label('document_number', 'Cheque Number*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
	                      <div class="col-md-6 col-sm-6 col-xs-12">
	                          {{ Form::text('document_number', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
	                      </div>
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <div class="row">
	                      {{ Form::label('sold_to', 'Sold To*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
	                      <div class="col-md-9 col-sm-9 col-xs-12">
	                          <select name="sold_to" class="select2_customer" style="width:300px; !important">
	                          	<option value="">Select Customer</option>
	                              @foreach( $customers as $sold_to )
	                                  <option value="{{ $sold_to->id }}"> {{ $sold_to->name }} - {{ $sold_to->phone }} </option>
	                              @endforeach
	                          </select><br> 
	                      	<a href="/admin/user/create" target="_blank">Create new customer</a>
	                      </div>
	                    </div>
	                  </div>

	              </div>
	              <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <input type="submit" class="btn btn-primary" value="Generate Invoice">
	              </div>
	              </form>
	            </div>
	          </div>
	        </div>
        @else
        	<a href="/admin/sales/{{ $car->sale->id }}/invoice" target="_blank" class="btn btn-primary">Generate Invoice </a>
        	@if( $car->sale->price > $car->sale->getAllPaymentsTotal() )
        		@can( 'pay-remaining-amount' )
		        	<button type="button" class="btn btn-success" data-toggle="modal" data-target=".pay-remaining">Pay Remaining Amount</button>
			        <div class="modal fade pay-remaining" tabindex="-1" role="dialog" aria-hidden="true">
			          <div class="modal-dialog modal-md">
			            <div class="modal-content">

			              <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                </button>
			                <h4 class="modal-title" id="myModalLabel2">Add Remaining Amount Details</h4>
			              </div>
			              <div class="modal-body">                
			                {!! Form::open(['url' => '/admin/sales/' . $car->sale->id . '/pay-remaining', 'method' => 'POST']) !!}
			                  <!-- General Start -->

			                  <div class="form-group">
			                    <div class="row">
			                      <div class="control-label col-md-3 col-sm-3 col-xs-12">
			                      	Approved Amount
			                      </div>		                      
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                          {{ $car->sale->getAllPaymentsTotal(true) }} {{ $car->currency->code }}
			                      </div>
			                    </div>
			                  </div>
			                  <div class="form-group">
			                    <div class="row">
			                      <div class="control-label col-md-3 col-sm-3 col-xs-12">
			                      	UnApproved Amount
			                      </div>		                      
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                          {{ $car->sale->getAllPaymentsTotal() - $car->sale->getAllPaymentsTotal(true) }} {{ $car->currency->code }}
			                      </div>
			                    </div>
			                  </div>

			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('received_amount', 'Received Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                          {{ Form::text('received_amount', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
			                      </div>
			                    </div>
			                  </div>

			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                          <select name="currency_id" class="form-control">
			                              @foreach( $currencies as $currency )
			                                  <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
			                              @endforeach
			                          </select> 
			                      </div>
			                    </div>
			                  </div>

			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('payment_method', 'Payment Method*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                          <select name="payment_method" class="form-control">
			                              @foreach( $payment_methods as $payment_method )
			                                  <option value="{{ $payment_method->id }}" data-code="{{$payment_method->code  }}"> {{ $payment_method->title }} </option>
			                              @endforeach
			                          </select> 
			                      </div>
			                    </div>
			                  </div>

			                  <div class="form-group document_number_input" style="display:none;">
			                    <div class="row">
			                      {{ Form::label('document_number', 'Cheque Number*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                          {{ Form::text('document_number', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
			                      </div>
			                    </div>
			                  </div>

			              </div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			                <input type="submit" class="btn btn-primary" value="Save">
			              </div>
			              </form>
			            </div>
			          </div>
			        </div>
			    @endcan
		    @endif

		    @can( 'approve-received-cash' )
		    	@if( $car->sale->hasUnapprovedPayment() )
		    		<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".approve_paid_amount">Approve Paid Amount</button>
			        <div class="modal fade approve_paid_amount" tabindex="-1" role="dialog" aria-hidden="true">
			          <div class="modal-dialog modal-md">
			            <div class="modal-content">

			              <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                </button>
			                <h4 class="modal-title" id="myModalLabel2">Add Approve Amount Details</h4>
			              </div>
			              <div class="modal-body">                
			                {!! Form::open(['url' => '/admin/sales/' . $car->sale->id . '/approve-paid', 'method' => 'POST']) !!}			                  
			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('payment_id', 'Payment ID*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                          <select name="payment_id" class="form-control">
			                          	<option value="">Select Paymnet</option>
			                              @foreach( $car->sale->getUnapprovedPayments() as $payment )
			                                  <option value="{{ $payment->id }}"> {{ $payment->paymentMethod->title }} - {{ $payment->amount }} {{ $payment->currency->code }} @if( $payment->document_number ) - Doc # {{ $payment->document_number }} @endif</option>
			                              @endforeach
			                          </select> 
			                      </div>
			                    </div>
			                  </div>

			              </div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			                <input type="submit" class="btn btn-primary" value="Save">
			              </div>
			              </form>
			            </div>
			          </div>
			        </div>
		    	@endif
		    @endcan

		    @can('change-car-customer')
		    	<button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bs-example-modal-sm5">Change Customer</button>
		        <div class="modal fade bs-example-modal-sm5" tabindex="-1" role="dialog" aria-hidden="true">
		          <div class="modal-dialog modal-md">
		            <div class="modal-content">

		              <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                </button>
		                <h4 class="modal-title" id="myModalLabel2">Add Invoice Details</h4>
		              </div>
		              <div class="modal-body">                
		                {!! Form::open(['url' => '/admin/sales/change-customer', 'method' => 'POST']) !!}
		                  <input type="hidden" name="sale_id" value="{{ $car->sale->id }}">		                 

		                  <div class="form-group">
		                    <div class="row">
		                      {{ Form::label('sold_to', 'Sold To*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		                      <div class="col-md-9 col-sm-9 col-xs-12">
		                          <select name="sold_to" class="form-control">
		                              @foreach( $customers as $sold_to )
		                                  <option value="{{ $sold_to->id }}" @if( $sold_to->id == $car->sale->sold_to ) selected @endif> {{ $sold_to->name }} </option>
		                              @endforeach
		                          </select> 
		                      </div>
		                    </div>
		                  </div>

		              </div>
		              <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <input type="submit" class="btn btn-primary" value="Generate Invoice">
		              </div>
		              </form>
		            </div>
		          </div>
		        </div>
		    @endcan
        @endif

		<table class="countries_list">
            <tbody>
				<tr>
					<td>Title</td>
					<td class="fs15 fw700 text-right">{{ $car->title }}</td>
				</tr>
				<tr>
					<td>Code</td>
					<td class="fs15 fw700 text-right">{{ $car->code }}</td>
				</tr>
				<tr>
					<td>Make</td>
					<td class="fs15 fw700 text-right">{{ $car->model->make->name }}</td>
				</tr>
				<tr>
					<td>Model</td>
					<td class="fs15 fw700 text-right">{{ $car->model->name }}</td>
				</tr>
				<tr>
					<td>Year</td>
					<td class="fs15 fw700 text-right">{{ $car->year }}</td>
				</tr>
				<tr>
					<td>Sale Price</td>
					<td class="fs15 fw700 text-right">{{ $car->sale_price }}</td>
				</tr>
				<tr>
					<td>Show On site</td>
					<td class="fs15 fw700 text-right">{{ ( $car->show_on_site == 1 ) ? 'Yes' : 'No' }}</td>
				</tr>
				<tr>
					<td>Sold</td>
					<td class="fs15 fw700 text-right">{{ ( $car->sold == 1 ) ? 'Yes' : 'No' }}</td>
				</tr>
				<tr>
					<td>Source</td>
					<td class="fs15 fw700 text-right">{{ $car->source->name }}</td>
				</tr>
				<tr>
					<td>Destination</td>
					<td class="fs15 fw700 text-right">{{ $car->destination->name }}</td>
				</tr>
				<tr>
					<td>Stock Type</td>
					<td class="fs15 fw700 text-right">{{ $car->stockType->title }}</td>
				</tr>
				@if($car->user)
					<tr>
						<td>User</td>
						<td class="fs15 fw700 text-right">{{ $car->user->name }}</td>
					</tr>
				@endif
				@if( $car->container )
					<tr>
						<td>Container Number</td>
						<td class="fs15 fw700 text-right">{{ $car->Container->number }}</td>
					</tr>
					<tr>
						<td>Container Arrival Date</td>						
						@if (strtotime($car->container->arrival_date) > time())
							<td class="fs15 fw700 text-right">{{ date('d-m-Y', strtotime($car->container->arrival_date)) }}</td>
						@else
							<td class="fs15 fw700 text-right">Arrived</td>
						@endif
					</tr>
				@endif
				<tr>
					<td>Created at</td>
					<td class="fs15 fw700 text-right">{{ $car->created_at }}</td>
				</tr>
				<tr>
					<td>Updated At</td>
					<td class="fs15 fw700 text-right">{{ $car->updated_at }}</td>
				</tr>
				<tr>
					<td>Last Updated By</td>
					<td class="fs15 fw700 text-right">{{ $car->updatedBy->name }}</td>
				</tr>
				<tr>
					<td>Description</td>
					<td class="fs15 fw700 text-right">{{ $car->description }}</td>
				</tr>
				@can('view-net-earnings')
					@if( $car->sold )
						<tr>
							<td><b>Net Earning</b></td>
							<td class="fs15 fw700 text-right"><b>{{ $car->getNetEarning() . ' ' . $default_currency->code }}</b></td>
						</tr>
					@endif
				@endcan
            </tbody>
        </table>
	</div>
	<div class="col-md-6">
		<div class="row image_container">
			<h3>Gallary</h3>
			@if( count( $car->images ) > 0 )
				@foreach( $car->images as $image )				
					<div class="col-md-4">
						<div class="thumbnail" style="height:auto;">
							<div class="image view view-first">								
								<img style="width: 100%; display: block;" src="{{ $image->url }}" alt="image">
								<div class="mask">
									<div class="tools tools-bottom">
	                                    <a data-fancybox="gallery" href="{{ $image->url }}"><i class="fa fa-file-image-o"></i></a>
	                                    @can('delete-car-picture')
                                            <a class="upload_delete" data-id="{{$image->id}}" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                                        @endcan
									</div>
								</div>
							</div>
						</div>			
					</div>
				@endforeach									
			@else
				No Images Found
			@endif			
		</div>
		@if( count( $car->images ) > 0 )
			<div class="row">
				<a href="javascript:void(0);" target="_blank" class="btn btn-default download_pictures_zip">Download ZIP</a>
			</div>	
		@endif
		@can('add-car-picture')
			<h2>Upload more car pictures</h2>
			{!! Form::open(['url' => '/car/' . $car->id . '/upload-pictures/', 'method' => 'PUT', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2']) !!}
					{{ Form::file('image[]', ['multiple' => 'multiple']) }}
					<input type="submit" value="Upload Pictures" class="btn btn-primary">
			{!! Form::close() !!}
		@endcan
		@can( 'view-sale-payments' )
			@if( isset( $car->sale ) )
				<div class="row">
					<h3>Sale Payments</h3>
					<table class="countries_list">
						<thead>
							<th>Payment Method</th>
							<th>Doc#</th>
							<th>Approved</th>
							<th>Amount</th>
						</thead>
						<tbody>
							@foreach( $car->sale->payments as $payment )
								<tr>
									<td> {{ $payment->paymentMethod->title }}</td>
									<td> {{ $payment->document_number }}</td>
									<td> {{ ( $payment->approved ) ? 'Yes' : 'No' }}</td>
									<td> {{ $payment->amount }} {{ $payment->currency->code }} </td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			@endif
		@endcan
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<h3>Properties</h3>
		<table class="countries_list">
            <tbody>
				@foreach( $car->properties as $property )
						<tr>
							<td>{{ $property->prop_name }}</td>
							<td class="fs15 fw700 text-right">{{ $property->value }}</td>
						</tr>
				@endforeach
			</tbody>
		</table>
		<h3>Expenses</h3>
		<table class="countries_list">
            <tbody>
				@foreach( $car->expenses as $expense )
						<tr>
							<td>{{ $expense->title }} @if( $expense->receivable_id ) (Receivable) @elseif( $expense->payable_id ) (Payable) @endif</td>
							<td class="fs15 fw700 text-right">{{ number_format( $expense->value ) }} {{ $expense->currency->code }}</td>
						</tr>
				@endforeach
				<tr>
					<td><b>Total</b></td>
					<td class="fs15 fw700 text-right"><b>{{ number_format( (int)$car->getTotalExpenses() ) }} AED</b></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-6">
		@can('view-car-costs')
			<h3>Costs</h3>
		@endcan
		<table class="countries_list">
            <tbody>
            	@can('view-car-costs')
					@foreach( $car->costs as $cost )					
							<tr>
								<td>{{ $cost->cost_name }}</td>
								<td class="fs15 fw700 text-right">{{ number_format( $cost->value ) }} {{ $cost->currency->code }}</td>
							</tr>					
					@endforeach
					<tr>
						<td><b>Total</b></td>
						<td class="fs15 fw700 text-right"><b>{{ number_format( $car->getTotalCost() ) }} {{ $default_currency->code }}</b></td>
					</tr>
				@endcan
				@can('view-car-total-costs')
					<tr>
						<td><b>Total with Expenses</b></td>
						<td class="fs15 fw700 text-right"><b>{{ number_format( $car->getNetCost() ) }} {{ $default_currency->code }}</b></td>
					</tr>
				@endcan
			</tbody>
		</table>
	</div>
</div>
@can('manage-responsibility')
	<div class="row">
		@if( isset( $car->responsibility ) )
			<h3>Expenses <small> <a href="/admin/user/{{ $car->responsibility->user->id }}/edit/">{{ $car->responsibility->user->name }}</a> is Responsible for this car </small></h3>
			@if( $car->responsibility->hasAllExpensesApproved() && !$car->responsibility->hasCompleted() )
				<a href="/admin/car-responsibility/{{ $car->responsibility->id }}/complete/?back_url={{ $_SERVER['REQUEST_URI'] }}" class="btn btn-success"> Complete Responsibility</a>
			@endif

			@if( $car->sold )			
				@can( 'add-receivable-expense' )
			        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".add_receivable_expense">Add Receivable Expense</button>
		            <div class="modal fade add_receivable_expense" tabindex="-1" role="dialog" aria-hidden="true">
		              <div class="modal-dialog modal-md">
		                <div class="modal-content">

		                  <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
		                    </button>
		                    <h4 class="modal-title" id="myModalLabel2">Add Receivable Expense Details</h4>
		                  </div>
		                  <div class="modal-body">                
		                    {!! Form::open(['url' => '/admin/car-expense/' . $car->id . '/add-receivable-expense', 'method' => 'POST']) !!}                        
		                      <input type="hidden" name="car_id" value="{{ $car->id }}">                    
		                      <div class="form-group">
		                        <div class="row">
		                          {{ Form::label('title', 'Title', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		                          <div class="col-md-6 col-sm-6 col-xs-12">
		                              {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
		                          </div>
		                        </div>
		                      </div>

		                      <div class="form-group">
		                        <div class="row">
		                          {{ Form::label('head_id', 'Account Head', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		                          <input name="head_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
		                          <div class="col-md-9 col-sm-9 col-xs-12">
		                              <select name="head_id" class="form-control">
		                                  <option value="">Select Account Head</option>
		                                  @foreach( $receivable_heads as $head )
		                                      <option value="{{ $head->id }}"> {{ $head->title }} </option>
		                                  @endforeach
		                              </select> 
		                          </div>
		                        </div>
		                      </div>

		                      <div class="form-group">
		                        <div class="row">
		                          {{ Form::label('value', 'Value', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		                          <div class="col-md-6 col-sm-6 col-xs-12">
		                              {{ Form::text('value', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
		                          </div>
		                        </div>
		                      </div>                

		                      <div class="form-group">
		                        <div class="row">
		                          {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		                          <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
		                          <div class="col-md-9 col-sm-9 col-xs-12">
		                              <select name="currency_id" class="form-control select2_currency" disabled="disabled">
		                                  @foreach( $currencies as $currency )
		                                      <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
		                                  @endforeach
		                              </select> 
		                          </div>
		                        </div>
		                      </div>

		                      <div class="form-group">
		                        <div class="row">
		                          {{ Form::label('invoice_image', 'Invoice Image', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		                          <div class="col-md-6 col-sm-6 col-xs-12">
		                              {{ Form::file('invoice_image') }}
		                          </div>
		                        </div>
		                      </div>
		                      
		                      <div class="form-group">
		                        <div class="row">
		                          {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		                          <div class="col-md-9 col-sm-9 col-xs-12">
		                          <textarea id="description" name="description" class="form-control"></textarea>
		                          </div>
		                        </div>
		                      </div>

		                  </div>
		                  <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                    <input type="submit" class="btn btn-primary" value="Save">
		                  </div>
		                  </form>
		                </div>
		              </div>
		            </div>
			    @endcan
			@else
				@can( 'add-payable-expense' )
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".add_payable_expense">Add Payable Expense</button>
			        <div class="modal fade add_payable_expense" tabindex="-1" role="dialog" aria-hidden="true">
			          <div class="modal-dialog modal-md">
			            <div class="modal-content">

			              <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
			                </button>
			                <h4 class="modal-title" id="myModalLabel2">Add Payable Expense Details</h4>
			              </div>
			              <div class="modal-body">                
			                {!! Form::open(['url' => '/admin/car-expense/' . $car->id . '/add-payable-expense', 'method' => 'POST']) !!}			                  
			                  <input type="hidden" name="car_id" value="{{ $car->id }}">	                  
			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('title', 'Title', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                          {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
			                      </div>
			                    </div>
			                  </div>

			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('head_id', 'Account Head', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <input name="head_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                          <select name="head_id" class="form-control">
			                          		<option value="">Select Account Head</option>
			                              @foreach( $payable_heads as $head )
			                                  <option value="{{ $head->id }}"> {{ $head->title }} </option>
			                              @endforeach
			                          </select> 
			                      </div>
			                    </div>
			                  </div>

			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('value', 'Value', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                          {{ Form::text('value', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
			                      </div>
			                    </div>
			                  </div>                

			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                          <select name="currency_id" class="form-control select2_currency" disabled="disabled">
			                              @foreach( $currencies as $currency )
			                                  <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
			                              @endforeach
			                          </select> 
			                      </div>
			                    </div>
			                  </div>

			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('invoice_image', 'Invoice Image', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-6 col-sm-6 col-xs-12">
			                          {{ Form::file('invoice_image') }}
			                      </div>
			                    </div>
			                  </div>
			                  
			                  <div class="form-group">
			                    <div class="row">
			                      {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
			                      <div class="col-md-9 col-sm-9 col-xs-12">
			                      <textarea id="description" name="description" class="form-control"></textarea>
			                      </div>
			                    </div>
			                  </div>

			              </div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			                <input type="submit" class="btn btn-primary" value="Save">
			              </div>
			              </form>
			            </div>
			          </div>
			        </div>
				@endcan
			@endif
	
			@if( count( $car->expenses ) > 0 )
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Expense Title</th>
							<th>Amount</th>
							<th>Date</th>
							<th>Approved</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach( $car->expenses as $key => $expense )
							<tr>
								<th scope="row">{{ $key + 1 }}</th>
								<td>{{ $expense->title }}</td>
								<td>{{ $expense->value }}</td>
								<td>{{ $expense->created_at }}</td>
								<td>{{ ( $expense->approved == 1 ) ? 'Yes' : 'No' }}</td>
								<td>@if( $expense->receivable_id ) Receivable @elseif( $expense->payable_id ) Payable @else - @endif</td>
								<td>
									<button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm{{$expense->id}}">View</button>
								    <div class="modal fade bs-example-modal-sm{{$expense->id}}" tabindex="-1" role="dialog" aria-hidden="true">
								        <div class="modal-dialog modal-md">
								          <div class="modal-content">						            
								              <div class="modal-header">
								                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
								                </button>
								                <h4 class="modal-title" id="myModalLabel2">Expense Detail</h4>
								              </div>
								              <div class="modal-body">
								              	<div class="row">
								              		<div class="col-md-4">Title</div>
								              		<div class="col-md-8">{{ $expense->title }}</div>
								              	</div>						              	
								              	<div class="row">
								              		<div class="col-md-4">Value</div>
								              		<div class="col-md-8">{{ $expense->value }} {{ $expense->currency->code }}</div>
								              	</div>
								              	<div class="row">
								              		<div class="col-md-4">Created at</div>
								              		<div class="col-md-8">{{ $expense->created_at }}</div>
								              	</div>
								              	<div class="row">
								              		<div class="col-md-4">Updated at</div>
								              		<div class="col-md-8">{{ $expense->updated_at }}</div>
								              	</div>
								              	<div class="row">
								              		<div class="col-md-4">Description</div>
								              		<div class="col-md-8">{{ $expense->description }}</div>
								              	</div>
								              	<div class="row">
								              		<div class="col-md-4">Images</div>
								              		<div class="col-md-8">
								              			@if( count( $expense->invoiceImage ) > 0 )
										                    @foreach( $expense->invoiceImage as $image )
										                        <a href="{{ $image->url }}" data-fancybox><img class="img-thumbnail" src="{{ $image->url }}" width="300"></a>
										                    @endforeach
										                @endif
								              		</div>
								              	</div>
								              </div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								              </div>
								          </div>
								        </div>
								    </div>
								    @can('manage-responsibility')
									    @if( $expense->approved == 0 )
											<a href="/admin/car-expense/{{ $expense->id }}/approve/?back_url={{ $_SERVER['REQUEST_URI'] }}" class="btn btn-success">Approve</a>
											<a href="/admin/car-expense/{{ $expense->id }}/delete/?back_url={{ $_SERVER['REQUEST_URI'] }}" class="btn btn-danger">Delete</a>
										@endif
									@endcan
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<p class="red">* Approved Expenses cannot be reverted back</p>		
			@else
				No Expenses Added
			@endif			
		@else		
			<a href="/admin/car-responsibility/create/?car_id={{$car->id}}" target="_blank" class="btn btn-primary"> Add Responsibility </a>
		@endif
	</div>
@endcan

<script type="text/javascript">

	$(document).ready(function(){
		$(".select2_customer").chosen();

		$(document).on( 'change', 'select[name=payment_method]', function(){
            if( $(this).find(':selected').data('code') == 'cheque' ){
                $('.document_number_input').show();
            }
            else{
                $('.document_number_input').hide();
            }
        });
	});
</script>
@stop