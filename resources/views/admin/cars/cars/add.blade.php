@extends('layouts/admin')
@section('content')
<h3> Add New Car </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row">    
    {!! Form::model($car, ['route' => ['cars.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
        <input name="destination_id" type="hidden" value="4">
        <input name="sale_type_id" type="hidden" value="4">
            
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>General </p>
            </div>
        </div>

        <!-- General Start -->
        <div class="form-group">
            {{ Form::label('title', 'Title*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('model', 'Model*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="model_id" class="form-control">
                    <option value="">Select model</option>
                    @foreach( $car_models as $model )
                        <option value="{{ $model->id }}"> {{ $model->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('year', 'Year*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('year', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('sale_price', 'Sale Price', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('sale_price', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

     <!--    <div class="form-group">
            {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}

            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="currency_id" class="form-control" disabled="disabled">
                    <option disabled="disabled">Select Currency</option>
                    @foreach( $currencies as $currency )
                        <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div> -->

        <div class="form-group">
            {{ Form::label('source', 'Source*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="source_id" class="form-control">
                    <option disabled="disabled">Select source</option>
                    @foreach( $car_regions as $source )
                        <option value="{{ $source->id }}"> {{ $source->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

    <!--     <div class="form-group">
            {{ Form::label('destination', 'Destination*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="destination_id" class="form-control">
                    <option value="">Select Destination</option>
                    @foreach( $car_regions as $destination )
                        <option value="{{ $destination->id }}" @if( $destination->code == 'UAE' ) selected @endif> {{ $destination->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div> -->

  <!--       <div class="form-group">
            {{ Form::label('sale_type', 'Sale type', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="sale_type_id" class="form-control">
                    <option disabled="disabled">Select Sale Type</option>
                    @foreach( $sale_types as $sale_type )
                        <option value="{{ $sale_type->id }}" @if( $sale_type->code == 'in-country-sale' ) selected @endif> {{ $sale_type->title }} </option>
                    @endforeach
                </select> 
            </div>
        </div> -->

        <div class="form-group">
            {{ Form::label('stock_type', 'Stock type', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="stock_type_id" class="form-control">
                    <option value="">Select Stock Type</option>
                    @foreach( $stock_types as $stock_type )
                        <option data-code="{{ $stock_type->code }}" value="{{ $stock_type->id }}"> {{ $stock_type->title }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
              {{ Form::label('user_id', 'Assign to User', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="user_id" class="select2_customer form-control">
                    <option value="">Select User</option>
                      @foreach( $customers as $user_id )
                          <option value="{{ $user_id->id }}"> {{ $user_id->name }} - {{ $user_id->phone }} - {{ $user_id->login }}</option>
                      @endforeach
                  </select>
              </div>        
        </div>

        <div class="form-group" id="agents_list" style="display:none;">
            {{ Form::label('agent', 'Agent', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="agent_id" class="form-control">
                    <option value="0">Select Agent</option>
                    @foreach( $agents as $agent )
                        <option value="{{ $agent->id }}" @if( $car->agent_id == $agent->id ) selected @endif> {{ $agent->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('show_on_site', 'Show On Site', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('show_on_site', null, null, [ 'class' => 'js-switch', 'id' => 'show_on_site' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('sold', 'Sold', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('sold', null, null, [ 'class' => 'js-switch', 'id' => 'sold' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('featured', 'Featured', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('featured', null, null, [ 'class' => 'js-switch', 'id' => 'featured' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('trending', 'Trending', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('trending', null, null, [ 'class' => 'js-switch', 'id' => 'trending' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('latest', 'Latest', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('latest', null, null, [ 'class' => 'js-switch', 'id' => 'latest' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea id="description" name="description" class="form-control"></textarea>
            </div>
        </div>
        
        <!-- General end -->
        <div class="ln_solid"></div>

        <!-- Cost Start -->
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Costs </p>
            </div>
        </div>

        @foreach( $car_costs as $cost )
            <div class="form-group">
                {{ Form::label( $cost->code, $cost->name . ( ( $cost->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}{{ $cost->currency->code }}
                <div class="col-md-1 col-sm-1 col-xs-2">
                    {{ Form::text( $cost->code, $cost->default_value, [ 'class' => 'car-costs-input form-control col-md-7 col-xs-12', 'data-conversion-rate' => $cost->currency->conversion ]) }}
                </div>
            </div>
        @endforeach

        <!-- Cost End -->
        <div class="ln_solid"></div>


        <!-- CostProperties Start -->
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Specifications</p>
            </div>
        </div>

        @foreach( $car_properties as $property )
            <div class="form-group">
                {{ Form::label( $property->code, $property->name . ( ( $property->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @if( $property->type->code == 'text' )

                        {{ Form::text( $property->code, null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                    
                    @elseif( $property->type->code == 'checkbox' )
                    
                        {{ Form::checkbox($property->code, null, null, [ 'class' => 'js-switch', 'id' => $property->code ]) }}
                    
                    @endif
                </div>
            </div>
        @endforeach

        <!-- Property End -->
        <div class="ln_solid"></div>


        <!-- Properties Start -->
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Pictures</p>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Car Images</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                @if( count( $car->images ) > 0 )
                    @foreach( $car->images as $image )
                        <img class="img-thumbnail" src="{{ $image->url }}" width="300">
                    @endforeach
                @endif
                {{ Form::file('image[]', ['multiple' => 'multiple']) }}
            </div>
        </div> 
        <!-- Property End -->
        
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var total = 0;

        $(document).on( 'change', 'select[name=stock_type_id]', function(){
            if( $('select[name=stock_type_id]').find(':selected').data('code') == 'agent-stock' ){
                $('#agents_list').show();
            }
            else{
                $('#agents_list').hide();
            }
        });

        $(document).on( 'input', 'input[name=invoice]', function(){
            var customs = $('input[name=customs]');
            if( typeof customs != undefined ){
                if( $(this).val() > 0 ){
                    var invoice = +$(this).val();
                    customs.val( ( ( invoice + 900 ) * 0.05 ).toFixed(2) );                    
                }
                else{
                    customs.val(0);
                }
            }
        });

        $(document).on( "input", '.car-costs-input', function(){
            if( $(this).attr('name') == 'additional-5-percent' ){
                return false;
            }
            
            total = 0;
            $('.car-costs-input').each(function(){
                if( $(this).attr('name') != 'additional-5-percent' ){
                    total += ( +$(this).val() * +$(this).data('conversion-rate') );
                }
            });
            
            $('input[name=additional-5-percent]').val( ( total * 0.05 ).toFixed(2) );
        });

        $(".select2_customer").chosen();

        $(document).on( 'change', 'select[name=payment_method]', function(){
            if( $(this).find(':selected').data('code') == 'cheque' ){
                $('.document_number_input').show();
            }
            else{
                $('.document_number_input').hide();
            }
        });
    });
</script>
@stop