@extends('layouts/admin')
@section('content')
<h3> Car List </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
@if( Session::has('errors') )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('errors') }}
    </div>
@endif

<form action="/admin/cars/">
    <div class="row">   
        <div class="col-md-3">
            <select name="model" class="form-control search_field model_chosen" placeholder="Model">
                <option value="">Select Car Model</option>
                @foreach( $models as $model )
                    <option  value="{{ $model->id }}" @if( ( isset( $_REQUEST['model'] ) ) &&  $_REQUEST['model'] == $model->id ) selected @endif>{{ $model->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3">
            <input name="vin_number" class="form-control search_field" placeholder="VIN #" value="{{ ( isset( $_REQUEST['vin_number'] ) ) ? $_REQUEST['vin_number'] : '' }}">
        </div>
        <div class="col-md-3">
            <input name="lot_number" class="form-control search_field" placeholder="LOT #" value="{{ ( isset( $_REQUEST['lot_number'] ) ) ? $_REQUEST['lot_number'] : '' }}">
        </div>
        <div class="col-md-3">
            <input name="year" class="form-control search_field" placeholder="Year" value="{{ ( isset( $_REQUEST['year'] ) ) ? $_REQUEST['year'] : '' }}">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <select name="sold" class="form-control">
                <option value="">Select Sold</option>
                <option value="1" @if( isset( $_REQUEST['sold'] ) && $_REQUEST['sold'] == '1' ) selected @endif>Yes</option>
                <option value="0" @if( isset( $_REQUEST['sold'] ) && $_REQUEST['sold'] === '0' ) selected @endif>No</option>
            </select>
        </div>        
    </div>
    <div class="row">
        <div class="col-sm-3">
            <input type="submit" value="Search" class="btn btn-default search_submit">
        </div>
    </div>
</form>
<div class="row">
    <div class="col-sm-9"></div>
    @can('add-car')
    	<div class="col-sm-3">
    		<div class="section_add text-right">
    			<a href="/admin/cars/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New Car</a>
    		</div>
    	</div>
    @endcan
</div>		
<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th class="column-title">Make </th>
        <th class="column-title">Model </th>
        <th class="column-title">Vin # </th>
        <th class="column-title">Lot # </th>
        <th class="column-title">Year </th>
        <th class="column-title">Show On site </th>
        <th class="column-title">Sale Price </th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @if( count( $cars ) == 0 )
            <tr class="even pointer">
                <td colspan="10">No Cars Found</td>                
            </tr>
        @else
            @foreach( $cars as $car )
            	<tr class="even pointer">
                    <td>{{ $car->model->make->name }}</td>
                    <td>{{ $car->model->name }}</td>
                    <td>{{ $car->propByCode('vin_number')->value }}</td>
                    <td>{{ $car->propByCode('lot_number')->value }}</td>
                    <td>{{ $car->year }}</td>
                    <td>{{ ( $car->show_on_site == 1 ) ? 'Yes' : 'No' }}</td>
                    <td>{{ ( $car->sale_price ) ? $car->sale_price : '0.00' }} {{ $car->currency->code }}</td>
                    <td>
                        <a class="btn btn-default source" href="/admin/cars/{{ $car->id }}/">View</a>
                        @can('edit-car')
                    	   <a class="btn btn-default source" href="/admin/cars/{{ $car->id }}/edit/">Edit</a>
                        @endcan
                        @can('delete-car')
                            {{ Form::open( [ 'url' => '/admin/cars/' . $car->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}                                
                                <a href="javascript:void(0);" class="btn car-delete-btn btn-danger source">Delete</a>
                            {{ Form::close() }}
                        @endcan
                    </td>
                </tr>    
            @endforeach
        @endif   
    </tbody>
  </table>    
</div>
<div class="row">
        <div class="col-md-10">
            {{ $cars->appends( $_REQUEST )->links() }}            
        </div>
        <div class="col-md-2 text-right">            
            {{ $cars->total() }} Cars Found
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.model_chosen').chosen();

        
        $('.car-delete-btn').click(function(e){
            e.preventDefault();
            if(confirm('You really want to delete this ?')){
                $(this).closest('form').submit();     
            }
        });
    });
</script>
@stop