@extends('layouts/admin')
@section('content')
<h3> Edit Car </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@elseif( Session::has('error') )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('error') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($car, ['route' => ['cars.update', $car->id], 'method' => 'PUT', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
        <input name="destination_id" type="hidden" value="4">
        <input name="sale_type_id" type="hidden" value="4">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>General </p>
            </div>
        </div>

        <!-- General Start -->
        <div class="form-group">
            {{ Form::label('title', 'Title*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('model', 'Model*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="model_id" class="form-control">
                    <option disabled="disabled">Select model</option>
                    @foreach( $car_models as $model )
                        <option value="{{ $model->id }}" @if( $model->id == $car->model_id ) selected @endif> {{ $model->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('year', 'Year*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('year', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('sale_price', 'Sale Price', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('sale_price', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

     <!--    <div class="form-group">
            {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="currency_id" class="form-control" disabled="disabled">
                    <option disabled="disabled">Select Currency</option>
                    @foreach( $currencies as $currency )
                        <option value="{{ $currency->id }}" @if( $car->currency_id == $currency->id ) selected @endif> {{ $currency->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
 -->
        <div class="form-group">
            {{ Form::label('source', 'Source*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="source_id" class="form-control">
                    <option disabled="disabled">Select source</option>
                    @foreach( $car_regions as $source )
                        <option value="{{ $source->id }}" @if( $car->source_id == $source->id ) selected @endif> {{ $source->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

<!--         <div class="form-group">
            {{ Form::label('destination', 'Destination*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="destination_id" class="form-control">
                    <option disabled="disabled">Select Destination</option>
                    @foreach( $car_regions as $destination )
                        <option value="{{ $destination->id }}" @if( $car->destination_id == $destination->id ) selected @endif> {{ $destination->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div> -->

<!--         <div class="form-group">
            {{ Form::label('sale_type', 'Sale type', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="sale_type_id" class="form-control">
                    <option disabled="disabled">Select Sale Type</option>
                    @foreach( $sale_types as $sale_type )
                        <option value="{{ $sale_type->id }}" @if( $car->sale_type_id == $sale_type->id ) selected @endif> {{ $sale_type->title }} </option>
                    @endforeach
                </select> 
            </div>
        </div> -->

        <div class="form-group">
            {{ Form::label('stock_type', 'Stock type', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="stock_type_id" class="form-control">
                    <option disabled="disabled">Select Sale Type</option>
                    @foreach( $stock_types as $stock_type )
                        <option data-code="{{ $stock_type->code }}" value="{{ $stock_type->id }}" @if( $car->stock_type_id == $stock_type->id ) selected @endif> {{ $stock_type->title }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
              {{ Form::label('user_id', 'Assign to User', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="user_id" class="select2_customer form-control">
                    <option value="">Select User</option>
                      @foreach( $customers as $user_id )
                          <option value="{{ $user_id->id }}" @if( $car->user_id == $user_id->id ) selected @endif> {{ $user_id->name }} - {{ $user_id->phone }} - {{ $user_id->login }}</option>
                      @endforeach
                  </select>
              </div>        
        </div>

        <div class="form-group" id="agents_list" @if( $car->stockType->code != 'agent-stock' ) style="display:none;" @endif>
            {{ Form::label('agent', 'Agent', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="agent_id" class="form-control">
                    <option value="0">Select Agent</option>
                    @foreach( $agents as $agent )
                        <option value="{{ $agent->id }}" @if( $car->agent_id == $agent->id ) selected @endif> {{ $agent->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('show_on_site', 'Show On Site', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('show_on_site', null, null, [ 'class' => 'js-switch', 'id' => 'show_on_site' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('sold', 'Sold', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('sold', null, null, [ 'class' => 'js-switch', 'id' => 'sold' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('featured', 'Featured', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('featured', null, null, [ 'class' => 'js-switch', 'id' => 'featured' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('trending', 'Trending', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('trending', null, null, [ 'class' => 'js-switch', 'id' => 'trending' ]) }}
            </div>
        </div>   

        <div class="form-group">
            {{ Form::label('latest', 'Latest', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('latest', null, null, [ 'class' => 'js-switch', 'id' => 'latest' ]) }}
            </div>
        </div>   

        <div class="form-group">
            {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea id="description" name="description" class="form-control">{{ $car->description }}</textarea>
            </div>
        </div>     
        
        <!-- General end -->
        <div class="ln_solid"></div>

        <!-- Cost Start -->
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Costs </p>
            </div>
        </div>

        @foreach( $car_costs as $cost )
            <div class="form-group">
                {{ Form::label( $cost->code, $cost->name . ( ( $cost->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}{{ $cost->currency->code }}
                <div class="col-md-1 col-sm-1 col-xs-2">                    
                    {{ Form::text( $cost->code, $cost->value, [ 'class' => 'car-costs-input form-control col-md-7 col-xs-12', 'data-conversion-rate' => $cost->currency->conversion ]) }}
                </div>
            </div>
        @endforeach

        <!-- Cost End -->
        <div class="ln_solid"></div>


        <!-- CostProperties Start -->
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Specifications</p>
            </div>
        </div>

        @foreach( $car_properties as $property )
            <div class="form-group">
                {{ Form::label( $property->code, $property->name . ( ( $property->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @if( $property->type->code == 'text' )
                        @if( !empty( $car->propByCode($property->code) ) )
                            {{ Form::text( $property->code, $car->propByCode($property->code)->value, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                        @endif
                    @elseif( $property->type->code == 'checkbox' )
                        @if( !empty( $car->propByCode($property->code) ) )
                            {{ Form::checkbox($property->code, ( $car->propByCode($property->code)->value == 1 ) ? true : false, null, [ 'class' => 'js-switch', 'id' => $property->code ]) }}
                        @endif
                    @endif
                </div>
            </div>
        @endforeach

        <!-- Property End -->
        <div class="ln_solid"></div>


        <!-- Properties Start -->
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Pictures</p>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Car Images</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                @if( count( $car->images ) > 0 )
                    <div class="row image_container">                                          
                        @foreach( $car->images as $image )
                            <div class="col-md-4 image_item" data-image-id="{{ $image->id }}">
                                <div class="thumbnail" style="height:auto;">
                                  <div class="image view view-first">
                                    <img style="width: 100%; display: block;" src="{{ $image->url }}" alt="image">
                                    <div class="mask">
                                      <div class="tools tools-bottom">
                                        <a href="#"><i class="fa fa-file-image-o"></i></a>
                                        @can('delete-car-picture')
                                            <a class="upload_delete" data-id="{{$image->id}}" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                                        @endcan
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
                {{ Form::file('image[]', ['multiple' => 'multiple']) }}
            </div>
        </div> 
        <!-- Property End -->
        
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var total = 0;
        $('.image_container').sortable({
            stop: function( event, ui ) {
                var image_map = {};
                $('.image_item').each(function(key, value){
                    image_map[$(this).data('image-id')] = key + 1;                    
                });

                $.ajax({
                    url : '/admin/cars/update-sort',
                    type : 'post',
                    data : { 'image_sort' : image_map },
                    success: function(data){
                        if( data == 'failure' ){
                            alert('unable to update sort');
                        } 
                    }
                });
            }
        });

        $(document).on( 'change', 'select[name=stock_type_id]', function(){
            if( $('select[name=stock_type_id]').find(':selected').data('code') == 'agent-stock' ){
                $('#agents_list').show();
            }
            else{
                $('#agents_list').hide();
            }
        });        

        $(document).on( 'input', 'input[name=invoice]', function(){
            var customs = $('input[name=customs]');
            if( typeof customs != undefined ){
                if( $(this).val() > 0 ){
                    var invoice = +$(this).val();
                    customs.val( ( ( invoice + 900 ) * 0.05 ).toFixed(2) );                    
                }
                else{
                    customs.val(0);
                }
            }
        });

        $(document).on( "input", '.car-costs-input', function(){
            if( $(this).attr('name') == 'additional-5-percent' ){
                return false;
            }
            
            total = 0;
            $('.car-costs-input').each(function(){
                if( $(this).attr('name') != 'additional-5-percent' ){
                    total += ( +$(this).val() * +$(this).data('conversion-rate') );
                }
            });
            
            $('input[name=additional-5-percent]').val( ( total * 0.05 ).toFixed(2) );
        });


        $(".select2_customer").chosen();

        $(document).on( 'change', 'select[name=payment_method]', function(){
            if( $(this).find(':selected').data('code') == 'cheque' ){
                $('.document_number_input').show();
            }
            else{
                $('.document_number_input').hide();
            }
        });
    });
</script>
@stop