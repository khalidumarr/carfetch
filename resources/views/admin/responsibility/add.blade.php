@extends('layouts/admin')
@section('content')
<h3> Add New Car Responsibility </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('error') )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('error') }}
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($responsibility, ['route' => ['car-responsibility.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <div class="form-group">
            {{ Form::label('user', 'Sales Man*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="user_id" class="form-control">
                    <option disabled="disabled">Select user</option>
                    @foreach( $users as $user )
                        <option value="{{ $user->id }}"> {{ $user->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('amount_released', 'Amount Released*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('amount_released', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="currency_id" class="form-control" disabled="disabled">
                    <option disabled="disabled">Select currency</option>
                    @foreach( $currencies as $currency )
                        <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>
    
        <h3>Select Car</h3>
        <div class="x_content">
        <table id="datatable-checkbox" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th><input type="checkbox" class="flat"></th>
              <th>Title</th>
              <th>Model</th>
              <th>year</th>
              <th>Source</th>
              <th>Destination</th>
              <th>Stock Type</th>
              <th>Created at</th>
            </tr>
          </thead>


          <tbody>
            @foreach( $cars as $car )
              <tr>
                <td><input type="checkbox" class="flat car_checkbox" name="car_id[{{ $car->id }}]" value="{{ $car->id }}"></td>
                <td>{{ $car->title }}</td>
                <td>{{ $car->model->name }}</td>
                <td>{{ $car->year }}</td>
                <td>{{ $car->source->name }}</td>
                <td>{{ $car->destination->name }}</td>
                <td>{{ $car->stockType->title }}</td>
                <td>{{ $car->created_at }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
        
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>

<script type="text/javascript">
     $(document).ready(function(){ 
         var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

         TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });          
        });

        $datatable.on('')

        TableManageButtons.init();
    });
</script>
@stop