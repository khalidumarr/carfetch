@extends('layouts/admin')
@section('content')
<div class="">
  <div class="page-title">
  </div>
  <div class="row">
    @if( Session::has('message') )
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('message') }}
        </div>
    @endif
    @if( Session::has('errors') )
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('errors') }}
        </div>
    @endif
  </div>

  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Inventory</h2>        
        <div class="clearfix"></div>
      </div>
      <div class="x_content"  style="overflow: scroll">
      <input type="button" class="btn btn-primary" onclick="tableToExcel('inventoryTable', 'W3C Example Table')" value="Export to Excel">
        <table class="table table-striped table-bordered" id="inventoryTable">
          <thead>
            <tr>
              <th>Title</th>
              <th>Year</th>
              <th>Make</th>     
              <th>Model</th>
              <th>Vin Number</th>
              <th>Lot Number</th>     
              <th>Source</th>
              <th>Destination</th>
              <th>Sale Type</th>
              <th>Stock Type</th>
              <th>Agent</th>
              <th>Show On Site</th>
              <th>Container</th>
              <th>Arrival Date</th>
              <th>Sale Price</th>
              <th>Total Cost</th>
              <th>Total Expenses</th>
              <th>Net Total</th>
              <th>Created At</th>
              <th>Updated At</th>
              <th>Last Updated By</th>
            </tr>
          </thead>


          <tbody>
            @foreach( $cars  as $car )
              <tr>
                <td>{{ $car->title }}</td>             
                <td>{{ $car->year }}</td>             
                <td>{{ $car->model->make->name }}</td>             
                <td>{{ $car->model->name }}</td>             
                <td>{{ $car->propByCode('vin_number')->value }}</td>             
                <td>{{ $car->propByCode('lot_number')->value }}</td>             
                <td>{{ $car->source->name }}</td>
                <td>{{ $car->destination->name }}</td>
                <td>{{ $car->saleType->title }}</td>
                <td>{{ $car->stockType->title }}</td>
                <td>{{ ( $car->agent ) ? $car->agent->name : '' }}</td>
                <td>{{ ( $car->show_on_site == 1 ) ? 'Yes' : 'No' }}</td>
                <td>{{ ( $car->container ) ? $car->container->number : '' }}</td>
                <td>{{ ( $car->container ) ? date( 'd-M-Y', strtotime( $car->container->arrival_date ) ) : '' }}</td>
                <td>{{ number_format( (int)$car->sale_price ) }}</td>
                <td>{{ number_format( (int)$car->getTotalCost() ) }}</td>
                <td>{{ number_format( (int)$car->getTotalExpenses() ) }}</td>
                <td>{{ number_format( (int)$car->getNetCost() ) }}</td>
                <td>{{ $car->created_at }}</td>
                <td>{{ $car->updated_at }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
</script>
@stop