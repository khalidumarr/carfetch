@extends('layouts/admin')
@section('content')
<h3> Roles List </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    	</button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row">
	<div class="col-sm-9">
		<form action="/admin/role/">
		</form>
	</div>
	<div class="col-sm-3">
		<div class="section_add text-right">
			<a href="/admin/role/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New Role</a>
		</div>
	</div>
</div>		
<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th>
          <input type="checkbox" id="check-all" class="flat">
        </th>
        <th class="column-title">Name </th>
        <th class="column-title">Code </th>
        <th class="column-title">Priority </th>
        <th class="column-title">Updated at </th>
        <th class="column-title">Created at </th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @if( count( $roles ) == 0 )
            <tr class="even pointer">
                <td colspan="5">No Roles Found</td>                
            </tr>
        @else
            @foreach( $roles as $role )
            	<tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->code }}</td>
                    <td> {{ $role->priority }} </td>
                    <td>{{ $role->created_at }}</td>
                    <td>{{ $role->updated_at }}</td>
                    <td>
                    	<a class="btn btn-default source" href="/admin/role/{{ $role->id }}/edit/">Edit</a>
                        {{ Form::open( [ 'url' => '/admin/role/' . $role->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                            {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                        {{ Form::close() }}
                    </td>
                </tr>    
            @endforeach
        @endif   
    </tbody>
  </table>
   {{ $roles->links() }}
</div>
@stop