@extends('layouts/admin')
@section('content')
<h3> User List </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    	</button>
        {{ Session::get('message') }}
    </div>
@endif
@if( Session::has('errors') )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('errors') }}
    </div>
@endif
<form action="/admin/user/">
    <div class="row">   
        <div class="col-md-3">
            <input name="name" class="form-control search_field" placeholder="Name" value="{{ ( isset( $_REQUEST['name'] ) ) ? $_REQUEST['name'] : '' }}">
        </div>
        @can( 'change-user-roles' )
            <div class="col-md-3">
                <select name="role" class="form-control search_field model_chosen" placeholder="Model">
                    <option value="">Select User Role</option>
                    @foreach( $roles as $role )
                        <option  value="{{ $role->id }}" @if( ( isset( $_REQUEST['role'] ) ) &&  $_REQUEST['role'] == $role->id ) selected @endif>{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
        @endcan
        <div class="col-md-3">
            <input name="email" class="form-control search_field" placeholder="Email" value="{{ ( isset( $_REQUEST['email'] ) ) ? $_REQUEST['email'] : '' }}">
        </div>
        <div class="col-md-3">
            <input name="phone" class="form-control search_field" placeholder="Phone Number" value="{{ ( isset( $_REQUEST['phone'] ) ) ? $_REQUEST['phone'] : '' }}">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <input type="submit" value="Search" class="btn btn-default search_submit">
        </div>
    </div>
</form>
<div class="row">
	<div class="col-sm-9">
		<form action="/admin/users/">
		</form>
	</div>
	<div class="col-sm-3">
		<div class="section_add text-right">
			<a href="/admin/user/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New User</a>
		</div>
	</div>
</div>		
<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th>
          <input type="checkbox" id="check-all" class="flat">
        </th>
        <th class="column-title">Full Name </th>
        <th class="column-title">Roles </th>
        <th class="column-title">Email </th>
        <th class="column-title">Active </th>
        <th class="column-title">Updated at </th>
        <th class="column-title">Created at </th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @foreach( $users as $user )
        	<tr class="even pointer">
                <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                <td>{{ $user->name }}</td>
                <td>
                    @foreach( $user->roles as $key => $role )
                        {{ $role->name }}
                        @if( count( $user->roles ) != ( $key + 1 ) )
                            |
                        @endif
                    @endforeach
                </td>
                <td> {{ $user->email }} </td>
                <td>{{ ( $user->active ) ? 'Yes' : 'No' }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->updated_at }}</td>
                <td>
                    @can('edit-user', $user)
                	   <a class="btn btn-default source" href="/admin/user/{{ $user->id }}/edit/">Edit</a>
                    @endcan

                    @can('delete-user')
                        {{ Form::open( [ 'url' => '/admin/user/' . $user->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                            {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                        {{ Form::close() }}
                    @endcan
                </td>
            </tr>    
        @endforeach   
    </tbody>
  </table>
   {{ $users->links() }}
</div>
@stop