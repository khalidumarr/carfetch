@extends('layouts/admin')
@section('content')
<h3> Edit User </h3>
@if( count( $errors ) > 0 )
    @if( Session::has('errors') )
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('errors') }}
        </div>
    @endif
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($user, ['route' => ['user.update', $user], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2', 'autocomplete' => "false" ]) !!}
        <div class="form-group">
            {{ Form::label('name', 'Full Name*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('name', null, [ 'id' => 'name', 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Email Address', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('email', null, [ 'id' => 'email', 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('phone', 'Phone Number (+971 xx xxxxxxx)', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('phone', null, [ 'id' => 'phone', 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('login', 'Login', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('login', null, [ 'id' => 'login', 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('address', 'Address', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-1">
                {{ Form::textarea('address', null, [ 'id' => 'address', 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('active', 'Active*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('active', null, null, [ 'class' => 'js-switch', 'id' => 'active' ]) }}
            </div>
        </div>        
    
        <div class="ln_solid"></div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">User Roles</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div id="user_type" class="btn-group" data-toggle="buttons">
                    @foreach( $roles as $role )
                        @can('change-user-roles')
                            <label class="btn btn-primary {{ in_array( $role->id, $user_roles ) ? 'active' : '' }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                <input type="checkbox" name="role[]" value="{{ $role->id }}" {{ in_array( $role->id, $user_roles ) ? 'checked="checked"' : '' }}> {{ ucwords( $role->name ) }}
                            </label>                        
                        @else
                            @if( $role->code == 'customer' )
                                <label class="btn btn-primary {{ in_array( $role->id, $user_roles ) ? 'active' : '' }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                    <input type="checkbox" name="role[]" value="{{ $role->id }}" {{ in_array( $role->id, $user_roles ) ? 'checked="checked"' : '' }}> {{ ucwords( $role->name ) }}
                                </label>
                            @endif    
                        @endcan
                    @endforeach
                </div>
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Profile Picture</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                @if( count( $user->images ) > 0 )
                    @foreach( $user->images as $image )
                        <a href="{{ $image->url }}" data-fancybox><img class="img-thumbnail" src="{{ $image->url }}" width="300"></a>
                    @endforeach
                @endif
                {{ Form::file('image') }}
            </div>
        </div>  
        <div class="ln_solid"></div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Emirates ID</label>
            <div class="col-md-6 col-sm-6 col-xs-12">            
                @if( $user->identityPicture )
                    <a href="{{ $user->identityPicture->url }}" data-fancybox><img class="img-thumbnail" src="{{ $user->identityPicture->url }}" width="300"></a>
                @endif
                {{ Form::file('identity_picture') }}
            </div>
        </div>
        <div class="ln_solid"></div>    
        <input class="myfakepassword" type="password" name="fakepasswordremembered" style="    width: 1px;
    height: 1px;
    border: none;"/>
        <div class="form-group">
            {{ Form::label('password', 'Password*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::password('password', null, [ 'class' => 'form-control col-md-7 col-xs-12' ,'autocomplete' =>'false' ]) }}
            </div>
        </div>       
        
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Update', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>
@stop