@extends('layouts/admin')
@section('content')
<h3> Add New User </h3>
@if( count( $errors ) > 0 )
    @if( Session::has('errors') )
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('errors') }}
        </div>
    @endif
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($user, ['route' => ['user.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <div class="form-group">
            {{ Form::label('name', 'Full Name*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('name', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email Address', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('email', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-12">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <a href="javascript:void(0);" class="email_generator btn btn-default"> Generate Default Email (*@gmail.com) </a>
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('phone', 'Phone Number (+971 xx xxxxxxx)', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('phone', null, [ 'id' => 'phone', 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('login', 'Login', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('login', null, [ 'id' => 'login', 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
        
        <div class="form-group">
            {{ Form::label('address', 'Address', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-1">
                <textarea id="address" name="address" class="form-control"></textarea>
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('active', 'Active*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::checkbox('active', null, null, [ 'class' => 'js-switch', 'id' => 'active' ]) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-12">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <a href="javascript:void(0);" class="pwd_generator btn btn-default"> Generate Default Password (123456) </a>
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password', 'Password*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::password('password', [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password_confirmation', 'Confirm Password*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::password('password_confirmation', [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>
    
        <div class="ln_solid"></div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">User Roles</label>            
            <div class="col-md-6 col-sm-6 col-xs-12">                
                <div id="role" class="btn-group" data-toggle="buttons">
                    @foreach( $roles as $role )
                        @can('change-user-roles')
                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                <input type="checkbox" name="role[]" value="{{ $role->id }}"> {{ ucwords( $role->name ) }}
                            </label>                        
                        @else
                            @if( $role->code == 'customer' )
                                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                    <input type="checkbox" name="role[]" value="{{ $role->id }}"> {{ ucwords( $role->name ) }}
                                </label>
                            @endif    
                        @endcan
                    @endforeach
                </div>
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.pwd_generator').click(function(){
            $('input[name=password]').val( '123456' );
            $('input[name=password_confirmation]').val( '123456' );
        });
        $('.email_generator').click(function(){
            $('input[name=email]').val( $('input[name="name"]').val() + '@gmail.com' );            
        });
    });
</script>
@stop