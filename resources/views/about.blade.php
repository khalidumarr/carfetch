@extends('layouts.frontend')
@section('content')
       <div class="login">
          <div class="wrap">
				<ul class="breadcrumb breadcrumb__t"><a class="home" href="#">Home</a>  / About</ul>
				<div class="section group">
				   <div class="labout span_1_of_about">
					 <h3>Testimonials</h3>
					  <div class="testimonials ">
						<div class="testi-item">
						<blockquote class="testi-item_blockquote">
							<a href="javascript:void(0);">The service here is excellent. They really take great care of you and the amenities are top notch. Always a pleasure to bring my car in. The purchase experience was really nice too. My salesperson was extremely friendly and helpful and I got a great deal!. </a>
							<div class="clear"></div>
						</blockquote>
							<small class="testi-meta"><span class="user">Jason alley </span>,
							<span class="info">A Regular Customer</span></div>
					   </div>
					   <div class="testimonials ">
						<div class="testi-item">
						<blockquote class="testi-item_blockquote">
							<a href="javascript:void(0);">After searching for almnost a month, I finally found al mutfawiq garage, they have good american cars with low prices, I bought my camaro and really satisfied with the deal, I'll recommend this to everyone who is looking for good USA Cars. </a>
							<div class="clear"></div>
						</blockquote>
							<small class="testi-meta"><span class="user"> Ahmed Ibrahim</span>,
							<span class="info">Resident of Sharjah</span><br></small></div>
					   </div>

				    </div>
				    <div class="cont span_2_of_about">
				       <h3>Our Mission</h3>
					   	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
					   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				   
		   <!-- Add fancyBox main JS and CSS files -->
		<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
		<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
		<script>
			$(document).ready(function() {
				$('.popup-with-zoom-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
			});
		});
		</script>
           </div>
		   <div class="clear"></div>	
		  </div>
	</div>	
   </div>
@endsection