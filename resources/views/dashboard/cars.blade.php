@extends('layouts.frontend')

@section('title_and_meta')
    <title>Al Mutafawiq | Buy Imported Used{{ ucwords((!empty($_REQUEST['make_code'])) ? ' ' . $_REQUEST['make_code'] : '') }}{{ ucwords((!empty($selected_model)) ? ' ' . $selected_model->name : '') }} Cars and Bikes in Dubai UAE</title>
    <meta name="title" content="Al Mutafawiq | Buy Imported Used{{ ucwords((!empty($_REQUEST['make_code'])) ? ' ' . $_REQUEST['make_code'] : '') }}{{ ucwords((!empty($selected_model)) ? ' ' . $selected_model->name : '') }} Cars and Bikes in Dubai UAE">
    <meta name="description" content="Buy your imported favorite{{ ucwords((!empty($_REQUEST['make_code'])) ? ' ' . $_REQUEST['make_code'] : '') }}{{ ucwords((!empty($selected_model)) ? ' ' . $selected_model->name : '') }} cars and bikes at best and cheap rates in Dubai UAE. Bank lease option available"/>   
@endsection

@section('content')
<div class="mens">    
        <div class="main">            
            <div class="wrap">
                @include('dashboard/tabs')                
                <h2 class="head">My Vehicles</h2>                    
                    <div class="mens-toolbar">
                        <div class="sort">
                            <div class="sort-by">
                                {{ $cars->total() }} found
                            </div>
                        </div>
                        <div class="pager">
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>     
                    <div class="top-box">       
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Title</th>
                                <th>Price</th>                            
                                <th>Lot Number</th>
                                <th>Arrival Date</th>                                
                                <th>Invoice</th>                                
                            </tr>
                        </thead>
                        <tbody>                                                    
                            @foreach( $cars as $car )  
                                <tr>
                                    <td>
                                        <a href="/cars/{{ $car->code }}">
                                            <img width="100" src="@if( $car->getMainImage() ) {{ trim( $car->getMainImage()->url ) }} @else /images/wargaeyar.jpg @endif" alt=""/>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/cars/{{ $car->code }}">
                                            {{ strlen( $car->title ) > 40 ? substr( $car->title, 0, 40 ) . "..." : $car->title }}
                                        </a>
                                    </td>
                                    <td>
                                        <?php $price = isset( $car->sale_price ) ? (int)$car->sale_price : 0 ?>
                                        <span class="actual">{{ number_format( $price ) }} {{ $default_currency->code }}</span>
                                    </td>
                                    <td>
                                        {{ $car->propByCode( 'lot_number' )->value }}
                                    </td>
                                    <td>
                                        @if (isset($car->container->arrival_date))
                                            @if (strtotime($car->container->arrival_date) > time())
                                                {{ date('d-m-Y', strtotime($car->container->arrival_date)) }}
                                            @else
                                                Arrived
                                            @endif
                                        @else
                                            -
                                        @endif                    
                                    </td>
                                    <td>
                                        @if($car->sale) 
                                            <a href="/dashboard/invoice/{{ $car->code }}?login=<?=$_REQUEST['login']?>" target="_blank" class="btn btn-primary">Generate Invoice </a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>                                                
                            @endforeach
                        </tbody>
                    </table>        
                    <div class="pagination-container">
                        {{ $cars->appends( $_REQUEST )->links() }}                        
                    </div>                                                        
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <script src="js/jquery.easydropdown.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#arrival_date_from').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_3"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#arrival_date_to').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_3"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $(document).on( 'click', '.search_interaction', function(){
                $('#search_form').submit();
            });

            $(document).on( 'click', '.filter_remove', function(e){
                e.preventDefault();
                var fieldName = $(this).data('input-name');

                if( fieldName == 'make_code' ){
                    $('input[name=model]').remove();    
                }

                $('input[name=' + fieldName + ']').remove();
                $('#search_form').submit();
            });
        });
    </script>
@endsection