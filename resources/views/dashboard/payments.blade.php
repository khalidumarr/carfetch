@extends('layouts.frontend')

@section('title_and_meta')
    <title>Al Mutafawiq | Buy Imported Used{{ ucwords((!empty($_REQUEST['make_code'])) ? ' ' . $_REQUEST['make_code'] : '') }}{{ ucwords((!empty($selected_model)) ? ' ' . $selected_model->name : '') }} Cars and Bikes in Dubai UAE</title>
    <meta name="title" content="Al Mutafawiq | Buy Imported Used{{ ucwords((!empty($_REQUEST['make_code'])) ? ' ' . $_REQUEST['make_code'] : '') }}{{ ucwords((!empty($selected_model)) ? ' ' . $selected_model->name : '') }} Cars and Bikes in Dubai UAE">
    <meta name="description" content="Buy your imported favorite{{ ucwords((!empty($_REQUEST['make_code'])) ? ' ' . $_REQUEST['make_code'] : '') }}{{ ucwords((!empty($selected_model)) ? ' ' . $selected_model->name : '') }} cars and bikes at best and cheap rates in Dubai UAE. Bank lease option available"/>   
@endsection

@section('content')
<div class="mens">    
        <div class="main">            
            <div class="wrap">
                @include('dashboard/tabs')                
                <h2 class="head">My Payments</h2>
                <div class="top-box"> 
                    Coming Soon
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.easydropdown.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#arrival_date_from').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_3"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#arrival_date_to').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_3"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });

            $(document).on( 'click', '.search_interaction', function(){
                $('#search_form').submit();
            });

            $(document).on( 'click', '.filter_remove', function(e){
                e.preventDefault();
                var fieldName = $(this).data('input-name');

                if( fieldName == 'make_code' ){
                    $('input[name=model]').remove();    
                }

                $('input[name=' + fieldName + ']').remove();
                $('#search_form').submit();
            });
        });
    </script>
@endsection