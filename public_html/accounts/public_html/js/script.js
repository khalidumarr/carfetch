﻿$(document).ready(function(){
	$(document).on('focusout', '.vat-participant', function(){              
		var target_field = $(this).data('target-field');        
		var vat = Math.ceil(($(this).val() * vat_factor));

		$('input[name="' + target_field + '"]').val(vat);
    });

    $('.upload_delete').click(function(){
	    if(confirm('You really want to delete this ?')){
	  		var id = $(this).data('id');
	  		$.ajax({
	  			method: 'POST',
	  			url : '/uploads/' + id,
	  			dataType : 'JSON',
	  			data : {
	  				'_method' : 'DELETE'
	  			},
	  			success: function( data ){
	  				if( data.status == true ){
	  					location.reload();
	  				}
	  			}
	  		});      
	    }
	});
});