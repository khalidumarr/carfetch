<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', 'IndexController@index');

Auth::routes();

// Resources Backend Routes
Route::resource('/user', 'Admin\User\UserController');
Route::resource('/role', 'Admin\User\RoleController');
Route::resource('/cars', 'Admin\Car\CarController');
Route::resource('/sales', 'Admin\Sale\SalesController');
Route::resource('/car-costs', 'Admin\Car\CarCostController');
Route::resource('/car-properties', 'Admin\Car\CarPropertyController');
Route::resource('/car-makes', 'Admin\Car\CarMakeController');
Route::resource('/car-models', 'Admin\Car\CarModelController');
Route::resource('/car-expense', 'Admin\Car\CarExpenseController');
Route::resource('/car-containers', 'Admin\Car\CarContainerController');
Route::resource('/accounts-heads', 'Admin\Account\HeadsController');
Route::resource('/accounts-payables', 'Admin\Account\PayablesController');
Route::resource('/accounts-receivables', 'Admin\Account\ReceivablesController');
Route::resource('/customer-receivables', 'Admin\Account\CustomerReceivablesController');
Route::resource('/accounts-expenses', 'Admin\Account\ExpensesController');
Route::resource('/car-responsibility', 'Admin\Responsibility\ResponsibilityController');
Route::resource('/uploads', 'Admin\UploadsController');
Route::resource('/options', 'Admin\optionController');
Route::resource('/banners', 'Admin\BannerController');
Route::resource('/report/sales', 'Admin\Reports\SalesController');
Route::resource('/report/expenses', 'Admin\Reports\ExpensesController');
Route::resource('/report/sales', 'Admin\Reports\SaleController');


// General Routes
Route::get('car-responsibility/{id}/modify', 'Admin\Responsibility\ResponsibilityController@modify');
Route::get('car-responsibility/{id}/complete', 'Admin\Responsibility\ResponsibilityController@complete');
Route::get('car-expense/{id}/approve', 'Admin\Car\CarExpenseController@approve');
Route::get('car-expense/{id}/delete', 'Admin\Car\CarExpenseController@delete');
Route::post('admin/accounts-receivables/receive', 'Admin\Account\ReceivablesController@receive');
Route::post('admin/accounts-receivables/release', 'Admin\Account\ReceivablesController@release');
Route::post('admin/accounts-payables/add-invoice', 'Admin\Account\PayablesController@addInvoice');
Route::post('admin/accounts-payables/release', 'Admin\Account\PayablesController@release');
Route::get('/agents-earnings', 'Admin\Account\PayablesController@agentsEarnings');
Route::get('sales/{id}/invoice/', 'Admin\Sale\SalesController@invoice');
Route::post('admin/sales/{id}/pay-remaining/', 'Admin\Sale\SalesController@payRemaining');
Route::post('admin/sales/{id}/approve-paid/', 'Admin\Sale\SalesController@approvePaid');
Route::get('report/inventory/', 'Admin\Reports\InventoryController@wholeInventory');
Route::get('report/my-earnings/', 'Admin\Reports\AgentsEarningsController@myEarnings');
Route::post('admin/sales/change-customer', 'Admin\Sale\SalesController@changeCustomer');
Route::post('admin/car-expense/{id}/add-payable-expense', 'Admin\Car\CarExpenseController@addPayableExpense');
Route::post('admin/car-expense/{id}/add-receivable-expense', 'Admin\Car\CarExpenseController@addReceivableExpense');
Route::get('get-dashboard', 'Admin\AdminController@dashboard');
Route::post('/contact/enquire/', 'ContactController@enquire');
Route::post('/contact/contact/', 'ContactController@contact');
Route::put('/car/{id}/upload-pictures/', 'Admin\Car\CarController@uploadPictures');
Route::post('/cars/update-sort/', 'Admin\Car\CarController@updateSort');


// Reports
Route::get('reports/get-total-receivable', 'Admin\Reports\ReceivableController@index');
Route::get('reports/get-total-payable', 'Admin\Reports\PayableController@index');
Route::get('reports/get-total-inventory', 'Admin\Reports\InventoryController@index');
Route::get('reports/get-net-position', 'Admin\Reports\GeneralController@index');
Route::get('reports/get-curr-month-expenses', 'Admin\Reports\ExpensesController@index');
Route::get('reports/get-monthly-expenses', 'Admin\Reports\ExpensesController@monthly');
Route::get('reports/get-curr-month-sales', 'Admin\Reports\SalesController@index');
Route::get('reports/get-arriving-containers', 'Admin\Reports\ContainersController@index');
Route::get('reports/top-selling-brands', 'Admin\Reports\InventoryController@brands');
Route::get('/reports/get-total-cash-in-hand', 'Admin\Reports\ReceivableController@cashInHand');
Route::get('/reports/my-received-earnings', 'Admin\Reports\PayableController@totalPaid');
Route::get('/reports/my-pending-earnings', 'Admin\Reports\PayableController@totalPending');
Route::get('/reports/get-cx-receivables', 'Admin\Reports\ReceivableController@customerReceivables');
Route::get('/reports/my-total-sales', 'Admin\Reports\SalesController@totalSales');
Route::get('/reports/my-units-available', 'Admin\Reports\InventoryController@unitsAvailable');




// New routes
Route::get('/sale', 'Admin\Sale\IndexController@index');
Route::get('/sale/{id}', 'Admin\Sale\IndexController@create');
Route::get('/sale/{id}/show', 'Admin\Sale\IndexController@show');
Route::get('/sale/{id}/delete', 'Admin\Sale\IndexController@delete');
Route::post('/sale', 'Admin\Sale\IndexController@store');
Route::post('/sale/{id}/upload-document', 'Admin\Sale\IndexController@uploadDocuments');
Route::post('/cars/import', 'Admin\Car\CarController@import');
Route::post('/cars/import-csv', 'Admin\Car\CarController@importCSV');
Route::get('/sync/car-models', 'Admin\Car\CarModelController@sync');
Route::get('/sale/{id}/generate-invoice', 'Admin\Sale\IndexController@generateInvoice');
Route::resource('/expenses', 'Admin\Account\ExpensesController');
Route::get('/notifications', 'Admin\NotificationController@index');


// API Routes
Route::get('/api/user', 'Api\User\UserController@get');
Route::get('/api/container', 'Api\Car\CarContainerController@get');
Route::get('/api/car/search', 'Api\Car\CarController@search');
