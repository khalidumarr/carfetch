<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesPayment extends Model
{
    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function user(){
    	return $this->belongsTo( 'App\Models\User', 'user_id' );
    }

    public function paymentMethod(){
    	return $this->belongsTo( 'App\Models\Dictionary', 'payment_method' );
    }

    public function updatedBy(){
    	return $this->belongsTo( 'App\Models\User', 'updated_by' );
    }

   public function getUserTotalChequesAmount( $userId ){
   		$cheques = self::where( 'user_id', $userId )->get();
   }
}
