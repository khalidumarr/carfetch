<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    public function make(){
		return $this->belongsTo( 'App\Models\CarMake', 'car_make_id' );
	}

	public function cars(){
		return $this->hasMany( 'App\Models\Car', 'model_id' );
	}
}
