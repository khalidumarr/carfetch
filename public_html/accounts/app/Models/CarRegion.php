<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarRegion extends Model
{
    public static function getDefaultRegion(){
    	// getting default region
        $defaultRegion = self::where( 'code', 'dubai' )->first();
        if( $defaultRegion ){
        	return $defaultRegion;
        }

        return self::_insertDefaultRegion();
    }

    private static function _insertDefaultRegion(){
    	$region = new self();
        $region->name = 'Dubai';
        $region->code = 'dubai';
        $region->save();
        return $region;
    }
}
