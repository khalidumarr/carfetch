<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarResponsibilityStatus extends Model
{
	protected $table = 'car_responsibility_statuses';
	
    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }
}
