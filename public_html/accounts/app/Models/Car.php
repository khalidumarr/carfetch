<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function setConnection($connection)
    {
        $this->connection = $connection;
        return $this;
    }
    public function model(){
    	return $this->belongsTo( 'App\Models\CarModel', 'model_id' );
    }

    public function source(){
    	return $this->belongsTo( 'App\Models\CarRegion', 'source_id' );
    }

    public function destination(){
    	return $this->belongsTo( 'App\Models\CarRegion', 'destination_id' );
    }

    public function saleType(){
    	return $this->belongsTo( 'App\Models\Dictionary', 'sale_type_id' );
    }

    public function stockType(){
    	return $this->belongsTo( 'App\Models\Dictionary', 'stock_type_id' );
    }

    public function agent(){
        return $this->belongsTo( 'App\Models\User', 'agent_id' );
    }

    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function sale(){
        return $this->hasOne( 'App\Models\Sale', 'car_id' );
    }

    public function container(){
        return $this->belongsTo( 'App\Models\CarContainer', 'container_id' );
    }

    public function properties(){
        return $this->hasMany( 'App\Models\CarPropsValue', 'car_id');        
    }

    public function costs(){
        return $this->hasMany( 'App\Models\CarCostsValue', 'car_id');        
    }

    public function updatedBy(){
        return $this->belongsTo( 'App\Models\User', 'updated_by');        
    }

    public function images(){
        return $this->morphMany('App\Models\Upload', 'uploadable')->orderBy('sort');
    }

    public function expenses(){
        return $this->hasMany( 'App\Models\CarExpense', 'car_id' );
    }

    public function responsibility(){
        return $this->hasOne( 'App\Models\CarResponsibility', 'car_id' );
    }

    public function getMainImage(){
        if( !$this->hasImages() ){
            return false;
        }
        
        foreach( $this->images as $image ){
            return $image;
        }

        return false;
    }

    public function hasImages(){
        if( isset( $this->images ) ){
            return true;
        }

        return false;
    }

    public function propByCode( $code ){        
        $propValue = new \App\Models\CarPropsValue();
        foreach( $this->properties as $prop ){
            if( $prop->prop_code == $code ){
                return $prop;
            }
        }

        return $propValue;
    }

    public function getNetEarning(){
        if( !$this->sale ){
            throw new \Exception("Car is not sold yet");
        }

        return round( $this->sale->price - $this->getNetCost() );
    }

    public function getNetCost(){
        return $this->getTotalCost() + $this->getTotalExpenses();
    }

    public function getTotalCost(){
        $total = 0;
        foreach( $this->costs as $cost ){
            $total += ( $cost->currency->conversion * $cost->value ); 
        }

        return $total;
    }

    public function getTotalExpenses(){
        $total = 0;
        foreach( $this->expenses as $expense ){
            if( $expense->receivable_id ){
                continue;
            }
            
            $total += ( $expense->currency->conversion * $expense->value );
        }

        return $total;
    }

    public function getAgentEarnings(){
        $netCost = $this->getNetCost();
        $salePrice = $this->sale->price;
        return (int)( 0.5 * ( $salePrice - $netCost ) );
    }

    public static function getAllCarsPrices(){
        $cars = self::all();
        $totalPrice = 0;
        foreach( $cars as $car ){
            $totalPrice += $car->getNetCost();
        }

        return $totalPrice;
    }

    public static function getNonContainedCars(){
        return self::where( 'container_id', null )->where('sold', 0)->get();
    }

    public static function getUniqueCode( $value ){
        $code = str_replace( ' ', '-', $value );
        if( self::where( 'code', $code )->first() ){
            $code .= '-' . rand( 0, 100 );            
            return self::getUniqueCode( $code );
        }

        return $code;
    }

    public static function findByVin($vin, $first = true)
    {
        $entity = new Car();        
        $entity = $entity->whereHas('properties', function($query) use ($vin){
            $query->where( 'prop_code', 'vin_number' )->where( 'value', $vin );
        });

        if ($first === true) {
            return $entity->first();            
        } else {
            return $entity->get();
        }
    }
}
