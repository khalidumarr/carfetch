<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CarResponsibilityStatus;

class CarResponsibility extends Model
{
	protected $table = 'cars_responsibilities';

    public function user(){
    	return $this->belongsTo( 'App\Models\User', 'user_id' );
    }

    public function car(){
    	return $this->belongsTo( 'App\Models\Car', 'car_id' );
    }

    public function status(){
    	return $this->belongsTo( 'App\Models\CarResponsibilityStatus', 'status_id' );
    }

    public function expenses(){
        return $this->hasMany( 'App\Models\CarExpense', 'responsibility_id');        
    }

    public function hasAllExpensesApproved(){
        $approved = true;
        foreach( $this->expenses as $expense ){
            if( $expense->approved != 1 ){
                $approved = false;
            }
        }

        return $approved;
    }

    public function hasCompleted(){
        if( $this->status_id == CarResponsibilityStatus::where('code', 'completed')->first()->id ){
            return true;
        }

        return false;
    }
}
