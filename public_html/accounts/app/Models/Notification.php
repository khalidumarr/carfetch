<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Car;

class Notification extends Authenticatable
{
    use Notifiable;  

    const TYPE_EXPORT_DOC = 'export_document_upload';

    public function sale(){
        return $this->belongsTo( 'App\Models\Sale', 'sale_id' );
    }
}
