<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public function uploadable(){
    	return $this->morphTo();
    }

    public static function reArrange( $mapping ){    	
    	foreach( $mapping as $key => $sort ){
    		$upload = self::find($key);
    		$upload->sort = $sort;
    		if( !$upload->save() ){
    			return false;
    		}
    	}

    	return true;
    }
}
