<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarCostsValue extends Model
{
    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function cost()
    {
        return $this->belongsTo( 'App\Models\CarCost', 'cost_id' );
    }

    public static function injectValues( $id, $carCosts ){
    	$costsValues 		= self::where( 'car_id', $id )->get();
    	$costsValuesIndex 	= [];
    	foreach( $costsValues as $value ){
    		$costsValuesIndex[$value->cost_code] = $value->value;
    	}

    	foreach( $carCosts as $key => $cost ){
    		if( isset( $costsValuesIndex[$cost->code] ) ){
    			$carCosts[$key]->value = $costsValuesIndex[$cost->code];
    		}
    	}

    	return $carCosts;
    }
}
