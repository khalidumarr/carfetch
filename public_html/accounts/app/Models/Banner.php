<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public static function getBanners(){
        return array_filter(scandir(storage_path('app/banners/')), function($file) { 
            return ( $file == '.' || $file == '..' || strpos( $file, 'original' ) !== false ) ? false : true; 
        });
    }
}
