<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Sale extends Model
{
    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function customer(){
        return $this->belongsTo( 'App\Models\User', 'sold_to' );
    }

    public function car(){
        return $this->belongsTo( 'App\Models\Car', 'car_id' );
    }

    public function country(){
        return $this->belongsTo( 'App\Models\Country', 'cx_country' );
    }

    public function notification(){
        return $this->hasOne( 'App\Models\Notification', 'sale_id' );
    }

    public function payments(){
        return $this->hasMany( 'App\Models\SalesPayment', 'sale_id');        
    }

    public function saleType(){        
        return $this->belongsTo( 'App\Models\Dictionary', 'sale_type_id' );
    }

    public static function getUniqueNumber(){
    	$number = '1' . mt_rand(10000,99999);
    	$sale = self::where( 'number', $number )->first();
    	if( $sale ){
    		self::getUniqueNumber();
    	}

    	return $number;
    }

    public static function getTotalByDate( $start, $end ){
        $sales = self::where( 'created_at', '>', $start )
                    ->where( 'created_at', '<', $end )->get();

        $total = 0;
        foreach( $sales as $sale ){
            $total += ( $sale->currency->conversion ) * ( $sale->price );
        }

        return $total;
    }

    public static function getDailyTotalByDate( $start, $end ){
        $sales = self::where( 'created_at', '>', $start )
                    ->where( 'created_at', '<', $end )->get();

        
        $dailySales = [];
        foreach( $sales as $sale ){
            $date = date( 'Y-m-d', strtotime( $sale->created_at ) );
            if( !isset( $dailySales[$date] ) ){
                $dailySales[$date] = 0;
            }

            $dailySales[$date] += ( $sale->currency->conversion ) * ( $sale->price );
        }

        // reformating array
        $dailySalesFormated = [];
        foreach( $dailySales as $date => $total ){
            $dailySalesFormated[] = [
                'd'         => $date,
                'sales'  => $total
            ];
        }

        return $dailySalesFormated;
    }

    public function hasUnapprovedPayment(){
        $salePayments = $this->payments;        
        if( count( $salePayments ) > 0 ){
            foreach( $salePayments as $payment ){
                if( $payment->approved != 1 ){
                    return true;
                }
            } 
        }

        return false;
    }

    public function getUnapprovedPayments(){
        $salePayments = $this->payments;
        $paymentsList = [];
        if( count( $salePayments ) > 0 ){
            foreach( $salePayments as $payment ){
                if( $payment->approved != 1 ){
                    $paymentsList[] = $payment;
                }
            }
        }

        return $paymentsList;
    }

    public function getAllPaymentsTotal( $approvedOnly = false ){
        $salePayments = $this->payments;
        $total = 0;
        if( count( $salePayments ) > 0 ){
            foreach( $salePayments as $payment ){
                if( $approvedOnly === true ){
                    if( $payment->approved == 1 ){
                        $total += $payment->amount;
                    }
                }
                else{
                    $total += $payment->amount;
                }
            }
        }

        return $total;
    }

    public static function getUserCustomers( $userId ){
        $sales      = self::where( 'sold_by', $userId )->get();        
        $customers  = [];

        foreach( $sales as $sale ){
            $customers[$sale->sold_to] = $sale->sold_to;
        }

        if( count( $customers ) > 0 ){
            return User::whereIn( 'id', $customers )->get();            
        }

        return array();
    }

    public function documents()
    {
        return $this->morphMany('App\Models\Upload', 'uploadable')->orderBy('sort');
    }

    public function hasDocuments(){
        if( isset( $this->documents ) ){
            return true;
        }

        return false;
    }
}
