<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';

    public static function getDefault(){
    	return self::where( 'is_default', 1 )->first();
    }

    public static function getByCode( $code ){
    	return self::where( 'code', $code )->first();	
    }
}
