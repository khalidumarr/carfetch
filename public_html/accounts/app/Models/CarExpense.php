<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarExpense extends Model
{
    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function responsibility(){
        return $this->belongsTo( 'App\Models\CarResponsibility', 'responsibility_id' );
    }

    public function invoiceImage(){
        return $this->morphMany('App\Models\Upload', 'uploadable');
    }

    public static function getCarTotal( $carId ){
    	return self::where( 'car_id', $carId )->sum('value');
    }

    public static function getCarTotalApproved( $carId ){
    	return self::where( 'car_id', $carId )->where( 'approved', 1 )->sum('value');
    }
}
