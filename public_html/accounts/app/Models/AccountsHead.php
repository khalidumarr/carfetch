<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Dictionary;

class AccountsHead extends Model
{
    public function currency(){
    	return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function type(){
    	return $this->belongsTo( 'App\Models\Dictionary', 'type_id' );	
    }

    public static function getByType( $code ){
    	$type = Dictionary::where( 'code', $code )->first();
    	if( !$type ){
    		throw new \Exception( 'No type found with provided text' );
    	}

    	return self::where('type_id', $type->id)->get();
    }
}
