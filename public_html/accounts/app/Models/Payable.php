<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AccountsHead;

class Payable extends Model
{
    public function createdBy(){
        return $this->belongsTo( 'App\Models\User', 'created_by' );
    }

    public function createdFor(){
        return $this->belongsTo( 'App\Models\User', 'created_for' );
    }

    public function currency(){
        return $this->belongsTo( 'App\Models\Currency', 'currency_id' );
    }

    public function head(){
        return $this->belongsTo( 'App\Models\AccountsHead', 'head_id' );
    }

    public function user(){
        return $this->belongsTo( 'App\Models\User', 'user_id' );
    }

    public function invoiceImage(){
        return $this->morphMany('App\Models\Upload', 'uploadable');
    }

    public static function getTotal(){
        $in  = self::where( 'debit', 0 )->sum('value');
        $out = self::where( 'debit', 1 )->sum('value');

        return $in - $out;
    }

    public static function getTotalPayableByHead( $headCode, $debit ){
        $head = AccountsHead::where( 'code', $headCode )->first();
        if( !$head ){
            return 0;
        }

        return self::where( 'debit', $debit )->where( 'head_id', $head->id )->sum('value');
    }    

    public static function getPaidByHead( $headCode ){
        $head = AccountsHead::where( 'code', $headCode )->first();
        if( !$head ){
            throw new \Exception( 'No head found with provided code' );
        }

        return self::where( 'debit', 1 )->where( 'head_id', $head->id )->sum('value');
    }

     public static function getDifferenceByHead( $headCode ){
        $head = AccountsHead::where( 'code', $headCode )->first();

        $payable  = self::where( 'debit', 0 )->where( 'head_id', $head->id )->sum('value');
        $paid = self::where( 'debit', 1 )->where( 'head_id', $head->id )->sum('value');

        return $payable - $paid;
    }
}
