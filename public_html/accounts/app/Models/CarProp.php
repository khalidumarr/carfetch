<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarProp extends Model
{
    public function type(){
		return $this->belongsTo( 'App\Models\CarPropsType', 'car_prop_type' );
	}
}
