<?php

namespace App\Library\Price;
use NumberToWords\NumberToWords;

class Price
{
	const CONVERSION_RATE = 3.67;
	public static function format($price)
	{		
		return number_format($price, 2) . ' AED';
	}

	public static function toAED($value)
	{
		return round(((int) $value * self::CONVERSION_RATE), 2);
	}

	public static function toWords($amount)
	{		
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');
        return $numberTransformer->toWords($amount) . ' dirhams';die;
	}
}