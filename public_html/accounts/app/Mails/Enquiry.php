<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Enquiry extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $phone;
    public $offerPrice;
    public $msg;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->name         = $data['name'];
        $this->email        = $data['email'];
        $this->phone        = $data['phone'];
        $this->offerPrice   = $data['offerPrice'];
        $this->msg          = $data['msg'];
        $this->url          = $data['url'];
        $this->subject('Contact');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        return $this->view('emails.enquiry');
    }
}