<?php

namespace App\Listeners;

use App\Events\UserUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccountsHead;
use App\Models\Currency;

class UserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdated  $event
     * @return void
     */
    public function handle(UserUpdated $event)
    {
        //
    }

    public function doUpdateAccountHead( $event ){        
        if( \Gate::allows( 'create-agent-payable', $event->user ) ){            
            $head = new AccountsHead();
            $head->title = 'Agent Earnings ' . $event->user->name;
            $head->code = 'agent-' . $event->user->id . '-earnings';
            $head->description = 'Agents Earnings';
            $head->currency_id = Currency::getDefault()->id;
            if( !$head->save() ){
                return false;
            }
        }

        return true;
    }
}
