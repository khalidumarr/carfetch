<?php

namespace App\Listeners;

use App\Events\CarSold;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Payable;
use App\Models\AccountsHead;
use App\Models\Currency;

class CarListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CarSold  $event
     * @return void
     */
    public function handle(CarSold $event)
    {
        //
    }

    public function calculateAgentCommission( $event ){
        $user = \Auth::user();
        if( $user->getApplicableRole()->code == 'sale-purchase' ){
            $payable                = new Payable();
            $payable->head_id       = AccountsHead::where( 'code', 'agent-' . $event->car->agent_id . '-earnings' )->first()->id;
            $payable->debit         = 0;
            $payable->value         = $event->car->getAgentEarnings();
            $payable->currency_id   = Currency::getDefault()->id;
            $payable->created_by    = \Auth::id();
            $payable->save();
        }
    }
}
