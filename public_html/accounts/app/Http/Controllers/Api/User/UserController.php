<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Models\User;
use App\Models\Role;
use App\Models\Upload;
use App\Models\AccountsHead;
use App\Models\Currency;
use App\Http\Criterias\UserCriteria;

class UserController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');  
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {        
        $request = $request->all();
        $entity = new User;
        foreach ($request as $key => $value) {
            $entity = $entity->where( $key, 'like' , '%' . $value . '%' );
        }
        
        $response = ['user' => null];
        if ($user = $entity->first()) {
            $response['user'] = $user->toArray();
            return json_encode($response);
        }

        return json_encode($response);
    }
}
