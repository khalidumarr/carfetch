<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Car;

class InventoryController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return number_format( Car::getAllCarsPrices(), 2 ) . ' AED';
    }

    public function unitsAvailable(){
        if( \Gate::allows( 'total-inventory-count' ) ){
            return count( car::where( 'sold', 0 )->get() );
        }
        else{            
            return count( car::where( 'sold', 0 )->where( 'agent_id', Auth::id() )->get() );
        }
    }

    public function wholeInventory(){
        $data = [
            'cars' => Car::where( 'sold', 0 )->get()
        ];

        return view('admin/reports/inventory', $data);
    }
}
