<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Receivable;
use App\Models\Sale;

class ReceivableController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Receivable::getTotal() . ' AED';
    }

    public function cashInHand(){
        return number_format( \Auth::user()->getTotalPayable(), 2 ) . ' AED';
    }

    public function customerReceivables(){
        if( \Gate::allows( 'own-customers-receivables' ) ){              
            $customers = Sale::getUserCustomers( \Auth::id() );            
        }
        else{
            $customers = User::getRoleUsers( 'customer' );
        }
        
        $data = [
            'total_payable' => 0,
            'total_paid'    => 0
        ];
        
        foreach( $customers as $customer ){
            $sales = Sale::where( 'sold_to', $customer->id )->get();

            $total_payable = 0;
            $total_paid = 0;
            foreach( $sales as $sale ){
                $total_payable  += $sale->price;
                $total_paid     += $sale->amount_paid;
            }

            $data = [
                'total_payable'     => $total_payable,
                'total_paid'        => $total_paid                
            ];
        }

        return ( $data['total_payable'] - $data['total_paid'] ) . ' AED';
    }
}
