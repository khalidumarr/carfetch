<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Payable;
use App\Models\Receivable;
use App\Models\Car;

class GeneralController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payable = Payable::getTotal();
        $receivable = Receivable::getTotal();
        $inventory = Car::getAllCarsPrices();

        return number_format( ( $inventory + $receivable - $payable ), 2 ) . ' AED';
    }
}
