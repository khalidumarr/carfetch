<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Payable;

class PayableController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Payable::getTotal() . ' AED';
    }

    /**
     * Total amount paid to agent.
     * 
     * @return \Illuminate\Http\Response
     */
    public function totalPaid(){
        $headName = 'agent-' . Auth::id() . '-earnings';
        return Payable::getPaidByHead( $headName ) . ' AED';
    }

    /**
     * Total Earnings of agent.
     * 
     * @return \Illuminate\Http\Response
     */
    public function totalPending(){
        $headName = 'agent-' . Auth::id() . '-earnings';
        return Payable::getDifferenceByHead( $headName ) . ' AED';
    }
}
