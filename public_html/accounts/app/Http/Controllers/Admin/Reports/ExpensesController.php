<?php

namespace App\Http\Controllers\Admin\Reports;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Expense;

class ExpensesController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'morris'        => Expense::getDailyTotalByDate( date('Y') . '-' . date('m') . '-01 00:00:00', date( 'y-m-d H:i:s' ) ),
            'current_month' => Expense::getTotalByDate( date('Y') . '-' . date('m') . '-01 00:00:00', date( 'y-m-d H:i:s' ) ) . ' AED',
            'current_week'  => Expense::getTotalByDate( date( 'Y-m-d H:i:s', strtotime( 'LAST SATURDAY' ) ), date( 'y-m-d H:i:s' ) ) . ' AED',
            'today'         => Expense::getTotalByDate( date( 'Y-m-d' ) . ' 00:00:00', date( 'Y-m-d H:i:s' ) ) . ' AED'
        ];

        return json_encode( $data );
    }

    public function monthly(){
        return json_encode( Expense::getMonthlyTotal() );
    }
}
