<?php

namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Controllers\Controller;
use App\Models\Role;

class RoleController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort       = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction  = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $roles      = Role::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data       = [
            'roles'     => $roles     
        ];

        return view( 'admin/users/roles/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view( 'admin/users/roles/add', [ 'role' => new Role() ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $role = new Role();
        $role->name         = $request->name;
        $role->code         = $request->code;
        $role->priority     = $request->priority;
        
        if( $role->save() ){
            return redirect('admin/role/')->with( 'message', 'Role Added Successfully!' );
        }
        else{
            return redirect('admin/role/')->with( 'errors', 'Problem Adding Role' );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find( $id );

        return view( 'admin/users/roles/edit', [ 'role' => $role ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $role = Role::find($id);
        if( $role ){
            $role->name         = $request->name;
            $role->code         = $request->code;
            $role->priority     = $request->priority;
            
            if( $role->save() ){
                return redirect('admin/role/')->with( 'message', 'Role Updated Successfully!' );
            }
            else{
                return redirect('admin/role/')->with( 'errors', 'Problem Updating Role' );
            }
        }

        return redirect('admin/role/')->with( 'errors', 'No Role Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if( $role ){
            if( $role->delete() ){
                return redirect('admin/role/')->with( 'message', 'Role Deleted Successfully' );                
            }

            return redirect('admin/role/')->with( 'errors', 'Problem Deleting Role' );
        }

        return redirect('admin/role/')->with( 'errors', 'No Role Found with provided ID' );
    }
}
