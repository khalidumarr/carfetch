<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Upload;

class UploadsController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/home');
    }

    public function show(Request $request, $filename)
    {
        $path = storage_path('app') . '/uploads/' . $filename;

        if(!\File::exists($path)) abort(404);

        $file = \File::get($path);
        $type = \File::mimeType($path);

        $response = \Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upload = Upload::find($id);        
        if( $upload ){
            if( $upload->delete() ){
                echo json_encode([
                    'status' => true,
                    'message' => 'File successfully deleted'
                ]);die;                
            }
        }

        echo json_encode([
            'status' => false,
            'message' => 'Unable to find the file to delete'
        ]);die;
    }
}
