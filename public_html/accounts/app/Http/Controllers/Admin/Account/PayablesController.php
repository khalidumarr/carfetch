<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Currency;
use App\Models\Receivable;
use App\Models\Payable;
use App\Models\Upload;
use App\Models\AccountsHead;
use App\Http\Criterias\PayableCriteria;

class PayablesController extends \App\Http\Controllers\Controller
{
    private $requiredFields = [
        'head_id'        => 'Account Head',
        'amount'         => 'Amount'        
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $criteria   = new PayableCriteria( $request );
        $payables = $criteria->execute();        

        $toPay = 0;
        $paid = 0;

        foreach ($payables as $payable) {
            if ($payable->debit == 0) {
                $toPay += $payable->value;                
            } else {
                $paid += $payable->value;
            }
        }

        $data = [ 
            'sales_users'       => User::all(),
            'heads'             => AccountsHead::all(),
            'currencies'        => Currency::all(),
            'shippers'          => User::getRoleUsers( 'shipper' ),
            'payables'          => $payables,
            'total_payable'     => Payable::getTotal(),
            'toPay'             => $toPay,
            'paid'              => $paid
        ];

        return view('admin/accounts/payables/list', $data);
    }

    public function addInvoice( Request $request ){
        foreach( $this->requiredFields as $field => $title ){
            if( !$request->get($field) ){
                return redirect( $request->get('back_url') )->with( 'error', $title . ' is Required' );
            }
        }

        \DB::beginTransaction();
        $payable = new Payable();
        $payable->head_id        = $request->head_id;
        $payable->value          = $request->amount;
        $payable->debit          = 0;
        $payable->currency_id    = $request->currency_id;
        $payable->created_by     = Auth::user()->id;

        if( $payable->save() ){
            if( $request->file('invoice_image') ){
                $image = $request->file('invoice_image');
                
                if( !$image->isValid() ){
                    \DB::rollback();
                    return redirect( $request->get('back_url') )->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type  = Payable::class; 
                $file->uploadable_id    = $payable->id; 
                $file->name             = str_replace( 'uploads/', '', $path ); 
                $file->url              = '/' . $path; 
                if( !$file->save() ){
                    \DB::rollback();
                    return redirect( $request->get('back_url') )->with( 'message', 'Invalid Image Format' );
                }                
            }

            \DB::commit();
            return redirect( $request->get('back_url') )->with( 'message', 'Successfully Added!' );
        }

        return redirect( $request->get('back_url') )->with( 'error', 'Problem Receiving!' );
    }

    public function release( Request $request ){        
        foreach( $this->requiredFields as $field => $title ){
            if( !$request->get($field) ){
                return redirect( $request->get('back_url') )->with( 'errors', $title . ' is Required' );
            }
        }        

        $currency = Currency::where('code', '=', 'AED')->first();
        $payable = new Payable();
        $payable->head_id        = $request->head_id;
        $payable->value          = $request->amount;
        $payable->debit          = 1;

        if ($request->user_id) {
            $payable->user_id = $request->user_id;
        }

        $payable->debit          = 1;
        $payable->currency_id    = $currency->id;
        $payable->created_by     = Auth::user()->id;

        if( $payable->save() ){
            if( $request->file('invoice_image') ){                
                $image = $request->file('invoice_image');
                
                if( !$image->isValid() ){
                    \DB::rollback();
                    return redirect( $request->get('back_url') )->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('payables');

                $file = new Upload();
                $file->uploadable_type  = Payable::class; 
                $file->uploadable_id    = $payable->id; 
                $file->name             = str_replace( 'payables/', '', $path ); 
                $file->url              = '/' . $path; 
                if( !$file->save() ){
                    \DB::rollback();
                    return redirect( $request->get('back_url') )->with( 'message', 'Invalid Image Format' );
                }                
            }

            \DB::commit();
            return redirect( $request->get('back_url') )->with( 'message', 'Successfully Added!' );
        }

        return redirect( $request->get('back_url') )->with( 'error', 'Problem Releasing!' );
    }

    public function destroy( Request $request, $id ){
        $payable = Payable::find( $id );
        if( !$payable ){
            return back()->with( 'error', 'No Payable found with provided ID!' );
        }

        if( $payable->delete() ){
            return back()->with( 'message', 'Payable Successfully Deleted!' );
        }

        return back()->with( 'error', 'Unable to delete Payable!' );
    }

    public function agentsEarnings( Request $request ){
        $agents = User::getRoleUsers( 'sale-purchase' );
        
        $data = array();
        foreach( $agents as $agent ){
            $data['list'][] = [
                'agent'     => $agent,
                'earned'    => (int)Payable::getTotalPayableByHead( 'agent-' . $agent->id . '-earnings', 0 ),
                'paid'      => (int)Payable::getTotalPayableByHead( 'agent-' . $agent->id . '-earnings', 1 )
            ];
        }

        $data['currencies'] = Currency::all();

        return view('admin/accounts/payables/agents-earnings', $data);
    }
}
