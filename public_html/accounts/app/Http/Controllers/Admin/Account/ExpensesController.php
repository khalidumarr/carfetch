<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Requests\ExpenseRequest;
use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\AccountsHead;
use App\Models\Currency;
use App\Models\Upload;
use App\Http\Criterias\ExpenseCriteria;

class ExpensesController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn      = 'id';
    public $sortDirection   = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if (!$request->get('date_from') || !$request->get('date_to')) {
            $dateFrom = date('Y-m-d', strtotime('-30 days'));
            $dateTo = date('Y-m-d H:i:s');

            $request->merge(['date_from' => $dateFrom, 'date_to' => $dateTo]);
            $_REQUEST['date_from'] = $dateFrom;
            $_REQUEST['date_to'] = $dateTo;
            $request->session()->now('notice', 'Showing last 30 days Expenses, Use Filters to view your desired Record');

            $_REQUEST['date_from'] = $dateFrom;
            $_REQUEST['date_to'] = $dateTo;
        }

        $criteria   = new ExpenseCriteria( $request );
        $expenses = $criteria->execute();

        $total = 0;
        $totalVat = 0;

        foreach ($expenses as $expense) {
            $total += $expense->value;
            $totalVat += $expense->vat;
        }        

        $data       = [
            'expenses'     => $expenses,
            'total' => $total,
            'totalVat' => $totalVat
        ];        

        return view( 'admin/accounts/expenses/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'heads'         => AccountsHead::all(),
            'expense'       => new Expense(),
            'currencies'    => Currency::all()
        ];

        return view( 'admin/accounts/expenses/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request)
    {
        $expense = new Expense();
        $expense->year           = date('Y');
        $expense->month          = date('m');
        $expense->head_id       = $request->head_id;
        $expense->value          = $request->value;
        $expense->vat           = $request->vat;
        $expense->trn           = $request->trn;
        $expense->currency_id    = $request->currency_id;
        $expense->added_by       = \Auth::user()->id;

        if( $expense->save() ){
            if( $request->file('image') ){
                foreach( $request->file('image') as $image ){
                    if( !$image->isValid() ){
                        return redirect('/expenses')->with( 'errors', 'Invalid Image Selected!' );
                    }

                    $path = $image->store('uploads');

                    $file = new Upload();
                    $file->uploadable_type = Expense::class; 
                    $file->uploadable_id = $expense->id; 
                    $file->name = str_replace( 'uploads/', '', $path ); 
                    $file->url = '/' . $path; 
                    if( !$file->save() ){
                        return redirect('/expenses')->with( 'message', 'Expense Added But unable to save the image!' );
                    }              
                }
            }

            return redirect('expenses')->with( 'message', 'Expense Added Successfully' );
        }
        else{
            return redirect('expenses')->with( 'errors', 'Problem Adding Expense' );
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'heads'         => AccountsHead::all(),
            'expense'       => Expense::find( $id ),
            'currencies'    => Currency::all()
        ];

        return view( 'admin/accounts/expenses/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseRequest $request, $id)
    {
        $expense = Expense::find($id);
        if( $expense ){
            $expense->year           = date('Y');
            $expense->month          = date('m');
            $expense->head_id        = $request->head_id;
            $expense->value          = $request->value;
            $expense->currency_id    = $request->currency_id;
            $expense->added_by       = \Auth::user()->id;
            $expense->vat           = $request->vat;
            $expense->trn           = $request->trn;
                
            if( $expense->save() ){
                if( $request->file('image') ){
                    foreach( $request->file('image') as $image ){
                        if( !$image->isValid() ){
                            return redirect('/expenses')->with( 'errors', 'Invalid Image Selected!' );
                        }

                        $path = $image->store('uploads');

                        $file = new Upload();
                        $file->uploadable_type = Expense::class; 
                        $file->uploadable_id = $expense->id; 
                        $file->name = str_replace( 'uploads/', '', $path ); 
                        $file->url = '/' . $path; 
                        if( !$file->save() ){
                            return redirect('/expenses')->with( 'message', 'Expense Added But unable to save the image!' );
                        }              
                    }
                }

                return redirect('expenses')->with( 'message', 'Expense Added Successfully' );
            }
            else{
                return redirect('expenses')->with( 'errors', 'Problem Adding Expense' );
            }
        }

        return redirect('admin/accounts-expenses')->with( 'errors', 'No Expense Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::find($id);
        if( $expense ){
            if( $expense->delete() ){
                return redirect('admin/accounts-expenses')->with( 'message', 'Expense Deleted Successfully' );                
            }

            return redirect('admin/accounts-expenses')->with( 'errors', 'Problem Deleting Expense' );
        }

        return redirect('admin/accounts-expenses')->with( 'errors', 'No Expense Found with provided ID' );
    }
}
