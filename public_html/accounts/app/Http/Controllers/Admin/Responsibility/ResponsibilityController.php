<?php

namespace App\Http\Controllers\Admin\Responsibility;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\CarResponsibility;
use App\Models\User;
use App\Models\Currency;
use App\Models\Receivable;
use App\Models\CarExpense;
use App\Models\AccountsHead;
use App\Models\Car;
use App\Models\CarResponsibilityStatus;
use App\Http\Requests\CarResponsibilityRequest;

class ResponsibilityController extends \App\Http\Controllers\Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort             = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction        = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        
        if( Auth::user()->getApplicableRole()->code != 'admin' ){
            $status = CarResponsibilityStatus::where('code', 'completed')->first()->id;
            $responsibilities = CarResponsibility::where( 'status_id', '<>', $status )
                            ->where( 'user_id', \Auth::id() )
                            ->orderBy( $sort, $direction )
                            ->paginate( self::RESULTS_PER_PAGE );
        }
        else{
            $responsibilities = CarResponsibility::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );            
        }

        $data = [
            'responsibilities' => $responsibilities     
        ];

        return view( 'admin/responsibility/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request )
    {
        $data = [
            'users' => User::getRoleUsers( 'repairing' ),
            'cars'  => Car::all(),
            'responsibility' => new CarResponsibility(),
            'currencies' => Currency::all()
        ];

        if( $request->car_id ){
            $data['responsibility']->car_id = $request->car_id;
        }

        return view( 'admin/responsibility/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarResponsibilityRequest $request)
    {
        if( count( $request->car_id ) > 1 ){
            return back()->with( 'error', 'Please select only one car!' );
        }

        // getting first key from the car array
        foreach( $request->car_id as $value ){
            $carId = $value;
            break;
        }

        \DB::beginTransaction();
        $responsibility  = new CarResponsibility();
        $responsibility->user_id        = $request->user_id;
        $responsibility->car_id         = $carId;
        $responsibility->status_id      = CarResponsibilityStatus::where( 'code', 'created' )->first()->id;
        if( !$responsibility->save() ){
            \DB::rollback();
            return redirect('admin/car-responsibility')->with( 'errors', 'Problem Adding Responsibility' );
        }

        $receivable = new Receivable();
        $receivable->head_id = AccountsHead::where('code', 'car-expense-added')->first()->id;
        $receivable->created_for = $request->user_id;
        $receivable->created_by = Auth::user()->id;
        $receivable->debit = 1;
        $receivable->value = $request->amount_released;
        $receivable->currency_id = $request->currency_id;

        if( !$receivable->save() ){
            \DB::rollback();
            return redirect('admin/car-responsibility')->with( 'errors', 'Problem Adding Receivable' );            
        }

        \DB::commit();
        return redirect('admin/car-responsibility')->with( 'message', 'Responsibility Added Successfully' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $responsibility = CarResponsibility::find( $id );
        return view( 'admin/responsibility/edit', [ 'responsibility' => $responsibility ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarResponsibilityRequest $request, $id)
    {
        $responsibility = CarResponsibility::find($id);
        if( $responsibility ){
            $responsibility->name         = $request->name;
            $responsibility->code         = $request->code;

            if( $responsibility->save() ){
                return redirect('admin/responsibility')->with( 'message', 'Responsibility Updated Successfully!' );
            }
            else{
                return redirect('admin/responsibility')->with( 'errors', 'Problem Updating Responsibility' );
            }
        }

        return redirect('admin/responsibility')->with( 'errors', 'No Responsibility Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $responsibility = CarResponsibility::find($id);
        if( $responsibility ){
            if( $responsibility->delete() ){
                return back()->with( 'message', 'Responsibility Deleted Successfully' );                
            }

            return back()->with( 'errors', 'Problem Deleting Responsibility' );
        }

        return back()->with( 'errors', 'No Responsibility Found with provided ID' );
    }

    public function modify( $id ){
        $responsibility = CarResponsibility::find( $id );            
        
        if( !$responsibility ){
            return redirect( '/admin/car-responsibility' )->with( 'errors', 'No Responsibility found with provided ID' );
        }

        // start managing responsibility
        if( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'start' ){
            if( $responsibility->status->code == 'created' ){
                $responsibility->status_id = CarResponsibilityStatus::where( 'code', "in-progress" )->first()->id;
                if( $responsibility->save() ){
                    return redirect( 'admin/car-responsibility/' . $id . '/modify/' )->with( 'message', 'Responsibility Initiated Successfully' );
                }
            }
            else{
                return redirect('/admin/car-responsibility')->with( 'errors', 'Responsibility status cannot be changed' );
            }
        }

        if( Auth::user()->getApplicableRole()->code != 'admin' ){
            if( ( $responsibility->user_id != Auth::user()->id ) ){                            
                return redirect( '/admin/car-responsibility' )->with( 'errors', 'Sorry This Responsibility doesn\'t belong to you' );       
            }
        }        

        $data = [
            'responsibility'    => $responsibility,
            'currencies'        => Currency::all(),
            'expense'           => new CarExpense(),
            'credit'            => \Auth::user()->getTotalPayable(),
            'payable_heads'     => AccountsHead::getByType( 'garage' ),
            'receivable_heads'  => AccountsHead::getByType( 'receivable-expenses' )
        ];
        
        return view( 'admin/responsibility/modify', $data );
    }

    public function complete( Request $request, $id ){
        $responsibility = CarResponsibility::find( $id );            
        
        if( !$responsibility ){
            return back()->with( 'errors', 'No Responsibility found with provided ID' );
        }

        if( !$responsibility->hasAllExpensesApproved() ){
            return back()->with( 'errors', 'Responsibility can not be completed as it has some Unapproved Expenses' );
        }

        $responsibility->status_id = CarResponsibilityStatus::where('code', 'completed')->first()->id;

        if( $responsibility->save() ){
            return back()->with( 'message', 'Responsibility Successfully Completed' );
        }

        return back()->with( 'errors', 'Problem Updating the Responsibility' );
    }
}
