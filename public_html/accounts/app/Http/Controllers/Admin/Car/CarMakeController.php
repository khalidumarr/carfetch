<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarMakeRequest;
use App\Http\Controllers\Controller;
use App\Models\CarMake;
use App\Models\Upload;

class CarMakeController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort       = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction  = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $makes      = CarMake::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data       = [
            'makes'     => $makes     
        ];

        return view( 'admin/cars/makes/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view( 'admin/cars/makes/add', [ 'make' => new CarMake() ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarMakeRequest $request)
    {
        $make = new CarMake();
        $make->name         = $request->name;
        $make->code         = $request->code;

        if( $make->save() ){
            if( $request->hasFile( 'logo' ) ){                
                if( !$request->file('logo')->isValid() ){
                    return redirect('admin/car-makes')->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $request->logo->store('uploads');

                // delete if available
                $prevImage = Upload::where( 'uploadable_type', CarMake::class )
                            ->where( 'uploadable_id', $make->id );
                if( $prevImage ){
                    $prevImage->delete();
                }

                $file = new Upload();
                $file->uploadable_type = CarMake::class; 
                $file->uploadable_id = $make->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return redirect('admin/car-makes/')->with( 'message', 'Car Make Added But unable to save the logo!' );
                }
            }

            return redirect('admin/car-makes/')->with( 'message', 'Car Make Added Successfully' );
        }
        else{
            return redirect('admin/car-makes')->with( 'errors', 'Problem Adding Car Make' );
        }

        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $make = CarMake::find( $id );

        return view( 'admin/cars/makes/edit', [ 'make' => $make ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarMakeRequest $request, $id)
    {
        $make = CarMake::find($id);
        if( $make ){
            $make->name         = $request->name;
            $make->code         = $request->code;

            if( $request->hasFile( 'logo' ) ){                
                if( !$request->file('logo')->isValid() ){
                    return redirect('admin/car-makes')->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $request->logo->store('uploads');

                // delete if available
                $prevImage = Upload::where( 'uploadable_type', CarMake::class )
                            ->where( 'uploadable_id', $make->id );
                if( $prevImage ){
                    $prevImage->delete();
                }

                $file = new Upload();
                $file->uploadable_type = CarMake::class; 
                $file->uploadable_id = $make->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return redirect('admin/user/')->with( 'message', 'Car Make Updated But unable to save the logo!' );
                }
            }
            
            if( $make->save() ){
                return redirect('admin/car-makes')->with( 'message', 'Car Make Updated Successfully!' );
            }
            else{
                return redirect('admin/car-makes')->with( 'errors', 'Problem Updating Car Make' );
            }
        }

        return redirect('admin/car-makes')->with( 'errors', 'No Car Make Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $make = CarMake::find($id);
        if( $make ){
            if( count($make->models()) <= 0 && $make->delete() ){
                return redirect('admin/car-makes')->with( 'message', 'Car Make Deleted Successfully' );                
            }

            return redirect('admin/car-makes')->with( 'errors', 'Problem Deleting Car Make' );
        }

        return redirect('admin/car-makes')->with( 'errors', 'No Car Make Found with provided ID' );
    }
}
