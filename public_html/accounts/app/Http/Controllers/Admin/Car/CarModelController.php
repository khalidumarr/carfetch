<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarModelRequest;
use App\Http\Controllers\Controller;
use App\Models\CarModel;
use App\Models\CarMake;
use App\Models\Upload;

class CarModelController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort       = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction  = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $models     = CarModel::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );
        
        $data       = [
            'models'     => $models     
        ];

        return view( 'admin/cars/models/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $makes      = CarMake::all();
        return view( 'admin/cars/models/add', [ 'model' => new CarModel(), 'makes' => $makes ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarModelRequest $request)
    {
        $model  = new CarModel();
        $model->name         = $request->name;
        $model->car_make_id  = $request->car_make_id;

        if( $model->save() ){
            return redirect('admin/car-models/')->with( 'message', 'Car Model Added Successfully' );
        }
        else{
            return redirect('admin/car-models')->with( 'errors', 'Problem Adding Car Model' );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = CarModel::find( $id );
        $makes = CarMake::all();
        return view( 'admin/cars/models/edit', [ 'model' => $model, 'makes' => $makes ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarModelRequest $request, $id)
    {
        $model = CarModel::find($id);
        if( $model ){
            $model->name          = $request->name;
            $model->car_make_id  = $request->car_make_id;
            
            if( $model->save() ){
                return redirect('admin/car-models')->with( 'message', 'Car Model Updated Successfully!' );
            }
            else{
                return redirect('admin/car-models')->with( 'errors', 'Problem Updating Car Model' );
            }
        }

        return redirect('admin/car-models')->with( 'errors', 'No Car Model Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CarModel::find($id);
        if( $model ){
            if( count($model->cars()) <= 0 && $model->delete() ){
                return redirect('admin/car-models')->with( 'message', 'Car Model Deleted Successfully' );                
            }

            return redirect('admin/car-models')->with( 'errors', 'Problem Deleting Car Model' );
        }

        return redirect('admin/car-models')->with( 'errors', 'No Car Model Found with provided ID' );
    }

    public function sync(Request $request){
        $remoteMakes = CarMake::on('main')->get();
        $localMakes  = CarMake::all();

        \DB::beginTransaction();

        // indexing remote makes
        $indexRemoteMakes = [];
        foreach ($remoteMakes as $remoteMake) {
            $indexRemoteMakes[$remoteMake->code]['make'] = $remoteMake;
            foreach ($remoteMake->models as $remoteModel) {
                $indexRemoteMakes[$remoteMake->code]['models'][$remoteModel->name] = $remoteModel;
            }
        }

        // indexing local makes
        $indexLocalMakes = [];
        foreach ($localMakes as $localMake) {
            $indexLocalMakes[$localMake->code]['make'] = $localMake;            
            foreach ($localMake->models as $localModel) {                
                $indexLocalMakes[$localMake->code]['models'][$localModel->name] = $localModel;
            }
        }
        
        // delete local makes/model
        foreach ($localMakes as $localMake) {
            if (!isset($indexRemoteMakes[$localMake->code])) {
                $carFound = false;
                foreach ($localMake->models as $localModel) {
                    if (count($localModel->cars) == 0) {
                        $localModel->delete();
                    } else {
                        $carFound = true;
                    }
                }

                if ($carFound === false) {
                    $localMake->delete();
                }
            } else {                
                foreach ($localMake->models as $localModel) {                    
                    if (!isset($indexRemoteMakes[$localMake->code]['models'][$localModel->name])) {                        
                        if (count($localModel->cars) == 0) {
                            echo $localModel->name . "<br>";
                            $localModel->delete();
                        }
                    }                    
                }
            }
        }

        // add local makes
        foreach ($remoteMakes as $remoteMake) {
            if (!isset($indexLocalMakes[$remoteMake->code])) {
                $localMake = new CarMake();
                $localMake->name = $remoteMake->name;
                $localMake->code = $remoteMake->code;
                $localMake->save();

                foreach ($remoteMake->models as $remoteModel) {
                    $localModel = new CarModel();
                    $localModel->name          = $remoteModel->name;
                    $localModel->car_make_id   = $localMake->id;
                    $localModel->save();
                }
            } else {
                $localMake = $indexLocalMakes[$remoteMake->code]['make'];                
                foreach ($remoteMake->models as $remoteModel) {
                    if (!isset($indexLocalMakes[$remoteMake->code]['models'][$remoteModel->name])) {
                        $localModel = new CarModel();
                        $localModel->name          = $remoteModel->name;
                        $localModel->car_make_id   = $localMake->id;
                        $localModel->save();
                    }
                }
            }
        }      

        \DB::commit();
        return redirect('cars/')->with( 'message', 'Car Makes and Models Successfully Synchronised' );
    }
}
