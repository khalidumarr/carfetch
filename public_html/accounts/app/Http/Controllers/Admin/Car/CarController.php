<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarRequest;
use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\CarMake;
use App\Models\CarModel;
use App\Models\CarProp;
use App\Models\CarContainer;
use App\Models\CarPropsValue;
use App\Models\CarCostsValue;
use App\Models\CarRegion;
use App\Models\Dictionary;
use App\Models\Currency;
use App\Models\CarCost;
use App\Models\Upload;
use App\Models\User;
use App\Models\Receivable;
use App\Models\AccountsHead;
use App\Models\Sale;
use App\Http\Criterias\CarSmartCriteria;

class CarController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {        
        $criteria   = new CarSmartCriteria( $request, 50 );
        $data       = [
            'models'        => CarModel::all(),
            'cars'          => $criteria->execute()
        ];

        return view( 'admin/cars/cars/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = [
            'car'           => new Car(),
            'car_regions'   => CarRegion::all(),
            'sale_types'    => Dictionary::getByType( 'sale_types' ),
            'stock_types'   => Dictionary::getByType( 'stock_types' ),
            'currencies'    => Currency::all(),
            'car_models'    => CarModel::all(),
            'car_properties'=> CarProp::all(),
            'car_costs'     => CarCost::all(),
            'agents'        => User::getRoleUsers( 'sale-purchase' )
        ];        
        
        return view( 'admin/cars/cars/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        \DB::beginTransaction();
        $car = new Car();        
        $car->code             = Car::getUniqueCode( $request->title );
        $car->model_id         = $request->model_id;
        $car->year             = $request->year;
        $car->sale_price       = $request->sale_price;
        $car->currency_id      = $request->currency_id;
        $car->sold             = ( $request->sold == 'on' ) ? 1 : 0;        
        $car->updated_by       = \Auth::user()->id;
        
        if( !$car->save() ){
            \DB::rollBack();
            return redirect('/cars')->with( 'errors', 'Problem Adding Car' );
        }

        $props = CarProp::all();        
        foreach( $props as $prop ){
            if( isset( $request->{$prop->code} ) ){
                $propVal = new CarPropsValue();
                $propVal->prop_id           = $prop->id;
                $propVal->car_id            = $car->id;
                $propVal->prop_type_code    = $prop->type->code;
                $propVal->prop_name         = $prop->name;
                $propVal->prop_code         = $prop->code;
                $propVal->value             = $request->{$prop->code};

                if( !$propVal->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'errors', 'Problem Adding ' . $prop->name . ' Property.' );
                }
            }
        }

        $costs = CarCost::all();
        foreach( $costs as $cost ){
            if( isset( $request->{$cost->code} ) ){
                $costVal = new CarCostsValue();
                $costVal->car_id            = $car->id;
                $costVal->cost_id           = $cost->id;
                $costVal->currency_id       = $cost->currency_id;
                $costVal->cost_name         = $cost->name;
                $costVal->cost_code         = $cost->code;
                $costVal->value             = (int)$request->{$cost->code};
                

                if( !$costVal->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'errors', 'Problem Adding ' . $cost->name . ' Cost.' );
                }
            }
        }

        if (isset($request->container_id)) {
            $car->container_id = $request->container_id;
        } else {
            $carContainer = CarContainer::where('number', $request->container_number)->first();
            if (!$carContainer) {
                $carContainer = new CarContainer();
                $carContainer->number          = $request->container_number;            
                $carContainer->declaration     = $request->container_declaration_number;                       

                if( !$carContainer->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'errors', 'Problem Adding Container.' );
                }     
            }

            $car->container_id = $carContainer->id;
        }
        
        if (!$car->save()) {
            \DB::rollBack();
            return redirect('/cars')->with( 'errors', 'Unable to attach Car to the Container.' );
        }

        \DB::commit();

        if( $request->file('image') ){
            foreach( $request->file('image') as $image ){
                if( !$image->isValid() ){
                    return redirect('/cars')->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type = Car::class; 
                $file->uploadable_id = $car->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return redirect('/cars')->with( 'message', 'Car Updated But unable to save the image!' );
                }              
            }
        }
        
        return redirect('/cars')->with( 'message', 'Car Added Successfully' );
               
    }

    public function importCSV(Request $request)
    {
        if( $request->file('file_csv') ){            
            $file = $request->file('file_csv');
            
            $row = 1;
            $results = [];
            if (($handle = fopen($file->path(), "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row++ == 1 || empty($data[0])) {
                        continue;
                    }

                    $results[] = [
                        'container_number' => preg_replace("/[^0-9a-zA-Z]/s", "", $data[0]),
                        'declaration' => preg_replace("/[^0-9a-zA-Z]/s", "", $data[1]),
                        'year' => preg_replace("/[^0-9]/s", "", $data[2]),
                        'model' => preg_replace("/[^0-9a-zA-Z- ]/s", "", $data[3]),
                        'vin_number' => preg_replace("/[^0-9a-zA-Z]/s", "", $data[4]),
                        'invoice_value' => preg_replace("/[^0-9\.]/s", "", $data[5]),
                        'customs' => preg_replace("/[^0-9\.]/s", "", $data[6])
                    ];                    
                }

                fclose($handle);
            }

            \DB::beginTransaction();
            $row = 0;
            foreach ($results as $result) {
                $row++;
                // Duplicating its Data                

                $model = CarModel::where('name', $result['model'])->first();                
                if (!$model) {
                    \DB::rollBack();
                    return redirect('/cars')->with( 'error', $result['model'] . ' Model not found in row # ' . $row );
                }
                
                if (Car::findByVin($result['vin_number'])) {
                    \DB::rollBack();
                    return redirect('/cars')->with( 'error', $result['vin_number'] . ' Car with same VIN number already exists in row # ' . $row );   
                }

                $car = new Car();            
                $car->code             = Car::getUniqueCode( $result['model'] );
                $car->model_id         = $model->id;
                $car->year             = $result['year'];
                $car->sale_price       = 0;

                $currency = Currency::getDefault();
                $car->currency_id      = $currency->id;
                $car->sold             = 0;
                $car->updated_by       = \Auth::user()->id;

                $carContainer = CarContainer::where('number', $result['container_number'])->first();
                if (!$carContainer) {
                    $carContainer = new CarContainer();
                    $carContainer->number          = $result['container_number'];            
                    $carContainer->declaration     = $result['declaration'];

                    if( !$carContainer->save() ){
                        \DB::rollBack();
                        return redirect('/cars')->with( 'errors', 'Problem Adding Container.' );
                    }     
                }

                $car->container_id = $carContainer->id;
                
                if( !$car->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'error', 'Problem Adding Car' );
                }
                
                $prop = CarProp::where('code', 'vin_number')->first();
                $propVal = new CarPropsValue();
                $propVal->prop_id           = $prop->id;
                $propVal->car_id            = $car->id;
                $propVal->prop_type_code    = $prop->type->code;
                $propVal->prop_name         = $prop->name;
                $propVal->prop_code         = $prop->code;
                $propVal->value             = $result['vin_number'];

                if( !$propVal->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'error', 'Problem Adding ' . $prop->name . ' Property in row # ' . $row );
                }

                $cost = CarCost::where('code', 'invoice')->first();
                $costVal = new CarCostsValue();
                $costVal->car_id            = $car->id;
                $costVal->cost_id           = $cost->id;
                $costVal->currency_id       = $cost->currency_id;
                $costVal->cost_name         = $cost->name;
                $costVal->cost_code         = $cost->code;
                $costVal->value             = $result['invoice_value'];

                if( !$costVal->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'errors', 'Problem Adding ' . $cost->name . ' Cost in row # ' . $row );
                }

                $cost = CarCost::where('code', 'customs')->first();
                $costVal = new CarCostsValue();
                $costVal->car_id            = $car->id;
                $costVal->cost_id           = $cost->id;
                $costVal->currency_id       = $cost->currency_id;
                $costVal->cost_name         = $cost->name;
                $costVal->cost_code         = $cost->code;
                $costVal->value             = $result['customs'];

                if( !$costVal->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'errors', 'Problem Adding ' . $cost->name . ' Cost in row # ' . $row );
                }
            }

            \DB::commit();  
        }

        return redirect('/cars')->with( 'message', 'Cars Imported Successfully' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $car = Car::find( $id );
        if( !$car ){
            return redirect( '/cars' )->with( 'errors', 'No car found with provided ID' );
        }

        $data = [
            'car'               => $car,
            'currencies'        => Currency::all(),
            'customers'         => User::getRoleUsers( 'customer' ),
            'payment_methods'   => Dictionary::getByType( 'payment_methods' ),
            'default_currency'  => Currency::getDefault(),
            'payable_heads'     => AccountsHead::getByType( 'garage' ),
            'receivable_heads'  => AccountsHead::getByType( 'receivable-expenses' )
        ];

        return view('admin/cars/cars/view', $data );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {             
        $data = [
            'car'           => Car::find( $id ),
            'car_regions'   => CarRegion::all(),
            'sale_types'    => Dictionary::getByType( 'sale_types' ),
            'stock_types'   => Dictionary::getByType( 'stock_types' ),
            'currencies'    => Currency::all(),
            'car_models'    => CarModel::all(),
            'car_properties'=> CarProp::all(),
            'car_costs'     => CarCostsValue::injectValues( $id, CarCost::all() ),
            'agents'        => User::getRoleUsers( 'sale-purchase' ),
            'containers'    => CarContainer::all()            
        ];

        return view( 'admin/cars/cars/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarRequest $request, $id)
    {                         
        $car = Car::find($id);
        if( !$car ){
            return redirect('/cars')->with( 'errors', 'No Car Found with provided ID' );
        }

        if ($car->sold) {
            return redirect('/cars')->with( 'errors', 'Sold cars cannot be edited' );   
        }

        \DB::beginTransaction();        
        $car->model_id         = $request->model_id;
        $car->year             = $request->year;
        $car->sale_price       = $request->sale_price;
        $car->currency_id      = $request->currency_id;
        $car->container_id     = $request->container_id;                        
        $car->sold             = ( $request->sold == 'on' ) ? 1 : 0;        
        $car->description      = $request->description;
        $car->updated_by       = \Auth::user()->id;        
        
        if( !$car->save() ){
            \DB::rollBack();
            return redirect('/cars')->with( 'errors', 'Problem Adding Car' );
        }

        $props = CarProp::all();
        foreach( $props as $prop ){
            if( isset( $request->{$prop->code} ) ){

                // create or getting existing prop value
                $propVal = $car->properties()->where('prop_code',  $prop->code )->first();

                if( !$propVal ){
                    $propVal = new CarPropsValue();
                }
                                
                $propVal->prop_id           = $prop->id;
                $propVal->car_id            = $car->id;
                $propVal->prop_type_code    = $prop->type->code;
                $propVal->prop_name         = $prop->name;
                $propVal->prop_code         = $prop->code;
                $propVal->value             = $request->{$prop->code};

                if( !$propVal->save() ){
                    \DB::rollBack();
                    return back()->with( 'errors', 'Problem Adding ' . $prop->name . ' Property.' );
                }
            }
        }

        $costs = CarCost::all();
        foreach( $costs as $cost ){
            if( isset( $request->{$cost->code} ) ){

                // create or getting existing cost value
                $costVal = $car->costs()->where('cost_code',  $cost->code )->first();
                if( !$costVal ){
                    $costVal = new CarCostsValue();
                }

                $costVal->car_id            = $car->id;
                $costVal->cost_id           = $cost->id;
                $costVal->currency_id       = $cost->currency_id;
                $costVal->cost_name         = $cost->name;
                $costVal->cost_code         = $cost->code;
                $costVal->value             = $request->{$cost->code};

                if( !$costVal->save() ){
                    \DB::rollBack();
                    return back()->with( 'errors', 'Problem Adding ' . $cost->name . ' Cost.' );
                }
            }
        }

        \DB::commit();

        if( $request->file('image') ){            
            foreach( $request->file('image') as $image ){
                if( !$image->isValid() ){
                    return redirect('/cars')->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type = Car::class; 
                $file->uploadable_id = $car->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return back()->with( 'message', 'Car Updated But unable to save the image!' );
                }              
            }
        }

        return back()->with( 'message', 'Car Updated Successfully' );        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();

        if( \Gate::denies('delete-car') ){
            return back()->with('errors', 'You are not allowed to delete car');
        }

        $car = Car::find($id);
        if( $car ){
            if( $car->delete() ){
                $carProps = CarPropsValue::where( 'car_id', $car->id );
                if( $carProps ){
                    if( $carProps->delete() ){
                        $carCosts = CarCostsValue::where( 'car_id', $car->id );
                        if( $carCosts ){
                            if( $carCosts->delete() ){
                                $carPhotos = Upload::where( 'uploadable_id', $car->id )->where( 'uploadable_type', Car::class );                                
                                if( $carPhotos->count() ){
                                    if( $carPhotos->delete() ){
                                        \DB::commit();
                                        return redirect('/cars')->with( 'message', 'Car Deleted Successfully' ); 
                                    }

                                    \DB::rollBack();
                                    return redirect('/cars')->with( 'errors', 'Unable to Delete Car Pictures' );                            
                                }

                                \DB::commit();
                                return redirect('/cars')->with( 'message', 'Car Deleted Successfully' );
                            }

                            \DB::rollBack();
                            return redirect('/cars')->with( 'errors', 'Unable to Delete Car Costs' );                        
                        }

                        \DB::commit();
                        return redirect('/cars')->with( 'message', 'Car Deleted Successfully' );                               
                    }

                    \DB::rollBack();
                    return redirect('/cars')->with( 'errors', 'Unable to Delete Car Porperties' ); 
                }

                \DB::commit();
                return redirect('/cars')->with( 'message', 'Car Deleted Successfully' );
            }

            \DB::rollBack();
            return redirect('/cars')->with( 'errors', 'Problem Deleting Car' );
        }

        return redirect('/cars')->with( 'errors', 'No Car Found with provided ID' );
    }

    public function uploadPictures( Request $request, $id ){
        $car = Car::find($id);
        if( $request->file('image') ){

            foreach( $request->file('image') as $image ){                 
                if( !$image->isValid() ){
                    return back()->with( 'errors', 'Invalid Image Selected!' );
                }

                $path = $image->store('uploads');

                $file = new Upload();
                $file->uploadable_type = Car::class; 
                $file->uploadable_id = $car->id; 
                $file->name = str_replace( 'uploads/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return back()->with( 'errors', 'Unable to save the image!' );
                }              
            }

            return back()->with( 'message', 'Image Added Successfully' ); 
        }

        return back()->with( 'errors', 'Please select the image' );
    }

    public function updateSort( Request $request ){
        if( Upload::reArrange( $request->get('image_sort') ) ){
            return 'success';
        }

        return 'failure';
    }

    public function import(Request $request)
    {
        if ($cars = $request->cars) {            
            foreach ($cars as $id => $state) {
                $originalCar = Car::on('main')->find($id);                

                // Duplicating its Data
                \DB::beginTransaction();

                $model = CarModel::where('name', $originalCar->model->name)->first();                
                if (!$model) {
                    \DB::rollBack();
                    return redirect('/cars')->with( 'error', 'Problem Adding Car, Try to sync models' );
                }

                $originalVinNumber = $originalCar->propByCode('vin_number')->value;
                if (Car::findByVin($originalVinNumber)) {
                    return redirect('/cars')->with( 'error', 'Car with same VIN number already exists' );   
                }

                $car = new Car();            
                $car->code             = Car::getUniqueCode( $originalCar->title );
                $car->model_id         = $model->id;
                $car->year             = $originalCar->year;
                $car->sale_price       = $originalCar->sale_price;
                $car->currency_id      = $originalCar->currency_id;                
                $car->sold             = 0;
                $car->updated_by       = \Auth::user()->id;
                
                if( !$car->save() ){
                    \DB::rollBack();
                    return redirect('/cars')->with( 'error', 'Problem Adding Car' );
                }

                $props = CarProp::all();        
                foreach( $props as $prop ){                    
                    foreach ($originalCar->properties as $originalProp) {
                        if ($prop->code == $originalProp->prop_code) {
                            $propVal = new CarPropsValue();
                            $propVal->prop_id           = $prop->id;
                            $propVal->car_id            = $car->id;
                            $propVal->prop_type_code    = $prop->type->code;
                            $propVal->prop_name         = $prop->name;
                            $propVal->prop_code         = $prop->code;
                            $propVal->value             = $originalProp->value;

                            if( !$propVal->save() ){
                                \DB::rollBack();
                                return redirect('/cars')->with( 'error', 'Problem Adding ' . $prop->name . ' Property.' );
                            }
                        }
                    }                    
                }

                foreach ($originalCar->images as $image) {
                    $path = '../../storage/app/uploads/' . $image->name;
                    if (file_exists($path)) {                        
                        $newPath = '../storage/app/uploads/'. $image->name;
                        copy($path, $newPath);

                        $file = new Upload();
                        $file->uploadable_type = Car::class; 
                        $file->uploadable_id = $car->id; 
                        $file->name = $image->name;
                        $file->url = $image->url;
                        if( !$file->save() ){
                            return redirect('/cars')->with( 'message', 'Car Updated But unable to save the image!' );
                        }
                    }                    
                }                
                \DB::commit();                
            }
        }
        
        return redirect('/cars')->with( 'message', 'Car Imported Successfully' );
    }
}
