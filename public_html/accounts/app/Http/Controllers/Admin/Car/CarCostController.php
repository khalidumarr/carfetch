<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarCostRequest;
use App\Http\Controllers\Controller;
use App\Models\CarCost;
use App\Models\Currency;

class CarCostController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $sort       = ( $request->get('sort_key') ) ? $request->get('sort_key') : $this->sortColumn;
        $direction  = ( $request->get('sort_direction') ) ? $request->get('sort_direction') : $this->sortDirection;
        $costs      = CarCost::orderBy( $sort, $direction )->paginate( self::RESULTS_PER_PAGE );

        $data       = [
            'costs'     => $costs     
        ];

        return view( 'admin/cars/costs/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currencies = Currency::all();
        return view( 'admin/cars/costs/add', [ 'cost' => new CarCost(), 'currencies' => $currencies ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarCostRequest $request)
    {
        $cost = new CarCost();
        $cost->name             = $request->name;
        $cost->code             = $request->code;
        $cost->currency_id      = $request->currency_id;
        $cost->default_value    = $request->default_value;
        $cost->is_required      = ( $request->is_required == 'on' ) ? 1 : 0;
        $cost->in_invoice      = ( $request->in_invoice == 'on' ) ? 1 : 0;
        $cost->is_vat      = ( $request->is_vat == 'on' ) ? 1 : 0;

        if( $cost->save() ){
            return redirect('/car-costs')->with( 'message', 'Car cost Added Successfully' );
        }
        else{
            return redirect('/car-costs')->with( 'errors', 'Problem Adding Car cost' );
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cost       = CarCost::find( $id );
        $currencies = Currency::all();
        return view( 'admin/cars/costs/edit', [ 'cost' => $cost, 'currencies' => $currencies ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarCostRequest $request, $id)
    {
        $cost = CarCost::find($id);
        if( $cost ){
            $cost->name         = $request->name;
            $cost->code         = $request->code;
            $cost->currency_id  = $request->currency_id;
            $cost->default_value= $request->default_value;
            $cost->is_required  = ( $request->is_required == 'on' ) ? 1 : 0;
            $cost->in_invoice      = ( $request->in_invoice == 'on' ) ? 1 : 0;
            $cost->is_vat      = ( $request->is_vat == 'on' ) ? 1 : 0;
            
            if( $cost->save() ){
                return redirect('/car-costs')->with( 'message', 'Car cost Updated Successfully!' );
            }
            else{
                return redirect('/car-costs')->with( 'errors', 'Problem Updating Car cost' );
            }
        }

        return redirect('/car-costs')->with( 'errors', 'No Car cost Found with provided ID' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cost = CarCost::find($id);
        if( $cost ){
            if( $cost->delete() ){
                return redirect('/car-costs')->with( 'message', 'Car cost Deleted Successfully' );                
            }

            return redirect('/car-costs')->with( 'errors', 'Problem Deleting Car cost' );
        }

        return redirect('/car-costs')->with( 'errors', 'No Car cost Found with provided ID' );
    }
}
