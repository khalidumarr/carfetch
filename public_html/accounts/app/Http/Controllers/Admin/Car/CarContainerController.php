<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Requests\CarContainerRequest;
use App\Http\Controllers\Controller;
use App\Models\CarContainer;
use App\Models\User;
use App\Models\Currency;
use App\Models\AccountsHead;
use App\Models\Car;
use App\Models\Payable;
use App\Http\Criterias\CarContainerCriteria;

class CarContainerController extends Controller
{
    const RESULTS_PER_PAGE  = 50;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $criteria   = new CarContainerCriteria( $request );   

        $containers = $criteria->execute();        
        $totalAmount = 0;
        $totalAmountPaid = 0;

        foreach ($containers as $container) {
            if ($container->payable) {
                $totalAmount += $container->payable->value;                    
            }                    
        }
        $data       = [
            'containers'        => $containers,
            'totalAmount'       => $totalAmount,            
            'shippers'          => User::getRoleUsers( 'shipper' ),
        ];

        return view( 'admin/cars/containers/list', $data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'shippers'  => User::getRoleUsers( 'shipper' ),
            'container' => new CarContainer(),
            'currency'=> Currency::where('code', '=', 'AED')->first(),
            'cars'      => Car::getNonContainedCars()
        ];

        return view( 'admin/cars/containers/add', $data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarContainerRequest $request)
    {        
        \DB::beginTransaction();
        $container = new CarContainer();
        $container->number          = $request->number;
        $container->shipper_id      = $request->shipper_id;
        $container->declaration      = $request->declaration;
        $container->arrival_date    = date( 'Y-m-d H:i:s' ,strtotime( $request->arrival_date ) );        
        $container->currency_id     = $request->currency_id;        

        if( $container->save() ){
            if ($request->amount) {

                if (!$request->shipper_id) {
                    return redirect('/car-containers')->with( 'errors', 'Shipper is mandatory in order to generate payable' );
                }

                $accountsHead = AccountsHead::where('code', '=', 'container-charges')->first();   
                $currency = Currency::where('code', '=', 'AED')->first();

                $payable = new Payable();
                $payable->head_id = $accountsHead->id;
                $payable->debit = 0;
                $payable->user_id = $request->shipper_id;
                $payable->value = $request->amount;
                $payable->currency_id = $currency->id;
                $payable->created_by = \Auth::user()->id;
                
                if (!$payable->save()) {
                    return redirect('/car-containers')->with( 'errors', 'Unable to add payable to container' );
                }
                
                $container->payable_id = $payable->id;            

                if (!$container->save()) {
                    return redirect('car-containers/')->with( 'message', 'Unable to add paybale to container' );
                }
            }

            \DB::commit();
            return redirect('car-containers/')->with( 'message', 'Car Container Added Successfully' );
        }
        else{
            \DB::rollBack();
            return redirect('car-containers')->with( 'errors', 'Problem Adding Car Container' );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'shippers'           => User::getRoleUsers( 'shipper' ),
            'container'          => CarContainer::find( $id ),
            'currency'         => Currency::where('code', '=', 'AED')->first(),
            'cars'               => Car::where('container_id', null)->get(),
            'cars_ids'           => CarContainer::getCarsIds( $id )
        ];

        return view( 'admin/cars/containers/view', $data );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $data = [
            'shippers'           => User::getRoleUsers( 'shipper' ),
            'container'          => CarContainer::find( $id ),
            'currency'           => Currency::where('code', '=', 'AED')->first(),
            'cars'               => Car::where('container_id', null)->get(),
            'cars_ids'           => CarContainer::getCarsIds( $id )
        ];

        return view( 'admin/cars/containers/edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarContainerRequest $request, $id)
    {        
        \DB::beginTransaction();
        $container = CarContainer::find($id);
        $container->number          = $request->number;
        $container->shipper_id      = $request->shipper_id;
        $container->declaration     = $request->declaration;
        $container->arrival_date    = date( 'Y-m-d H:i:s' ,strtotime( $request->arrival_date ) );                

        if( $container->save() ){            
            if ($request->amount) {
                if (!$request->shipper_id) {
                    return redirect('/car-containers')->with( 'errors', 'Shipper is mandatory in order to generate payable' );
                }

                $accountsHead = AccountsHead::where('code', '=', 'container-charges')->first();   
                $currency = Currency::where('code', '=', 'AED')->first();

                if ($container->payable) {
                    $payable = $container->payable;
                } else {
                    $payable = new Payable();
                }
                
                $payable->head_id = $accountsHead->id;
                $payable->debit = 0;
                $payable->value = $request->amount;
                $payable->user_id = $request->shipper_id;
                $payable->currency_id = $currency->id;
                $payable->created_by = \Auth::user()->id;
                $payable->save();

                $container->payable_id = $payable->id;

                if (!$container->save()) {
                    return redirect('car-containers/')->with( 'message', 'Unable to add paybale to container' );
                }
            }

            \DB::commit();
            return redirect('/car-containers/')->with( 'message', 'Car Container updated Successfully' );
        }
        else{
            \DB::rollBack();
            return redirect('/car-containers')->with( 'errors', 'Problem updating Car Container' );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $container = CarContainer::find($id);
        if( $container ){
            if( $container->delete() ){
                return redirect('car-containers')->with( 'message', 'Car Container Deleted Successfully' );                
            }

            return redirect('car-containers')->with( 'errors', 'Problem Deleting Car Container' );
        }

        return redirect('car-containers')->with( 'errors', 'No Car Container Found with provided ID' );
    }
}
