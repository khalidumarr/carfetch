<?php

namespace App\Http\Controllers\Admin\Sale;

use Illuminate\Http\Request;
use App\Http\Requests\SaleRequest;
use App\Http\Requests\PayRemainingRequest;
use App\Http\Requests\SaleApproveAmountRequest;
use App\Http\Controllers\Controller;
use App\Events\UserCreated;
use App\Events\CarSold;
use App\Models\Currency;
use App\Models\Sale;
use App\Models\Upload;
use App\Models\Notification;
use App\Models\Receivable;
use App\Models\User;
use App\Models\Role;
use App\Models\AccountsHead;
use App\Models\CarRegion;
use App\Models\Car;
use App\Models\CarCost;
use App\Models\SalesPayment;
use App\Models\Dictionary;
use App\Models\Country;
use App\Models\CarCostsValue;
use App\Http\Criterias\SaleCriteria;
use NumberToWords\NumberToWords;

class IndexController extends Controller
{
    const VAT_FACTOR = 0.05;
    const RESULTS_PER_PAGE  = 10;
    public $sortColumn     = 'id';
    public $sortDirection  = 'desc';

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $criteria   = new SaleCriteria( $request );

        if (!$request->get('date_from') || !$request->get('date_to')) {
            $dateFrom = date('Y-m-d', strtotime('-30 days'));
            $dateTo = date('Y-m-d H:i:s');

            $request->merge(['date_from' => $dateFrom, 'date_to' => $dateTo]);
            $request->session()->now('notice', 'Showing 30 days sales, Use Filters to view your desired Record');

            $_REQUEST['date_from'] = $dateFrom;
            $_REQUEST['date_to'] = $dateTo;
        }    

        $sales = $criteria->execute();

        $total = 0;
        $totalVat = 0;

        foreach ($sales as $sale) {            
            $total += $sale->price;
            $totalVat += $sale->vat;
        }        

        $data       = [
            'sales'     => $sales,
            'total'     => $total,
            'totalVat'  => $totalVat            
        ];        

        return view( 'admin/sales/list', $data );
    }

    public function show(Request $request, $id)
    {
        $data = [
            'sale' => Sale::find($id)
        ];
        
        return view('admin/sales/show', $data);
    }

    public function uploadDocuments(Request $request, $id)
    {
        $sale = Sale::find($id);

        if (!$sale) {
            return redirect('/sale')->with( 'error', 'No sale found with provided ID' );
        }

        if( $request->file('documents') ){
            foreach( $request->file('documents') as $document ){
                if( !$document->isValid() ){
                    return redirect('/cars')->with( 'errors', 'Invalid Document Type Selected!' );
                }

                $path = $document->store('sales_documents');

                $file = new Upload();
                $file->uploadable_type = Sale::class; 
                $file->uploadable_id = $sale->id; 
                $file->name = str_replace( 'sales_documents/', '', $path ); 
                $file->url = '/' . $path; 
                if( !$file->save() ){
                    return redirect('/sale/' . $id . '/show')->with( 'message', 'Unable to upload one of the sales documents' );
                } elseif(!isset($notification)) {
                    $notification = Notification::where('sale_id','=', $sale->id)->first();
                    if ($notification) {            
                        $notification->state = 'disabled';
                        $notification->save();            
                    }
                }                              
            }

            return redirect('/sale/' . $id . '/show')->with( 'message', 'Documents are Successfully updated on sale.' );        
        } else {
            return redirect('/sale/' . $id . '/show')->with( 'error', 'No document selected' );        
        }
    }

    public function create(Request $request, $id)
    {               
        $inventory = Car::find($id);        
        if ($inventory->is_sold) {            
            return redirect('/sale')->with( 'error', 'Car is already sold' );
        }

        $data       = [
            'inventory'     => $inventory,
            'inventory_id' => $id,
            'costs' => CarCostsValue::injectValues($id, $inventory->costs),
            'vat_factor' => self::VAT_FACTOR,
            'countries' => Country::all()
        ];

        return view( 'admin/sales/index', $data );   
    }

    public function store(Request $request)
    {                          
        $user = User::where('phone', '=', $request->customer_phone)->first();
        if (!$user) {
            \DB::beginTransaction();
            $user = new User();
            $user->name     = $request->customer_name;
            $user->email    = 'john' . time() . '@doe.com';
            $user->phone    = $request->customer_phone;
            $user->address  = 'united arab emirates';
            $user->active   = 1;
            $user->password = bcrypt( '123456' );        

            if( $user->save() ){
                $role = Role::where('code', 'customer')->first();
                if( !$user->roles()->sync( [$role->id] ) ){
                    \DB::rollBack();                
                    return back()->with( ['error' => 'Roles cannot be assigned to the user'] );
                }            

                event( new UserCreated( $user ) );                                
            }
            else{
                \DB::rollBack();
                return back()->with( ['error' => 'Unable to create user'] );
            }

            $user_id = $user->id;
        } else {
            $user_id = $user->id;
        }

        $car = Car::find($request->inventory);
        if( !$car ){
            return back()->with( ['error' => 'No Car Found with provided ID'] );
        }

        $car->sold = 1;
        if( !$car->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to mark car as sold' );
        }

        $sale_price = 0;
        $vat = 0;
        switch ($request->sale_type) {
            case 'export':
                $sale_price = $request->export_sale_price;   
                if (!$sale_price) {
                    return back()->with( 'error', 'Specify Sale Price' );
                }
            break;

            case 'service':
                $costs = CarCost::all();
                foreach( $costs as $cost ){
                    if( isset( $request->{$cost->code} ) && $cost->in_invoice ){
                        // create or getting existing cost value
                        $costVal = $car->costs()->where('cost_code',  $cost->code )->first();                        
                        $costVal->value           = $request->{$cost->code};
                        $costVal->vat             = $request->{$cost->code . '_vat'};
                        $sale_price               += $costVal->value;

                        if( !$costVal->save() ){
                            \DB::rollBack();
                            return back()->with( 'error', 'Problem Adding ' . $cost->name . ' Cost.' );
                        }

                        if ($cost->is_vat) {
                            $vat += $costVal->vat;
                        }
                    }
                } 

                if (!$sale_price) {
                    return back()->with( 'error', 'Specify Sale Price' );
                }               
            break;

            case 'general':
                $sale_price = $request->general_sale_price;
                $vat = $this->calculateVat($sale_price);

                if (!$sale_price) {
                    return back()->with( 'error', 'Specify Sale Price' );
                }
            break;

            default:
                if ($request->notify) {
                    $sale_price = $request->general_sale_price;
                    $notification = new Notification();
                    $notification->product_id = $car->id;
                    $notification->state = 'active';
                    $notification->type = Notification::TYPE_SALE_NOTIFICATION;
                    $notification->description = $request->notify_description;
                    $notification->save();  
                }
            break;
        }

        $saleType = Dictionary::getByType('sale_types',$request->sale_type)->first();
        $paymentMethod = Dictionary::getByType('payment_methods', 'cash')->first();        

        $currency = Currency::where('code', 'AED')->first()->id;
        $sale                           = new Sale();
        $sale->price                    = $sale_price;
        $sale->vat                      = $vat;
        $sale->currency_id              = $currency;
        $sale->car_id                   = $request->inventory;
        $sale->sold_to                  = $user_id;
        $sale->sold_by                  = \Auth::id();
        $sale->number                   = Sale::getUniqueNumber();
        $sale->manual_bill_number       = '123456';
        $sale->cx_name                  = $user->name;
        $sale->cx_phone                 = $user->phone;
        $sale->cx_city                  = CarRegion::getDefaultRegion()->id;
        $sale->cx_country               = $request->customer_country;
        $sale->cx_trn                   = $request->customer_trn;        
        $sale->amount_paid              = $sale_price;
        $sale->payment_method           = $paymentMethod->id;
        $sale->sale_type_id             = $saleType->id;
        $sale->created_at               = date('Y-m-d H:i:s', strtotime($request->sale_date));
        
        if( !$sale->save() ){
            \DB::rollBack();
            return back()->with( 'error', 'Unable to save Sale' );
        }    

        // generating notification
        if ($request->sale_type == 'export') {
            $notification = new Notification();
            $notification->sale_id = $sale->id;
            $notification->state = 'active';
            $notification->type = Notification::TYPE_EXPORT_DOC;
            $notification->description = 'Pending Export papers';
            if (!$notification->save()) {
                return back()->with( 'error', 'Unable to genrate notification' );
            }
        } 

        \DB::commit();
        return redirect('sale/' . $sale->id . '/generate-invoice')->with( 'message', 'Car Sold Successfully' );
    }

    private function calculateVat($price)
    {
        return ceil(($price * self::VAT_FACTOR));
    }

    public function generateInvoice(Request $request, $id)
    {        
        $sale = Sale::find($id);
        $totalValue = 0;
        $totalVat = 0;
        $total = 0;

        foreach ($sale->car->costs as $cost) {
            $totalValue += $cost->value;
            $totalVat += $cost->vat;
            $total += ( $cost->value + $cost->vat );
        }

        $data = [
            'sale' => $sale,
            'total' => $total,
            'totalVat' => $totalVat,
            'totalValue' => $totalValue
        ];

        return view( 'admin/sales/invoice/' . $sale->saleType->code, $data );
    }

    public function delete(Request $request, $id)
    {
        $sale = Sale::find($id);
        $car = $sale->car;
        $car->sold = 0;
        $car->save();
        
        if ($notification = $sale->notification) {
            $notification->delete();
        }                

        $sale->delete();


        return redirect('sale')->with( 'message', 'Transaction Successfully Deleted' );;
    }
}