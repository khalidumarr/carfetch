<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/admin/uploads/*',
        '/admin/banners*',
        '/admin/cars/update-sort*',
        'sale*',
        '/uploads/*',
        '/cars/import',
        '/cars/import-csv',
        '/cars/sync-models',
        '/api/container/*'
    ];
}
