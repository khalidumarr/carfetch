<?php

namespace App\Http\Criterias;
use App\Models\Car;
use App\Models\CarModel;
use App\Models\CarMake;

class CarSmartCriteria extends BaseCriteria{	
	const RESULTS_PER_PAGE = 50;
	const DEFAULT_SORT_FIELD = 'id';
	const DEFAULT_SORT_DIRECTION = 'desc';
	public function __construct( $request, $perPage = '' ){
		$this->request = $request;
		$this->perPage = ($perPage) ? $perPage : self::RESULTS_PER_PAGE;
	}

	public function execute($connection = ''){
		$searchFields = $this->getSearchFields();		
		$entity = new Car();

		if ($connection) {
			$entity = $entity->setConnection($connection);
		}

		if( !empty( $searchFields['search-keyword'] ) ){			
			// Make
			if ($make = CarMake::where( 'name', 'like', '%' . $searchFields['search-keyword'] . '%' )->first()) {
				$models = $make->models;
				foreach ($models as $model) {
					$entity = $entity->orWhere( 'model_id', $model->id );					
				}
			}			

			// Model
			if ($model = CarModel::where( 'name', $searchFields['search-keyword'] )->first()) {
				$entity = $entity->orWhere( 'model_id', $model->id );
			}			

			// VIN			
			$entity = $entity->orWhereHas('properties', function($query) use ($searchFields){
				$query->where( 'prop_code', 'vin_number' )->where( 'value', 'like', '%' . $searchFields['search-keyword'] . '%' );
			});

			// LOT NUMBER
			$entity = $entity->orWhereHas('properties', function($query) use ($searchFields){
				$query->where( 'prop_code', 'lot_number' )->where( 'value', 'like', '%' . $searchFields['search-keyword'] . '%' );
			});

			$entity = $entity->orWhere( 'year', $searchFields['search-keyword'] );						
		}		

		if( isset( $searchFields['sold'] ) ){
			$searchFields['sold'] = ($searchFields['sold'] == 'on') ? 1 : 0;
			$entity = $entity->where( 'sold', $searchFields['sold'] );
		}
				
		$sortFields = $this->getSortFields();
		if( !empty( $sortFields ) ){
			foreach( $sortFields as $field => $direction ){
				$entity = $entity->orderBy( $field, $direction );
			}
		}
		else{
			$entity = $entity->orderBy( self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION );
		}
		
		return $entity->paginate( $this->perPage );
	}
}