<?php

namespace App\Http\Criterias;
use App\Models\Expense;
use App\Models\Role;

class ExpenseCriteria extends BaseCriteria{
	const RESULTS_PER_PAGE = 50;
	const DEFAULT_SORT_FIELD = 'id';
	const DEFAULT_SORT_DIRECTION = 'desc';
	public function __construct( $request ){
		$this->request = $request;
	}

	public function execute(){
		$searchFields = $this->getSearchFields();		
		$entity = new Expense();

		if( !empty( $searchFields ) ){			
			if( isset( $searchFields['date_from'] ) ){				
				$entity = $entity->where( 'created_at', '>=' , date('Y-m-d H:i:s', strtotime($searchFields['date_from'])) );
			}

			if( isset( $searchFields['date_to'] ) ){
				$entity = $entity->where( 'created_at', '<=' , date('Y-m-d H:i:s', strtotime($searchFields['date_to'])) );
			}			
		}		
				
		$sortFields = $this->getSortFields();
		if( !empty( $sortFields ) ){
			foreach( $sortFields as $field => $direction ){
				$entity = $entity->orderBy( $field, $direction );
			}
		}
		else{
			$entity = $entity->orderBy( self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION );
		}
		
		return $entity->get();
	}
}