<?php

namespace App\Http\Criterias;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;

class UserCriteria extends BaseCriteria{
	const RESULTS_PER_PAGE = 50;
	const DEFAULT_SORT_FIELD = 'id';
	const DEFAULT_SORT_DIRECTION = 'desc';
	public function __construct( $request ){
		$this->request = $request;
	}

	public function execute(){
		$searchFields = $this->getSearchFields();		
		$entity = new User();

		if( !empty( $searchFields ) ){
			if( isset( $searchFields['name'] ) ){
				$entity = $entity->where( 'name', 'like' , '%' . $searchFields['name'] . '%' );
			}

			if( isset( $searchFields['email'] ) ){
				$entity = $entity->where( 'email', 'like' , '%' . $searchFields['email'] . '%' );
			}

			if( isset( $searchFields['phone'] ) ){
				$entity = $entity->where( 'phone', 'like' , '%' . $searchFields['phone'] . '%' );
			}

			if( isset( $searchFields['role'] ) ){
				$entity = $entity->whereHas('roles', function($query) use ($searchFields){
					$query->where( 'role_id', $searchFields['role'] );
				});
			}
		}

		if( in_array( \Auth::user()->getApplicableRole()->code,[ 'sale-purchase', 'sales', 'repairing' ] ) ){
            $role = Role::where( 'code', 'customer' )->first();
	        $userRoles = UserRole::where( 'role_id', $role->id )->get();
	        $userIds = [];
	        foreach( $userRoles as $userRole ){
	            $userIds[] = $userRole->user_id;
	        }

	        $entity = $entity->whereIn( 'id', $userIds );
        }
				
		$sortFields = $this->getSortFields();
		if( !empty( $sortFields ) ){
			foreach( $sortFields as $field => $direction ){
				$entity = $entity->orderBy( $field, $direction );
			}
		}
		else{
			$entity = $entity->orderBy( self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION );
		}
		
		return $entity->paginate( self::RESULTS_PER_PAGE );
	}
}