<?php

namespace App\Http\Criterias;
use App\Models\User;
use App\Models\Payable;

class PayableCriteria extends BaseCriteria{
	const RESULTS_PER_PAGE = 50;
	const DEFAULT_SORT_FIELD = 'id';
	const DEFAULT_SORT_DIRECTION = 'desc';
	public function __construct( $request ){
		$this->request = $request;
	}

	public function execute(){
		$searchFields = $this->getSearchFields();		
		$entity = new Payable();

		if( !empty( $searchFields ) ){			
			if( isset( $searchFields['date_from'] ) ){				
				$entity = $entity->where( 'created_at', '>=' , date('Y-m-d H:i:s', strtotime($searchFields['date_from'])) );
			}

			if( isset( $searchFields['date_to'] ) ){
				$entity = $entity->where( 'created_at', '<=' , date('Y-m-d H:i:s', strtotime($searchFields['date_to'])) );
			}

			// if( isset( $searchFields['user_id'] ) ){
			// 	$entity = $entity->whereHas( 'user', function($query) use ($searchFields){
			// 		$query->where( 'user_id', User::find($searchFields['user_id'])->first()->id );
			// 	});
			// }		
		}		
				
		$sortFields = $this->getSortFields();
		if( !empty( $sortFields ) ){
			foreach( $sortFields as $field => $direction ){
				$entity = $entity->orderBy( $field, $direction );
			}
		}
		else{
			$entity = $entity->orderBy( self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION );
		}
		
		return $entity->get();
	}
}