<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch( $this->method() ){
            case 'GET':break;

            case 'DELETE': {
                return [
                    'id' => 'required'
                ];
            }


            case 'POST': {
                return [
                    'head_id'    => 'required',
                    'value'         => 'required',
                    'currency_id'   => 'required'
                ];
            }


            case 'PUT':
            case 'PATCH': {
                return [
                    'head_id'    => 'required',
                    'value'         => 'required',
                    'currency_id'   => 'required'
                ];
            }

            default:break;
        }
    }
}
