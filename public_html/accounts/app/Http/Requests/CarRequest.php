<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\CarProp;
use App\Models\CarCost;
use App\Models\Car;
use Illuminate\Http\Request;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch( $this->method() ){
            case 'GET':break;

            case 'DELETE': {
                return [
                    'id' => 'required'
                ];
            }


            case 'POST': {                              
                $fields = [                    
                    'model_id'          => 'required',
                    'year'              => 'required|integer|min:4',                                        
                ];

                $props = CarProp::all();
                foreach( $props as $prop ){
                    if( $prop->is_required == 1 ){
                        $fields[$prop->code] = 'required';                        
                    }

                    if( in_array( $prop->code, [ 'vin_number', 'lot_number' ] ) ){
                        $fields[$prop->code] .= '|unique:car_props_values,value';
                    }
                }

                $costs = CarCost::all();
                foreach( $costs as $cost ){
                    if( $cost->is_required == 1 ){
                        $fields[$cost->code] = 'required';                        
                    }
                }
                
                return $fields;
            }


            case 'PUT':
            case 'PATCH': {                
                 $fields = [                    
                    'model_id'          => 'required',
                    'year'              => 'required|integer|min:4',                                        
                ];

                $props = CarProp::all();
                foreach( $props as $prop ){
                    if( in_array( $prop->code, [ 'vin_number', 'lot_number' ] ) ){
                        $fields[$prop->code] = 'unique:car_props_values,value,' . Car::find( $this->route('car') )->propByCode( $prop->code )->id;
                    }

                    if( $prop->is_required == 1 ){
                        if( isset( $fields[$prop->code] ) ){
                            $fields[$prop->code] .= '|required';                                         
                        }
                        else{
                            $fields[$prop->code] = 'required';                                            
                        }
                    }                    
                }

                $costs = CarCost::all();
                foreach( $costs as $cost ){
                    if( $cost->is_required == 1 ){
                        $fields[$cost->code] = 'required';                        
                    }
                }

                return $fields;
            }

            default:break;
        }
    }
}
