<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch( $this->method() ){
            case 'GET':break;

            case 'DELETE': {
                return [
                    'id' => 'required'
                ];
            }


            case 'POST': {
                return [
                    'car_id'            => 'required',
                    'amount'            => 'required',
                    'received_amount'   => 'required',
                    'payment_method'    => 'required',
                    'currency_id'       => 'required',
                    'sold_to'           => 'required'
                ];
            }


            case 'PUT':
            case 'PATCH': {
                return [
                    'car_id'            => 'required',
                    'amount'            => 'required',
                    'received_amount'   => 'required',
                    'currency_id'       => 'required',
                    'payment_method'    => 'required',
                    'sold_to'           => 'required'
                ];
            }

            default:break;
        }
    }
}
