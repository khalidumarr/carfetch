-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2017 at 11:55 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carfetch`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts_heads`
--

CREATE TABLE `accounts_heads` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(250) NOT NULL,
  `code` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `is_reoccurring` tinyint(3) NOT NULL DEFAULT '0',
  `default_value` varchar(10) DEFAULT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts_heads`
--

INSERT INTO `accounts_heads` (`id`, `title`, `code`, `description`, `is_reoccurring`, `default_value`, `currency_id`, `created_at`, `updated_at`) VALUES
(1, 'Cash Released for Repairing', 'cash-released-for-repairing', 'when the cash is given to repair team for fixing the damaged carsss', 0, NULL, 1, '2017-02-22 15:52:09', '2017-02-22 11:52:09'),
(4, 'Car Expense Added', 'car-expense-added', 'When repair team buy something and add expense.', 0, NULL, 1, '2017-02-22 17:20:39', '0000-00-00 00:00:00'),
(5, 'Container Charges', 'container-charges', 'Each container contains 4 cars, Payment is done in bulk', 0, NULL, 1, '2017-02-22 14:40:30', '2017-02-22 14:40:30'),
(6, 'car sold', 'car-sold', 'when car is sold to customer', 0, NULL, 1, '2017-02-22 17:51:25', '2017-02-22 17:51:25'),
(7, 'Yard Rent', 'yard-rent', 'showroom rent', 1, '3000', 1, '2017-02-23 15:02:18', '2017-02-23 15:02:18'),
(8, 'Security Charges', 'security-charges', 'Security Guard for yard', 0, NULL, 1, '2017-02-23 15:50:42', '2017-02-23 15:50:42'),
(9, 'Refund Received From Repair', 'refund-received-from-repair', 'When repair team refund the amount give to them for repairing', 0, NULL, 1, '2017-02-25 08:46:15', '2017-02-25 08:46:15'),
(10, 'Agent earnings Sajjad', 'agent-1-earnings', 'Agent''s commision', 0, NULL, 1, '2017-03-12 09:13:12', '2017-03-12 08:13:12'),
(11, 'Agent Earnings sales and purchase', 'agent-8-earnings', 'agent earnings', 0, NULL, 1, '2017-03-12 10:37:38', '2017-03-12 10:37:38'),
(16, 'Agent Earnings Arslan', 'agent-13-head', 'Agents Earnings', 0, NULL, 1, '2017-03-12 12:23:30', '2017-03-12 12:23:30'),
(17, 'Garage Fee', 'garage-fee', 'Garage Fee', 0, NULL, 1, '2017-03-13 11:55:57', '2017-03-13 11:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) NOT NULL,
  `code` varchar(250) NOT NULL,
  `model_id` int(11) UNSIGNED NOT NULL,
  `year` varchar(4) NOT NULL,
  `show_on_site` tinyint(3) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `sale_price` varchar(10) DEFAULT NULL,
  `sold` tinyint(3) NOT NULL,
  `description` text NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL,
  `trending` tinyint(3) UNSIGNED NOT NULL,
  `source_id` int(11) UNSIGNED NOT NULL,
  `destination_id` int(11) UNSIGNED NOT NULL,
  `sale_type_id` int(11) UNSIGNED NOT NULL,
  `stock_type_id` int(11) UNSIGNED NOT NULL,
  `agent_id` int(11) UNSIGNED NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `container_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `title`, `code`, `model_id`, `year`, `show_on_site`, `updated_by`, `sale_price`, `sold`, `description`, `featured`, `trending`, `source_id`, `destination_id`, `sale_type_id`, `stock_type_id`, `agent_id`, `currency_id`, `container_id`, `created_at`, `updated_at`) VALUES
(65, 'Ford Mustang White Color', 'ford-mustang-white-color', 5, '2013', 1, 6, '75000', 1, 'Best car in town', 0, 0, 2, 4, 5, 3, 8, 1, 1, '2017-03-13 08:07:30', '2017-03-13 07:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `cars_responsibilities`
--

CREATE TABLE `cars_responsibilities` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `car_id` int(11) UNSIGNED NOT NULL,
  `status_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars_responsibilities`
--

INSERT INTO `cars_responsibilities` (`id`, `user_id`, `car_id`, `status_id`, `created_at`, `updated_at`) VALUES
(10, 11, 65, 2, '2017-02-25 14:58:08', '2017-02-25 14:58:08');

-- --------------------------------------------------------

--
-- Table structure for table `car_containers`
--

CREATE TABLE `car_containers` (
  `id` int(11) UNSIGNED NOT NULL,
  `shipper_id` int(11) UNSIGNED NOT NULL,
  `number` varchar(50) NOT NULL,
  `arrival_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` varchar(10) NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_containers`
--

INSERT INTO `car_containers` (`id`, `shipper_id`, `number`, `arrival_date`, `amount`, `currency_id`, `created_at`, `updated_at`) VALUES
(1, 7, '98765', '2017-03-14 19:00:00', '10', 1, '2017-03-13 06:36:09', '2017-03-13 06:36:09');

-- --------------------------------------------------------

--
-- Table structure for table `car_costs`
--

CREATE TABLE `car_costs` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `default_value` varchar(10) DEFAULT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `is_required` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_costs`
--

INSERT INTO `car_costs` (`id`, `name`, `code`, `default_value`, `currency_id`, `is_required`, `created_at`, `updated_at`) VALUES
(5, 'Invoice', 'invoice', NULL, 2, 1, '2017-02-25 12:33:10', '2017-02-25 12:33:10'),
(6, 'Local Shipping', 'local-shipping', '75', 2, 1, '2017-02-24 07:41:18', '2017-02-24 07:41:18'),
(7, 'Container', 'container', '700', 2, 1, '2017-02-24 07:41:54', '2017-02-24 07:41:54'),
(8, 'Dubai Port', 'dubai-port', '240', 2, 1, '2017-02-24 07:42:24', '2017-02-24 07:42:24'),
(9, 'Customs', 'customs', '0', 2, 1, '2017-02-24 07:50:32', '2017-02-24 07:50:32'),
(10, 'Service Charges', 'service-charges', '0', 1, 0, '2017-02-24 07:49:04', '2017-02-24 07:49:04'),
(11, 'Wire Fee', 'wire-fee', '45', 2, 0, '2017-02-24 07:50:16', '2017-02-24 07:50:16'),
(12, 'Fixed Expense', 'fixed-expense', '500', 2, 0, '2017-02-24 07:51:56', '2017-02-24 07:51:56'),
(13, 'Additional(5%)', 'additional-5-percent', '0', 1, 0, '2017-02-24 20:00:28', '2017-02-24 20:00:28');

-- --------------------------------------------------------

--
-- Table structure for table `car_costs_values`
--

CREATE TABLE `car_costs_values` (
  `id` int(11) NOT NULL,
  `car_id` int(11) UNSIGNED NOT NULL,
  `cost_id` int(11) UNSIGNED NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `cost_name` varchar(50) NOT NULL,
  `cost_code` varchar(50) NOT NULL,
  `value` varchar(10) NOT NULL,
  `notes` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_costs_values`
--

INSERT INTO `car_costs_values` (`id`, `car_id`, `cost_id`, `currency_id`, `cost_name`, `cost_code`, `value`, `notes`, `created_at`, `updated_at`) VALUES
(25, 65, 5, 2, 'Invoice', 'invoice', '12405', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(26, 65, 6, 2, 'Local Shipping', 'local-shipping', '225', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(27, 65, 7, 2, 'Container', 'container', '675', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(28, 65, 8, 2, 'Dubai Port', 'dubai-port', '240', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(29, 65, 9, 2, 'Customs', 'customs', '665.25', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(30, 65, 10, 1, 'Service Charges', 'service-charges', '0', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(31, 65, 11, 2, 'Wire Fee', 'wire-fee', '45', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(32, 65, 12, 2, 'Fixed Expense', 'fixed-expense', '500', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(33, 65, 13, 1, 'Additional(5%)', 'additional-5-percent', '0', NULL, '2017-02-25 12:38:04', '2017-02-25 12:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `car_expenses`
--

CREATE TABLE `car_expenses` (
  `id` int(11) UNSIGNED NOT NULL,
  `car_id` int(11) UNSIGNED NOT NULL,
  `responsibility_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `value` varchar(11) NOT NULL,
  `approved` tinyint(3) NOT NULL,
  `payable_id` int(11) UNSIGNED DEFAULT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_expenses`
--

INSERT INTO `car_expenses` (`id`, `car_id`, `responsibility_id`, `title`, `description`, `value`, `approved`, `payable_id`, `currency_id`, `created_at`, `updated_at`) VALUES
(21, 65, 8, 'Right Side Door', NULL, '900', 1, 0, 1, '2017-02-25 12:46:48', '2017-02-25 12:46:48'),
(22, 65, 8, 'General parts', '1- a\r\n2- b', '10000', 1, 0, 1, '2017-02-25 12:47:24', '2017-02-25 12:47:24'),
(27, 65, 0, 'Garage Fee', 'this is description of garage fee.', '100', 0, 17, 1, '2017-03-13 13:17:44', '2017-03-13 12:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `car_makes`
--

CREATE TABLE `car_makes` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `logo` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_makes`
--

INSERT INTO `car_makes` (`id`, `name`, `code`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'Toyota', 'toyota', NULL, '2017-02-10 01:18:16', '2017-02-10 01:18:16'),
(3, 'Nissan', 'nissan', NULL, '2017-02-10 01:51:47', '2017-02-10 01:51:47'),
(4, 'Cheverlet', 'cheverlet', NULL, '2017-02-10 10:09:03', '2017-02-10 10:09:03'),
(5, 'Ford', 'ford', NULL, '2017-02-25 12:31:45', '2017-02-25 12:31:45');

-- --------------------------------------------------------

--
-- Table structure for table `car_models`
--

CREATE TABLE `car_models` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `car_make_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_models`
--

INSERT INTO `car_models` (`id`, `name`, `created_at`, `updated_at`, `car_make_id`) VALUES
(1, 'Corolla', '2017-02-10 05:51:28', '2017-02-10 01:51:28', 1),
(2, 'Tiida', '2017-02-10 05:59:19', '2017-02-10 01:59:19', 3),
(3, 'Camaro', '2017-02-10 10:09:12', '2017-02-10 10:09:12', 4),
(4, 'Prado', '2017-02-10 14:45:42', '2017-02-10 14:45:42', 1),
(5, 'Mustang', '2017-02-25 12:31:56', '2017-02-25 12:31:56', 5);

-- --------------------------------------------------------

--
-- Table structure for table `car_props`
--

CREATE TABLE `car_props` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `car_prop_type` int(11) UNSIGNED NOT NULL,
  `show_on_site` tinyint(3) UNSIGNED NOT NULL,
  `is_required` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_props`
--

INSERT INTO `car_props` (`id`, `name`, `code`, `car_prop_type`, `show_on_site`, `is_required`, `created_at`, `updated_at`) VALUES
(2, 'Lot Number', 'lot_number', 1, 1, 0, '2017-02-10 12:21:58', '2017-02-10 08:21:58'),
(3, 'Vin Number', 'vin_number', 1, 1, 1, '2017-02-10 13:12:30', '2017-02-10 09:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `car_props_options`
--

CREATE TABLE `car_props_options` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_props_types`
--

CREATE TABLE `car_props_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_props_types`
--

INSERT INTO `car_props_types` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Text', 'text', '2017-02-10 06:10:31', '0000-00-00 00:00:00'),
(2, 'Checkbox', 'checkbox', '2017-02-10 06:10:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_props_values`
--

CREATE TABLE `car_props_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `prop_id` int(11) UNSIGNED NOT NULL,
  `car_id` int(11) UNSIGNED NOT NULL,
  `prop_name` varchar(50) NOT NULL,
  `prop_type_code` varchar(50) NOT NULL,
  `prop_code` varchar(50) NOT NULL,
  `value` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_props_values`
--

INSERT INTO `car_props_values` (`id`, `prop_id`, `car_id`, `prop_name`, `prop_type_code`, `prop_code`, `value`, `created_at`, `updated_at`) VALUES
(7, 2, 63, 'Lot Number', 'text', 'lot_number', '1235643', '2017-02-24 19:46:59', '2017-02-24 19:46:59'),
(8, 3, 63, 'Vin Number', 'text', 'vin_number', 'k09df9837dsf', '2017-02-24 19:46:59', '2017-02-24 19:46:59'),
(9, 2, 64, 'Lot Number', 'text', 'lot_number', '12321', '2017-02-25 08:03:09', '2017-02-25 08:03:09'),
(10, 3, 64, 'Vin Number', 'text', 'vin_number', 'r4oi987hyhg', '2017-02-25 08:03:09', '2017-02-25 08:03:09'),
(11, 2, 65, 'Lot Number', 'text', 'lot_number', '16723618', '2017-02-25 12:38:04', '2017-02-25 12:38:04'),
(12, 3, 65, 'Vin Number', 'text', 'vin_number', '1ZVBP8C53D5200797', '2017-02-25 12:38:04', '2017-02-25 12:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `car_regions`
--

CREATE TABLE `car_regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `code` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_regions`
--

INSERT INTO `car_regions` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Copart', 'copart', '2017-02-10 11:31:03', '0000-00-00 00:00:00'),
(2, 'IAAI', 'iaai', '2017-02-10 11:31:03', '0000-00-00 00:00:00'),
(3, 'Manheim', 'manhein', '2017-02-10 11:31:27', '0000-00-00 00:00:00'),
(4, 'United Arab Emirates', 'UAE', '2017-02-10 14:11:56', '0000-00-00 00:00:00'),
(5, 'Dubai', 'dubai', '2017-02-23 07:43:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `car_responsibility_statuses`
--

CREATE TABLE `car_responsibility_statuses` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_responsibility_statuses`
--

INSERT INTO `car_responsibility_statuses` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Created', 'created', '2017-02-11 19:25:11', '0000-00-00 00:00:00'),
(2, 'In Progress', 'in-progress', '2017-02-11 19:25:11', '0000-00-00 00:00:00'),
(3, 'Completed', 'completed', '2017-02-11 19:25:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `conversion` varchar(10) NOT NULL,
  `code` varchar(50) NOT NULL,
  `is_default` tinyint(3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `conversion`, `code`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 'Dirhams (United Arab Emirates)', '1', 'AED', 1, '2017-02-10 07:33:30', '0000-00-00 00:00:00'),
(2, 'Dollar (United States Of America)', '3.67', 'USD', 0, '2017-02-10 07:33:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dictionary`
--

CREATE TABLE `dictionary` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `code` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dictionary`
--

INSERT INTO `dictionary` (`id`, `type`, `title`, `code`, `created_at`, `updated_at`) VALUES
(1, 'stock_types', 'Bidders Stock', 'bidder-stock', '2017-02-23 07:57:16', '0000-00-00 00:00:00'),
(2, 'stock_types', 'Showroom stock', 'showroom-stock', '2017-02-23 07:57:16', '0000-00-00 00:00:00'),
(3, 'stock_types', 'Agent Stock', 'agent-stock', '2017-02-23 07:57:42', '0000-00-00 00:00:00'),
(4, 'sale_types', 'in country Sale', 'in-country-sale', '2017-02-23 08:05:08', '0000-00-00 00:00:00'),
(5, 'sale_types', 'Export Sale', 'export-sale', '2017-02-23 08:05:08', '0000-00-00 00:00:00'),
(6, 'payment_methods', 'Cash', 'cash', '2017-02-23 08:08:15', '0000-00-00 00:00:00'),
(7, 'payment_methods', 'Cheque', 'cheque', '2017-02-23 08:08:15', '0000-00-00 00:00:00'),
(8, 'payment_methods', 'Credit/Debit Card', 'card', '2017-02-23 08:08:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) UNSIGNED NOT NULL,
  `year` int(4) UNSIGNED NOT NULL,
  `month` int(2) UNSIGNED NOT NULL,
  `head_id` int(11) UNSIGNED NOT NULL,
  `value` varchar(10) NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `added_by` int(11) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `year`, `month`, `head_id`, `value`, `currency_id`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 2017, 2, 1, '21', 1, 6, '2017-02-17 14:14:33', '2017-02-17 14:19:39'),
(2, 2017, 2, 7, '1000', 1, 6, '2017-02-23 19:05:29', '2017-02-23 19:05:29'),
(3, 2017, 2, 8, '2800', 1, 6, '2017-02-20 19:50:52', '2017-02-23 19:50:52'),
(4, 2017, 1, 8, '1500', 1, 6, '2017-01-23 20:22:58', '2017-02-23 20:22:58');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_02_26_121317_add_unapproved_amount_paid_to_sales_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payables`
--

CREATE TABLE `payables` (
  `id` int(10) UNSIGNED NOT NULL,
  `head_id` int(11) UNSIGNED NOT NULL,
  `debit` tinyint(3) NOT NULL,
  `value` varchar(10) NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payables`
--

INSERT INTO `payables` (`id`, `head_id`, `debit`, `value`, `currency_id`, `created_by`, `created_at`, `updated_at`) VALUES
(6, 7, 0, '12000', 1, 6, '2017-02-25 08:57:09', '2017-02-25 08:57:09'),
(7, 5, 0, '8000', 1, 6, '2017-02-25 11:47:02', '2017-02-25 11:47:02'),
(8, 5, 1, '800', 1, 6, '2017-02-25 11:47:31', '2017-02-25 11:47:31'),
(9, 10, 0, '516', 1, 6, '2017-02-25 12:27:43', '2017-02-25 12:27:43'),
(10, 10, 1, '500', 1, 6, '2017-03-12 09:36:44', '2017-03-12 09:36:44'),
(11, 11, 0, '3474', 1, 8, '2017-03-12 17:19:46', '2017-03-12 16:12:40'),
(12, 11, 1, '10', 1, 6, '2017-03-12 16:15:08', '2017-03-12 16:15:08'),
(13, 11, 1, '20', 1, 6, '2017-03-13 05:05:45', '2017-03-13 05:05:45'),
(14, 11, 1, '1', 1, 6, '2017-03-13 05:07:02', '2017-03-13 05:07:02'),
(15, 11, 0, '3474', 1, 11, '2017-03-13 06:39:51', '2017-03-13 06:39:51'),
(16, 11, 0, '3474', 1, 6, '2017-03-13 07:07:30', '2017-03-13 07:07:30'),
(17, 17, 0, '100', 1, 6, '2017-03-13 12:17:44', '2017-03-13 12:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `receivables`
--

CREATE TABLE `receivables` (
  `id` int(11) NOT NULL,
  `head_id` int(11) UNSIGNED NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `created_for` int(11) UNSIGNED NOT NULL,
  `debit` tinyint(3) UNSIGNED NOT NULL,
  `value` varchar(15) NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `additional_data` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receivables`
--

INSERT INTO `receivables` (`id`, `head_id`, `created_by`, `created_for`, `debit`, `value`, `currency_id`, `additional_data`, `created_at`, `updated_at`) VALUES
(29, 4, 6, 1, 1, '3000', 1, NULL, '2017-02-25 08:28:37', '2017-02-25 08:28:37'),
(30, 4, 1, 1, 0, '10', 1, NULL, '2017-02-25 08:41:00', '2017-02-25 08:41:00'),
(31, 1, 6, 1, 0, '2990', 1, NULL, '2017-02-25 08:49:43', '2017-02-25 08:49:43'),
(32, 1, 6, 1, 1, '10', 1, NULL, '2017-02-25 08:53:01', '2017-02-25 08:53:01'),
(33, 6, 8, 9, 1, '31354', 1, NULL, '2017-02-25 09:11:52', '2017-02-25 09:11:52'),
(34, 6, 8, 9, 0, '30000', 1, NULL, '2017-02-25 09:11:52', '2017-02-25 09:11:52'),
(35, 1, 6, 1, 1, '1000', 1, NULL, '2017-02-25 11:51:57', '2017-02-25 11:51:57'),
(36, 6, 8, 9, 1, '25000', 1, NULL, '2017-02-25 12:03:27', '2017-02-25 12:03:27'),
(37, 6, 8, 9, 0, '20000', 1, NULL, '2017-02-25 12:03:27', '2017-02-25 12:03:27'),
(38, 4, 6, 11, 1, '5000', 1, NULL, '2017-02-25 12:43:02', '2017-02-25 12:43:02'),
(39, 4, 11, 11, 0, '900', 1, NULL, '2017-02-25 12:46:48', '2017-02-25 12:46:48'),
(40, 4, 11, 11, 0, '10000', 1, NULL, '2017-02-25 12:47:24', '2017-02-25 12:47:24'),
(41, 1, 6, 11, 1, '6000', 1, NULL, '2017-02-25 12:48:10', '2017-02-25 12:48:10'),
(42, 6, 13, 9, 1, '70000', 1, NULL, '2017-02-25 13:00:26', '2017-02-25 13:00:26'),
(43, 6, 13, 9, 0, '50000', 1, NULL, '2017-02-25 13:00:26', '2017-02-25 13:00:26'),
(44, 6, 6, 12, 1, '70000', 1, NULL, '2017-02-25 13:02:40', '2017-02-25 13:02:40'),
(45, 6, 6, 12, 0, '50000', 1, NULL, '2017-02-25 13:02:40', '2017-02-25 13:02:40'),
(46, 6, 13, 12, 1, '70000', 1, NULL, '2017-02-25 13:07:48', '2017-02-25 13:07:48'),
(47, 6, 13, 12, 0, '50000', 1, NULL, '2017-02-25 13:07:48', '2017-02-25 13:07:48'),
(48, 4, 6, 11, 1, '1000', 1, NULL, '2017-02-25 14:49:36', '2017-02-25 14:49:36'),
(49, 6, 6, 9, 1, '30000', 1, NULL, '2017-02-27 12:31:14', '2017-02-27 12:31:14'),
(51, 6, 6, 9, 0, '1', 1, NULL, '2017-02-27 14:00:18', '2017-02-27 14:00:18'),
(52, 6, 6, 9, 0, '10000', 1, NULL, '2017-02-27 14:00:59', '2017-02-27 14:00:59'),
(53, 6, 6, 9, 0, '19999', 1, NULL, '2017-02-27 14:02:08', '2017-02-27 14:02:08'),
(54, 6, 1, 9, 1, '10000', 1, NULL, '2017-03-10 09:30:19', '2017-03-10 09:30:19'),
(55, 6, 1, 9, 1, '10000', 1, NULL, '2017-03-10 09:31:07', '2017-03-10 09:31:07'),
(56, 6, 6, 9, 0, '90000', 1, NULL, '2017-03-10 09:43:52', '2017-03-10 09:43:52'),
(68, 6, 8, 12, 1, '72000', 1, NULL, '2017-03-12 16:12:40', '2017-03-12 16:12:40'),
(69, 6, 11, 9, 1, '10', 1, NULL, '2017-03-13 06:39:50', '2017-03-13 06:39:50'),
(70, 6, 6, 9, 1, '10', 1, NULL, '2017-03-13 07:07:30', '2017-03-13 07:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `priority` int(3) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `code`, `priority`, `updated_at`, `created_at`) VALUES
(1, 'Admin', 'admin', 2, '2017-02-12 17:12:33', '2017-02-08 14:30:42'),
(2, 'Sales', 'sales', 1, '2017-02-12 17:12:36', '2017-02-08 14:33:28'),
(3, 'Repairing', 'repairing', 3, '2017-02-17 11:47:38', '2017-02-17 11:47:38'),
(4, 'Sales & Purchase', 'sale-purchase', 4, '2017-02-22 15:38:51', '2017-02-17 11:54:37'),
(5, 'Customer', 'customer', 6, '2017-02-21 13:45:01', '2017-02-21 13:44:30'),
(6, 'Shipper', 'shipper', 5, '2017-02-21 13:44:50', '2017-02-21 13:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` varchar(10) NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `car_id` int(11) UNSIGNED NOT NULL,
  `sold_to` int(11) UNSIGNED NOT NULL,
  `sold_by` int(11) UNSIGNED NOT NULL,
  `number` varchar(6) NOT NULL,
  `manual_bill_number` varchar(50) NOT NULL,
  `cx_name` varchar(100) NOT NULL,
  `cx_phone` varchar(16) DEFAULT NULL,
  `cx_city` int(11) UNSIGNED NOT NULL,
  `amount_paid` varchar(10) NOT NULL,
  `payment_method` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `price`, `currency_id`, `car_id`, `sold_to`, `sold_by`, `number`, `manual_bill_number`, `cx_name`, `cx_phone`, `cx_city`, `amount_paid`, `payment_method`, `created_at`, `updated_at`) VALUES
(26, '72000', 1, 65, 12, 8, '143713', '', 'Hussain', '0526800224', 5, '0', 6, '2017-03-13 08:20:21', '2017-03-13 07:20:21'),
(28, '10', 1, 65, 9, 6, '180875', '12345', 'Customer', '0526800224', 5, '0', 6, '2017-03-13 07:07:30', '2017-03-13 07:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `sales_payments`
--

CREATE TABLE `sales_payments` (
  `id` int(11) UNSIGNED NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `payment_method` int(11) UNSIGNED NOT NULL,
  `amount` varchar(10) NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `document_number` varchar(30) DEFAULT NULL,
  `approved` tinyint(3) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `updated_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_payments`
--

INSERT INTO `sales_payments` (`id`, `sale_id`, `payment_method`, `amount`, `currency_id`, `document_number`, `approved`, `user_id`, `updated_by`, `created_at`, `updated_at`) VALUES
(22, 26, 6, '40000', 1, NULL, 0, 12, 8, '2017-03-12 16:12:40', '2017-03-12 16:12:40'),
(23, 27, 6, '1', 1, NULL, 0, 9, 11, '2017-03-13 06:39:50', '2017-03-13 06:39:50'),
(24, 28, 6, '9', 1, NULL, 0, 9, 6, '2017-03-13 07:07:30', '2017-03-13 07:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `sale_types`
--

CREATE TABLE `sale_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_types`
--

INSERT INTO `sale_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'In Country Sale', '2017-02-10 08:06:15', '0000-00-00 00:00:00'),
(2, 'Export Sale', '2017-02-10 08:06:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stock_types`
--

CREATE TABLE `stock_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_types`
--

INSERT INTO `stock_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Bidders Stock', '2017-02-10 08:05:46', '0000-00-00 00:00:00'),
(2, 'Showroom Stock', '2017-02-10 08:05:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(11) UNSIGNED NOT NULL,
  `uploadable_type` varchar(50) NOT NULL,
  `uploadable_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `url` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `uploadable_type`, `uploadable_id`, `name`, `url`, `created_at`, `updated_at`) VALUES
(8, 'App\\Models\\User', 2, 'xWfWhXn2kC5nhYXsN8DEj03CZoeebHsUL53ok6rW.png', '/uploads/xWfWhXn2kC5nhYXsN8DEj03CZoeebHsUL53ok6rW.png', '2017-02-08 16:24:30', '2017-02-08 16:24:30'),
(10, 'App\\Models\\User', 1, 'vCp3oFgjChmcM0bkPHlQypu7v5nyMFX6OcYEEX4g.jpeg', '/uploads/vCp3oFgjChmcM0bkPHlQypu7v5nyMFX6OcYEEX4g.jpeg', '2017-02-08 16:25:28', '2017-02-08 16:25:28'),
(11, 'App\\Models\\CarMake', 1, 'TSFtTf2HuGQuzRq4znlU1XZbhENv0xFkYLABfFNx.png', '/uploads/TSFtTf2HuGQuzRq4znlU1XZbhENv0xFkYLABfFNx.png', '2017-02-10 01:18:16', '2017-02-10 01:18:16'),
(20, 'App\\Models\\Carexpense', 8, 'axs6yo0YVoXs7a89Mxl4gryFdBoEw6X3h5dskXhn.jpeg', '/uploads/axs6yo0YVoXs7a89Mxl4gryFdBoEw6X3h5dskXhn.jpeg', '2017-02-14 16:50:11', '2017-02-14 16:50:11'),
(21, 'App\\Models\\User', 9, 'JWYZGuSOOtRpCpKWVd8iwaZ5u06N04gb97eNZVkY.jpeg', '/uploads/JWYZGuSOOtRpCpKWVd8iwaZ5u06N04gb97eNZVkY.jpeg', '2017-02-25 09:36:36', '2017-02-25 09:36:36'),
(24, 'App\\Models\\UserIdentity', 9, 'A4Gs1pYwfXU2ALjq0m8Mtq84EG5eu4ltVwsJ1UKA.jpeg', '/uploads/A4Gs1pYwfXU2ALjq0m8Mtq84EG5eu4ltVwsJ1UKA.jpeg', '2017-02-25 10:08:23', '2017-02-25 10:08:23'),
(25, 'App\\Models\\Car', 65, 'jtjjCdGRYTuKrZQwHexaqsnertGiMl10FSByXFHd.jpeg', '/uploads/jtjjCdGRYTuKrZQwHexaqsnertGiMl10FSByXFHd.jpeg', '2017-02-25 12:38:48', '2017-02-25 12:38:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_picture` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `active`, `phone`, `address`, `password`, `remember_token`, `identity_picture`, `created_at`, `updated_at`) VALUES
(1, 'Umar', 'khalidumar03@gmail.com', 1, NULL, NULL, '$2y$10$tYWo.olgmZO8rCVwSFDfu.HMwKS456emSze3/7McYeUaBbS8ywUXa', 'h75BHfCgsESFWfNCGEuASzwANkW84b8bzQEqGAuw8OeNRHvKRWwFkWIzNb5V', 0, '2017-02-01 13:31:59', '2017-02-25 08:29:38'),
(6, 'khalid khan', 'khalid@awok.ae', 1, '0526800224', '', '$2y$10$b0sj/xtpOXG2a/lQrczxJ.vHe1Dk.L01R9.qD4f7wFgohydHzUiJ6', 'cZrHnxsURZGj9yD1Jp0hVd56qn7FqWz659tp5whRvc7PbV345rAOfmEOVZWv', 0, '2017-02-12 15:45:41', '2017-02-22 15:40:24'),
(7, 'w8', 'w8@gmail.com', 1, NULL, '', '$2y$10$aU39paoK2oRkBb2sP27WxeVmt51WQEEPDpqWE1v..Z3HQuDysA3OS', NULL, 0, '2017-02-21 14:23:56', '2017-02-21 14:23:56'),
(8, 'Sales and Purchase', 'sale@purchase.com', 1, NULL, '', '$2y$10$6KTidz9b2kqdc1GW6H0XC.W636M00FCivmsAZaFu/02igzzaBkHgC', 'CIkWryESM38AHGSJYzYM0wgTZnGIVSGKb37KunVksnuqNmpzBXSzZAgC8toM', 0, '2017-02-22 15:40:10', '2017-02-22 15:40:10'),
(9, 'Customer', 'me@customer.com', 1, '0526800224', '308 Abdullah Building, Al barsha, Dubai, UAE', '$2y$10$3gnTali4wblXpRoesL2O5OBH27bP/TR6LNmq1S8/554H.yGLgmn.e', NULL, 24, '2017-02-22 17:56:06', '2017-02-25 10:08:23'),
(10, 'Sajjad', 'sajad@gmail.com', 1, '0526800224', NULL, '$2y$10$Ejenhm5ikIQfpdBygbi7TO2Iw2Vuy.Cu0QO6hgii68IHzy8rjmJFO', '9HOBLJjLqDTyr6rmkYz4aIC8h4BZnqSLcjNgvdHhWPKESMQCzZ11pQfJeYN1', NULL, '2017-02-25 12:22:04', '2017-02-25 12:22:04'),
(11, 'Arif', 'arif@gmail.com', 1, '0526800224', NULL, '$2y$10$6Sva2zTURpq4ReX4PG3xZ.1DRKCQDakwy/sDYkKl.elYHM7/rzZbO', 'myftqJFqkPtsknwGb88ILTUxPXEuA62bCHva0A5gXczrLuMYxBoCgFYUiimF', NULL, '2017-02-25 12:40:48', '2017-02-25 12:40:48'),
(12, 'Hussain', 'hussain@gmail.com', 1, '0526800224', NULL, '$2y$10$D3CzwibUhtLFoGgtgoC20ejZf1YcfE8YnL70dU0n.T36Qxo8bFzKG', NULL, NULL, '2017-02-25 12:54:03', '2017-02-25 12:54:03'),
(13, 'Arslan', 'arslan@gmail.com', 1, '526800224', 'http://www.awok.com', '$2y$10$4uQ/xjHG3SlMfquHhBKUM.SOiLHONu7JsjjNtd/fnvzg4oj.fMoQ.', 'JFOUeSl12C5VeujupIS2uXP9tedLRISSxlSS82Tn5fMcMKNjTskHJP1hXY4l', NULL, '2017-02-25 12:59:23', '2017-02-25 12:59:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(6, 1, '2017-02-12 19:45:41', '2017-02-12 19:49:56'),
(7, 6, '2017-02-21 18:23:56', '2017-02-21 18:23:56'),
(8, 4, '2017-02-22 19:40:10', '2017-02-22 19:40:10'),
(9, 5, '2017-02-22 21:56:06', '2017-02-22 21:56:06'),
(1, 2, '2017-02-25 12:12:28', '2017-02-25 12:12:28'),
(10, 4, '2017-02-25 12:22:05', '2017-02-25 12:22:05'),
(11, 3, '2017-02-25 12:40:48', '2017-02-25 12:40:48'),
(12, 5, '2017-02-25 12:54:03', '2017-02-25 12:54:03'),
(13, 2, '2017-03-12 13:23:56', '2017-03-12 13:23:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts_heads`
--
ALTER TABLE `accounts_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars_responsibilities`
--
ALTER TABLE `cars_responsibilities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `car_id` (`car_id`);

--
-- Indexes for table `car_containers`
--
ALTER TABLE `car_containers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_costs`
--
ALTER TABLE `car_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_costs_values`
--
ALTER TABLE `car_costs_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_expenses`
--
ALTER TABLE `car_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_makes`
--
ALTER TABLE `car_makes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_models`
--
ALTER TABLE `car_models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_props`
--
ALTER TABLE `car_props`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_props_options`
--
ALTER TABLE `car_props_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_props_types`
--
ALTER TABLE `car_props_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_props_values`
--
ALTER TABLE `car_props_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_regions`
--
ALTER TABLE `car_regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_responsibility_statuses`
--
ALTER TABLE `car_responsibility_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dictionary`
--
ALTER TABLE `dictionary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payables`
--
ALTER TABLE `payables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receivables`
--
ALTER TABLE `receivables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_types`
--
ALTER TABLE `sale_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_types`
--
ALTER TABLE `stock_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD KEY `FK_user_roles_users` (`user_id`),
  ADD KEY `FK_user_roles_roles` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts_heads`
--
ALTER TABLE `accounts_heads`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `cars_responsibilities`
--
ALTER TABLE `cars_responsibilities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `car_containers`
--
ALTER TABLE `car_containers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `car_costs`
--
ALTER TABLE `car_costs`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `car_costs_values`
--
ALTER TABLE `car_costs_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `car_expenses`
--
ALTER TABLE `car_expenses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `car_makes`
--
ALTER TABLE `car_makes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `car_models`
--
ALTER TABLE `car_models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `car_props`
--
ALTER TABLE `car_props`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `car_props_options`
--
ALTER TABLE `car_props_options`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `car_props_types`
--
ALTER TABLE `car_props_types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `car_props_values`
--
ALTER TABLE `car_props_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `car_regions`
--
ALTER TABLE `car_regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `car_responsibility_statuses`
--
ALTER TABLE `car_responsibility_statuses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dictionary`
--
ALTER TABLE `dictionary`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payables`
--
ALTER TABLE `payables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `receivables`
--
ALTER TABLE `receivables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `sales_payments`
--
ALTER TABLE `sales_payments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `sale_types`
--
ALTER TABLE `sale_types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stock_types`
--
ALTER TABLE `stock_types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `FK_user_roles_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_user_roles_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
