@extends('layouts.frontend')
@section('content')
    <!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            @foreach( $banners as $banner )
                <div class="slide"> 
                    <!-- Slide image -->
                    <img src="/uploads/banners/{{ $banner }}" alt=""/>                    
                </div>
            @endforeach
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
    <!--/slider -->
    <div class="main">
        <div class="wrap">
            <div class="section group">
                <div class="span_3_of_3">
                    <h2 class="head">Featured Cars</h2>
                    <div class="top-box">
                        @foreach( $featured as $car )
                            <div class="col_1_of_3 span_1_of_3"> 
                                <a href="/cars/{{ $car->code }}">
                                    <div class="inner_content clearfix">
                                        <div class="product_image">
                                            <img src="@if( $car->getMainImage() ){{ $car->getMainImage()->url }}@else {{ "/images/wargaeyar.jpg" }} @endif" alt=""/>
                                        </div>
                                        <div class="price">
                                            <div class="cart-left">
                                                <p class="title">{{ $car->title }}</p>
                                                <div class="price1">
                                                    <span class="actual">{{ $car->sale_price }} {{ $car->currency->code }}</span>
                                                </div>
                                            </div>
                                        </div>              
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        <div class="clear"></div>
                    </div>
                    <h2 class="head">Trending</h2>
                    <div class="top-box1">
                        @foreach( $trending as $car )
                            <div class="col_1_of_3 span_1_of_3"> 
                                <a href="/cars/{{ $car->code }}">
                                    <div class="inner_content clearfix">
                                        <div class="product_image">
                                            <img src="@if( $car->getMainImage() ){{ $car->getMainImage()->url }}@else {{ "/images/wargaeyar.jpg" }} @endif" alt=""/>
                                        </div>
                                        <div class="price">
                                            <div class="cart-left">
                                                <p class="title">{{ $car->title }}</p>
                                                <div class="price1">
                                                    <span class="actual">{{ $car->sale_price }} {{ $car->currency->code }}</span>
                                                </div>
                                            </div>
                                        </div>              
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        <div class="clear"></div>
                    </div>  
                    <h2 class="head">Recently Added</h2>    
                    <div class="section group">
                        @foreach( $recent as $car )
                            <div class="col_1_of_3 span_1_of_3"> 
                                <a href="/cars/{{ $car->code }}">
                                    <div class="inner_content clearfix">
                                        <div class="product_image">
                                            <img src="@if( $car->getMainImage() ){{ $car->getMainImage()->url }}@else {{ "/images/wargaeyar.jpg" }} @endif" alt=""/>
                                        </div>
                                        <div class="price">
                                            <div class="cart-left">
                                                <p class="title">{{ $car->title }}</p>
                                                <div class="price1">
                                                    <span class="actual">{{ $car->sale_price }} {{ $car->currency->code }}</span>
                                                </div>
                                            </div>
                                        </div>              
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        <div class="clear"></div>
                    </div>                                                  
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
@endsection