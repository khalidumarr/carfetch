@extends('layouts.frontend')
@section('content')
<div class="mens">    
        <div class="main">
            <div class="wrap">
                <ul class="breadcrumb breadcrumb__t"><a class="home" href="#">Home</a>  / Cars</ul>
                <div class="rsidebar span_1_of_left">   
                    <form id="search_form">             
                        @if( !empty( $_REQUEST['make_code'] ) )
                            <input type="hidden" name="make_code" value="{{ $_REQUEST['make_code'] }}">
                            <section  class="sky-form">
                                <h5 class="m_1">Model Name</h5>
                                <div class="row row1 scroll-pane">
                                    <div class="col col-4">
                                        @foreach( $models as $model )
                                            <label class="checkbox"><input class="search_interaction" type="checkbox" name="model" value="{{ $model->id }}" @if( isset( $_REQUEST['model'] ) && $_REQUEST['model'] == $model->id ) checked @endif><i></i>{{ $model->name }}</label>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        @else
                            <section  class="sky-form">
                                <h5 class="m_1">Make Name</h5>
                                <div class="row row1 scroll-pane">
                                    <div class="col col-4">
                                        @foreach( $makes as $make )
                                            <label class="checkbox"><input class="search_interaction" type="checkbox" name="make_code" value="{{ $make->code }}" @if( isset( $_REQUEST['make_code'] ) && $_REQUEST['make_code'] == $make->code ) checked @endif><i></i>{{ $make->name }}</label>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        @endif

                        <section  class="sky-form">
                            <h5 class="m_1">Year</h5>
                            <div class="row row1 scroll-pane">
                                <div class="col col-4">
                                    <input type="text" name="min_year" class="form-control year_control" placeholder="Min year" value="@if( isset( $_REQUEST['min_year'] ) ) {{ trim( $_REQUEST['min_year'] ) }} @endif">
                                    <input type="text" name="max_year" class="form-control year_control" placeholder="Max year" value="@if( isset( $_REQUEST['max_year'] ) ) {{ trim( $_REQUEST['max_year'] ) }} @endif">
                                </div>
                                <input type="submit" value="Submit" class="btn btn-primary form-control">   
                            </div>
                        </section>

                        <section  class="sky-form">
                            <div class="row row1 scroll-pane">
                                <div class="col col-4">
                                                                                                                                         
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
                <div class="cont span_2_of_3">
                <h2 class="head">Car Search</h2>
                    <div class="active_filters">
                        @if( !empty( $_REQUEST['make_code'] ) )
                            <div class="filter">
                                Make: {{ $_REQUEST['make_code'] }} <a href="/" data-input-name="make_code" class="filter_remove">X </a>
                            </div>
                        @endif

                        @if( !empty( $_REQUEST['model'] ) )
                            <div class="filter">
                                Model: {{ $selected_model->name }} <a href="/" data-input-name="model" class="filter_remove">X </a>
                            </div>
                        @endif

                        @if( !empty( $_REQUEST['year'] ) )
                            <div class="filter">
                                Year: {{ $_REQUEST['year'] }} <a href="/" data-input-name="year" class="filter_remove">X </a>
                            </div>
                        @endif
                    </div>
                    <div class="mens-toolbar">
                        <div class="sort">
                            <div class="sort-by">
                                {{ $cars->total() }} Results found
                            </div>
                        </div>
                        <div class="pager">
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?php $chunks = $cars->getCollection()->chunk(3); ?>
                    @foreach( $chunks as $chunk )
                        <div class="top-box">
                            @foreach( $chunk as $car )
                                <div class="col_1_of_3 span_1_of_3"> 
                                    <a href="/cars/{{ $car->code }}">
                                        <div class="inner_content clearfix">
                                            <div class="product_image">
                                                <img src="@if( $car->getMainImage() ) {{ trim( $car->getMainImage()->url ) }} @else /images/wargaeyar.jpg @endif" alt=""/>
                                            </div>
                                            @if( $car->featured )
                                                <div class="sale-box1">
                                                    <span class="on_sale title_shop">featured</span>
                                                </div> 
                                            @elseif( $car->trending )
                                                <div class="sale-box2">
                                                    <span class="on_sale title_shop">Trending</span>
                                                </div>
                                            @elseif( strtotime( $car->created_at ) > strtotime( '-7 days' ) )
                                                <div class="sale-box">
                                                    <span class="on_sale title_shop">New</span>
                                                </div>
                                            @endif
                                            <div class="price">
                                                <div class="cart-left">
                                                    <p class="title">{{ strlen( $car->title ) > 40 ? substr( $car->title, 0, 40 ) . "..." : $car->title }}</p>
                                                    <div class="price1">
                                                        <?php $price = isset( $car->sale_price ) ? (int)$car->sale_price : 0 ?>
                                                        <span class="actual">{{ number_format( $price ) }} {{ $default_currency->code }}</span>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>              
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            <div class="clear"></div>
                        </div>  
                    @endforeach
                    <div class="pagination-container">
                        {{ $cars->appends( $_REQUEST )->links() }}                        
                    </div>                                                        
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <script src="js/jquery.easydropdown.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on( 'click', '.search_interaction', function(){
                $('#search_form').submit();
            });

            $(document).on( 'click', '.filter_remove', function(e){
                e.preventDefault();
                var fieldName = $(this).data('input-name');

                if( fieldName == 'make_code' ){
                    $('input[name=model]').remove();    
                }

                $('input[name=' + fieldName + ']').remove();
                $('#search_form').submit();
            });
        });
    </script>
@endsection