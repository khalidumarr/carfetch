@extends('layouts.frontend')
@section('content')
<div class="login">
       <div class="wrap">
	    <ul class="breadcrumb breadcrumb__t"><a class="home" href="#">Home</a>  / Contact</ul>
	    	@if( Session::has('message') )
			    <div class="alert alert-success alert-dismissible fade in" role="alert">
			        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			        </button>
			        {{ Session::get('message') }}
			    </div>
			@endif
	    	<div class="map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3606.1597206712995!2d55.40344155020426!3d25.332420883754118!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9492073bcc9afd19!2sAlmutafawiq+Used+Cars+Trade.!5e0!3m2!1sen!2sae!4v1486144284405" width="100%" height="375" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>			
			<h2 class="head">Send Message</h2>
		   <div class="content-top">		   					   
			   	{!! Form::open(['url' => '/contact/contact']) !!}
					<div class="to">
                     	<input data-validation="required" name="name" type="text" class="text" placeholder="Name">
					 	<input data-validation="required" name="email" type="text" class="text" placeholder="Email" style="margin-left: 10px">
					 	<input data-validation="required" name="phone" type="text" class="text" placeholder="phone">
					</div>
					<div class="to">
					</div>
					<div class="text">
	                   <textarea data-validation="required" name="message" placeholder="Message:">Message</textarea>
	                </div>
	                <div class="submit">
	               		<input type="submit" value="Submit">
	                </div>
               </form>
                
            </div>
       </div> 
    </div>
    <script type="text/javascript">
		$(window).load(function() {
			$.validate();
		});
	</script>
@endsection