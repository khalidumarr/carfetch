@extends('layouts/admin')
@section('content')
<!-- Widgets -->
<div class="body">
    <div class="row clearfix">
        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <a class="icon" href="/cars">
                    <i class="material-icons">playlist_add_check</i>
                </a>
                <div class="content">
                    <div class="text">INVENTORY</div>                    
                </div>
            </div>
        </div>            
        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <a class="icon" href="/user">
                    <i class="material-icons">person_add</i>
                </a>
                <div class="content">
                    <div class="text">USERS</div>                    
                </div>
            </div>
        </div>        
        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-blue hover-expand-effect">
                <a class="icon" href="/car-containers">
                    <i class="material-icons">directions_boat</i>
                </a>
                <div class="content">
                    <div class="text">CONTAINERS</div>                            
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-grey hover-expand-effect">
                <a class="icon" href="/accounts-heads">
                    <i class="material-icons">account_balance</i>
                </a>
                <div class="content">
                    <div class="text">ACCOUNTS</div>                            
                </div>
            </div>
        </div>
    </div>
                
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-3 logo-dashbaord-center"><img src="/images/logo.png"></div>
        <div class="col-md-5"></div>
    </div>
</div>


<style type="text/css">
	.logo-dashbaord-center{
		padding-top: 200px;
	}
</style>
@stop