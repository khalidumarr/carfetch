@extends('layouts/admin')
@section('content')

<!-- top tiles -->
<div class="row ">
	<div class="tile_count">
		<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-user"></i>Cars Available for sale</span>
			<div class="count non_sold_cars">0</div>
		</div>	
	</div>
</div>
<!-- /top tiles -->


<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			url : '/admin/reports/my-units-available',
			success : function(data){
				$('.non_sold_cars').text( data );
			}
		});
	});
</script>
@stop