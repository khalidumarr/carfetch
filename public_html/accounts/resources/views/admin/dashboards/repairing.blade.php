@extends('layouts/admin')
@section('content')
  <!-- top tiles -->
<div class="row ">
	<div class="tile_count">
		<div class="col-md-4 col-sm-8 col-xs-6 tile_stats_count">
		  <span class="count_top"><i class="fa fa-user"></i> Total Cash in Hand</span>
		  <div class="count green total_cash_in_hand">0 AED</div>
		</div>
	</div>
</div>
  <!-- /top tiles -->

<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			url : '/admin/reports/get-total-cash-in-hand',
			success : function(data){
				$('.total_cash_in_hand').text( data );
			}
		});
	});
</script>
@stop