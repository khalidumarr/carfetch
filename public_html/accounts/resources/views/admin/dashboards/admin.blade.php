@extends('layouts/admin')
@section('content')
<div class="row tile_count">
	<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
	<span class="count_top"><i class="fa fa-user"></i> Total Receivable</span>
		<div class="count total_receivable_count">0 AED</div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-clock-o"></i> Total Payable</span>
		<div class="count total_payable_count">0 AED</div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> Total Inventory </span>
		<div class="count green total_inventory_count">0 AED</div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> Net Position</span>
		<div class="count total_net_position_count">0 AED</div>
	</div>
	<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i>Cars Available for sale</span>
		<div class="count non_sold_cars">0</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Expenses Summary </h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="demo-container" >
	                  <div id="morris-chart-area"></div>
	                </div>
	                <div class="clearfix"></div>
					<div class="tiles">
						<div class="col-md-4 tile">
						<span>Current Month Expenses</span>
							<h2  class="curr_month_expeses">0 AED</h2>
							<span class="sparkline11 graph" style="height: 160px;">
								<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
							</span>
						</div>
						<div class="col-md-4 tile">
							<span>Current Week Expenses</span>
							<h2 class="curr_week_expenses">0 AED</h2>
							<span class="sparkline22 graph" style="height: 160px;">
								<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
							</span>
						</div>
						<div class="col-md-4 tile">
							<span>Today Expenses</span>
							<h2 class="today_expenses">0 AED</h2>
							<span class="sparkline11 graph" style="height: 160px;">
								<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
							</span>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Sales Summary </h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="demo-container2">
						<div id="morris-chart-area2"></div>
					</div>
					<div class="tiles">
						<div class="col-md-4 tile">
						<span>Current Month Sales</span>
							<h2 class="curr_month_sales">231809 AED</h2>
							<span class="sparkline11 graph" style="height: 160px;">
								<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
							</span>
						</div>
						<div class="col-md-4 tile">
							<span>Current Week Sales</span>
							<h2 class="curr_week_sales">231809 AED</h2>
							<span class="sparkline22 graph" style="height: 160px;">
								<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
							</span>
						</div>
						<div class="col-md-4 tile">
							<span>Today Sales</span>
							<h2 class="today_sales">231809 AED</h2>
							<span class="sparkline11 graph" style="height: 160px;">
								<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
							</span>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
  <div class="col-md-4">
    <div class="x_panel">
      <div class="x_title">
        <h2>Arriving Containers</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content containers_list">
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Monthly Expenses</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <a href="http://carfetch.dev/admin/accounts-expenses/create" target="_blank" class="btn btn-small btn-default">Add Expense</a>
      <div class="x_content monthly_expenses_progress">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$.ajax({
		url : '/admin/reports/get-total-receivable',
		success : function(data){
			$('.total_receivable_count').text( data );
		}
	});

	$.ajax({
		url : '/admin/reports/get-total-payable',
		success : function(data){
			$('.total_payable_count').text( data );
		}
	});

	$.ajax({
		url : '/admin/reports/get-total-inventory',
		success : function(data){
			$('.total_inventory_count').text( data );
		}
	});

	$.ajax({
		url : '/admin/reports/get-net-position',
		success : function(data){
			$('.total_net_position_count').text( data );
		}
	});

	$.ajax({
		url : '/admin/reports/my-units-available',
		success : function(data){
			$('.non_sold_cars').text( data );
		}
	});

	$.ajax({
		url : '/admin/reports/get-curr-month-expenses',
		dataType : 'json',
		success : function(data){
			$('.curr_month_expeses').text( data.current_month );
			$('.curr_week_expenses').text( data.current_week );
			$('.today_expenses').text( data.today );
			var morris_data = data.morris;
			Morris.Area({
			  element: 'morris-chart-area',
			  data: morris_data,
			  xkey: 'd',
			  ykeys: ['expenses'],
			  labels: ['Expenses'],
			  smooth: false,
			});
		}
	});

	$.ajax({
		url : '/admin/reports/get-curr-month-sales',
		dataType : 'json',
		success : function(data){
			$('.curr_month_sales').text( data.current_month );
			$('.curr_week_sales').text( data.current_week );
			$('.today_sales').text( data.today );
			var morris_data = data.morris;
			Morris.Area({
			  element: 'morris-chart-area2',
			  data: morris_data,
			  xkey: 'd',
			  ykeys: ['sales'],
			  labels: ['Sales'],
			  smooth: false,
			});
		}
	});

	$.ajax({
		url : '/admin/reports/get-monthly-expenses',
		dataType : 'json',
		success : function(data){
			$.each( data, function( key, value ){
				$('.monthly_expenses_progress').append('<div class="widget_summary"><div class="w_left w_20"><span>' + value.month + '</span></div><div class="w_center w_55"><div class="progress"><div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + value.percentage + '%;"><span class="sr-only">' + value.percentage + '% Complete</span></div></div></div><div class="w_right w_35"><small>' + value.expenses + ' AED</small></div><div class="clearfix"></div></div>');
			});
		}
	});

	$.ajax({
		url : '/admin/reports/get-arriving-containers',
		dataType : 'json',
		success : function(data){
			$.each( data, function( key, value ){
				$('.containers_list').append('<article class="media event"><a class="pull-left date"><p class="month">' + value.month + '</p><p class="day">' + value.day + '</p></a><div class="media-body"><a class="title" href="/admin/car-containers/'+ value.id +'">W8</a></div></article>');
			});
		}
	});
});
</script>

@stop