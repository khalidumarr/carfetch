@extends('layouts/admin')
@section('content')
<div class="header">
	<a href="/sale" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
	<h3>Sales Detail</h3>	
</div>
<div class="body">
	<div class="row">
		<div class="col-md-6">
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>Invoice Numnber</td>
						<td>{{ $sale->number }}</td>
					</tr>
					<tr>
						<td>Sale Price</td>
						<td>{{ Price::format($sale->price) }}</td>
					</tr>
					<tr>
						<td>Sale Vat</td>
						<td>{{ Price::format($sale->vat) }}</td>
					</tr>
					<tr>
						<td>Customer Name</td>
						<td>{{ $sale->cx_name }}</td>
					</tr>
					<tr>
						<td>Customer Phone</td>
						<td>{{ $sale->cx_phone }}</td>
					</tr>
					<tr>
						<td>Customer Trn</td>
						<td>{{ $sale->cx_trn }}</td>
					</tr>
					<tr>
						<td>Sale Date</td>
						<td>{{ $sale->created_at }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			<h3>Sales Documents</h3>
			@if ($sale->hasDocuments()) 
				@foreach ($sale->documents as $document)
					<a href="{{ $document->url }}" target="_blank"> <img class="img-thumbnail" width="200" src="{{ $document->url }}"></a>
				@endforeach
			@endif

			<form action="/sale/{{ $sale->id }}/upload-document" method="POST" enctype="multipart/form-data">
				<div class="form-group">
		          <div class="row">
		            {{ Form::label('documents[]', 'Upload Sales Documents', [ 'class' => 'control-label' ] ) }}	            
		            {{ Form::file('documents[]', ['multiple' => 'multiple']) }}
		          </div>
		        </div>
		        <input type="submit" class="btn btn-primary" value="Upload">
			</form>
		</div>
	</div>	
</div>
@stop