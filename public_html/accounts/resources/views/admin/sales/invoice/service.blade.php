@extends('layouts/admin')
@section('content')
@include('admin/sales/invoice/header')


  <div class="row">
    <div class="col-xs-8">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Vin #</th>                                                
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{{ $sale->car->model->make->name }} {{ $sale->car->model->name }} {{ $sale->car->year }}</td>
            <td>{{ $sale->car->propByCode( 'vin_number' )->value }}</td>                        
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12 table">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Title</th>
                <th>Amount</th>
                <th>Vat</th>                        
                <th>Vat %</th>                        
                <th>Total</th>                                
              </tr>
            </thead>
            <tbody>              
              @foreach ($sale->car->costs as $cost)                
                @if (!$cost->cost || !$cost->cost->in_invoice)
                  @continue
                @endif
                <tr>
                  <td>{{ $cost->cost->name }}</td>
                  <td>{{ Price::format($cost->value) }}</td>
                  <td>{{ Price::format($cost->vat) }}</td>
                  <td>{{ ($cost->vat) ? '5%' : '0%' }}</td>
                  <td>{{ Price::format(($cost->value + $cost->vat)) }}</td>
                </tr>
              @endforeach
              <tr style="background: #bfbfbf">
                <td><b>Total</b></td>
                <td><b>{{ Price::format($sale->price) }}</b></td>                
                <td><b>{{ Price::format($sale->vat) }}</b></td>
                <td></td>
                <td><b>{{ Price::format($sale->price + $sale->vat) }}</b></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
    </div>
  </div>
@include('admin/sales/invoice/footer')
@stop