<div class="body">
  <div class="row">
    <div class="col-sm-3">      
    </div>
    <div class="col-sm-6">
      <div class="tax_invoice">
        <h3>Tax Invoice</h3>
        <small>TRN: 100369195100003</small>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="pull-right">
        Date: {{ date('d-m-Y', strtotime($sale->created_at)) }}
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-3">
      <img src="/images/logo.png" width="200">
    </div>    
  </div>

  <div class="row">
    <div class="col-xs-5">
      From<br>
      <strong>Al Mutafawiq Used Cars</strong>
      <br>Shed 7, Industrial area 4,Sharjah UAE
      <br>Phone: 0554457898,0501631761,0569854255
      <br>Email: almutafawiqusedcars@gmail.com      
    </div>  

    <div class="col-xs-3">
        To<br>
      <strong>{{ $sale->customer->name }}</strong>
      <br>{{ $sale->country->name }}
      <br>Customer TRN: {{ $sale->cx_trn }}
    </div> 

    <div class="col-xs-3">      
      <div class="pull-right">
        <b>Invoice #{{ $sale->number }}</b>              
      </div>
      <br>                  
    </div> 
  </div>