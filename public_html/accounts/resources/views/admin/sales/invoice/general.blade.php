@extends('layouts/admin')
@section('content')
@include('admin/sales/invoice/header')  
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <div class="row">
          <div class="col-xs-12 table">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th style="width: 30%">Name</th>
                  <th>Vin #</th>
                  <th>Color</th>                        
                  <th>Amount</th>
                  <th>Vat %</th>
                  <th>Vat</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ $sale->car->model->make->name }} {{ $sale->car->model->name }} {{ $sale->car->year }}</td>
                  <td>{{ $sale->car->propByCode( 'vin_number' )->value }}</td>
                  <td><?=($sale->car->color) ? $sale->car->color : '-'?></td>                                                                  
                  <td>{{ number_format( $sale->price ) }} AED</td>
                  <td>5%</td>
                  <td>{{ Price::format($sale->vat) }}</td>                  
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@include('admin/sales/invoice/footer')
@stop