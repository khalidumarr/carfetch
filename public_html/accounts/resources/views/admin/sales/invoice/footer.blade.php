<div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-6" style="font-size: 12px">
      <h3>Amount in Words</h3>
      <p class="text-muted well well-sm no-shadow" >                    
        {{ Price::toWords($sale->price) }}
      </p>
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td>{{ Price::format( $sale->price ) }}</td>
            </tr>
            <tr>
              <th>Total Vat:</th>
              <td>{{ Price::format( $sale->vat ) }}</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>{{ Price::format( $sale->price + $sale->vat ) }}</td>
            </tr>
            <tr>
              <th>Paid:</th>
              <td>{{ Price::format( $sale->amount_paid + $sale->vat ) }}</td>
            </tr>            
            <tr>
              <th>Balance:</th>
              <td>{{ Price::format( ( $sale->price - $sale->amount_paid ) ) }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.col -->
  </div>

  <div class="row">
    <div class="col-xs-12">
      <button class="btn btn-default no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>                  
    </div>
  </div>  
</div>  

<style type="text/css">
  @media print{    
      .body{
        font-size: 12px !important;
      }
      .no-print{
          display: none !important;
      }
  }

  .tax_invoice
  {
    text-align: center;
  }
</style>