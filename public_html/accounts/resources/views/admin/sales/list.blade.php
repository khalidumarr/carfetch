@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/accounts-heads" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
    <h3>Transactions</h3>    
</div>
<div class="body">
    <div class="row">
        <form action="/sale" method="GET">
            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="date_from">Date From: </label>
                        <input id="date_from" autocomplete="off" name="date_from" type="text" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_from'])){{ $_REQUEST['date_from'] }}@endif">
                    </div>
                </div>
            </div>    

            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="date_to">Date To: </label>
                        <input id="date_to" autocomplete="off" name="date_to" type="text" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_to'])){{ $_REQUEST['date_to'] }}@endif">
                    </div>
                </div>
            </div>    
            <div class="col-sm-3">
                <div class="input-group">
                    <br>
                    <input type="submit" value="Search" class="btn btn-default search_submit">
                </div>
            </div>  
        </form>
    </div>
	<div class="body table-responsive">
        <table class="table jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title">Item</th>
                    <th class="column-title">Vin # </th>
                    <th class="column-title">Sale Type </th>
                    <th class="column-title">Customer Name</th>
                    <th class="column-title">Amount</th>
                    <th class="column-title">VAT</th>                            
                    <th class="column-title">Created At</th>                            
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                </tr>
            </thead>

            <tbody>
                @if( count( $sales ) == 0 )
                    <tr class="even pointer">
                        <td colspan="6">No Sale Found</td>                
                    </tr>
                @else
                    @foreach( $sales as $sale )                    
                        <tr class="even pointer">
                            <td>{{ $sale->car->model->make->name }}</td>
                            <td>{{ $sale->car->propByCode('vin_number')->value }}</td>
                            <td>{{ $sale->saleType->title }}</td>
                            <td>{{ $sale->customer->name }}</td>
                            <td>{{ $sale->price }}</td>
                            <td>{{ $sale->vat }}</td>                                 
                            <td>{{ date('Y-m-d', strtotime($sale->created_at)) }}</td>                                 
                            <td style="width: 300px">                                       
                                <a class="btn btn-default source" href="/sale/{{ $sale->id }}/show/">View</a>
                                <a class="btn btn-danger source delete_transaction" data-id="{{ $sale->id }}">Delete</a>
                                <a class="btn btn-success source" href="/sale/{{ $sale->id }}/generate-invoice/">Generate Invoice</a>                                        
                            </td>
                        </tr>    
                    @endforeach
                @endif  
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $total }} AED</td>
                    <td>{{ $totalVat }} AED</td>
                    <td></td>
                    <td></td>
                </tr> 
            </tbody>
        </table>                        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.delete_transaction').click(function(e){
            e.preventDefault();
            var id = $(this).data('id');
            if (confirm("Are you sure, you want to delete this ?")) {
                window.location.href = '/sale/' + id + '/delete/';
            }
        });

        $('#date_from').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });        

        $('#date_to').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });        
    });
</script>
@stop