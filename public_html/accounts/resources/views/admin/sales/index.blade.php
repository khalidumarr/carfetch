@extends('layouts/admin')
@section('content')
<div class="header">
    <h2>
        Sale Information        
    </h2>    
</div>
<div class="body">
    <form action="/sale" method="POST" class="form-horizontal form-label-left">
        <input type="hidden" name="user_id" value="">
        <input type="hidden" name="sale_type" value="">
        <input type="hidden" name="inventory" value="{{ $inventory_id }}">
        <div class="form-group">
            <label for="customer_phone" class="control-label col-md-3 col-sm-3 col-xs-12">Customer Phone</label>
            <div class="col-md-5">
                <div class="form-line">
                    <input type="number" name="customer_phone" required="true" id="customer_phone" class="form-control" placeholder="05XXXXXXXX" />
                </div> 
            </div>
        </div>        
        <div class="form-group">
            <div class="customer_name_loader preloader pl-size-xs" style="display: none;">
                <div class="spinner-layer pl-red-grey">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <label for="customer_name" class="control-label col-md-3 col-sm-3 col-xs-12">Customer Name</label>
            <div class="col-md-5">
                <div class="form-line">
                    <input type="text" name="customer_name" id="customer_name" required="true" class="form-control" placeholder="Customer Name" />
                </div> 
            </div>
        </div>                

        <div class="form-group">
            <label for="customer_trn" class="control-label col-md-3 col-sm-3 col-xs-12">Customer TRN</label>
            <div class="col-md-5">
                <div class="form-line">
                    <input type="text" name="customer_trn" id="customer_trn" class="form-control" placeholder="Customer TRN" />
                </div> 
            </div>
        </div>                

        <div class="form-group">
            <label for="customer_couuntry" required="true" class="control-label col-md-3 col-sm-3 col-xs-12">Customer Country</label>
            <div class="col-md-5">                
                <select id="customer_country" name="customer_country" class="form-control">
                    <option>Select Country ...</option>
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                    @endforeach
                </select>                                    
            </div>
        </div>                
        <div class="form-group">
            <label for="sale_date" class="control-label col-md-3 col-sm-3 col-xs-12">Sale Date</label>
            <div class="col-md-5">
                <div class="form-line">
                    <input type="text" name="sale_date" id="sale_date" required="true" class="form-control" value="{{ date('d-m-Y') }}" placeholder="Sale Date" />
                </div> 
            </div>
        </div>                
        <div class="row">                
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a href="javascript:void(0);" class="sale_type_trigger" data-target="oouae" data-type="export" style="text-decoration: none; color: #ffffff">
                    <div class="body bg-red" style="text-align: center">                         
                        <span class="checkbox_container"></span>
                        OUT OF UAE
                    </div>                    
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <a href="javascript:void(0);" class="sale_type_trigger" data-target="ss" data-type="service" style="text-decoration: none; color: #ffffff">
                        <div class="body bg-cyan" style="text-align: center">
                            <span class="checkbox_container"></span>
                            SHIPPING SERVICE
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <a href="javascript:void(0);" class="sale_type_trigger" data-target="wiuae" data-type="general" style="text-decoration: none; color: #ffffff">                        
                        <div class="body bg-purple" style="text-align: center">
                            <span class="checkbox_container"></span>
                            WITH IN UAE
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3"></div>    
            <div class="col-lg-12">
                <div class="invoice_detail" id="oouae" style="display: none;">
                    <div class="form-group">                        
                        <label for="sale_price" class="control-label col-md-3 col-sm-3 col-xs-12">Sale Price</label>
                        <div class="col-md-6">
                            <div class="form-line">
                                <input type="text" id="sale_price" name="export_sale_price" class="form-control" placeholder="AED" />
                            </div> 
                        </div>
                    </div>                
                </div>
                <div class="invoice_detail" id="ss" style="display: none;">                    
                    <div class="form-group">                       
                        <div class="col-md-3"><b>Name</b></div> 
                        <div class="col-md-3">                             
                            <b>Cost</b>
                        </div>
                        <div class="col-md-3">                             
                            <b>Vat Value</b>
                        </div>
                    </div>
                    @foreach( $costs as $cost )                           
                        @if (!isset($cost->cost) || !$cost->cost->in_invoice)
                            @continue
                        @endif
                        <div class="form-group">
                            <div class="col-md-3"> 
                                {{ $cost->cost->name }}
                            </div>                            
                            <div class="col-md-3"> 
                                <div class="form-line">                   
                                    {{ Form::text( $cost->cost->code, $cost->value, 
                                        [ 
                                            'class' => 'car-costs-input form-control', 
                                            'data-conversion-rate' => $cost->currency->conversion, 
                                            'data-vat-factor' => $vat_factor, 
                                            'data-vat-participant' => $cost->cost->is_vat ]
                                        ) 
                                    }}                                    
                                </div>
                            </div>
                            <div class="col-md-3"> 
                                <div class="form-line">                   
                                    {{ Form::text( $cost->cost->code . '_vat', (($cost->is_vat) ? $cost->vat: 0), [ 'class' => 'car-costs-input-vat form-control', 'data-conversion-rate' => $cost->currency->conversion ]) }}                                    
                                </div>
                            </div> AED                            
                        </div>
                    @endforeach                    

                </div>
                <div class="invoice_detail" id="wiuae" style="display: none;">
                    <div class="form-group">                        
                        <label for="sale_price" class="control-label col-md-3 col-sm-3 col-xs-12">Sale Price</label>
                        <div class="col-md-6">
                            <div class="form-line">
                                <input type="text" id="sale_price" name="general_sale_price" class="form-control" placeholder="AED" />
                            </div> 
                        </div>
                    </div>                
                </div>
                <input type="submit" class="invoice-btn btn btn-block btn-lg btn-success waves-effect" value="Generate Invoice" style="display: none;">
            </div>        
        </div>
    </form>
</div>

<script type="text/javascript">    
    $(document).ready(function(){
        $('.sale_type_trigger').click(function(){
            $('.invoice_detail').hide();            
            $('#' + $(this).data('target')).show();

            $('.sale_type_trigger').find('.checkbox_container').html('');
            $(this).find('.checkbox_container').html('<div class="demo-google-material-icon"> <i class="material-icons">check_box</i></div>');
            $('.invoice-btn').show();
            $('input[name="sale_type"]').val($(this).data('type'));
        })

        $(document).on('focusout', 'input[name=customer_phone]', function(){
            var phoneNumber = $(this).val();
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/api/user',
                beforeSend: function(){
                    $('.customer_name_loader').show();
                },
                data: {
                    phone: phoneNumber
                },
                success: function(response) {
                    $('.customer_name_loader').hide();
                    if (response.user) {
                        $('input[name="customer_name"]').val(response.user.name);
                        $('input[name="user_id"]').val(response.user.id);                        
                    }
                }
            });
        });

         $(document).on('focusout', '.car-costs-input', function(){
            if ($(this).data('vat-participant') == 1) {                
                var vat = Math.ceil(($(this).val() * vat_factor));
                $('input[name="' + $(this).attr('name') + '_vat' + '"]').val(vat);
            }
         });
    });
</script>
@stop