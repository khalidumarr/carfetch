@extends('layouts/admin')
@section('content')
<div class="header">
	<h3>Notification</h3>
</div>
<div class="body">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>Year</th>
						<th>Model</th>
						<th>VIN Number</th>
						<th>Type</th>
						<th>Description</th>
						<th>Created At</th>
						<th></th>
					</tr>
					@foreach ($notifications as $notification)
						<tr>
							<td>{{ $notification->sale->car->year }}</td>
							<td>{{ $notification->sale->car->model->name }}</td>
							<td>{{ substr($notification->sale->car->propByCode('vin_number')->value, -6) }}</td>
							<td>{{ $notification->type }}</td>
							<td>{{ $notification->description }}</td>
							<td>{{ $notification->created_at }}</td>
							<td><a class="btn btn-primary" href="/sale/{{ $notification->sale_id }}/show">View</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop