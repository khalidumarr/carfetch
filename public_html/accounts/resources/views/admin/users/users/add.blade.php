@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/user" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
    <h3> Add New User </h3>
</div>
<div class="body">
    <div class="row"> 
        <div class="col-md-12">
            {!! Form::model($user, ['route' => ['user.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
                <div class="form-group">
                    {{ Form::label('name', 'Full Name*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::text('name', null, [ 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('email', 'Email Address', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::text('email', null, [ 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="javascript:void(0);" class="email_generator btn btn-default"> Generate Default Email (*@gmail.com) </a>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('phone', 'Phone Number (+971 xx xxxxxxx)', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::text('phone', null, [ 'id' => 'phone', 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ Form::label('address', 'Address', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-1">
                        <div class="form-line">
                            <textarea id="address" name="address" class="form-control"></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('active', 'Active*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">                                                                
                        <div class="switch">
                            <label><input type="checkbox" name="active" checked=""><span class="lever switch-col-orange"></span></label>
                        </div>
                    </div>
                </div>        

                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="javascript:void(0);" class="pwd_generator btn btn-default"> Generate Default Password (123456) </a>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('password', 'Password*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::password('password', [ 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('password_confirmation', 'Confirm Password*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::password('password_confirmation', [ 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>
            
                <div class="ln_solid"></div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">User Roles</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        
                            @foreach( $roles as $role )
                                @can('change-user-roles')
                                    <input type="checkbox" id="{{$role->id}}" class="chk-col-red" name="role[]" value="{{ $role->id }}">
                                    <label for="{{$role->id}}">{{ ucwords( $role->name ) }}</label>                    
                                @else
                                    @if( $role->code == 'customer' )
                                        <label class="btn btn-primary {{ in_array( $role->id, $user_roles ) ? 'active' : '' }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                            <input type="checkbox" name="role[]" value="{{ $role->id }}"> {{ ucwords( $role->name ) }}
                                        </label>
                                    @endif    
                                @endcan
                            @endforeach
                        
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
                    </div>
                </div>
            {!! Form::close() !!}        
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.pwd_generator').click(function(){
            $('input[name=password]').val( '123456' );
            $('input[name=password_confirmation]').val( '123456' );
        });
        $('.email_generator').click(function(){
            $('input[name=email]').val( $('input[name="name"]').val() + '@gmail.com' );            
        });
    });
</script>
@stop