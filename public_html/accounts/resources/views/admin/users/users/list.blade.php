@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>
    <h3> User List </h3>
</div>
<div class="body">        
    <div class="row">
        <div class="col-md-12">
                <form action="/user/" class="form-horizontal form-label-left">
                    <div class="row">   
                        <div class="col-md-3">
                            <div class="form-line">
                                <input name="name" class="form-control search_field" placeholder="Name" value="{{ ( isset( $_REQUEST['name'] ) ) ? $_REQUEST['name'] : '' }}">                    
                            </div>
                        </div>
                        @can( 'change-user-roles' )
                            <div class="col-md-3">
                                <div class="form-line">
                                    <select name="role" class="form-control search_field model_chosen" placeholder="Model">
                                        <option value="">Select User Role</option>
                                        @foreach( $roles as $role )
                                            <option  value="{{ $role->id }}" @if( ( isset( $_REQUEST['role'] ) ) &&  $_REQUEST['role'] == $role->id ) selected @endif>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endcan
                        <div class="col-md-3">
                            <div class="form-line">
                                <input name="email" class="form-control search_field" placeholder="Email" value="{{ ( isset( $_REQUEST['email'] ) ) ? $_REQUEST['email'] : '' }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-line">
                                <input name="phone" class="form-control search_field" placeholder="Phone Number" value="{{ ( isset( $_REQUEST['phone'] ) ) ? $_REQUEST['phone'] : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="submit" value="Search" class="btn btn-default search_submit">
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-9">
                        <form action="/admin/users/">
                        </form>
                    </div>
                    <div class="col-sm-3">
                        <div class="section_add text-right">
                            <a href="/user/create" type="button" class="btn btn-primary btn-md"> Add New User</a>
                        </div>
                    </div>
                </div>      
                <div class="table-responsive">
                  <table class="table table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th>
                          <input type="checkbox" id="check-all" class="flat">
                        </th>
                        <th class="column-title">Full Name </th>
                        <th class="column-title">Roles </th>
                        <th class="column-title">Email </th>
                        <th class="column-title">Active </th>
                        <th class="column-title">Updated at </th>
                        <th class="column-title">Created at </th>
                        <th class="column-title no-link last"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                        @foreach( $users as $user )
                            <tr class="even pointer">
                                <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                                <td>{{ $user->name }}</td>
                                <td>
                                    @foreach( $user->roles as $key => $role )
                                        {{ $role->name }}
                                        @if( count( $user->roles ) != ( $key + 1 ) )
                                            |
                                        @endif
                                    @endforeach
                                </td>
                                <td> {{ $user->email }} </td>
                                <td>{{ ( $user->active ) ? 'Yes' : 'No' }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->updated_at }}</td>
                                <td>
                                    <div style="float: left; width: 50%;padding-right: 30px;">
                                        @can('edit-user', $user)
                                           <a class="btn btn-default source" href="/user/{{ $user->id }}/edit/">Edit</a>
                                        @endcan                            
                                    </div>
                                    <div style="float: left; width: 50%;padding-right: 30px">
                                        @can('delete-user')
                                            {{ Form::open( [ 'url' => '/user/' . $user->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                                                {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                                            {{ Form::close() }}
                                        @endcan                            
                                    </div>
                                </td>
                            </tr>    
                        @endforeach   
                    </tbody>
                  </table>
                   {{ $users->links() }}
                </div>
        </div>
    </div>    
</div>
@stop