@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/accounts-heads" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>
    <h3> Edit Account Head </h3>
</div>
<div class="body">
    <div class="row"> 
        <div class="col-md-12">
            {!! Form::model($head, ['route' => ['accounts-heads.update', $head], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                <div class="form-group">
                    {{ Form::label('title', 'Title*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::text('title', null, [ 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('code', 'Code*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::text('code', null, [ 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('type_id', 'Type*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}            
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            <select name="type_id" class="form-control">
                                <option value="">Select Type</option>
                                @foreach( $types as $type )
                                    <option value="{{ $type->id }}" @if( $head->type_id == $type->id ) selected @endif> {{ $type->title }} </option>
                                @endforeach
                            </select> 
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('default_value', 'Value', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::text('default_value', null, [ 'class' => 'form-control' ]) }}
                        </div>
                    </div>
                </div>                 
                <div class="form-group">
                    {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            {{ Form::textarea('description', null, [ 'class' => 'form-control', 'rows' => 2 ]) }}
                        </div>
                    </div>
                </div>
            
                <div class="ln_solid"></div> 
                
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
                    </div>
                </div>
            {!! Form::close() !!}            
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on( 'input', 'input[name=name], input[name=title]', function(){
            $('input[name=code]').val( $(this).val().split(' ').join('-').toLowerCase() );
        })
    });
</script>
@stop