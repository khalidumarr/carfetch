@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>
    <h3>Account Heads List</h3>
</div>
<div class="body">
    <div class="row">        
        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-purple hover-expand-effect">
                <a class="icon" href="/car-costs">
                    <i class="material-icons">settings</i>
                </a>
                <div class="content">
                    <div class="text">COSTS SETTING</div>                            
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <a class="icon" href="/sale">
                    <i class="material-icons">attach_money</i>
                </a>
                <div class="content">
                    <div class="text">TRANSACTIONS</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-lime hover-expand-effect">
                <a class="icon" href="/accounts-payables">
                    <i class="material-icons">call_made</i>
                </a>
                <div class="content">
                    <div class="text">PAYABLES</div>                            
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12">
            <div class="info-box bg-red hover-expand-effect">
                <a class="icon" href="/expenses">
                    <i class="material-icons">layers</i>
                </a>
                <div class="content">
                    <div class="text">EXPENSES</div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <form action="/accounts-heads/">
            </form>
        </div>
        <div class="col-sm-3">
            <div class="section_add text-right">
                <a href="/accounts-heads/create" type="button" class="btn btn-primary btn-md"></span> Add Account Head</a>
            </div>
        </div>
    </div>      
    <div class="table-responsive">
      <table class="table table-striped jambo_table bulk_action">
        <thead>
          <tr class="headings">
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th class="column-title">Title </th>
            <th class="column-title">Type </th>
            <th class="column-title">Value </th>
            <th class="column-title">Updated at </th>
            <th class="column-title">Created at </th>
            <th class="column-title no-link last"><span class="nobr">Action</span>
            </th>
          </tr>
        </thead>

        <tbody>
            @if( count( $heads ) == 0 )
                <tr class="even pointer">
                    <td colspan="7">No Account heads Found</td>                
                </tr>
            @else
                @foreach( $heads as $head )
                    <tr class="even pointer">
                        <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                        <td>{{ $head->title }}</td>
                        <td>{{ $head->type->title }}</td>
                        <td>{{ Price::format($head->value) }}</td>
                        <td>{{ $head->created_at }}</td>
                        <td>{{ $head->updated_at }}</td>
                        <td>
                            <a class="btn btn-default source" href="/accounts-heads/{{ $head->id }}/edit/">Edit</a>
                        </td>
                    </tr>    
                @endforeach
            @endif   
        </tbody>
      </table>
       {{ $heads->links() }}
    </div>
</div>
@stop