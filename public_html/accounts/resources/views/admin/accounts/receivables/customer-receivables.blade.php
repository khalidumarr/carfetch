@extends('layouts/admin')
@section('content')

<div class="">
  <div class="page-title">
    <div class="row">
      @if( Session::has('message') )
          <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              {{ Session::get('message') }}
          </div>
      @endif
      @if( Session::has('errors') )
          <div class="alert alert-danger alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              {{ Session::get('errors') }}
          </div>
      @endif
    </div>
  </div>

  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Customer Receivables</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Customer Name</th>
              <th>Customer Phone</th>
              <th>Total Purchases</th>
              <th>Total Payable</th>
              <th>Total Paid</th>
              <th>Reamining</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
            @foreach( $customers as $customer )
              <tr>
                <td>{{ $customer['user']->name }}</td>
                <td>{{ $customer['user']->phone }}</td>
                <td>{{ $customer['total_purchases'] }}</td>
                <td>{{ $customer['total_payable'] }}</td>
                <td>{{ $customer['total_paid'] }}</td>
                <td>{{ $customer['total_payable'] - $customer['total_paid'] }}</td>
                <td>
                  @if( count( $customer['user']->purchases ) > 0 )
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target=".show-cx-cars-{{ $customer['user']->id }}">View Cars</button>
                    <div class="modal fade show-cx-cars-{{ $customer['user']->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-md">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel2">Add Invoice Details</h4>
                          </div>
                          <div class="modal-body">                
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th>Title</th>
                                  <th>Price</th>
                                  <th>Paid Amount</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                @if( count( $customer['user']->purchases ) > 0 )                              
                                  @foreach( $customer['user']->purchases as $purchase )                                
                                    <tr>
                                    <td>{{ $purchase->car->title }}</td>
                                    <td>{{ $purchase->price }}</td>
                                    <td>{{ $purchase->amount_paid }}</td>
                                    <td><a class="btn btn-default source" target="_blank" href="/admin/cars/{{ $purchase->car->id }}/">View</a></td>
                                  </tr>
                                  @endforeach
                                @else
                                  <tr>
                                    <td colspan="4"> No Purchases Found</td>
                                  </tr>
                                @endif
                              </tbody>
                            </table>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Print Invoice">
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  @endif
                </td>                
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@stop