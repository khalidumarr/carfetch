@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/accounts-heads" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
    <h3> Monthly Expenses List </h3>    
</div>
<div class="body">    
    <div class="row">
        <form action="/expenses" method="GET">
            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="date_from">Date From: </label>
                        <input id="date_from" name="date_from" type="text" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_from'])){{ $_REQUEST['date_from'] }}@endif">
                    </div>
                </div>
            </div>    

            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="date_to">Date To: </label>
                        <input id="date_to" name="date_to" type="text" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_from'])){{ $_REQUEST['date_to'] }}@endif">
                    </div>
                </div>
            </div>    
            <div class="col-sm-3">
                <div class="input-group">
                    <br>
                    <input type="submit" value="Search" class="btn btn-default search_submit">
                </div>
            </div>  
        </form>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <form action="/admin/accounts-expenses/">
            </form>
        </div>
        <div class="col-sm-3">
            <div class="section_add text-right">
                <a href="/expenses/create" type="button" class="btn btn-primary btn-md"> Add Current Month Expense</a>
            </div>
        </div>
    </div>      
    <div class="table-responsive">
      <table class="table table-striped jambo_table bulk_action">
        <thead>
          <tr class="headings">
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th class="column-title">Year </th>
            <th class="column-title">Month </th>
            <th class="column-title">Expense </th>
            <th class="column-title">Value</th>
            <th class="column-title">Vat</th>
            <th class="column-title">Created at</th>
            <th class="column-title">Updated at</th>
            <th class="column-title no-link last"><span class="nobr">Action</span>
            </th>
          </tr>
        </thead>

        <tbody>
            @if( count( $expenses ) == 0 )
                <tr class="even pointer">
                    <td colspan="8">No Monthly Expeses Found</td>                
                </tr>
            @else
                @foreach( $expenses as $expense )
                    <tr class="even pointer">
                        <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                        <td>{{ $expense->year }}</td>
                        <td>{{ $expense->month }}</td>
                        <td>{{ $expense->head->title }}</td>
                        <td>{{ $expense->value }} {{ $expense->currency->code }}</td>
                        <td>{{ $expense->vat }} {{ $expense->currency->code }}</td>
                        <td>{{ $expense->created_at }}</td>
                        <td>{{ $expense->updated_at }}</td>
                        <td>
                            <a class="btn btn-default source" href="/accounts-expenses/{{ $expense->id }}/edit/">Edit</a>
                            {{ Form::open( [ 'url' => '/accounts-expenses/' . $expense->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                                {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                            {{ Form::close() }}
                        </td>
                    </tr>    
                @endforeach

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $total }}</td>
                    <td>{{ $totalVat }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endif   
        </tbody>
      </table>
       
    </div>
</div>
@stop