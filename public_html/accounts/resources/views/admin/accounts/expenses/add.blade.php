@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/expenses" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
    <h3> Add New Monthly Expense </h3>    
</div>

<div class="body">
    <div class="row"> 
        {!! Form::model($expense, ['route' => ['expenses.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
            <div class="form-group">
                {{ Form::label('head_id', 'Accounts Head*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="head_id" class="form-control select2_expense">
                        <option disabled="disabled">Select Head</option>
                        @foreach( $heads as $head )
                            <option value="{{ $head->id }}"> {{ $head->title }} </option>
                        @endforeach
                    </select> 
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('value', 'Value*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('value', null, [ 'class' => 'vat-participant form-control', 'data-target-field' => 'vat' ]) }}                        
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('vat', 'Vat*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('vat', null, [ 'class' => 'form-control' ]) }}                        
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('trn', 'TRN*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('trn', null, [ 'class' => 'form-control' ]) }}                        
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="currency_id" class="form-control" disabled="disabled">
                        <option disabled="disabled">Select Cost Currency</option>
                        @foreach( $currencies as $currency )
                            <option value="{{ $currency->id }}" > {{ $currency->name }} </option>
                        @endforeach
                    </select> 
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Receipt Image</label>
                <div class="col-md-6 col-sm-6 col-xs-12">                    
                    {{ Form::file('image[]', ['multiple' => 'multiple']) }}
                </div>                
            </div>
        
            <div class="ln_solid"></div> 
            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@stop