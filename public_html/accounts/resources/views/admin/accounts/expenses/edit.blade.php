@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/expenses" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
    <h3> Expenses List </h3>    
</div>

<div class="body">
    <div class="row">         
        {!! Form::model($expense, ['route' => ['accounts-expenses.update', $expense], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
            <div class="form-group">
                {{ Form::label('head_id', 'Account Head*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="head_id" class="form-control select2_expense">
                        <option disabled="disabled">Select Account Head</option>
                        @foreach( $heads as $key => $head )
                            <option value="{{ $head->id }}"> {{ $head->title }} </option>
                        @endforeach
                    </select> 
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('value', 'Value*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('value', null, [ 'class' => 'form-control' ]) }}                        
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('vat', 'Vat*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('vat', null, [ 'class' => 'form-control' ]) }}                        
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('trn', 'TRN*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('trn', null, [ 'class' => 'form-control' ]) }}                        
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="currency_id" class="form-control" disabled="disabled">
                        <option disabled="disabled">Select Cost Currency</option>
                        @foreach( $currencies as $currency )
                            <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                        @endforeach
                    </select> 
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Receipt Image</label>
                <div class="col-md-6 col-sm-6 col-xs-12">                    
                    {{ Form::file('image[]', ['multiple' => 'multiple']) }}
                </div>                
            </div>

            <div class="form-group">
                <div class="col-md-3 col-sm-3 col-xs-12"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @foreach( $expense->images as $image )
                        <div class="col-md-4 image_item" data-image-id="{{ $image->id }}">
                            <div class="thumbnail" style="height:auto;">
                              <div class="image view view-first">
                                <img style="width: 100%; display: block;" src="{{ $image->url }}" alt="image">
                                <div class="mask">
                                  <div class="tools tools-bottom">
                                    <a href="#"><i class="fa fa-file-image-o"></i></a>                                
                                    <a class="upload_delete" data-id="{{$image->id}}" href="javascript:void(0);">Delete</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    {{ Form::submit('Update', [ 'class' => 'btn btn-success' ] ) }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@stop