@extends('layouts/admin')
@section('content')
<div class="header">
  <a href="/accounts-heads" class="backarrow">
      <i class="material-icons">keyboard_backspace</i>
  </a> 
  <h3>Total Payables (<small>{{ $total_payable }} AED</small>)</h3>
</div>
<div class="body"> 
  <div class="row">
    <form action="/accounts-payables" method="GET">
        <div class="col-md-3">
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">person</i>
            </span>
            <div class="form-line">                
                <label for="user_id">User: </label>
                <select name="user_id" class="form-control select2_expense">
                    <option>Select Shipper</option>
                    @foreach( $shippers as $user )
                        <option value="{{ $user->id }}" @if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] == $user->id) selected @endif> {{ $user->name }} </option>
                    @endforeach
                </select> 
            </div>                        
          </div>
        </div>
        
        <div class="col-md-3">                
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">date_range</i>
                </span>
                <div class="form-line">
                    <label for="date_from">Date From: </label>
                    <input id="date_from" name="date_from" type="text" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_from'])){{ $_REQUEST['date_from'] }}@endif">
                </div>
            </div>
        </div>    

        <div class="col-md-3">                
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">date_range</i>
                </span>
                <div class="form-line">
                    <label for="date_to">Date To: </label>
                    <input id="date_to" name="date_to" type="text" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_from'])){{ $_REQUEST['date_to'] }}@endif">
                </div>
            </div>
        </div>    
        <div class="col-sm-3">
            <div class="input-group">
                <br>
                <input type="submit" value="Search" class="btn btn-default search_submit">
            </div>
        </div>  
    </form>
  </div> 
  <div class="row">
    <div class="col-md-11"></div>
    <div class="col-md-1">
      <!-- Small modal -->
      <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-sm2">Release</button>
      <div class="modal fade bs-example-modal-sm2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel2">Release Amount</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(['url' => '/admin/accounts-payables/release', 'method' => 'POST', 'files' => true]) !!}
            <input type="hidden" name="back_url" value="{{ $_SERVER['REQUEST_URI'] }}">

                <div class="form-group">
                  <div class="row">
                    {{ Form::label('head_id', 'Accounts Head*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-line">
                        <select name="head_id" class="form-control select2_expense">
                            <option disabled="disabled">Select Head</option>
                            @foreach( $heads as $head )
                                <option value="{{ $head->id }}" data-code="{{ $head->code }}"> {{ $head->title }} </option>
                            @endforeach
                        </select> 
                      </div>                        
                    </div>
                  </div>
                </div>

                <div class="form-group shipper" style="display: none;">
                  <div class="row">
                    {{ Form::label('user_id', 'Shipper', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-line">
                        <select name="user_id" class="form-control select2_expense">
                            <option>Select Shipper</option>
                            @foreach( $shippers as $shipper )
                                <option value="{{ $shipper->id }}"> {{ $shipper->name }} </option>
                            @endforeach
                        </select> 
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-line">
                        {{ Form::text('amount', null, [ 'class' => 'form-control' ]) }}
                      </div>
                    </div>
                  </div>
                </div>   

                <div class="form-group">
                  <div class="row">
                    {{ Form::label('invoice_image', 'Invoice Image', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::file('invoice_image') }}
                    </div>
                  </div>
                </div>            
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" value="Add">
            </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /modals -->
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>Account Head</th>              
            <th>To User</th>              
            <th>Debit</th>
            <th>Value</th>
            <th>Created By</th>
            <th>Date Create</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          @foreach( $payables as $payable )
            <tr>                
              <td>{{ $payable->head->title }}</td>
              <td>{{ ($payable->user) ? $payable->user->name : ''}}</td>
              <td>{{ ( $payable->debit == 1 ) ? 'Yes' : 'No' }}</td>
              <td>{{ $payable->value }} {{ $payable->currency->code }}</td>
              <td>{{ $payable->createdBy->name }}</td>
              <td>{{ $payable->created_at }}</td>
              <td>
                @if ($image = $payable->invoiceImage->first())
                  <a class="btn btn-default" target="_blank" href="{{ $image->url }}">View ivoice</a> 
                @endif

                {{ Form::open( [ 'url' => '/admin/accounts-payables/' . $payable->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                {{ Form::close() }}
              </td>
            </tr>
          @endforeach
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
              <div class="">
                <table class="table">
                  <tr>
                    <th>To Pay</th>
                    <th>Paid</th>
                  </tr>
                  <tr>
                    <td>{{ Price::format($toPay) }}</td>
                    <td>{{ Price::format($paid) }}</td>
                  </tr>
                </table>
              </div>
            </td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $(document).on('change', 'select[name="head_id"]', function(){
      var selected = $('select[name="head_id"] option:selected');
      if (selected.data('code') == 'container-charges') {
        $('.shipper').show();
        $(this).val(selected.val());
      }
    });      
  });
</script>
@stop