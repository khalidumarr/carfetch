@extends('layouts/admin')
@section('content')
<div class="">
  <div class="page-title">
  </div>
  <div class="row">
    @if( Session::has('message') )
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('message') }}
        </div>
    @endif
    @if( Session::has('errors') )
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('errors') }}
        </div>
    @endif
  </div>

  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Customer Receivables</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Agent Name</th>
              <th>Earned</th>
              <th>Paid</th>     
              <th>Pending</th>         
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
            @foreach( $list as $item )
              <tr>
                <td>{{ $item['agent']->name }}</td>
                <td>{{ number_format( $item['earned'] ) }}</td>
                <td>{{ number_format( $item['paid'] ) }}</td>
                <td>{{ number_format( $item['earned'] - $item['paid'] ) }}</td>
                <td>
                  <!-- Small modal -->
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm2-{{ $item['agent']->id }}">Release</button>
                <div class="modal fade bs-example-modal-sm2-{{ $item['agent']->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel2">Release Amount</h4>
                      </div>
                      <div class="modal-body">
                      {!! Form::open(['url' => '/admin/accounts-payables/release', 'method' => 'POST']) !!}
                      <input type="hidden" name="back_url" value="{{ $_SERVER['REQUEST_URI'] }}">
                      <input type="hidden" name="head_id" value="{{ $item['agent']->accountHead()->id }}">

                          <div class="form-group">
                            <div class="row">
                              {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  {{ Form::text('amount', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                              </div>
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="row">
                              {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                              <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                  <select name="currency_id" class="form-control" disabled="disabled">
                                      @foreach( $currencies as $currency )
                                          <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                                      @endforeach
                                  </select> 
                              </div>
                            </div>
                          </div>                
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Add">
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- /modals -->
                </td>                
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop