@extends('layouts/admin')
@section('content')

<div class="page-title no-print">
  <div class="title_left">
    <h3>Adjust & Upload</h3>
  </div>  
</div>
<div class="container" id="crop-avatar">
    <!-- Current avatar -->
    <div class="avatar-view" title="Click to upload new Banner">
      <img src="/images/upload.png">
    </div>

    <!-- Cropping modal -->
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form class="avatar-form" action="/admin/banners" enctype="multipart/form-data" method="post">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">Upload Banner</h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Upload image and data -->
                <div class="avatar-upload">
                  <input type="hidden" class="avatar-src" name="avatar_src">
                  <input type="hidden" class="avatar-data" name="avatar_data">
                  <label for="avatarInput">Upload</label>
                  <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                </div>

                <!-- Crop and preview -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="avatar-wrapper"></div>
                  </div>
                </div>

                <div class="row avatar-btns">
                  <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
                  </div>
                </div>
              </div>
            </div>         
          </form>
        </div>
      </div>
    </div><!-- /.modal -->

    <!-- Loading state -->
    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
  </div>

<div class="clearfix"></div>
<div class="page-title no-print">
  <div class="title_left">
    <h3>Banners</h3>    
  </div>  
</div>
<div class="row">
	@foreach( $banners as $banner )	
		<div class="col-md-3">	
			<div class="thumbnail" style="height:auto;">
				<div class="image view view-first">
					<img src="/uploads/banners/{{ $banner }}">
					<div class="mask">
						<div class="tools tools-bottom">
	                        <a data-fancybox href="/uploads/banners/{{ $banner }}"><i class="fa fa-file-image-o"></i></a>                        
	                        <a class="upload_banner_delete" data-id="{{ $banner }}" href="javascript:void(0);"><i class="fa fa-times"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
    @endforeach
</div>

    <script>
      $(function() {
        $('.upload_banner_delete').click(function() {
        	var img = $(this).data('id');
        	$.ajax({
			  url:"/admin/banners/destroy/",
			  data : {
			  	'image' : img
			  },
			  type:"delete",
			  success:function( data ){
			    if( data == 'success' ){
			    	location.reload();
			    }
			    else{
			    	alert("Unable to delete the banner");
			    }
			  }
			});
        });
      });
    </script>
@stop