@extends('layouts/admin')
@section('content')
<div class="">
  <div class="page-title">
  </div>
  <div class="row">
    @if( Session::has('message') )
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('message') }}
        </div>
    @endif
    @if( Session::has('errors') )
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            {{ Session::get('errors') }}
        </div>
    @endif
  </div>

  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>My Earnings</h2>        
        <div class="clearfix"></div>
      </div>
      <div class="x_content"  style="overflow: scroll">      
        <table class="table table-striped table-bordered" id="inventoryTable">
          <thead>
            <tr>
              <th>Model</th>
              <th>Year</th>
              <th>Vin Number</th>
              <th>Lot Number</th>     
              <th>Earnings</th>
              <th>Sale Price</th>
              <th>Earnings</th>              
            </tr>
          </thead>
          <tbody>
            @foreach( $cars  as $car )
              <tr>
                <td>{{ $car->model->name }}</td>             
                <td>{{ $car->year }}</td>             
                <td>{{ $car->propByCode('vin_number')->value }}</td>             
                <td>{{ $car->propByCode('lot_number')->value }}</td>             
                <td>{{ $car->model->make->name }}</td>             
                <td>{{ number_format( (int)$car->sale->price ) }}</td>
                <td>{{ number_format( (int)$car->getAgentEarnings() ) }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop