@extends('layouts/admin')
@section('content')
<h3> Responsibility # 12 </h3>
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
@if( Session::has('errors') )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('errors') }}
    </div>
@endif
<div class="row">
	<div class="col-md-6">
		<h4>{{ $responsibility->car->title }}</h4>
		<table class="countries_list">
      <tbody>
        <tr>
          <td>Title</td>
          <td class="fs15 fw700 text-right">{{ $responsibility->car->title }}</td>
        </tr>
        <tr>
          <td>Code</td>
          <td class="fs15 fw700 text-right">{{ $responsibility->car->code }}</td>
        </tr>
        <tr>
          <td>Make</td>
          <td class="fs15 fw700 text-right">{{ $responsibility->car->model->make->name }}</td>
        </tr>
        <tr>
          <td>Model</td>
          <td class="fs15 fw700 text-right">{{ $responsibility->car->model->name }}</td>
        </tr>
        <tr>
          <td>Year</td>
          <td class="fs15 fw700 text-right">{{ $responsibility->car->year }}</td>
        </tr>
        @foreach( $responsibility->car->properties as $property )
            <tr>
              <td>{{ $property->prop_name }}</td>
              <td class="fs15 fw700 text-right">{{ $property->value }}</td>
            </tr>
        @endforeach
      </tbody>
    </table>
	</div>
	<div class="col-md-6">
		<div class="animated flipInY">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-money"></i></div>
				<div class="count blue">{{ number_format( $credit, 2 ) }} AED</div>
				<h3>Total Cash in hand</h3>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
    @if( $responsibility->status->code == 'created' )
      <a href="/admin/car-responsibility/{{ $responsibility->id }}/modify/?action=start" class="btn btn-primary"> Start Managing </a>
    @else
      @if( $credit > 0 && !$responsibility->car->sold )
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Add Expense</button>
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              {!! Form::model($expense, ['route' => ['car-expense.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2">Add Expense</h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" name="car_id" value="{{ $responsibility->car->id }}">
                  <input type="hidden" name="responsibility_id" value="{{ $responsibility->id }}">
                  <input type="hidden" name="back_url" value="{{ $_SERVER['REQUEST_URI'] }}">
                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('title', 'Title', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('value', 'Value', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::text('value', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                      </div>
                    </div>
                  </div>                

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="currency_id" class="form-control select2_currency" disabled="disabled">
                              @foreach( $currencies as $currency )
                                  <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('invoice_image', 'Invoice Image', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::file('invoice_image') }}
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea id="description" name="description" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-primary" value="Add">
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Add Expense \ -->
      @else
        <a href="javascript:void(0);" class="btn btn-default" disabled="disabled">Out of credit, Ask administrator to assign you more credit</a>
      @endif
    @endif

    @can('manage-responsibility')
      @if( $responsibility->hasAllExpensesApproved() && !$responsibility->hasCompleted() )
        <a href="/admin/car-responsibility/{{ $responsibility->id }}/complete/?back_url={{ $_SERVER['REQUEST_URI'] }}" class="btn btn-success"> Complete Responsibility</a>
      @endif
    @endcan        
    
    @if( $responsibility->car->sold )
      @can( 'add-receivable-expense' )
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".add_receivable_expense">Add Receivable Expense</button>
        <div class="modal fade add_receivable_expense" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Add Receivable Expense Details</h4>
              </div>
              <div class="modal-body">                
                {!! Form::open(['url' => '/admin/car-expense/' . $responsibility->car->id . '/add-receivable-expense', 'method' => 'POST']) !!}                        
                  <input type="hidden" name="car_id" value="{{ $responsibility->car->id }}">                    
                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('title', 'Title', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('head_id', 'Account Head', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <input name="head_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="head_id" class="form-control">
                              <option value="">Select Account Head</option>
                              @foreach( $receivable_heads as $head )
                                  <option value="{{ $head->id }}"> {{ $head->title }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('value', 'Value', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::text('value', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                      </div>
                    </div>
                  </div>                

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                      <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="currency_id" class="form-control select2_currency" disabled="disabled">
                              @foreach( $currencies as $currency )
                                  <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                              @endforeach
                          </select> 
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('invoice_image', 'Invoice Image', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          {{ Form::file('invoice_image') }}
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="row">
                      {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                      <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea id="description" name="description" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save">
              </div>
              </form>
            </div>
          </div>
        </div>
      @endcan
    @else
      @can( 'add-payable-expense' )
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".add_payable_expense">Add Payable Expense</button>
            <div class="modal fade add_payable_expense" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Add Payable Expense Details</h4>
                  </div>
                  <div class="modal-body">                
                    {!! Form::open(['url' => '/admin/car-expense/' . $responsibility->car->id . '/add-payable-expense', 'method' => 'POST']) !!}                        
                      <input type="hidden" name="car_id" value="{{ $responsibility->car->id }}">                    
                      <div class="form-group">
                        <div class="row">
                          {{ Form::label('title', 'Title', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                          <div class="col-md-6 col-sm-6 col-xs-12">
                              {{ Form::text('title', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="row">
                          {{ Form::label('head_id', 'Account Head', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                          <input name="head_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <select name="head_id" class="form-control">
                                  <option value="">Select Account Head</option>
                                  @foreach( $receivable_heads as $head )
                                      <option value="{{ $head->id }}"> {{ $head->title }} </option>
                                  @endforeach
                              </select> 
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="row">
                          {{ Form::label('value', 'Value', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                          <div class="col-md-6 col-sm-6 col-xs-12">
                              {{ Form::text('value', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
                          </div>
                        </div>
                      </div>                

                      <div class="form-group">
                        <div class="row">
                          {{ Form::label('currency', 'Currency', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                          <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
                          <div class="col-md-9 col-sm-9 col-xs-12">
                              <select name="currency_id" class="form-control select2_currency" disabled="disabled">
                                  @foreach( $currencies as $currency )
                                      <option value="{{ $currency->id }}"> {{ $currency->name }} </option>
                                  @endforeach
                              </select> 
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="row">
                          {{ Form::label('invoice_image', 'Invoice Image', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                          <div class="col-md-6 col-sm-6 col-xs-12">
                              {{ Form::file('invoice_image') }}
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="row">
                          {{ Form::label('description', 'Description', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                          <div class="col-md-9 col-sm-9 col-xs-12">
                          <textarea id="description" name="description" class="form-control"></textarea>
                          </div>
                        </div>
                      </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save">
                  </div>
                  </form>
                </div>
              </div>
            </div>
      @endcan
    @endif
    <div class="x_panel">
      <div class="x_title">
        <h2>Expenses List <small>Includes Parts, Labour and General maintenance </small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @if( count( $responsibility->car->expenses ) > 0 )
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Expense Title</th>
                <th>Description</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Approved</th>
                <th>Type</th>
                <th>Action</th>
                <thh
              </tr>
            </thead>
            <tbody>
              @foreach( $responsibility->car->expenses as $key => $expense )
                <tr>
                  <th scope="row">{{ $key + 1 }}</th>
                  <td>{{ $expense->title }}</td>
                  <td>{{ $expense->description }}</td>
                  <td>{{ $expense->value }}</td>
                  <td>{{ $expense->created_at }}</td>
                  <td>{{ ( $expense->approved == 1 ) ? 'Yes' : 'No' }}</td>
                  <td>@if( $expense->receivable_id ) Receivable @elseif( $expense->payable_id ) Payable @else - @endif</td>
                  <td>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm{{$expense->id}}">View</button>
                    <div class="modal fade bs-example-modal-sm{{$expense->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-md">
                          <div class="modal-content">                       
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel2">Expense Detail</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-4">Title</div>
                                  <div class="col-md-8">{{ $expense->title }}</div>
                                </div>                            
                                <div class="row">
                                  <div class="col-md-4">Value</div>
                                  <div class="col-md-8">{{ $expense->value }} {{ $expense->currency->code }}</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">Created at</div>
                                  <div class="col-md-8">{{ $expense->created_at }}</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">Updated at</div>
                                  <div class="col-md-8">{{ $expense->updated_at }}</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">Description</div>
                                  <div class="col-md-8">{{ $expense->description }}</div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">Images</div>
                                  <div class="col-md-8">
                                    @if( count( $expense->invoiceImage ) > 0 )
                                        @foreach( $expense->invoiceImage as $image )
                                            <img class="img-thumbnail" src="{{ $image->url }}" width="300">                                       
                                        @endforeach
                                    @endif
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                          </div>
                        </div>
                    </div>

                    @if( $expense->approved == 0 )
                      <a href="/admin/car-expense/{{ $expense->id }}/approve/?back_url={{ $_SERVER['REQUEST_URI'] }}" class="btn btn-success">Approve</a>
                      <a href="/admin/car-expense/{{ $expense->id }}/delete/?back_url={{ $_SERVER['REQUEST_URI'] }}" class="btn btn-danger">Delete</a>
                    @endif
                  </td>
                </tr>
              @endforeach
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><b>{{ $responsibility->car->getTotalExpenses() }}</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        @else
          No Expense Added
        @endif
      </div>
    </div>
	</div>
</div>
@stop