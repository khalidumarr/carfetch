@extends('layouts/admin')
@section('content')

@if( Auth::user()->getApplicableRole()->code == 'admin' )
    <h3> Car Responsibilities List </h3>
@elseif( Auth::user()->getApplicableRole()->code == 'sales' )
    <h3> My Responsibilities </h3>
@endif
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    	</button>
        {{ Session::get('message') }}
    </div>
@endif
@if( Session::has('errors') )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('errors') }}
    </div>
@endif
<div class="row">
	<div class="col-sm-9">
		<form action="/admin/car-responsibility/">
		</form>
	</div>
	<div class="col-sm-3">
        @if( Auth::user()->getApplicableRole()->code == 'admin' )
    		<div class="section_add text-right">
    			<a href="/admin/car-responsibility/create" type="button" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-plus"></span> Add New Responsibility</a>
    		</div>
        @endif
	</div>
</div>		
<div class="table-responsive">
   <table id="datatable-checkbox2" class="table table-striped table-bordered">
    <thead>
      <tr class="headings">
        <th>
          <input type="checkbox" id="check-all" class="flat">
        </th>
        <th class="column-title">User </th>
        <th class="column-title">Model </th>
        <th class="column-title">Year </th>
        <th class="column-title">Vin # </th>
        <th class="column-title">Lot #</th>
        <th class="column-title">Created at </th>
        <th class="column-title">Updated at </th>
        <th class="column-title no-link last"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>

    <tbody>
        @if( count( $responsibilities ) == 0 )
            <tr class="even pointer">
                <td colspan="6">No Car Responsibilities Found</td>                
            </tr>
        @else
            @foreach( $responsibilities as $responsibility )
            	<tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                    <td>{{ $responsibility->user->name }}</td>
                    <td>{{ $responsibility->car->model->name }}</td>
                    <td>{{ $responsibility->car->year }}</td>
                    <td>{{ $responsibility->car->propByCode('vin_number')->value }}</td>
                    <td>{{ $responsibility->car->propByCode('lot_number')->value }}</td>
                    <td>{{ $responsibility->created_at }}</td>
                    <td>{{ $responsibility->updated_at }}</td>
                    <td>
                        @if( in_array( Auth::user()->getApplicableRole()->code, [ 'repairing', 'admin' ] ) )
                            <a class="btn btn-default source" href="/admin/car-responsibility/{{ $responsibility->id }}/modify/">Modify</a>
                        @endif

                        @if( Auth::user()->getApplicableRole()->code == 'admin' )
                            @if( $responsibility->hasCompleted() )
                                Already Completed
                            @else
                                {{ Form::open( [ 'url' => '/admin/car-responsibility/' . $responsibility->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                                    {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                                {{ Form::close() }}
                            @endif
                        @endif
                    </td>
                </tr>    
            @endforeach
        @endif   
    </tbody>
  </table>
   {{ $responsibilities->links() }}
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 2, 'desc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });

        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
    });
</script>
@stop