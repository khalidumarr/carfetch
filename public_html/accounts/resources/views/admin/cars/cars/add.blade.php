@extends('layouts/admin')
@section('content')
<div class="header">   
    <a href="/cars" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>              
    <h3> Add New Car </h3>
</div>
<div class="body">
    <div class="row">    
        {!! Form::model($car, ['route' => ['cars.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
            <input name="destination_id" type="hidden" value="4">
            <input name="sale_type_id" type="hidden" value="4">
                                        
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">General</h2>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('container_number', 'Container Number*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">    
                    {{ Form::text('container_number', null, [ 'class' => 'form-control' ]) }}
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="declaration_number_loader preloader pl-size-xs" style="display: none;">
                    <div class="spinner-layer pl-red-grey">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                {{ Form::label('container_declaration_number', 'Container Declaration Number*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">    
                    {{ Form::text('container_declaration_number', null, [ 'class' => 'form-control' ]) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('model', 'Model*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="model_id" class="form-control">
                        <option value="">Select model</option>
                        @foreach( $car_models as $model )
                            <option value="{{ $model->id }}"> {{ $model->name }} </option>
                        @endforeach
                    </select> 
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('year', 'Year*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">    
                        {{ Form::text('year', null, [ 'class' => 'form-control' ]) }}
                    </div>
                </div>
            </div>
            
            <!-- General end -->
            <div class="ln_solid"></div>

            <!-- Cost Start -->
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">Costs</h2>
                </div>
            </div>

            @foreach( $car_costs as $cost )
                <div class="form-group">
                    {{ Form::label( $cost->code, $cost->name . ( ( $cost->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}{{ $cost->currency->code }}
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-line">    
                            {{ Form::text( $cost->code, $cost->default_value, [ 'class' => 'car-costs-input form-control', 'data-conversion-rate' => $cost->currency->conversion ]) }}
                        </div>
                    </div>
                </div>
            @endforeach

            <!-- Cost End -->
            <div class="ln_solid"></div>


            <!-- CostProperties Start -->
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">Specifications</h2>
                </div>
            </div>

            @foreach( $car_properties as $property )
                <div class="form-group">
                    {{ Form::label( $property->code, $property->name . ( ( $property->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">    
                            @if( $property->type->code == 'text' )

                                {{ Form::text( $property->code, null, [ 'class' => 'form-control' ]) }}
                            
                            @elseif( $property->type->code == 'checkbox' )
                            
                                {{ Form::checkbox($property->code, null, null, [ 'class' => 'js-switch', 'id' => $property->code ]) }}
                            
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach

            <!-- Property End -->
            <div class="ln_solid"></div>          
            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>                
</div>        
<script type="text/javascript">
    $(document).ready(function(){
        var total = 0;

        $(document).on('focusout', 'input[name=container_number]', function(){
            var container_number = $(this).val();
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/api/container',
                beforeSend: function(){
                    $('.declaration_number_loader').show();
                },
                data: {
                    container_number: container_number
                },
                success: function(response) {
                    $('.declaration_number_loader').hide();
                    if (response) {
                        $('input[name="container_declaration_number"]').val(response.declaration);
                        $('input[name="container_id"]').val(response.id);                        
                    }
                }
            });
        });

        $(document).on( 'change', 'select[name=stock_type_id]', function(){
            if( $('select[name=stock_type_id]').find(':selected').data('code') == 'agent-stock' ){
                $('#agents_list').show();
            }
            else{
                $('#agents_list').hide();
            }
        });

        $(document).on( 'input', 'input[name=invoice]', function(){
            var customs = $('input[name=customs]');
            if( typeof customs != undefined ){
                if( $(this).val() > 0 ){
                    var invoice = +$(this).val();
                    customs.val( ( ( invoice + 900 ) * 0.05 ).toFixed(2) );                    
                }
                else{
                    customs.val(0);
                }
            }
        });

        $(document).on( "input", '.car-costs-input', function(){
            if( $(this).attr('name') == 'additional-5-percent' ){
                return false;
            }
            
            total = 0;
            $('.car-costs-input').each(function(){
                if( $(this).attr('name') != 'additional-5-percent' ){
                    total += ( +$(this).val() * +$(this).data('conversion-rate') );
                }
            });
            
            $('input[name=additional-5-percent]').val( ( total * 0.05 ).toFixed(2) );
        });
    });
</script>
@stop