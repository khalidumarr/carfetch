@extends('layouts/admin')
@section('content')
<div class="header">
    <div class="row">
        <div class="col-md-11">
            <a href="/" class="backarrow">
                <i class="material-icons">keyboard_backspace</i>
            </a> 
            <h3>Inventory</h3>            
        </div>
        <div class="col-md-1">
            <a href="/sync/car-models" class="btn btn-success pull-right">Sync Models</a>
        </div>
    </div>                
</div>
<div class="body">
    <div class="row clearfix">
        <div class="col-sm-12">
            <form action="/cars/">
                <div class="row">                                        
                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="col-md-4">
                            <div class="form-line">                                      
                                <input name="search-keyword" class="form-control search_field" placeholder="VIN #, LOT #, Model, Make, Year" value="{{ ( isset( $_REQUEST['search-keyword'] ) ) ? $_REQUEST['search-keyword'] : '' }}">
                            </div>
                        </div>                                                                                                         
                    </div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="col-md-4">
                            <select class="form-control" name="sold">
                                <option value="">All</option>
                                <option value="on" @if (isset($_REQUEST['sold']) && $_REQUEST['sold'] == 'on') selected @endif>Sold</option>
                                <option value="off" @if (isset($_REQUEST['sold']) && $_REQUEST['sold'] == 'off') selected @endif>Unsold</option>
                            </select>
                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="submit" value="Search" class="btn btn-default search_submit">
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-sm-8"></div>
                @can('add-car')
                    <div class="col-sm-1">
                        <div class="section_add">
                            <button type="button" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Import</button>
                            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" style="display: none;">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="defaultModalLabel">Import Invertory</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" class="import-form" action="/cars/import">                                                    
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <label for="search">VIN #</label>
                                                        <input type="text" id="search" name="vin_number" placeholder="" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <label for="search">LOT #</label>
                                                        <input type="text" id="search" name="lot_number" placeholder="" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <label for="search">Year</label>
                                                        <input type="text" id="search" name="year" placeholder="" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" id="search_import" name="search" value="Search" class="form-control btn btn-success">
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 import-loader" style="text-align: center; display: none;">
                                                        <div class="preloader pl-size-xl">
                                                            <div class="spinner-layer">
                                                                <div class="circle-clipper left">
                                                                    <div class="circle"></div>
                                                                </div>
                                                                <div class="circle-clipper right">
                                                                    <div class="circle"></div>
                                                                </div>
                                                            </div>
                                                        </div>              
                                                    </div>
                                                </div>                                                                                

                                                <div class="row">
                                                    <div class="body table-responsive">
                                                        <table class="table jambo_table bulk_action">
                                                            <thead>
                                                                <tr class="headings">
                                                                    <th class="column-title">Select</th>
                                                                    <th class="column-title">Make </th>
                                                                    <th class="column-title">Model </th>
                                                                    <th class="column-title">Vin # </th>
                                                                    <th class="column-title">Lot # </th>
                                                                    <th class="column-title">Year </th>                                                                                                
                                                                </tr>
                                                            </thead>

                                                            <tbody class="search_table_body">
                                                                
                                                            </tbody>
                                                        </table>                
                                                    </div>  
                                                </div>                                                
                                            </form>                                                    
                                        </div>
                                        <div class="modal-footer">                                                
                                            <button type="button" class="btn btn-primary perform_import">Import</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                                
                        </div>                            
                    </div>
                    <div class="col-sm-1">
                        <div class="section_add text-right">
                            <a href="/cars/create" type="button" class="btn btn-primary btn-md">Add Inventory</a>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="section_add text-right">
                            <form action="/cars/import-csv" class="import-csv-form" method="POST" enctype="multipart/form-data">
                                <input type="file" name="file_csv" id="imgupload" style="display:none"/>                                 
                                <input type="submit" id="OpenImgUpload" class="btn btn-warning btn-md" value="Import CSV">
                            </form>
                        </div>
                    </div>
                @endcan
            </div> 
        </div>        
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="body table-responsive">
                <table class="table jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Make </th>
                            <th class="column-title">Model </th>
                            <th class="column-title">Vin # </th>
                            <th class="column-title">Lot # </th>
                            <th class="column-title">Year </th>                            
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        @if( count( $cars ) == 0 )
                            <tr class="even pointer">
                                <td colspan="10">No Cars Found</td>                
                            </tr>
                        @else
                            @foreach( $cars as $car )
                                <tr class="even pointer">
                                    <td>{{ $car->model->make->name }}</td>
                                    <td>{{ $car->model->name }}</td>
                                    <td>{{ $car->propByCode('vin_number')->value }}</td>
                                    <td>{{ $car->propByCode('lot_number')->value }}</td>
                                    <td>{{ $car->year }}</td>                                    
                                    <td>       
                                        @if (!$car->sold)                                 
                                            <a class="btn btn-success source" href="/sale/{{$car->id}}">Sell</a>
                                            @can('edit-car')
                                               <a class="btn btn-default source" href="/cars/{{ $car->id }}/edit/">Edit</a>
                                            @endcan                                        
                                        @endif                                        

                                        <a class="btn btn-primary source" href="/cars/{{ $car->id }}/">View</a>
                                        @can('delete-car')
                                            @if (!$car->sold)                                 
                                                {{ Form::open( [ 'url' => '/cars/' . $car->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}                                
                                                    <a href="javascript:void(0);" class="btn car-delete-btn btn-danger source">Delete</a>
                                                {{ Form::close() }}
                                            @endif
                                        @endcan

                                        @if ($car->sold)     
                                            <a class="btn btn-warning source" target="_blank" href="/sale/{{ $car->sale->id }}/generate-invoice/">Generate Invoice</a>
                                        @endif
                                    </td>
                                </tr>    
                            @endforeach
                        @endif   
                    </tbody>
                </table>                  
            </div> 

            <div class="row">
                <div class="col-md-10">
                    {{ $cars->appends( $_REQUEST )->links() }}
                </div>
                <div class="col-md-2 text-right">            
                    {{ $cars->total() }} Cars Found
                </div>
            </div>                
        </div>
    </div>    
</div>	

<script type="text/javascript">
    $(document).ready(function(){

        $('#OpenImgUpload').click(function(e){ e.preventDefault(); $('#imgupload').trigger('click'); });

        $("#imgupload").change(function(){
            $('.import-csv-form').submit();
        });

        
        $('.car-delete-btn').click(function(e){
            e.preventDefault();
            if(confirm('You really want to delete this ?')){
                $(this).closest('form').submit();     
            }
        });

        $('#search_import').click(function(e){
            e.preventDefault();
            var vin_number = $('input[name="vin_number"]').val();
            var lot_number = $('input[name="lot_number"]').val();
            var year = $('input[name="year"]').val();

            console.log(323);
            $.ajax({
                url: '/api/car/search',
                dataType: 'json',
                type: 'get',
                data: {
                    vin_number : vin_number,
                    lot_number: lot_number,
                    year: year
                },
                beforeSend: function() {
                    $('.import-loader').show();
                },
                success: function(data) {
                    $('.import-loader').hide();
                    var html  = '';                 
                    $('.search_table_body').html('');   
                    $.each(data, function(key, value) { 
                        html += '<tr><td><input name="cars['+value.id+']" type="checkbox" name="select_for_import" id="select_for_import_'+value.id+'"><label for="select_for_import_'+value.id+'"></label></td>';
                        html += '<td>' + value.make.name + '</td>';
                        html += '<td>' + value.model.name + '</td>';                        
                        html += '<td>' + findInArray(value.properties, 'prop_code', 'vin_number') + '</td>';
                        html += '<td>' + findInArray(value.properties, 'prop_code', 'lot_number') + '</td>';
                        html += '<td>' + value.year + '</td><tr>';
                    });                    
                    $('.search_table_body').html('').append(html);
                }
            });
            return false;
        });

        $('.perform_import').click(function(e){
            $('.import-form').submit();
        });

        function findInArray(arr, intentKey, intentValue)
        {               
            var result = '';
            $.each(arr, function(key, value) {                   
                if (value[intentKey] == intentValue) {                                                
                    result = value.value;
                    return;
                }
            });

            return result;
        }
    });
</script>
@stop