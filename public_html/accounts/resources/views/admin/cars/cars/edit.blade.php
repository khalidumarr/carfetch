@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/cars" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
    <h3> Edit Car </h3>    
</div>
<div class="body">
    <div class="row"> 
        {!! Form::model($car, ['route' => ['cars.update', $car->id], 'method' => 'PUT', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
            <input name="currency_id" type="hidden" value="{{ App\Models\Currency::getDefault()->id }}">
            <input name="destination_id" type="hidden" value="4">
            <input name="sale_type_id" type="hidden" value="4">
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">General</h2>
                </div>
            </div>            

            <div class="form-group">
                {{ Form::label('model', 'Model*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        <select name="model_id" class="form-control">
                            <option disabled="disabled">Select model</option>
                            @foreach( $car_models as $model )
                                <option value="{{ $model->id }}" @if( $model->id == $car->model_id ) selected @endif> {{ $model->name }} </option>
                            @endforeach
                        </select> 
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('year', 'Year*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('year', $car->year, [ 'class' => 'form-control' ]) }}
                    </div>
                </div>
            </div>
            
            <!-- General end -->
            <div class="ln_solid"></div>

            <!-- Cost Start -->

            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">Costs</h2>
                </div>
            </div>

            @foreach( $car_costs as $cost )
                <div class="form-group">
                    {{ Form::label( $cost->code, $cost->name . ( ( $cost->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}{{ $cost->currency->code }}
                    <div class="col-md-1 col-sm-1 col-xs-2">    
                        <div class="form-line">
                            {{ Form::text( $cost->code, $cost->value, [ 'class' => 'car-costs-input form-control', 'data-conversion-rate' => $cost->currency->conversion ]) }}                        
                        </div>                
                    </div>
                </div>
            @endforeach

            <!-- Cost End -->
            <div class="ln_solid"></div>


            <!-- CostProperties Start -->
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">Specifications</h2>
                </div>
            </div>

            @foreach( $car_properties as $property )
                <div class="form-group">
                    {{ Form::label( $property->code, $property->name . ( ( $property->is_required ) ? '*' : '' ), [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-line">
                            @if( $property->type->code == 'text' )
                                @if( !empty( $car->propByCode($property->code) ) )
                                    {{ Form::text( $property->code, $car->propByCode($property->code)->value, [ 'class' => 'form-control' ]) }}
                                @endif
                            @elseif( $property->type->code == 'checkbox' )
                                @if( !empty( $car->propByCode($property->code) ) )
                                    {{ Form::checkbox($property->code, ( $car->propByCode($property->code)->value == 1 ) ? true : false, null, [ 'class' => 'js-switch', 'id' => $property->code ]) }}
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach

            <!-- Property End -->
            <div class="ln_solid"></div>

            <!-- Properties Start -->
            <div class="form-group">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">Container</h2>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('container', 'Container*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        <select name="container_id" class="form-control">
                            <option disabled="disabled">Select Container</option>
                            @foreach( $containers as $container )
                                <option value="{{ $container->id }}" @if( $container->id == $car->container_id ) selected @endif> {{ $container->number . "  [" . $container->declaration . "]" }} </option>
                            @endforeach
                        </select> 
                    </div>
                </div>
            </div>

            <!-- Pictures Start -->
            <div class="form-group" style="display: none">
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2 class="card-inside-title">Pictures</h2>
                </div>
            </div>

            <div class="form-group" style="display: none;">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Car Images</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    @if( count( $car->images ) > 0 )
                        <div class="row image_container">                                          
                            @foreach( $car->images as $image )
                                <div class="col-md-4 image_item" data-image-id="{{ $image->id }}">
                                    <div class="thumbnail" style="height:auto;">
                                      <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="{{ $image->url }}" alt="image">
                                        <div class="mask">
                                          <div class="tools tools-bottom">
                                            <a href="#"><i class="fa fa-file-image-o"></i></a>
                                            @can('delete-car-picture')
                                                <a class="upload_delete" data-id="{{$image->id}}" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                                            @endcan
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    {{ Form::file('image[]', ['multiple' => 'multiple']) }}
                </div>
            </div> 
            <!-- Property End -->
            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var total = 0;        
        $(document).on( 'change', 'select[name=stock_type_id]', function(){
            if( $('select[name=stock_type_id]').find(':selected').data('code') == 'agent-stock' ){
                $('#agents_list').show();
            }
            else{
                $('#agents_list').hide();
            }
        });        

        $(document).on( 'input', 'input[name=invoice]', function(){
            var customs = $('input[name=customs]');
            if( typeof customs != undefined ){
                if( $(this).val() > 0 ){
                    var invoice = +$(this).val();
                    customs.val( ( ( invoice + 900 ) * 0.05 ).toFixed(2) );                    
                }
                else{
                    customs.val(0);
                }
            }
        });

        $(document).on( "input", '.car-costs-input', function(){
            if( $(this).attr('name') == 'additional-5-percent' ){
                return false;
            }
            
            total = 0;
            $('.car-costs-input').each(function(){
                if( $(this).attr('name') != 'additional-5-percent' ){
                    total += ( +$(this).val() * +$(this).data('conversion-rate') );
                }
            });
            
            $('input[name=additional-5-percent]').val( ( total * 0.05 ).toFixed(2) );
        });
    });
</script>
@stop