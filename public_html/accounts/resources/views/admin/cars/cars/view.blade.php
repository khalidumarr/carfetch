@extends('layouts/admin')
@section('content')
<div class="header">
	<a href="/cars" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a> 
	<h3>Vehicle Details</h3>
</div>
<div class="body">
	<div class="row">
		<div class="col-md-6">
			<h3>General Details</h3>
			<table class="table countries_list table-striped">
	            <tbody>					
					<tr>
						<td>Code</td>
						<td class="fs15 fw700 text-right">{{ $car->code }}</td>
					</tr>
					<tr>
						<td>Make</td>
						<td class="fs15 fw700 text-right">{{ $car->model->make->name }}</td>
					</tr>
					<tr>
						<td>Model</td>
						<td class="fs15 fw700 text-right">{{ $car->model->name }}</td>
					</tr>
					<tr>
						<td>Year</td>
						<td class="fs15 fw700 text-right">{{ $car->year }}</td>
					</tr>
					<tr>
						<td>Sale Price</td>
						<td class="fs15 fw700 text-right">{{ $car->sale_price }}</td>
					</tr>					
					<tr>
						<td>Sold</td>
						<td class="fs15 fw700 text-right">{{ ( $car->sold == 1 ) ? 'Yes' : 'No' }}</td>
					</tr>					
					<tr>
						<td>Created at</td>
						<td class="fs15 fw700 text-right">{{ $car->created_at }}</td>
					</tr>
					<tr>
						<td>Updated At</td>
						<td class="fs15 fw700 text-right">{{ $car->updated_at }}</td>
					</tr>
					<tr>
						<td>Last Updated By</td>
						<td class="fs15 fw700 text-right">{{ $car->updatedBy->name }}</td>
					</tr>
					<tr>
						<td>Description</td>
						<td class="fs15 fw700 text-right">{{ $car->description }}</td>
					</tr>				
	            </tbody>
	        </table>
		</div>
		<div class="col-md-6">
		</div>
		@if( $car->container )
			<div class="row car_containers">
				<h3>Container Detail</h3>
				<div class="col-md-4">
					<table class="table table-striped">
						<tbody>					
							<tr>
								<td>Number</td>
								<td class="fs15 fw700 text-right">{{ $car->Container->number }}</td>
							</tr>
							<tr>
								<td>Declaration Number</td>
								<td class="fs15 fw700 text-right">{{ $car->Container->declaration }}</td>
							</tr>
							<tr>
								<td>Arrival Date</td>
								<td class="fs15 fw700 text-right">{{ $car->Container->arrival_date }}</td>
							</tr>
							<tr>
								<td>Amount</td>
								<td class="fs15 fw700 text-right">{{ $car->Container->amount }}</td>
							</tr>
						</tbody>
					</table>
				</div>				
			</div>				
		@endif
	</div>
	<div class="row">
		<div class="col-md-6">
			<h3>Properties</h3>
			<table class="table table-striped">
	            <tbody>
					@foreach( $car->properties as $property )
							<tr>
								<td>{{ $property->prop_name }}</td>
								<td width="50%" class="fs15 fw700 pull-right">{{ $property->value }}</td>
							</tr>
					@endforeach
				</tbody>
			</table>		
		</div>
		<div class="col-md-6">
			@can('view-car-costs')
				<h3>Costs</h3>
			@endcan
			<table class="table table-striped">
	            <tbody>
	            	@can('view-car-costs')
						@foreach( $car->costs as $cost )					
								<tr>
									<td>{{ $cost->cost_name }}</td>
									<td class="pull-right">{{ Price::format( $cost->value ) }}</td>
								</tr>					
						@endforeach
						<tr>
							<td><b>Total</b></td>
							<td class="fs15 fw700 text-right"><b>{{ Price::format( $car->getTotalCost() ) }}</b></td>
						</tr>
					@endcan				
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){

		$(document).on( 'change', 'select[name=payment_method]', function(){
            if( $(this).find(':selected').data('code') == 'cheque' ){
                $('.document_number_input').show();
            }
            else{
                $('.document_number_input').hide();
            }
        });
	});
</script>
@stop