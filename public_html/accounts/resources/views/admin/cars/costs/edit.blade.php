@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/car-costs" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>
    <h3> Edit Car Cost </h3>        
</div>
<div class="body">    
    <div class="row"> 
        {!! Form::model($cost, ['route' => ['car-costs.update', $cost], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
            <div class="form-group">
                {{ Form::label('name', 'Name*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('name', null, [ 'class' => 'form-control' ]) }}                        
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('currency', 'Currency*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="currency_id" class="form-control">
                        <option disabled="disabled">Select Cost Currency</option>
                        @foreach( $currencies as $currency )
                            <option value="{{ $currency->id }}" @if( $currency->id == $cost->currency_id ) selected @endif> {{ $currency->name }} </option>
                        @endforeach
                    </select> 
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('code', 'Code*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('code', null, [ 'class' => 'form-control' ]) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('default_value', 'Default Value*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-line">
                        {{ Form::text('default_value', null, [ 'class' => 'form-control' ]) }}
                    </div>
                </div>
            </div>

            <div class="form-group">                    
                <div class="col-md-3"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">        
                    <input type="checkbox" name="is_required" id="is_required" @if($cost->is_required) checked="" @endif>            
                    <label for="is_required">Is Required ?</label>
                </div>                    
            </div>

            <div class="form-group">                    
                <div class="col-md-3"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">        
                    <input type="checkbox" name="in_invoice" id="in_invoice" @if($cost->in_invoice) checked="" @endif>
                    <label for="in_invoice">In Invoice ?</label>
                </div>                    
            </div>

            <div class="form-group">                    
                <div class="col-md-3"></div>
                <div class="col-md-6 col-sm-6 col-xs-12">        
                    <input type="checkbox" name="is_vat" id="is_vat" @if($cost->is_vat) checked="" @endif>            
                    <label for="is_vat">Is Vat ?</label>
                </div>                    
            </div>                
        
            <div class="ln_solid"></div>  
            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    {{ Form::submit('Update', [ 'class' => 'btn btn-success' ] ) }}
                </div>
            </div>
        {!! Form::close() !!}
    </div>   
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on( 'input', 'input[name=name], input[name=title]', function(){
            $('input[name=code]').val( $(this).val().split(' ').join('-').toLowerCase() );
        });
    });
</script>
@stop