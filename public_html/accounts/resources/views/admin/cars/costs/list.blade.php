@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>
    <h3> Car Costs List </h3>
</div>
<div class="body">            
    <div class="row">
        <div class="col-sm-9">
            <form action="/admin/car-costs/">
            </form>
        </div>
        <div class="col-sm-3">
            <div class="section_add text-right">
                <a href="/car-costs/create" type="button" class="btn btn-primary btn-md">Add New Car Cost</a>
            </div>
        </div>
    </div>      
    <div class="table-responsive">
      <table class="table table-striped jambo_table bulk_action">
        <thead>
          <tr class="headings">
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th class="column-title">Name </th>
            <th class="column-title">Code </th>
            <th class="column-title">Currency </th>
            <th class="column-title">Updated at </th>
            <th class="column-title">Created at </th>
            <th class="column-title no-link last"><span class="nobr">Action</span>
            </th>
          </tr>
        </thead>

        <tbody>
            @if( count( $costs ) == 0 )
                <tr class="even pointer">
                    <td colspan="5">No Car Costs Found</td>                
                </tr>
            @else
                @foreach( $costs as $property )
                    <tr class="even pointer">
                        <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                        <td>{{ $property->name }}</td>
                        <td>{{ $property->code }}</td>
                        <td>{{ $property->currency->name }}</td>
                        <td>{{ $property->created_at }}</td>
                        <td>{{ $property->updated_at }}</td>
                        <td>
                            <a class="btn btn-default source" href="/car-costs/{{ $property->id }}/edit/">Edit</a>                            
                        </td>
                    </tr>    
                @endforeach
            @endif   
        </tbody>
      </table>
       {{ $costs->links() }}
    </div>    
</div>

@stop