@extends('layouts/admin')
@section('content')
<div class="header">
    <a href="/" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>
    <h3> Car Containers List </h3>
</div>
<div class="body">         
    <div class="row">
        <form action="/car-containers" method="GET">
            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="date_from">Created From: </label>
                        <input id="date_from" name="date_from" type="text" class="form-control date" autocomplete="off" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_from'])){{ $_REQUEST['date_from'] }}@endif">
                    </div>
                </div>
            </div>    

            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="date_to">Created To: </label>
                        <input id="date_to" name="date_to" type="text" class="form-control date" autocomplete="off" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['date_to'])){{ $_REQUEST['date_to'] }}@endif">
                    </div>
                </div>
            </div>

            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="arrival_date_from">Arrival Date From: </label>
                        <input id="arrival_date_from" name="arrival_date_from" type="text" autocomplete="off" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['arrival_date_from'])){{ $_REQUEST['arrival_date_from'] }}@endif">
                    </div>
                </div>
            </div>    

            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">date_range</i>
                    </span>
                    <div class="form-line">
                        <label for="arrival_date_to">Arrival Date To: </label>
                        <input id="arrival_date_to" name="arrival_date_to" type="text" autocomplete="off"" class="form-control date" placeholder="Ex: 30/07/2016" value="@if (isset($_REQUEST['arrival_date_to'])){{ $_REQUEST['arrival_date_to'] }}@endif">
                    </div>
                </div>
            </div>

            <div class="col-md-3">                
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">directions_boat</i>
                    </span>                    
                    <label for="shipper_id">Shipper: </label>
                    <div class="form-line">
                        <select name="shipper_id" class="form-control select2_shipper">
                            <option value="">Select Shipper</option>
                            @foreach( $shippers as $shipper )
                                <option value="{{ $shipper->id }}" @if( isset($_REQUEST['shipper_id']) && $_REQUEST['shipper_id'] == $shipper->id ) selected @endif> {{ $shipper->name }} </option>
                            @endforeach
                        </select>                             
                    </div>                    
                </div>
            </div>    
            <div class="col-sm-3">
                <div class="input-group">
                    <br>
                    <input type="submit" value="Search" class="btn btn-default search_submit">
                </div>
            </div>  
        </form>        
    </div>   
    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <div class="section_add text-right">
                <a href="/car-containers/create" type="button" class="btn btn-primary btn-md">Add Container</a>
            </div>            
        </div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped jambo_table bulk_action">
        <thead>
          <tr class="headings">
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th class="column-title">Shipper </th>
            <th class="column-title">Number </th>
            <th class="column-title">Arrival </th>
            <th class="column-title">Amount </th>            
            <th class="column-title">Updated at </th>
            <th class="column-title no-link last"><span class="nobr">Action</span>
            </th>
          </tr>
        </thead>

        <tbody>
            @if( count( $containers ) == 0 )
                <tr class="even pointer">
                    <td colspan="7">No Car Containers Found</td>                
                </tr>
            @else
                @foreach( $containers as $container )
                    <tr class="even pointer">
                        <td class="a-center "><input type="checkbox" class="flat" name="table_records"></td>
                        <td>{{ ($container->shipper) ? $container->shipper->name : '-' }}</td>
                        <td>{{ $container->number }}</td>
                        <td>{{ date('Y-m-d', strtotime($container->arrival_date)) }}</td>
                        <td>{{ ($container->payable) ? Price::format($container->payable->value) : '-' }}</td>                        
                        <td>{{ $container->updated_at }}</td>
                        <td>
                            <a class="btn btn-primary source" href="/car-containers/{{ $container->id }}/">View</a>
                            @can('edit-container')
                               <a class="btn btn-default source" href="/car-containers/{{ $container->id }}/edit/">Edit</a>
                            @endcan

                            @can('delete-container')
                                {{ Form::open( [ 'url' => '/car-containers/' . $container->id, 'method' => 'DELETE', 'style' => 'display:inline;' ] ) }}
                                    {{ Form::submit('Delete', [ 'class' => 'btn btn-danger source' ] ) }}
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>    
                @endforeach
                <tr style="background: #ddd">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ Price::format($totalAmount) }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endif   
        </tbody>
      </table>       
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#date_from').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });        

        $('#date_to').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });        

        $('#arrival_date_from').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });        

        $('#arrival_date_to').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });        
    });
</script>
@stop