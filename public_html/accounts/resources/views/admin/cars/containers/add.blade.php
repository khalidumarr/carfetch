@extends('layouts/admin')
@section('content')
<div class="header">
  <a href="/accounts-heads" class="backarrow">
    <i class="material-icons">keyboard_backspace</i>
  </a> 
  <h3> Add New Car Container </h3>  
</div>
<div class="body">
  <div class="row"> 
    {!! Form::model($container, ['route' => ['car-containers.store'], 'method' => 'POST', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        
      <div class="form-group">
          <label for="number" class="control-label col-md-3 col-sm-3 col-xs-12">Shipper*</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="shipper_id" class="form-control select2_shipper">
                <option value="">Shipper</option>
                @foreach( $shippers as $shipper )
                    <option value="{{ $shipper->id }}"> {{ $shipper->name }} </option>
                @endforeach
            </select> 
          </div>
      </div>
      
      <div class="form-group">
          {{ Form::label('number', 'Number*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-line">
              {{ Form::text('number', null, [ 'class' => 'form-control' ]) }}                  
            </div>
          </div>
      </div>

      <div class="form-group">
          {{ Form::label('declaration', 'Declaration*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-line">
                {{ Form::text('declaration', null, [ 'class' => 'form-control' ]) }}
              </div>
          </div>
      </div>

      <div class="form-group">
          {{ Form::label('arrival_date', 'Arrival Date*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-line">
              {{ Form::text('arrival_date', null, [ 'class' => 'form-control','autocomplete' => "off" ]) }}
            </div>
          </div>
      </div>

      <div class="form-group">
          {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-line">
              {{ Form::text('amount', null, [ 'class' => 'form-control' ]) }}
            </div>
          </div>
      </div>                  
    </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {{ Form::submit('Save', [ 'class' => 'btn btn-success' ] ) }}
        </div>
    </div>
  {!! Form::close() !!}
  </div>
</div>

<script type="text/javascript">
     $(document).ready(function(){        
        $('#arrival_date').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
@stop