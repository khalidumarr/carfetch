@extends('layouts/admin')
@section('content')
<div class="header">
	<a href="/car-containers" class="backarrow">
        <i class="material-icons">keyboard_backspace</i>
    </a>
	<h3> Edit Car Container </h3>	
</div>
<div class="body">
	<div class="row"> 
		<div class="col-sm-12">
			{!! Form::model($container, ['route' => ['car-containers.update', $container], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left car-container-form', 'id' => 'demo-form2' ]) !!}	
		        <input type="hidden" name="currency_id" value="1">
		        <div class="form-group">
		            <label for="number" class="control-label col-md-3 col-sm-3 col-xs-12">Shipper*</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		            	<div class="form-line">
		            		<select name="shipper_id" class="form-control select2_shipper">
								<option value="">Shipper</option>
								@foreach( $shippers as $shipper )
									<option value="{{ $shipper->id }}" @if( $container->shipper_id == $shipper->id ) selected @endif> {{ $shipper->name }} </option>
								@endforeach
			              	</select> 
		            	</div>	              
		            </div>
		        </div>
		        
		        <div class="form-group">
		            {{ Form::label('number', 'Number*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		            <div class="col-md-6 col-sm-6 col-xs-12">
		            	<div class="form-line">
		                	{{ Form::text('number', null, [ 'class' => 'form-control' ]) }}
		            	</div>
		            </div>
		        </div>

		        <div class="form-group">
		            {{ Form::label('declaration', 'Declaration*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		            <div class="col-md-6 col-sm-6 col-xs-12">
		            	<div class="form-line">
		                	{{ Form::text('declaration', null, [ 'class' => 'form-control' ]) }}
		                </div>
		            </div>
		        </div>

		        <div class="form-group">
		            {{ Form::label('arrival_date', 'Arrival Date*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		            <div class="col-md-6 col-sm-6 col-xs-12">
		            	<div class="form-line">
		                	{{ Form::text('arrival_date', null, [ 'class' => 'form-control' ,'autocomplete' => "off" ]) }}
		                </div>
		            </div>
		        </div>

		        <div class="form-group">
		            {{ Form::label('amount', 'Amount*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
		            <div class="col-md-5 col-sm-5 col-xs-11">
		            	<div class="form-line">
		                	<input type="text" name="amount" class="form-control" value="{{ ($container->payable) ? $container->payable->value : '' }}">		                	
		                </div>		                
		            </div>
		            <div class="col-md-1">
		            	{{ $currency->code }}
		            </div>
		        </div>	        		        
		    
		        <div class="ln_solid"></div>
		        <h3>Loaded Cars</h3>
		        <div class="x_content">
		          <table id="datatable-checkbox2" class="table table-striped table-bordered">
		            <thead>
		              <tr>
		                <th></th>
		                <th>Selected</th>
		                <th>Model</th>
		                <th>Vin #</th>
		                <th>Lot #</th>
		                <th>year</th>		                
		                <th>Created at</th>
		              </tr>
		            </thead>
		            <tbody>
		              @foreach( $container->cars as $car )
		                <tr>
		                  <td><input type="checkbox" class="flat" name="cars[{{ $car->id }}]" value="{{ $car->id }}" @if( in_array( $car->id, $cars_ids ) ) checked @endif></td>
		                  <td>@if( in_array( $car->id, $cars_ids ) ) Yes @else No @endif</td>
		                  <td>{{ $car->model->name }}</td>
		                  <td>{{ $car->propByCode('vin_number')->value }}</td>
		                  <td>{{ $car->propByCode('lot_number')->value }}</td>
		                  <td>{{ $car->year }}</td>		                  
		                  <td>{{ $car->created_at }}</td>
		                </tr>
		              @endforeach
		            </tbody>
		          </table>
		        </div>
		        
		        <div class="form-group">
		            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		                {{ Form::submit('Save', [ 'class' => 'btn btn-success container-save-btn' ] ) }}
		            </div>
		        </div>
		    {!! Form::close() !!}
		</div>	    
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#arrival_date').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
          format: "YYYY-MM-DD"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });        
    });
</script>
@stop