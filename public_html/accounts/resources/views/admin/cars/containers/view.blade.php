@extends('layouts/admin')
@section('content')
<div class="header">
  <a href="/car-containers" class="backarrow">
    <i class="material-icons">keyboard_backspace</i>
  </a>
  <h3> Car Container </h3>  
</div>
<div class="body">
  <div class="row"> 
    <div class="col-sm-12">        
      <table class="table table-striped">
        <tbody>
          <tr>
            <td>Number</td>
            <td>{{ $container->number }}</td>
          </tr>
          <tr>
            <td>Declaration</td>
            <td>{{ $container->declaration }}</td>
          </tr>
          <tr>
            <td>Arrival Date</td>
            <td>{{ ($container->arrival_date) ? $container->arrival_date : '-' }}</td>
          </tr>
          <tr>
            <td>Amount</td>            
              <td>
                @if ($container->payable)
                  {{ Price::format($container->payable->value) }}
                @else
                  Amount not Defined
                @endif
              </td>            
          </tr>
        </tbody>
      </table>       
      <div class="ln_solid"></div>
      <h3>Loaded Cars</h3>
      <div class="x_content">
        <table id="datatable-checkbox2" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Model</th>
              <th>Vin #</th>
              <th>Lot #</th>
              <th>year</th>
              <th>Created at</th>
            </tr>
          </thead>
          <tbody>
            @foreach( $container->cars as $car )
              <tr>                  
                <td>{{ $car->model->name }}</td>
                <td>{{ $car->propByCode('vin_number')->value }}</td>
                <td>{{ $car->propByCode('lot_number')->value }}</td>
                <td>{{ $car->year }}</td>              
                <td>{{ $car->created_at }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div> 
    </div>                 
  </div>
</div>
@stop