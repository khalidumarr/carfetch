@extends('layouts/admin')
@section('content')
<h3> Edit Car Model </h3>
@if( count( $errors ) > 0 )
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif( Session::has('message') )
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        {{ Session::get('message') }}
    </div>
@endif
<div class="row"> 
    {!! Form::model($model, ['route' => ['car-models.update', $model], 'method' => 'put', 'role' => 'form', 'files' => true, 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2' ]) !!}
        <div class="form-group">
            {{ Form::label('make', 'Make*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="car_make_id" class="form-control">
                    <option disabled="disabled">Select Car Make</option>
                    @foreach( $makes as $make )
                        <option value="{{ $make->id }}" @if( $make->id == $model->car_make_id ) selected @endif> {{ $make->name }} </option>
                    @endforeach
                </select> 
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('name', 'Name*', [ 'class' => 'control-label col-md-3 col-sm-3 col-xs-12' ] ) }}
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('name', null, [ 'class' => 'form-control col-md-7 col-xs-12' ]) }}
            </div>
        </div>        
    
        <div class="ln_solid"></div>
        
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {{ Form::submit('Update', [ 'class' => 'btn btn-success' ] ) }}
            </div>
        </div>
    {!! Form::close() !!}
</div>
@stop