<ul class="nav side-menu">
  <li><a href=""><i class="fa fa-home"></i> Dashboard </a></li>
  <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/user">List</a></li>
      <li><a href="/role">Roles</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-automobile"></i> Cars <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/cars">List</a></li>
      <li><a href="/car-costs">Costs List</a></li>
      <li><a href="/car-properties">Properties</a></li>
      <li><a href="/car-makes">Makes</a></li>
      <li><a href="/car-models">Models</a></li>               
      <li><a href="/car-containers">Containers</a></li>               
    </ul>
  </li>
  <li><a><i class="fa fa-book"></i> Accounts <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/accounts-heads">Account Heads</a></li>
      <li><a href="/accounts-payables">Payables</a></li>
      <li><a href="/accounts-expenses">Expenses</a></li>
      <li><a href="/accounts-receivables">Receivables</a></li>
      <li><a href="/customer-receivables">Customer Receivables</a></li>
      <li><a href="/agents-earnings">Agent's Earnings</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-recycle"></i> Repair Responsibility <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/car-responsibility">Responsible User Cars</a></li>                   
    </ul>
  </li>
  <li><a><i class="fa fa-bar-chart"></i> Reports <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li class=""><a href="/get-dashboard">Dashboard</a></li>
      <li class=""><a href="/report/inventory">Inventory</a></li>
      <li class=""><a href="/report/sales">Sales</a></li>
      <li><a href="/report/expenses">Expenses</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-wrench"></i> Settings <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/banners">Banners</a></li>      
    </ul>
  </li>                  
</ul>