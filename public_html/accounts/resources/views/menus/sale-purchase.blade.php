<ul class="nav side-menu">
  <li><a href=""><i class="fa fa-home"></i> Dashboard </a></li>
  <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/user">List</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-automobile"></i> Cars <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/cars">List</a></li>              
    </ul>
  </li> 
  <li><a><i class="fa fa-book"></i> Accounts <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/customer-receivables">Customer Receivables</a></li>
    </ul>
  </li>    
  <li><a><i class="fa fa-bar-chart"></i> Reports <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li class=""><a href="/report/my-earnings/">My Earnings</a></li>
    </ul>
  </li>            
</ul>