$(document).ready(function(){
	$('.upload_delete').click(function(){
    if(confirm('You really want to delete this ?')){
  		var id = $(this).data('id');
  		$.ajax({
  			method: 'POST',
  			url : '/admin/uploads/' + id,
  			dataType : 'JSON',
  			data : {
  				'_method' : 'DELETE'
  			},
  			success: function( data ){
  				if( data.status == true ){
  					location.reload();
  				}
  			}
  		});      
    }
	});

	/////////////////////
	// expendses table //
	/////////////////////
	var handleDataTableButtons = function() {
      if ($("#datatable-buttons").length) {
        $("#datatable-buttons").DataTable({
          dom: "Bfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();

    //////////////////////
    // expenses table \ //
    //////////////////////

    $(document).on('click','.download_pictures_zip', function(e){
      e.preventDefault();
      var zip = new JSZip();
      var img = zip.folder("images");

      var images = $('.image_container img');
      var itemsProcessed = 0;
      images.each(function(key, value){
        toDataUrl(value.currentSrc, function(base64Img) {
            itemsProcessed++;
            var index = base64Img.indexOf(",");
            if (index !== -1) {
                base64Img = base64Img.substring(index + 1, base64Img.length);
            }
            
            img.file( key + '.jpeg' , base64Img, {base64: true}); 

            if(itemsProcessed === images.length) {
              saveZip( zip );
            }           
        });        
      });
    });

    function saveZip( zip ){
      zip.generateAsync({type:"blob"})
      .then(function(content) {
          saveAs(content, "images.zip");
      });
    }

    function toDataUrl(url, callback) {
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
          callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.responseType = 'blob';
      xhr.send();
    }
});

function show_data( data ){
	console.log( data );
}


