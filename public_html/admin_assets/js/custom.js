$(document).ready(function(){
	/*** CHECKOUT USER INFO START ***/
	$(document).on('change', 'select[name="country"]', function(){
		var country_id = $(this).val();
		console.log(country_id);
		$.ajax({
			url 	: '/location/get-states/',
			dataType: 'json',
			data 	: {
				'country_id' : country_id
			},
			success : function(data){
				var toPutInDom = [];
				$.each(data, function(k, v){
					toPutInDom.push("<option value='" + v.id + "'> " + v.name + " </option>");					
				});

				$('select[name="state"]').empty().append( toPutInDom );
			} 
		});
	});
	/*** CHECKOUT USER INFO END ***/
});