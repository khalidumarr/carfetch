<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', 'IndexController@index');

Auth::routes();

// Resource Frontend Routes
Route::get('/cars', 'CarsController@index');
Route::get('/cars/{slug}', 'CarsController@details');
Route::get('/about', 'AboutController@index');
Route::get('/contact', 'ContactController@index');
Route::get('/terms', 'TermsController@index');
Route::get('/dashboard', 'DashboardController@cars')->name('dashboard');
Route::get('/dashboard/invoice/{slug}', 'DashboardController@invoice')->name('invoice');
Route::get('/dashboard/payments', 'DashboardController@payments')->name('payments');

// Resources Backend Routes
Route::resource('/admin/user', 'Admin\User\UserController');
Route::resource('/admin/role', 'Admin\User\RoleController');
Route::resource('/admin/cars', 'Admin\Car\CarController');
Route::resource('/admin/sales', 'Admin\Sale\SalesController');
Route::resource('/admin/car-costs', 'Admin\Car\CarCostController');
Route::resource('/admin/car-properties', 'Admin\Car\CarPropertyController');
Route::resource('/admin/car-makes', 'Admin\Car\CarMakeController');
Route::resource('/admin/car-models', 'Admin\Car\CarModelController');
Route::resource('/admin/car-expense', 'Admin\Car\CarExpenseController');
Route::resource('/admin/car-containers', 'Admin\Car\CarContainerController');
Route::resource('/admin/accounts-heads', 'Admin\Account\HeadsController');
Route::resource('/admin/accounts-payables', 'Admin\Account\PayablesController');
Route::resource('/admin/accounts-receivables', 'Admin\Account\ReceivablesController');
Route::resource('/admin/customer-receivables', 'Admin\Account\CustomerReceivablesController');
Route::resource('/admin/accounts-expenses', 'Admin\Account\ExpensesController');
Route::resource('/admin/car-responsibility', 'Admin\Responsibility\ResponsibilityController');
Route::resource('/admin/uploads', 'Admin\UploadsController');
Route::resource('/admin/options', 'Admin\optionController');
Route::resource('/admin/banners', 'Admin\BannerController');
Route::resource('/admin/report/sales', 'Admin\Reports\SalesController');
Route::resource('/admin/report/expenses', 'Admin\Reports\ExpensesController');
Route::resource('/admin/report/sales', 'Admin\Reports\SaleController');


// General Routes
Route::get('/admin', 'Admin\AdminController@index');
Route::get('admin/car-responsibility/{id}/modify', 'Admin\Responsibility\ResponsibilityController@modify');
Route::get('admin/car-responsibility/{id}/complete', 'Admin\Responsibility\ResponsibilityController@complete');
Route::get('admin/car-expense/{id}/approve', 'Admin\Car\CarExpenseController@approve');
Route::get('admin/car-expense/{id}/delete', 'Admin\Car\CarExpenseController@delete');
Route::post('admin/accounts-receivables/receive', 'Admin\Account\ReceivablesController@receive');
Route::post('admin/accounts-receivables/release', 'Admin\Account\ReceivablesController@release');
Route::post('admin/accounts-payables/add-invoice', 'Admin\Account\PayablesController@addInvoice');
Route::post('admin/accounts-payables/release', 'Admin\Account\PayablesController@release');
Route::get('/admin/agents-earnings', 'Admin\Account\PayablesController@agentsEarnings');
Route::get('admin/sales/{id}/invoice/', 'Admin\Sale\SalesController@invoice');
Route::post('admin/sales/{id}/pay-remaining/', 'Admin\Sale\SalesController@payRemaining');
Route::post('admin/sales/{id}/approve-paid/', 'Admin\Sale\SalesController@approvePaid');
Route::get('admin/report/inventory/', 'Admin\Reports\InventoryController@wholeInventory');
Route::get('admin/report/my-earnings/', 'Admin\Reports\AgentsEarningsController@myEarnings');
Route::post('admin/sales/change-customer', 'Admin\Sale\SalesController@changeCustomer');
Route::post('admin/car-expense/{id}/add-payable-expense', 'Admin\Car\CarExpenseController@addPayableExpense');
Route::post('admin/car-expense/{id}/add-receivable-expense', 'Admin\Car\CarExpenseController@addReceivableExpense');
Route::get('admin/get-dashboard', 'Admin\AdminController@dashboard');
Route::post('/contact/enquire/', 'ContactController@enquire');
Route::post('/contact/contact/', 'ContactController@contact');
Route::put('/car/{id}/upload-pictures/', 'Admin\Car\CarController@uploadPictures');
Route::post('/admin/cars/update-sort/', 'Admin\Car\CarController@updateSort');


// Reports
Route::get('admin/reports/get-total-receivable', 'Admin\Reports\ReceivableController@index');
Route::get('admin/reports/get-total-payable', 'Admin\Reports\PayableController@index');
Route::get('admin/reports/get-total-inventory', 'Admin\Reports\InventoryController@index');
Route::get('admin/reports/get-net-position', 'Admin\Reports\GeneralController@index');
Route::get('admin/reports/get-curr-month-expenses', 'Admin\Reports\ExpensesController@index');
Route::get('admin/reports/get-monthly-expenses', 'Admin\Reports\ExpensesController@monthly');
Route::get('admin/reports/get-curr-month-sales', 'Admin\Reports\SalesController@index');
Route::get('admin/reports/get-arriving-containers', 'Admin\Reports\ContainersController@index');
Route::get('admin/reports/top-selling-brands', 'Admin\Reports\InventoryController@brands');
Route::get('/admin/reports/get-total-cash-in-hand', 'Admin\Reports\ReceivableController@cashInHand');
Route::get('/admin/reports/my-received-earnings', 'Admin\Reports\PayableController@totalPaid');
Route::get('/admin/reports/my-pending-earnings', 'Admin\Reports\PayableController@totalPending');
Route::get('/admin/reports/get-cx-receivables', 'Admin\Reports\ReceivableController@customerReceivables');
Route::get('/admin/reports/my-total-sales', 'Admin\Reports\SalesController@totalSales');
Route::get('/admin/reports/my-units-available', 'Admin\Reports\InventoryController@unitsAvailable');

// Special Route for static files
Route::get('/uploads/{filename}', function ($filename)
{
    $path = storage_path('app') . '/uploads/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/uploads/banners/{filename}', function ($filename)
{
    $path = storage_path('app') . '/banners/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});